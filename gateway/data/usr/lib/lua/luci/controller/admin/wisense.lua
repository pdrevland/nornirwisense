
module("luci.controller.admin.wisense", package.seeall)

package.path = (package.path or "") .. ";/root/wl_dmons/?.lua"

require "luci.json"
require "luci.util"
require "nixio"
require "nixio.util"
require "luci.httpclient"
require "socket"
require "wl"
local bit   = require "bit"


function index()

    entry({"wisense"}, template("admin/wisense"), "WiSense", 1)

    entry({"wisense", "cfg"}, template("admin/wisense_cfg"))

    entry({"wisense", "info"}, call("get_info"))

    entry({"wisense", "scan"}, call("scan_sensors"))

end

function get_info()
    local client_timestamp = tonumber(luci.http.formvalue('ts')) or 0

    --repeat
        fd = io.open(wl.SR_SENSORS_FILE, "r")
        if fd then
            data = fd:read("*a")
            data = luci.util.restore_data(data)
            fd:close()
        end
        data = data or {["ts"]=0}
    --until client_timestamp < data.ts

    -- Add time now in json data
    data.now = socket.gettime()*1000;

    luci.http.prepare_content("application/json")
    luci.http.header("Access-Control-Allow-Origin", "*")
    luci.http.write(luci.json.encode(data))
end

local function send_wizzi_cmd(obj, noresp)
    local so = nixio.socket("unix", "stream")
    local resp, err

    -- Connecting socket
    if so:connect(wl.SK_ALP_LUCI_PATH) then

        -- Sending the object
        so:write(luci.util.serialize_data(obj))

        -- Get response if we are waiting for it
        if not noresp then
            resp = so:readall(wl.SK_READ_LEN)
        end

        -- Done
        so:close()

        -- If we are not waiting for resp
        -- then return
        if noresp then
            return true
        end

        -- Unserialize object
        resp = resp and luci.util.restore_data(resp)

        if not resp then
            resp = {status = "ERR", msg = "Daemon did not respond"}
            err = true
        end

    elseif not noresp then
        resp = {status = "ERR", msg = "Could not connect with daemon's socket"}
        err = true
    end

    return resp, err
end

local function ping()
    local sensors = {}

    local obj = {__cmd       = "ALP+RF",
                 file_id     = 1,
                 file_len    = 8,
                 uid         = 'FFFFFFFFFFFFFFFF',
                 fwd         = 1,
                 offset      = 0,
               }

    local resp = send_wizzi_cmd(obj)

    -- get time now
    local now = socket.gettime()*1000

    if resp and resp.data then
        -- Get all uids
        for i,r in ipairs(resp.data) do
            if r.file_data then
                sensors[wl.data_to_hexstr(r.file_data)] = {ts=now}
            end
        end
    end

    return sensors
end

local function set_list_or_raise_error(list)
    -- Set command in obj
    list.__cmd = "SET_LIST"

    local so = nixio.socket("unix", "stream")
    local resp, err

    -- Connecting socket
    if so:connect(wl.SK_SENSOR_DATA_PATH) then

        -- Sending the object
        so:write(luci.util.serialize_data(list))

        -- Get response if we are waiting for it
        resp = so:readall(wl.SK_READ_LEN)

        -- Done
        so:close()

        -- Unserialize object
        resp = resp and luci.util.restore_data(resp)

        if not resp or resp.status ~= "OK" then
            error("Could not set list in sensor_data process")
        end

    else
        error("Could not set list in sensor_data process")
    end

    -- Unset command in obj
    list.__cmd = nil

end

function scan_sensors()
    -- Ping motes
    local data_sensors = ping()

    -- Build the complete obj to send
    local list = {ts=socket.gettime()*1000, data=data_sensors}

    -- set list in sensor_data process
    set_list_or_raise_error(list)

    -- return obj list
    luci.http.prepare_content("application/json")
    luci.http.header("Access-Control-Allow-Origin", "*")
    luci.http.write(luci.json.encode(list))
end
