--[[
LuCI - Lua Configuration Interface

Copyright 2008 Steven Barth <steven@midlink.org>
Copyright 2008 Jo-Philipp Wich <xm@leipzig.freifunk.net>
Copyright 2011 Bart Van Der Meerssche <bart.vandermeerssche@flukso.net>

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

	http://www.apache.org/licenses/LICENSE-2.0

$Id: dash7.lua 3672 2008-10-31 09:35:11Z Cyrus $
]]--

module("luci.controller.admin.dash7", package.seeall)

package.path = (package.path or "") .. ";/root/wl_dmons/?.lua"

require"luci.json"
require"luci.util"
require "nixio"
require "nixio.util"
require "luci.httpclient"
local bit     = require "bit"
local wl      = require "wl"

function index()

    package.path = (package.path or "") .. ";/root/wl_dmons/?.lua"
    wl = require "wl"

    entry({"dash7"}, template("admin/dash7"), "Dash7", 2)

    entry({"send_cmd"}, call("cmd_exec"))
    entry({"dash7", "send_cmd"}, call("cmd_exec"))

    entry({"dash7_register"}, call("register_gw"))
    entry({"dash7", "dash7_register"}, call("register_gw"))

    entry({"dash7_server"}, call("set_server"))
    entry({"dash7", "dash7_server"}, call("set_server"))

    entry({"wl_dmon_stop"}, call("wl_dmon_stop"))
    entry({"dash7", "wl_dmon_stop"}, call("wl_dmon_stop"))

    entry({"wl_dmon_start"}, call("wl_dmon_start"))
    entry({"dash7", "wl_dmon_start"}, call("wl_dmon_start"))

    entry({"hrst_mote"}, call("hrst_mote"))
    entry({"dash7", "hrst_mote"}, call("hrst_mote"))

    entry({"reboot"}, call("reboot"))
    entry({"dash7", "reboot"}, call("reboot"))

    entry({"read_calib"}, call("read_calib"))
    entry({"dash7", "read_calib"}, call("read_calib"))

    entry({"err_log"}, call("err_log"))
    entry({"dash7", "err_log"}, call("err_log"))

    entry({"read_rst_file"}, call("read_rst_file"))
    entry({"dash7", "read_rst_file"}, call("read_rst_file"))

    entry({"rst_err_log"}, call("rst_err_log"))
    entry({"dash7", "rst_err_log"}, call("rst_err_log"))

    entry({"rst_calib_file"}, call("rst_calib_file"))
    entry({"dash7", "rst_calib_file"}, call("rst_calib_file"))

    entry({"rst_log"}, call("rst_log"))
    entry({"dash7", "rst_log"}, call("rst_log"))

    entry({"register_vpn"}, call("register_vpn"))
    entry({"dash7", "register_vpn"}, call("register_vpn"))

    entry({"current_vpn"}, call("current_vpn"))
    entry({"dash7", "current_vpn"}, call("current_vpn"))

    entry({"api"}, call("generic_exec"))
    entry({"dash7", "api"}, call("generic_exec"))

    -- Define all parents nodes
    -- This is a known bug: https://dev.openwrt.org/ticket/13293

    entry({"dash7", "api", "display"})
    entry({"dash7", "api", "cup"})


    -- Read and Write commands
    entry({"dash7", "api", "wf"}, call("api_wf_cb"))
    entry({"dash7", "api", "wff"}, call("api_wff_cb"))
    entry({"dash7", "api", "rf"}, call("api_rf_cb"))


    -- Code Upgrade commands
    entry({"dash7", "api", "cup", "upload"}, call("api_cup_upload"))

    entry({"dash7", "api", "cup", "transfer"}, call("api_cup_xfer"))

    entry({"dash7", "api", "cup", "transfer", "status"}, call("api_cup_xfer_status"))

    entry({"dash7", "api", "cup", "exec"}, call("api_cup_exec"))

    entry({"dash7", "api", "cup", "remove"}, call("api_cup_remove"))

    entry({"dash7", "api", "cup", "list"}, call("api_cup_list"))

    entry({"dash7", "api", "cup", "fs_type"}, call("api_cup_fs_type"))

    local gw_conf = wl.get_gw_config()

    if gw_conf and gw_conf.uid then
        entry({"dash7", "api", "display", gw_conf.uid}, call("api_display_cb"))
    end
end

function set_display(disp_id, disp_msg)

    local obj = {__cmd = "DISP_UPDATE", disp_id = disp_id, disp_msg = disp_msg}

    local resp, err = send_wizzi_cmd(obj)

    if err then
        return false
    else
        return true
    end

end

local function save_reset(ref)
    local fd, msg = io.open(wl.RST_FILE, "a")
    fd:write(ref .. " RST: " .. os.date("%a %b %d %X", os.time()) .. "\n")
    fd:close()
end

function rst_log()
    luci.http.prepare_content("application/json")
    luci.http.header("Access-Control-Allow-Origin", "*")
    local resp = wl.exec_bash("/root/wl_scripts/reset_log.sh")
    luci.http.write(luci.json.encode({status = "OK", data = resp}))
end

function wl_dmon_stop()
    luci.http.prepare_content("application/json")
    luci.http.header("Access-Control-Allow-Origin", "*")
    local resp = wl.exec_bash("/etc/init.d/gateworks stop")
    luci.http.write(luci.json.encode({status = "OK", data = resp}))
end

function wl_dmon_start()
    luci.http.prepare_content("application/json")
    luci.http.header("Access-Control-Allow-Origin", "*")
    save_reset('DMONS')
    local resp = wl.exec_bash("/etc/init.d/gateworks start")
    luci.http.write(luci.json.encode({status = "OK", data = resp}))
end

function hrst_mote()
    luci.http.prepare_content("application/json")
    luci.http.header("Access-Control-Allow-Origin", "*")
    save_reset('MOTE')
    local resp = wl.exec_bash("/root/wl_scripts/hrst_mote.sh")
    luci.http.write(luci.json.encode({status = "OK", data = resp}))
end

function reboot()
    luci.http.prepare_content("text/plain")
    luci.http.header("Access-Control-Allow-Origin", "*")
    luci.http.write("Rebooting ...")
    save_reset('REBOOT')
    wl.hrst_gateworks()
end

function send_wizzi_cmd(obj, noresp)
    local so = nixio.socket("unix", "stream")
    local resp, err

    -- Connecting socket
    if so:connect(wl.SK_ALP_LUCI_PATH) then

        -- Sending the object
        so:write(luci.util.serialize_data(obj))

        -- Get response if we are waiting for it
        if not noresp then
            resp = so:readall(wl.SK_READ_LEN)
        end

        -- Done
        so:close()

        -- If we are not waiting for resp
        -- then return
        if noresp then
            return true
        end

        -- Unserialize object
        resp = resp and luci.util.restore_data(resp)

        if not resp then
            resp = {status = "ERR", msg = "Daemon did not respond"}
            err = true
        end

    elseif not noresp then
        resp = {status = "ERR", msg = "Could not connect with daemon's socket"}
        err = true
    end

    return resp, err
end

function generic_exec()
    luci.http.prepare_content("application/json")
    luci.http.header("Access-Control-Allow-Origin", "*")

    local resp = send_wizzi_cmd(luci.http.formvalue())

    luci.http.write(luci.json.encode(resp))
end

function read_rst_file()
    luci.http.prepare_content("text/plain")
    luci.http.header("Access-Control-Allow-Origin", "*")

    local fd, msg = io.open(wl.RST_FILE, "r")

    if not fd then
        luci.http.write("There it no recording of resets")
        return
    end

    local data = fd:read("*all")
    luci.http.write(data)

    fd:close()
end

function err_log()
    luci.http.prepare_content("text/plain")
    luci.http.header("Access-Control-Allow-Origin", "*")

    local fd, msg = io.open(wl.ERR_LOG_FILE, "r")

    if not fd then
        luci.http.write("There is no such file")
        return
    end

    luci.http.write(fd:read("*all"))

    fd:close()
end

function rst_err_log()
    luci.http.prepare_content("application/json")
    luci.http.header("Access-Control-Allow-Origin", "*")
    local resp = wl.exec_bash("rm " .. wl.ERR_LOG_FILE)
    luci.http.write(luci.json.encode({status = "OK", data = resp}))
end


function read_calib()
    luci.http.prepare_content("text/plain")
    luci.http.header("Access-Control-Allow-Origin", "*")

    local fd, msg = io.open(wl.CALIB_FILE, "r")

    if not fd then
        luci.http.write("There it no recording of calibrated motes")
        return
    end

    local data
    local tb = {}
    local ar = {}
    repeat
        data = fd:read("*line")
        if not data then break end

        data = luci.util.restore_data(data)
        if data.uid or data.ts then
            data.ts = os.date("%a %b %d %X", data.ts)
            if not tb[data.uid] then
                table.insert(ar, data.uid)
                tb[data.uid] = {}
            end
            table.insert(tb[data.uid], data)
        end
    until not data

    table.sort(ar)

    for i,uid in ipairs(ar) do
        luci.http.write(uid .. "\tCalib dates")
        for j,data in ipairs(tb[uid]) do
            luci.http.write("\t|" .. data.ts .. ", " .. "CA VAL: " .. data.cal_val .. ", " .. "CA VAR: " .. data.cal_var .."|")
        end
        luci.http.write("\n")
    end

    fd:close()
end

function rst_calib_file()
    luci.http.prepare_content("application/json")
    luci.http.header("Access-Control-Allow-Origin", "*")
    local resp = wl.exec_bash("rm " .. wl.CALIB_FILE)
    luci.http.write(luci.json.encode({status = "OK", data = resp}))
end


local function mysplit(inputstr, sep)
        if sep == nil then
                sep = "%s"
        end
        t={} ; i=1
        for str in string.gmatch(inputstr, "([^"..sep.."]+)") do
                t[i] = string.char(tonumber(str))
                i = i + 1
        end
        return table.concat(t)
end

function api_rf_cb()
    luci.http.prepare_content("application/json")
    luci.http.header("Access-Control-Allow-Origin", "*")

    local obj = {__cmd       = "ALP+RF",
                 file_id     = tonumber(luci.http.formvalue('file_id')),
                 file_len    = tonumber(luci.http.formvalue('file_len')),
                 file_block  = tonumber(luci.http.formvalue('file_block')),
                 uid         = luci.http.formvalue('uid'),
                 fwd         = tonumber(luci.http.formvalue('fwd')),
                 offset      = tonumber(luci.http.formvalue('offset')),
                 timeout     = tonumber(luci.http.formvalue('timeout')),
                 resp        = luci.http.formvalue('resp'),
               }

    local resp = send_wizzi_cmd(obj)

    -- Transform binary data into csv data
    if resp and resp.data then
        for i,r in ipairs(resp.data) do
            if r.file_data then
                r.file_data = wl.pp_data(r.file_data)
            end
        end
    end

    luci.http.write(luci.json.encode(resp))
end

local function fversion(bin_version)
    local str = tostring(string.byte(bin_version, 1))

    for i=2,#bin_version do
        str = str .. '-' .. tostring(string.byte(bin_version, i))
    end

    return str
end

function api_cup_upload()

    local fp_code, fp_fs
    -- Remove remaining tmp files
    os.remove(wl.CUP_CODE_TMP_FILE)
    os.remove(wl.CUP_FS_TMP_FILE)

    luci.http.setfilehandler(
        function(meta, chunk, eof)
            if meta.name == wl.CUP_CODE_UPLOAD_FORM_NAME then
                if not fp_code then
                    fp_code = io.open(wl.CUP_CODE_TMP_FILE, "w+")
                end
                if chunk then
                    fp_code:write(chunk)
                end
                if eof then
                    fp_code:close()
                end
            elseif meta.name == wl.CUP_FS_UPLOAD_FORM_NAME then
                if not fp_fs then
                    fp_fs = io.open(wl.CUP_FS_TMP_FILE, "w+")
                end
                if chunk then
                    fp_fs:write(chunk)
                end
                if eof then
                    fp_fs:close()
                end
            end
        end
    )


    -- If if at least one file has being uploaded
    local code_file_name = luci.http.formvalue(wl.CUP_CODE_UPLOAD_FORM_NAME)
    local fs_file_name = luci.http.formvalue(wl.CUP_FS_UPLOAD_FORM_NAME)

    if not( code_file_name and #code_file_name> 0) and not ( fs_file_name and #fs_file_name> 0) then
        error("missing form " .. tostring(wl.CUP_CODE_UPLOAD_FORM_NAME .. " or " .. wl.CUP_FS_UPLOAD_FORM_NAME))
    end

    local ref_name = luci.http.formvalue("ref_name")

    if not ref_name then
        error("missing ref_name")
    end

    -- Building file name as /etc/dash7/cup_images/csm_1-25-3-14.bin
    local cup_code_file_name    = wl.CUP_UPLOAD_FOLDER .. ref_name .. "_code.bin"
    local cup_fs_file_name      = wl.CUP_UPLOAD_FOLDER .. ref_name .. "_fs.bin"

    -- Make cup upload folder if it does not exist
    nixio.fs.mkdir(wl.CUP_UPLOAD_FOLDER)

    -- Renaming the downloaded file to this one
    -- XXX: nixio function and os.rename fail for unknown reason
    --if not nixio.fs.move(wl.CUP_TMP_FILE, cup_code_file_name) then
    --    error("could not save file " .. tostring(cup_code_file_name))
    --end

    -- calling os.execute for now
    local cmd = "mv " ..  wl.CUP_CODE_TMP_FILE .. " " .. cup_code_file_name
    os.execute(cmd)
    local cmd = "mv " ..  wl.CUP_FS_TMP_FILE .. " " .. cup_fs_file_name
    os.execute(cmd)
end

function api_cup_remove()
    luci.http.header("Access-Control-Allow-Origin", "*")
    local status, msg = os.remove(wl.CUP_UPLOAD_FOLDER .. luci.http.formvalue('ref_name') .. "_code.bin")
    local status, msg = os.remove(wl.CUP_UPLOAD_FOLDER .. luci.http.formvalue('ref_name') .. "_fs.bin")

    if not status then
        error(msg)
    end
end

function api_cup_list()
    local i, t = 0, {}
    for filename in io.popen('ls -a "'..wl.CUP_UPLOAD_FOLDER..'"'):lines() do
        i = i + 1

        local _,_, ref_name, suffix = string.find(filename, '(.*)_(.*)')

        if suffix and (suffix == "code.bin" or suffix == "fs.bin") then
            table.insert(t, filename)
        end

    end

    luci.http.prepare_content("application/json")
    luci.http.header("Access-Control-Allow-Origin", "*")
    luci.http.write(luci.json.encode(t))
end

function api_cup_fs_type()
    local ref_name = luci.http.formvalue('ref_name')

    if not ref_name then
        error("missing ref name")
    end

    local fd, msg, err_n = io.open(wl.CUP_UPLOAD_FOLDER .. ref_name .. "_fs.bin")

    if not fd then
        -- If the file does not exist
        -- Return type N
        if err_n == 2 then
            luci.http.prepare_content("application/json")
            luci.http.header("Access-Control-Allow-Origin", "*")
            luci.http.write(luci.json.encode({["type"] = 'N'}))
            return

        else
            error(msg)
        end
    end

    -- Read the type
    fd:seek("end", -5)
    local t = fd:read(1)
    fd:close()

    luci.http.prepare_content("application/json")
    luci.http.header("Access-Control-Allow-Origin", "*")
    luci.http.write(luci.json.encode({["type"] = t}))
end

local function update_xfer_file(status, info)
    local json_obj =    {
                            status = status,
                            info = info,
                        }

    local json_str = luci.json.encode(json_obj)


    local fd = io.open(wl.CUP_XFER_STATUS_FILE, "w+")
    fd:write(json_str)
    fd:close()
end

local function read_xfer_file()
    local fd = io.open(wl.CUP_XFER_STATUS_FILE, "r")
    local json_str
    if fd then
        json_str = fd:read("*all")
        fd:close()
    end

    json_str = json_str or luci.json.encode({status = wl.CUP_XFER_STATUS_NO_XFER})

    return json_str and luci.json.decode(json_str) or {status = wl.CUP_XFER_STATUS_NO_XFER}, json_str
end


local function api_cup(action, params)
    params.action = action

    if params.fwd then
        params.fwd          = tonumber(params.fwd) or error("Wrong fwd format")
    end

    if params.bin_offset then
        params.bin_offset   = tonumber(params.bin_offset) or error("Wrong bin_offset format")
    end

    if params.chunk_size then
        params.chunk_size   = tonumber(params.chunk_size) or error("Wrong chunk_size format")
    end

    if params.fwd and params.fwd > 0 and not params.uid then
        error("missing uid")
    end

    if not params.ref_name then
        error("missing ref name")
    end

    params.__cmd = "CUP"
    params.ref_name = luci.http.formvalue('ref_name')

    if not params.ref_name or #params.ref_name  == 0 then
        error("Missing ref_name")
    end

    luci.http.prepare_content("application/json")

    if (action == "transfer") then
        -- Update xfer status to ongoing to lock other requests
        update_xfer_file(wl.CUP_XFER_STATUS_ON_GOING)
    end

    -- Send cmd and do not wait for response
    local resp = send_wizzi_cmd(params, action == "transfer")
    luci.http.write(luci.json.encode(resp))

    return resp
end

function api_cup_xfer_status()
    local xfer_status, json_str = read_xfer_file()

    luci.http.prepare_content("application/json")
    luci.http.header("Access-Control-Allow-Origin", "*")
    luci.http.write(json_str)

    if xfer_status.status == wl.CUP_XFER_STATUS_DONE_ERR or xfer_status.status == wl.CUP_XFER_STATUS_DONE_OK then
        update_xfer_file(wl.CUP_XFER_STATUS_NO_XFER)
    end
end

--  .ref_name   (mandatory)
--  .bin_offset (optional, default 0)
--  .chunk_size (optional, default wl.CUP_CHUNK_SIZE)
--  .uid        (optional, mandatory if fwd is greater then 0)
--  .fwd        (optional, default 0)
--  .action     (optional, set to "transfer" to just transfer
function api_cup_xfer()
    luci.http.header("Access-Control-Allow-Origin", "*")

    local xfer_status = read_xfer_file()
    if xfer_status.status == wl.CUP_XFER_STATUS_ON_GOING then
        error("A transfer is already on going")
    end

    local resp, err = api_cup("transfer", luci.http.formvalue())
    return resp
end

--  .ref_name   (mandatory)
--  .bin_offset (optional, default 0)
--  .chunk_size (optional, default wl.CUP_CHUNK_SIZE)
--  .uid        (optional, mandatory if fwd is greater then 0)
--  .fwd        (optional, default 0)
--  .action     (optional, set to "transfer" to just transfer
function api_cup_exec()
    luci.http.header("Access-Control-Allow-Origin", "*")
    local resp = api_cup("upgrade", luci.http.formvalue())
    return resp
end

local function api_wf_p(cmd)
    luci.http.prepare_content("application/json")
    luci.http.header("Access-Control-Allow-Origin", "*")


    local res, data = pcall( mysplit, luci.http.formvalue('file_data'), ",")

    local obj = {__cmd       = cmd,
                 file_id     = tonumber(luci.http.formvalue('file_id')),
                 file_block  = tonumber(luci.http.formvalue('file_block')),
                 uid         = luci.http.formvalue('uid'),
                 file_path   = luci.http.formvalue('file_path'),
                 file_data   = data,
                 fwd         = tonumber(luci.http.formvalue('fwd')),
                 offset      = tonumber(luci.http.formvalue('offset')),
                 resp        = luci.http.formvalue('resp'),
                 timeout     = tonumber(luci.http.formvalue('timeout')),
               }

    local resp = send_wizzi_cmd(obj)

    luci.http.write(luci.json.encode(resp))
end

function api_wf_cb()
    api_wf_p("ALP+WF")
end

function api_wff_cb()
    api_wf_p("ALP+WFF")
end

function api_display_cb()

    luci.http.prepare_content("application/json")
    luci.http.header("Access-Control-Allow-Origin", "*")

    local data = wl.get_gw_config()

    if data.display_update_off then
        luci.http.write(luci.json.encode({status = "ERR", msg = "Local forced dislay is on"}))
        return
    end

    if not luci.http.formvalue('msg') then
        luci.http.write(luci.json.encode({status = "ERR", msg = "Param msg is missing"}))
        return
    end

	local pathinfo = luci.http.getenv("PATH_INFO")

    -- Get the last node
    local disp_id = pathinfo:match("/([^/]*)/?$")

    if set_display(disp_id, luci.http.formvalue('msg')) then
        luci.http.write(luci.json.encode({status = "OK", msg = obj}))
    else
        luci.http.write(luci.json.encode({status = "ERR", msg = "Could not connect with daemon"}))
    end

end



function cmd_exec()

    local so = nixio.socket("unix", "stream")

    luci.http.prepare_content("text/plain")
    luci.http.header("Access-Control-Allow-Origin", "*")

    -- executing command remove
    if luci.http.formvalue('remove') and luci.http.formvalue('address') then

        -- Connecting socket
        so:connect(wl.SK_REPORT_PATH)

        so:write(luci.util.serialize_data({__uid = luci.http.formvalue('address'), __cmd = "remove"}));

        -- Wait OK
        local ok = so:read(100) or 'ERR'
        luci.http.write(ok .. ":Entry removed")
    end

    so:close()
end



function set_server()
    luci.http.prepare_content("application/json")
    luci.http.header("Access-Control-Allow-Origin", "*")

    -- Read configuration file to get the uid
    local data = wl.get_server_config()
    if not data then
        luci.http.write(luci.json.encode({status = "error", errors = "CFG FILE"}))
        return
    end

    if luci.http.formvalue('server') and luci.http.formvalue('server') ~= "" then

        data.protocol, data.server = luci.http.formvalue('server'):match("(%w+://)([^/]*)")

        if not data.protocol or not data.server then
            luci.http.write(luci.json.encode({status = "error", errors = "Wrong format: protocol://server"}))
            return
        end

        -- Write the new server in file
        res = wl.set_server_config(data)
        if not res then
            luci.http.write(luci.json.encode({status = "error", errors = "FD WRITE"}))
            return
        end
    else
        luci.http.write(luci.json.encode({status = "error", errors = "Empty Server Address"}))
        return
    end

    luci.http.write(luci.json.encode({status = "ok"}))
end

local function get_vpn_server()

    local fd = io.open(wl.OPENVPN_CONF_FILE, "r")
    if not fd then
        error("Could not open openvpn file")
    end

    for line in fd:lines() do
        local _,_,vpn_server = line:find("^# @VPNSERVER (.*)$")
        if vpn_server then
            return vpn_server
        end
    end
end

function current_vpn()
    luci.http.prepare_content("application/json")
    luci.http.header("Access-Control-Allow-Origin", "*")

    local vpn_server = get_vpn_server()

    luci.http.write(luci.json.encode({vpn_server = vpn_server}))
end

function register_vpn()
    luci.http.prepare_content("application/json")
    luci.http.header("Access-Control-Allow-Origin", "*")


    -- Get server and gw infos from file
    local gw_cfg = wl.get_gw_config()
    local serv_cfg = wl.get_server_config()
    local vpn_server = luci.http.formvalue('vpn_server')

    if not vpn_server or #vpn_server == 0 then
        luci.http.write(luci.json.encode({status = "error", msg = "Param vpn_server is missing"}))
        return
    end

    -- If we have all infos
    if serv_cfg and gw_cfg and vpn_server and
       serv_cfg.protocol and gw_cfg.uid and serv_cfg.password and serv_cfg.server then

        -- Assemble the uri to post the data
        local uri = string.format(wl.HTTP_URI_REG_VPN_FM, serv_cfg.protocol, gw_cfg.uid,
                                    serv_cfg.password, serv_cfg.server)

        local http_opt = {headers = {Accept="application/json", ["Content-type"] = "application/json"}}

        -- Compose the body of the command
        http_opt.body = string.format(wl.HTTP_BODY_REG_VPN_FM, vpn_server)

        -- Execute the command
        local resp_serv, serv_msg = luci.httpclient.request_to_buffer(uri, http_opt)

        -- XXX In some gateways there is more then the json object that is returned
        local _,_, resp =  string.find(resp_serv or "", "(\{.*\})")

        if resp then
            luci.http.write(resp)
        else
            luci.http.write(luci.json.encode({status = "error", errors = tostring(serv_msg)}))
        end
    else 
        luci.http.write(luci.json.encode({status = "error", errors = "INCOMPLETE CONF FILES"}))
    end
end

function register_gw()
    luci.http.prepare_content("application/json")
    luci.http.header("Access-Control-Allow-Origin", "*")

    -- Read configuration file to get the uid
    local data_server = wl.get_server_config()
    local data_gw = wl.get_gw_config()
    if not data_server or not data_gw then
        luci.http.write(luci.json.encode({status = "error", errors = "CFG FILE"}))
        return
    end

    -- Generate a password
    math.randomseed(os.time())
    data_server.password = string.format("%x%x", math.random(1, 0xfffffff), math.random(1, 0xfffffff))


    local uri = string.format(wl.HTTP_URI_REG_GW_FM, data_server.protocol, luci.http.formvalue('login'),
                                luci.http.formvalue('password'), data_server.server)

    local http_opt = {headers = {Accept="application/json", ["Content-type"] = "application/json"}}

    -- Compose the body of the command
    http_opt.body = string.format(wl.HTTP_BODY_REG_GW_FM, data_gw.uid, data_server.password)

    -- Execute the command
    local resp_serv = luci.httpclient.request_to_buffer(uri, http_opt)

    -- XXX In some gateways there is more then the json object that is returned
    local _,_, resp =  string.find(resp_serv or "", "(\{.*\})")

    resp_p = luci.json.decode(resp or "")

    if resp_p then

        if resp_p and resp_p.status == "ok" then
            -- Write the new password in file
            res = wl.set_server_config(data_server)
            if not res then
                luci.http.write(luci.json.encode({status = "error", errors = "FD WRITE"}))
            else
                -- OK
                luci.http.write(resp)
            end
        else
            luci.http.write(luci.json.encode({status = "error", errors = luci.util.serialize_data(resp)}))
        end

    else
        luci.http.write("JSON DECODE ERROR " .. resp or "nil")
    end

end

