#!/bin/ash
# Export the gpios if they are not exported yet
# XXX: This does not need to be done and redone here
#      Check a better place for these lines
echo "17" > /sys/class/gpio/export
echo "18" > /sys/class/gpio/export

echo out > /sys/class/gpio/gpio17/direction
echo out > /sys/class/gpio/gpio18/direction

echo 0 > /sys/class/gpio/gpio17/value
echo 0 > /sys/class/gpio/gpio18/value
sleep 1
echo 1 > /sys/class/gpio/gpio17/value
echo 1 > /sys/class/gpio/gpio18/value

echo in > /sys/class/gpio/gpio17/direction
echo in > /sys/class/gpio/gpio18/direction
