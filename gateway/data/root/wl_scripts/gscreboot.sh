#!/bin/ash
SECS=5 ;# range 1-5 - period the board will stay off
i2cset -f -y 0 0x20 6 $((SECS % 256)); SECS=$((SECS >> 8))
i2cset -f -y 0 0x20 7 $((SECS % 256)); SECS=$((SECS >> 8))
i2cset -f -y 0 0x20 8 $((SECS % 256)); SECS=$((SECS >> 8))
i2cset -f -y 0 0x20 9 $((SECS % 256))
i2cset -f -y 0 0x20 1 0x4       ;# set add time bit
i2cset -f -y 0 0x20 1 0x3       ;# enable (put board to sleep)
