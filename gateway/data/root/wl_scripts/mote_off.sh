#!/bin/ash

#Force the mote to be off
echo out > /sys/class/gpio/gpio17/direction
echo out > /sys/class/gpio/gpio18/direction

echo 0 > /sys/class/gpio/gpio17/value
echo 0 > /sys/class/gpio/gpio18/value
