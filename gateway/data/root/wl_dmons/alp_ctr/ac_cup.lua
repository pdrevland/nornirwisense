----------------------------------------
-- Dependencies
----------------------------------------
local p_color   = require "p_color"
local mt        = require "ac_mt"
local bit       = require "bit"
local crc32     = require "ac_crc32"
local json      = require "luci.json"
local util      = require "luci.util"
----------------------------------------

----------------------------------------------------
-- Print functions
-- Set a empty function to disable a print
----------------------------------------------------
local p_pref = "CUP:"
local p_err = function (...) p_color.p_err(p_pref, unpack(arg)) end
local p_dbg = function (...) p_color.p_dbg(p_pref, unpack(arg)) end
local p_inf = function (...) p_color.p_inf(p_pref, unpack(arg)) end
local p_log = function (...) p_color.p_log(p_pref, unpack(arg)) end

---------------------------
-- Global object
---------------------------
ac_cup =    {
                SIG_START           = 0,
                SIG_END             = 1,
                SIG_INTEGRITY_OK    = 2,
                SIG_INTEGRITY_ERR   = 3,
            }

----------------------------------------------------
-- PRIVATE FUNCTIONS
----------------------------------------------------

-- =================================================
-- get_fs_offset
-- =================================================
-- @brief   Read the file system image type and get
--          the offset to write the file system
-- @param   fs_bin: The file system binary block
-- @retval  1) nil, msg: if error
--          2) The offset to write the file system
----------------------------------------------------
local function get_fs_offset(fs_bin)
    -- Type is a byte before the crc address
    local t = string.sub(fs_bin, -5, -5)

    -- Type device: the whole file system
    if t == wl.CUP_FS_TYPE_DEVICE then
        p_inf("FS - RESET DEVICE")
        return wl.CUP_FS_DEVICE_OFFSET

    -- Type calib, do not erase the uid file
    elseif t == wl.CUP_FS_TYPE_CALIB then
        p_inf("FS - RESET CALIB")
        return wl.CUP_FS_CALIB_OFFSET

    -- Type config, do not erase the calib or the uid file
    elseif t == wl.CUP_FS_TYPE_CONFIG then
        p_inf("FS - RESET CONFIG")
        -- Get the offset from file
        return wl.bytes_to_int(string.sub(fs_bin, -9, -6))
    else
        return nil, "Unknow file system type"
    end
end

-- =================================================
-- calculate_crc
-- =================================================
-- @brief   Calculate the crc32 of a block if bytes
-- @param   block: The block to calculate the crc32
-- @retval  the crc32
----------------------------------------------------
local function calculate_crc(block)
    p_dbg("CRC BLOCK SIZE", tostring(#block))
    local crc = crc32.init()
    crc = crc32.addblock(crc, block)
    crc = crc32.finalize(crc)
    return crc
end

-- =================================================
-- read_all_file
-- =================================================
-- @brief   Read an entire file and load it into the
--          memory
-- @param   filename: The file to read from
-- @retval  1) nil, msg: if error
--          2) The content of the file as a binary block
----------------------------------------------------
local function read_all_file(filename)
    p_inf("Reading", filename)
    -- Open the file
    local fd, msg = io.open(filename, "r")

    -- In case of error return nil and the error message
    if not fd then
        p_err("Error while opening", filename)
        return nil, msg
    end

    local block = fd:read("*all")
    p_inf("Read", #block, "from", filename)
    return block
end

-- =================================================
-- crc_check
-- =================================================
-- @brief   Check if the crc of the file is the
--          same as the one appended at the end of it
-- @param   bin: The file data to get the expected crc
--              to calculate the crc for comparison
-- @param   filename: The file name to print in case
--              of errors
-- @retval  1) nil, msg: if error
--          2) true: if the crc matches
----------------------------------------------------
local function crc_check(bin, filename)
    -- The file must have at least the crc32
    if #bin < 4 then
        return nil, "Invalid size of bin file"
    end

    local expected_crc = wl.bytes_to_int(string.sub(bin, -4, -1))

    local crc = calculate_crc(string.sub(bin, 1, -5))

    if crc == expected_crc then
        p_dbg("CRC OK", tostring(wl.data_to_hexstr(wl.int_to_bytes(crc, false, 4))))
        return true
    else
        return nil, "CRC in file " .. (filename or "") .. " does not match. Calculated "
            .. tostring(wl.data_to_hexstr(wl.int_to_bytes(crc, false, 4))) ..
            " expecting " .. tostring(wl.data_to_hexstr(wl.int_to_bytes(expected_crc, false, 4)))
            .. " File size " .. tostring(#bin)
    end
end

-- =================================================
-- assemble_img
-- =================================================
-- @brief   Construct the image composed by the code
--          and the file system to be written in
--          the cup file
-- @param   code_bin:   The code binary block
-- @param   fs_bin:     The file system binary block
-- @retval  1) nil, msg: if error
--          2) img, crc: the composed image and its crc
--              to be written in the cup_cfg file
----------------------------------------------------
local function assemble_img(code_bin, fs_bin)
    -- Check if both files are empty
    if #code_bin == 0 and #fs_bin == 0 then
        return nil, "Code and FS files are empty"
    end
    -- Calculate crc image of the concatenation
    local crc = crc32.init()
    crc = crc32.addblock(crc, code_bin)
    crc = crc32.addblock(crc, fs_bin)
    crc = crc32.finalize(crc)

    -- Calculate the padding
    local padding = {}
    local remain = (#code_bin + #fs_bin + 4) % wl.CUP_WORD_SIZE
    if remain ~= 0 then
        for i=1,wl.CUP_WORD_SIZE-remain do
            padding[i] = 0
        end
    end

    -- Assembling image with code_bin, fs_bin, crc and padding
    local img = code_bin .. fs_bin .. wl.int_to_bytes(crc, false, 4) .. string.char(unpack(padding))

    p_inf("Image len", #img, "Code len", #code_bin, "FS len", #fs_bin)

    -- We are done
    return img, crc
end

-- =================================================
-- wait_sig
-- =================================================
-- @brief   Wait for a cup signal between sig1 and sig2
--          to arrive (do not consume CPU time)
-- @sig1    The low value in the signal range
-- @sig2    The high value int the signal range
-- @timeout The timeout to wait for the signal
-- @retval  1) nil: If timeout
--          2) The received signal
----------------------------------------------------
local function wait_sig(sig1, sig2, timeout)
    local sig_rcved, sig

    local time_limit = os.time() + timeout
    repeat
        local to = time_limit - os.time()
        if to <= 0 then
            break
        end

        local msg = mt.read_raw(to)

        if msg and #msg == 1 then
            sig = string.byte(msg)
            if sig >= sig1 and sig <= sig2 then
                sig_rcved = true
            end
        end
    until sig_rcved or os.time() > time_limit

    return sig_rcved and sig
end

-- =================================================
-- hrst_watch
-- =================================================
-- @brief   Wait for the write signals to reset the mote.
--          Reset the mote when SIG_INTEGRITY_OK arrives
--          Reset the mote when SIG_END arrives
-- @retval  1) nil, msg: if error
--          2) true: if success
----------------------------------------------------
local function hrst_watch()

    -- Wait for integrity check message
    p_dbg("Waiting SIG_INTEGRITY_OK")
    local sig = wait_sig(ac_cup.SIG_INTEGRITY_OK, ac_cup.SIG_INTEGRITY_ERR, wl.CUP_START_TO)

    if not sig then
        return nil, "Integrity signal timeout"

    elseif sig == ac_cup.SIG_INTEGRITY_ERR then
        return nil, "Cup integrity error"
    end

    mt.hrst()

    -- Wait for start message
    p_dbg("Waiting SIG_START")
    local sig = wait_sig(ac_cup.SIG_START, ac_cup.SIG_START, wl.CUP_START_TO)

    if not sig then
        return nil, "Cup sig start was not received. The mote is probably using an older version."
                    .. "You need to HRST after the upgrade is finished"
    end

    -- Wait for end message
    p_dbg("Waiting SIG_END")
    local sig = wait_sig(ac_cup.SIG_END, ac_cup.SIG_END, wl.CUP_END_TO)
    if not sig then
        return nil, "Cup sig end was not received."
    end

    p_dbg("Restarting Mote ...")
    mt.hrst()

    return true
end

-- =================================================
-- seg_iter
-- =================================================
-- @brief   Returns a block (or segment) iterator in the image.
--          Used to transfer by blocks.
--          The iterator will return a block with max size
--          defined ty chunk_max_size and its offset
-- @param   xfer_img: The image to iterate
-- @param   bin_offset: The offset to start the iteration
-- @param   chunk_max_size: The max size of the block returned
--          by the iterator
-- @retval  The iterator function
----------------------------------------------------
local function seg_iter (xfer_img, bin_offset, chunk_max_size)
    -- Set the closure of the iterator to save its current offset
    local cur_offset = bin_offset

    return function()
            -- Save the current offset to return it
            local chunk_offset = cur_offset

            -- Get the chunk
            local chunk = string.sub(xfer_img, cur_offset + 1, cur_offset + chunk_max_size)

            -- Update chunk_offset upvalue to the next iteration
            cur_offset = cur_offset + #chunk

            if #chunk == 0 then
                chunk = nil
            end

            -- Return the chunk and the chunk_offset this chunk should be
            -- written on the mote
            -- NOTE: In case of End-Of-File, chunk will be nil
            return chunk, chunk_offset
        end
end

-- =================================================
-- wf_or_error
-- =================================================
-- @brief   Perform a write command or raise an error
-- @param   o: The write object.
--          @see alp_mt.wf
-- @retval  none
----------------------------------------------------
local function wf_or_error(o)
    local resp, msg

    for i=1,wl.CUP_RETRIES do
        resp, msg = mt.wf(o)

        if resp then
            break
        end
    end

    if not resp then
        error(msg)
    elseif resp.error_code ~= 0 then
        error("wf error code " .. tostring(resp.error_code))
    end
end

-- =================================================
-- rf_or_error
-- =================================================
-- @brief   Perform a read command or raise an error
-- @param   o: The read object.
--          @see alp_mt.rf
-- @retval  none
----------------------------------------------------
local function rf_or_error(o)
    local resp, msg

    for i=1,wl.CUP_RETRIES do
        resp, msg = mt.rf(o)

        if resp then
            break
        end
    end

    if not resp then
        error(msg, 3)
    elseif resp.error_code then
        error("rf error code " .. tostring(resp.error_code), 3)
    end

    return resp
end

-- =================================================
-- read_cmd_ch
-- =================================================
-- @brief   Read the Dash7 phy channel where we send
--          ALP commands (in the local mote)
-- @retval  The channel
----------------------------------------------------
local function read_cmd_ch()
    local resp = rf_or_error({file_id = wl.GENERAL_CONFIG_FILE_ID, offset = 5, file_len = 1})

    local ch = string.byte(resp.file_data)
    p_inf(string.format("Current cmd ch 0x%02X", ch))
    return ch
end

-- =================================================
-- set_gw_cmd_ch
-- =================================================
-- @brief   Write the Dash7 phy channel where we send
--          ALP commands (in the local mote)
-- @retval  none
----------------------------------------------------
local function set_gw_cmd_ch(ch)
    if not ch then
        return
    end

    p_inf(string.format("Setting cmd channel in wm_gw to 0x%02X", ch))
    wf_or_error({file_id = wl.GENERAL_CONFIG_FILE_ID, offset = 5, file_data = string.char(ch)})
end

-- =================================================
-- set_csm_cmd_ch
-- =================================================
-- @brief   Write the Dash7 phy channel where we send
--          ALP commands (in the remote mote)
-- @retval  none
----------------------------------------------------
local function set_csm_cmd_ch(ch, uid)
    if not ch then
        return
    end

    p_inf(string.format("Setting cmd channel in wm_csm to 0x%02X", ch))
    wf_or_error({file_id = wl.SLEEP_SCAN_FILE_ID, offset = 0, file_data = string.char(ch), uid = uid, fwd = 1})
end

-- =================================================
-- unset_ss_sched
-- =================================================
-- @brief   Disable the scan scheduler in the remote mote
-- @param   uid: The remote mote uid to unset the scan scheduler
-- @retval  The old configuration to be restored by set_ss_sched
----------------------------------------------------
local function unset_ss_sched(uid)

    p_inf("Unseting ss_sched in wm_csm")

    local resp = rf_or_error({file_id = wl.NET_SETTINGS_FILE_ID, offset = 4, file_len = 1, uid = uid, fwd = 1})

    local cfg = string.byte(resp.file_data)

    p_inf(string.format("ss_cfg 0x%02x", cfg))

    wf_or_error({file_id = wl.NET_SETTINGS_FILE_ID, offset = 4, file_data = string.char(bit.band(cfg, 0x7f)), uid = uid, fwd = 1})

    return cfg
end

-- =================================================
-- set_ss_sched
-- =================================================
-- @brief   Restore the scan scheduler as it was before
--          calling unset_ss_sched
-- @param   cfg: The value returned by the unset_ss_sched
-- @param   uid: The remote mote uid to unset the scan scheduler
-- @retval  none
----------------------------------------------------
local function set_ss_sched(cfg, uid)
    if not cfg then
        return
    end
    p_inf("Restoring ss_sched in wm_csm")

    wf_or_error({file_id = wl.NET_SETTINGS_FILE_ID, offset = 4, file_data = string.char(bit.bor(cfg, 0x80)), uid = uid, fwd = 1})
end

-- =================================================
-- set_permanent_fg_listen
-- =================================================
-- @brief   Set the remote to constantly listen for
--          Foreground frames
-- @param   uid: The remote mote uid to set the fg listen
-- @retval  The old configuration to be restored by
--          unset_permanent_fg_listen
----------------------------------------------------
local function set_permanent_fg_listen(uid)
    p_inf("Setting FG permanent listen in wm_csm")

    local resp = rf_or_error({file_id = wl.SLEEP_SCAN_FILE_ID, offset = 1, file_len = 1, uid = uid, fwd = 1})

    wf_or_error({file_id = wl.SLEEP_SCAN_FILE_ID, offset = 1, file_data = string.char(0x80), uid = uid, fwd = 1})

    p_inf(string.format("scan cfg 0x%02x", string.byte(resp.file_data)))

    return string.byte(resp.file_data)
end

-- =================================================
-- unset_adv_n_csma
-- =================================================
-- @brief   Remove advertising and csma (as we are supposed
--          to be in a dedicated channel) in local mote
-- @retval  The old configuration to be restored by
--          set_adv_n_csma
----------------------------------------------------
local function unset_adv_n_csma()
    p_inf("Disabling adv and csma in wm_gw")

    -- Read current value of advertising period
    local resp = rf_or_error({file_id = wl.GENERAL_CONFIG_FILE_ID, offset = 2, file_len = 3})

    wf_or_error({file_id = wl.GENERAL_CONFIG_FILE_ID, offset = 2, file_data = string.char(0,0, wl.D7A_CSMA_NONE)})

    return resp.file_data
end

-- =================================================
-- set_adv_n_csma
-- =================================================
-- @brief   Restore advertising and csma in local mote
-- @param   file_data: the data returned by unset_adv_n_csma
-- @retval  none
----------------------------------------------------
local function set_adv_n_csma(file_data)
    if not file_data then
        return
    end
    p_inf("Restoring adv and csma in wm_gw")
    wf_or_error({file_id = wl.GENERAL_CONFIG_FILE_ID, offset = 2, file_data = file_data})
end

-- =================================================
-- unset_permanent_fg_listen
-- =================================================
-- @brief   Restore the listen configuration modified by
--          set_permanent_fg_listen function
-- @param   uid: The remote mote uid to set the fg listen
-- @param   file_data: The value returned by set_permanent_fg_listen
-- @retval  none
----------------------------------------------------
local function unset_permanent_fg_listen(file_data, uid)
    if not file_data then
        return
    end
    p_inf("Restoring scan in wm_csm")

    -- XXX We are setting the background timeout hardcoded here to 8, then if the script crashes before
    -- executing this function, in the next time the transfer will execute, it will set to the right
    -- value in the end.
    -- NOTE: If we change this value in the mote, we should change it here too
    --wf_or_error({file_id = wl.SLEEP_SCAN_FILE_ID, offset = 1, file_data = file_data, uid = uid, fwd = 1})
    wf_or_error({file_id = wl.SLEEP_SCAN_FILE_ID, offset = 1, file_data = string.char(0x08), uid = uid, fwd = 1})
end

-- =================================================
-- set_gw_rx_ch
-- =================================================
-- @brief   Change the channel where the gateway
--          listen for reports
-- @param   ch: The new channel to listen
-- @retval  none
----------------------------------------------------
local function set_gw_rx_ch(ch)

    p_inf(string.format("Setting GW rx channel to 0x%02x", ch))

    -- Read current value of advertising period
    local resp = rf_or_error({file_id = wl.SLEEP_SCAN_FILE_ID, file_len = 1})

    --var resp = httpGet("http://" + ip + "/cgi-bin/luci/dash7/api/wf?file_id=4&resp=true&file_data=0x"+ch);
    wf_or_error({file_id = wl.SLEEP_SCAN_FILE_ID, file_data = string.char(ch)})

    return string.byte(resp.file_data)
end

-- =================================================
-- set_remote_transfer
-- =================================================
-- @brief   Prepare both motes (the local and the remote)
--          for the transfer.
--          Change the channel, remove advertising,
--          disable scan scheduler, the the remote
--          to listen for FG frames
--
-- NOTE:    Call this function in protect mode with pcall
--
-- @retval  none
----------------------------------------------------
local function set_remote_transfer(uid, original_params, cup_ch)
    p_inf(string.format("Setting remote transfer. Cup channel 0x%02x", cup_ch))

    -- Change gw rx channel, then we will not be bother by sensor reports
    original_params.report_ch = set_gw_rx_ch(cup_ch)

    -- Set in wm_csm cmd channel to CUP channel
    -- If wm_csm didn't responde, assume it is
    -- already in CUP_CHANNEL
    local status, msg = pcall(set_csm_cmd_ch, cup_ch, uid)

    -- Read current cmd channel for future rollback
    original_params.cmd_ch = read_cmd_ch()

    -- Set in wm_gw cmd channel to CUP channel
    set_gw_cmd_ch(cup_ch)

    -- Disable ss scheduler, to the scan will be activate ASAP
    original_params.ss_cfg = unset_ss_sched(uid)

    -- Set wm_csm to infinite rx and listen to fg frames
    original_params.scan_cfg = set_permanent_fg_listen(uid)

    -- Disable advertising and csma in wm_gw
    original_params.adv_n_csma = unset_adv_n_csma()

    -- Log original parameters
    p_log("Original params:", util.serialize_data(original_params))
end

-- =================================================
-- unset_remote_transfer
-- =================================================
-- @brief   Restore the configurations changed by set_remote_transfer
--
-- @retval  none
----------------------------------------------------
local function unset_remote_transfer(uid, original_params)
    local ok = true
    local error_msg = ""

    -- Set wm_csm to listen to BG frames and unset permanent listen
    local status, msg = pcall(unset_permanent_fg_listen, original_params.scan_cfg, uid)

    if not status then
        ok = false
        error_msg = tostring(msg or "") .. " Fail to unset permanent listen in wm_csm. " .. error_msg
    end

    -- Restore adv and csma
    local status, msg = pcall(set_adv_n_csma, original_params.adv_n_csma)

    if not status then
        ok = false
        error_msg = tostring(msg or "") .. " Fail to reset adv and csma in wm_gw. " .. error_msg
    end

    -- Enable ss scheduler
    local status, msg = pcall(set_ss_sched, original_params.ss_cfg, uid)

    if not status then
        ok = false
        error_msg = tostring(msg or "") .. " Fail to set scan sched in wm_csm. " .. error_msg
    end

    -- Restore cmd ch in wm_csm
    local status, msg = pcall(set_csm_cmd_ch, original_params.cmd_ch, uid)

    if not status then
        ok = false
        error_msg = tostring(msg or "") .. " Fail restore cmd channel in wm_csm. " .. error_msg
    end

    -- Restore cmd ch in wm_gw
    local status, msg = pcall(set_gw_cmd_ch, original_params.cmd_ch)

    if not status then
        ok = false
        error_msg = tostring(msg or "") .. " Fail restore cmd channel in wm_gw. " .. error_msg
    end

    -- Restore rx channel in gw
    local status, msg = pcall(set_gw_rx_ch, original_params.report_ch)

    if not status then
        ok = false
        error_msg = tostring(msg or "") .. " Fail restore rx channel in wm_gw. " .. error_msg
    end

    return ok, error_msg

end

-- =================================================
-- update_xfer_file_progress
-- =================================================
-- @brief   Write into the CUP_XFER_STATUS_FILE the current
--          progress (in percentage) of the transfer.
--          Used to inform LuCI about the progress of the
--          transfer.
-- @param   percentage: The percentage to be written into
--              the status file
-- @retval  none
----------------------------------------------------
local function update_xfer_file_progress(percentage)
    local json_obj = {status = wl.CUP_XFER_STATUS_ON_GOING, percentage = percentage}

    local json_str = json.encode(json_obj)

    local fd = io.open(wl.CUP_XFER_STATUS_FILE, "w+")
    fd:write(json_str)
    fd:close()
end

-- =================================================
-- update_xfer_file
-- =================================================
-- @brief   Write in the CUP_XFER_STATUS_FILE that the
--          transfer is finished (success of fail)
--          Used to inform LuCI about the completion
--          of the transfer.
-- @param   ok: true if the transfer succeeded
-- @param   msg: Error message if not ok
-- @param   code_len: The size of the transfered code image
-- @param   fs_len: The size of the transfered file system image
-- @param   xfer_len: The total size of the transfered image
-- @param   xfer_time: The time it took to complete the transfer
-- @retval  none
----------------------------------------------------
local function update_xfer_file(ok, msg, code_len, fs_len, xfer_len, xfer_time)
    -- Transform the ok in a string
    local status = ok and wl.CUP_XFER_STATUS_DONE_OK or wl.CUP_XFER_STATUS_DONE_ERR

    -- Define the object to be written into the CUP_XFER_STATUS_FILE
    local json_obj =    {
                            status = status,
                            info =  {
                                        msg = msg,
                                        code_len = code_len,
                                        fs_len   = fs_len,
                                        xfer_len = xfer_len,
                                        xfer_time = xfer_time,
                                    }
                        }

    -- Encode in json format
    local json_str = json.encode(json_obj)

    -- Save in the file
    local fd = io.open(wl.CUP_XFER_STATUS_FILE, "w+")
    fd:write(json_str)
    fd:close()

    -- Log the message
    p_log(json_str)
end

-- =================================================
-- transfer_file
-- =================================================
-- @brief   Transfer the image to the mote
-- @param   xfer_img:       The image to be transfered
-- @param   bin_offset:     The offset to start the transfer
-- @param   chunk_max_size: The max size of the chunk in
--                          the ALP write command
-- @param   uid             The uid of the remote mote (not
--                          used if CUP is local, i.e., fwd = 0)
-- @param   fwd             The forwarding to make the transfer
-- @param   cup_ch          The channel to make the transfer
--
-- @retval  1)  nil, msg: if error
--          2)  true if success
----------------------------------------------------
local function transfer_file(xfer_img, bin_offset, chunk_max_size, uid, fwd, cup_ch)
    -- Original parameter to be restored in OTA CUP
    local orig_params = {}

    -- The return must be made at the end of this function in case of OTA to undo
    -- the modified parameters
    local ret       = true
    local ret_msg   = nil

    -- Prepare remote transfer to optimize the transfer time
    if fwd > 0 then
        local status, msg = pcall(set_remote_transfer, uid, orig_params, cup_ch)

        -- If we failed
        if not status then

            -- Try to rollback
            local rb_status, rb_msg = unset_remote_transfer(uid, orig_params)

            if not rb_status then
                msg = "Failed to rollback. " .. tostring(msg) .. tostring(rb_msg)
            end

            msg = "Fail to set remote transfer." .. tostring(msg) .. tostring(rb_msg)
            p_err(msg)
            return nil, msg
        end
    end

    -- Save the current time for updating the status file
    local ts = os.time()
    local last_file_update = ts

    -- Update file to indicate the percentage we already transfered
    update_xfer_file_progress(0)

    -- For each chunk of the file, starting with a specific
    -- offset and having a size of chunk_size or less
    -- write the chunk into the mote and
    for chunk, chunk_offset in seg_iter(xfer_img, bin_offset, chunk_max_size) do

        local resp, msg

        for i=1,wl.CUP_RETRIES do

            p_inf("Transfer offset", chunk_offset)

            local chunk_len = string.len(chunk or "")

            -- Try to write the chunk
            -- Do not set dst id, it should be already ok
            resp, msg = mt.wf({file_id = wl.CUP_CODE_FILE_ID, file_block = wl.CUP_CODE_FILE_BLOCK, file_data = chunk, offset = chunk_offset, uid = uid, fwd = fwd}, true)

            -- If ok then go to the next chunk
            if resp and resp.error_code then
                break

            -- If not ok log an error and retry
            else
                p_err("Fail to transfer the chunk offset", chunk_offset, "Retry", i, util.serialize_data(resp), msg or tostring(error_code))
            end
        end

        -- If we couldn't write the chunk
        if not resp or resp.error_code ~= 0 then
            ret = nil
            ret_msg = "Could not write chunk offset " .. tostring(chunk_offset) .. " " .. util.serialize_data(resp) .. " " .. tostring(msg),
            p_err(ret_msg)
            break
        end

        -- Update percentage if its time
        if os.time() - last_file_update > wl.CUP_XFER_PROGRESS_UPDATE_TIME then
            local percentage = math.floor(((chunk_offset + chunk_max_size)*100)/#xfer_img)
            p_inf("Progress: " .. tostring(percentage) .. "% ", chunk_offset+chunk_max_size, #xfer_img)
            update_xfer_file_progress(percentage)
            last_file_update = os.time()
        end

    end

    if ret then
        p_inf("Transfer finished", os.time() - ts, "seconds")
    end

    -- Undo remote transfer preparations
    if fwd > 0 then
        local status, msg = unset_remote_transfer(uid, orig_params)
        if not status then
            if ret then
                ret_msg = "Transfer done, but unable to unset remote transfer configurations. " .. tostring(msg) .. ". " .. tostring(ret_msg)
            else
                ret_msg = "Transfer failed and unable to unset remote transfer configurations. " .. tostring(msg) .. ". " .. tostring(ret_msg)
            end

            return nil, ret_msg
        end
    end


    return ret, ret_msg
end

-- =================================================
-- update_cfg_file
-- =================================================
-- @brief   Write in the configuration file to start
--          the code upgrade
-- @param   uid: The uid of the remote mote (if fwd > 0)
-- @param   fwd: The forwarding of the code upgrade
-- @param   crc: The crc of the image
-- @param   code_len: The code len in the image
-- @param   fs_len: The file system len in the image
-- @param   fs_offset: The offset to write the file system
-- @param   src_start: The address of the image in the flash
-- @param   code_dst_start: The destination address of the code
-- @retval  1) true: if success
--          2) nil, msg: if error
----------------------------------------------------
local function update_cfg_file(uid, fwd, crc, code_len, fs_len, fs_offset, src_start, code_dst_start)

    p_inf("Updating cfg file")

    --cup_cfg = [0,0,0,
    --    0xC0DEA700,# cmd
    --    cup_eeprom.length,# fs_size
    --    options[:fs_offset],# fs_offset
    --    sig,# signature
    --    0x0800F200,# src_start
    --    cup_code.length,# code_size
    --    0x08000000,# code_dest_start
    --]

    -- Build the string of bytes to be written in the cup cfg file
    local fdata = string.char(0,0,0)                        ..
                  wl.int_to_bytes(wl.CUP_CMD, false, 4)     ..
                  wl.int_to_bytes(fs_len, false, 2)         ..
                  wl.int_to_bytes(fs_offset, false, 2)      ..
                  wl.int_to_bytes(crc, false, 4)            ..
                  wl.int_to_bytes(src_start, false, 4)      ..
                  wl.int_to_bytes(code_len, false, 4)       ..
                  wl.int_to_bytes(code_dst_start, false, 4)

    p_inf("up cfg size", #fdata)

    -- Write the data into the file
    local resp, msg = mt.wf({file_id = wl.CUP_CFG_FILE_ID, file_block = wl.CUP_CFG_FILE_BLOCK, file_data=fdata, uid = uid, fwd = fwd})

    if resp and resp.error_code and resp.error_code == 0 then
        return true, msg
    else
        return nil, msg or tostring(resp.error_code)
    end
end


-- =================================================
-- xfer_action_exec
-- =================================================
-- @brief   Perform the transfer of the image.
--          High level function: Check the crc, assemble the
--          complete image and then call transfer_file function
--          to effectively transfer the file.
--          Update the STATUS_FILE when the transfer is finished
--          to inform LuCI about the completion.
-- @param   o: The object containing parameters to perform
--          the transfer with the following fields:
--              .bin_offset (The offset to transfer the file)
--              .chunk_size (The maximum size of the chunk to be written)
--              .uid        (The remote uid if CUP is OTA)
--              .fwd        (The forwarding of CUP)
--              .cup_ch     (The channel to perform the CUP)
-- @param   code_bin    (optional): The binary of the code to be written (as an
--                                      array of bytes
-- @param   fs_bin      (optional): The binary of the file system to be written
--                                      (as an array of bytes)
-- @retval  1) nil, msg: if error
--          2) true: if success
----------------------------------------------------
local function xfer_action_exec(o, code_bin, fs_bin)

    -- Check crc of the code file if it exist, otherwise consider it as an empty string
    if code_bin then
        local status , msg = crc_check(code_bin, "code.bin")
        if not status then
            update_xfer_file(nil, msg)
            return nil, msg
        end
    else
        code_bin = ""
    end

    -- Check crc of the fs file if it exist, otherwise consider it as an empty string
    if fs_bin then
        local status, msg = crc_check(fs_bin, "fs.bin")
        if not status then
            update_xfer_file(nil, msg)
            return nil, msg
        end
    else
        fs_bin = ""
    end

    -- Assemble image to be transfered
    local xfer_img, msg = assemble_img(code_bin, fs_bin)
    if not xfer_img then
        update_xfer_file(nil, msg)
        return nil, msg
    end

    -- Do the transfer saving its duration
    local ts = os.time()
    local status, msg = transfer_file(xfer_img, o.bin_offset, o.chunk_size, o.uid, o.fwd, o.cup_ch)
    ts = os.time() - ts

    -- Update the transfer status file to done or failed
    update_xfer_file(status, msg, #code_bin, #fs_bin, #xfer_img, ts)

    -- In case of error return nil and the error message
    if not status then
        p_err("Transfer failed", ts, "seconds")
        return nil, msg
    end

    p_inf("Transfer total:", ts, "seconds")

    return true
end

-- =================================================
-- upgrade_action_exec
-- =================================================
-- @brief   Execute the code upgrade.
--          Read the images, calculate the CRC, read
--          the offset of the file system according to
--          its type (D,T,C for device, calib/tune, config)
--          and wait for the CUP signals sent by the mote
--          to perform the appropriate reset when needed
-- @param   o: The object containing parameters to perform
--          the transfer with the following fields:
--              .src_start      (optional, where the code is loaded)
--              .code_dst_start (optional, where to copy the code)
--              .uid            (The remote uid if CUP is OTA)
--              .fwd            (The forwarding of CUP)
--
-- @param   code_bin    (optional): The binary of the code to be written (as an
--                                      array of bytes
-- @param   fs_bin      (optional): The binary of the file system to be written
--                                      (as an array of bytes)
-- @retval  1) nil, msg: if error
--          2) true: if success
----------------------------------------------------
local function upgrade_action_exec(o, code_bin, fs_bin)
    local fs_offset

    -- Check crc of the fs file if it exist, otherwise consider it as an empty string
    if fs_bin then
        -- Check crc of the fs file
        -- This is performed here because a transfer is not
        -- necessary done with an upgrade. Thus the fs must be
        -- check to ensure we get the proper type and header 0 address
        local status, msg = crc_check(fs_bin, "fs.bin")
        if not status then
            return nil, msg
        end

        -- Get the offset to write the file system
        fs_offset, msg = get_fs_offset(fs_bin)

        if not fs_offset then
            return nil, msg
        end

        p_inf("FS OFFSET", fs_offset)

        if not fs_offset then
            return nil, msg
        end

    -- If the fs file does not exist, consider it as an empty string
    else
        fs_bin = ""
    end

    -- If the code fie does not exist, considerer it as an empty string
    code_bin = code_bin or ""

    -- Assemble img to get the global crc
    local status, crc = assemble_img(code_bin, fs_bin)

    if not status then
        return nil, crc
    end

    local src_start = o.src_start or wl.CUP_SRC_START
    local code_dst_start = o.code_dst_start or wl.CUP_CODE_DST_START

    -- Start the upgrade - write into cup_cfg
    local status, msg = update_cfg_file(o.uid, o.fwd, crc, #code_bin, #fs_bin, fs_offset or 65535, src_start, code_dst_start)

    -- In case of error return nil and the error message
    if not status then
        p_err("Upgrade failed")
        return nil, msg
    end

    p_inf("Upgrade initialized")

    -- hard rst mode if the upgrade is meant to the local mote
    if o.fwd == 0 then
        -- Wait for the proper signals and reset the local mote when needed
        local status, msg = hrst_watch()
        if not status then
            return nil, msg
        end
    end

    return true
end

----------------------------------------------------
-- PUBLIC FUNCTIONS
----------------------------------------------------

-- =================================================
-- ac_cup.init
-- =================================================
-- @brief   Initialize cup.
--          Remove the CUP_XFER_STATUS_FILE to erase
--          all previous state
-- @retval  none
----------------------------------------------------
function ac_cup.init()
    -- Remove xfer file to indicate no xfer is going on
    os.remove(wl.CUP_XFER_STATUS_FILE)
end


-- =================================================
-- ac_cup.cup
-- =================================================
-- @brief   The main function that execute CUP actions
--          depending on the .action field in the o object.
-- @param   o: The object containing the action's parameters:
--              o = {
--               .ref_name      (mandatory) The image reference name
--               .bin_offset    (optional, default 0) used by the transfer action
--                                  provide the feature to resume a transfer
--               .chunk_size    (optional, default wl.CUP_CHUNK_SIZE) used by
--                                  transfer action to set the its fragmentation.
--                                  The transfer will send the image by chunks with
--                                  max size of chunk_size. This number should be a
--                                  multiple of the MCU page size)
--               .uid           (optional, mandatory if fwd is greater then 0)
--                                  If CUP OTA, the address of the remote mote
--               .fwd           (optional, default 0) The forwarding of the CUP
--               .cup_ch        (optional, default wl.CUP_CHANNEL) the channel to
--                                  tranfer cup file
--               .action        (optional) set to:
--                              "transfer": to just transfer the file to the mote
--                              "upgrade":  to write in cup cfg file and upgrade the code
--                              nil:        to do both
--              }
--
-- @retval  nil, msg: if error
--          true: if success
----------------------------------------------------
function ac_cup.cup (o)
    ----------------
    -- PARAM CHECKS
    ----------------
    if not o.ref_name then
        return nil, "Missing param ref_name"
    end

    o.bin_offset = tonumber(o.bin_offset) or 0
    o.chunk_size = tonumber(o.chunk_size) or wl.CUP_CHUNK_SIZE
    o.fwd = tonumber(o.fwd) or 0
    o.cup_ch = tonumber(o.cup_ch) or wl.CUP_CHANNEL
    if o.fwd > 0 then
        if not o.uid or #o.uid ~= wl.S_UID_SIZE then
            local msg = "Missing or wrong param UID"

            if not o.action or o.action == "transfer" then
                update_xfer_file(nil, msg)
            end

            return nil, msg
        end
    end

    if not o.ref_name or #o.ref_name == 0 then
        local msg = "Missing or wrong param REF_NAME"

        if not o.action or o.action == "transfer" then
            update_xfer_file(nil, msg)
        end

        return nil, msg
    end

    if o.action and (o.action ~= "upgrade" and o.action ~= "transfer") then
        local msg = "Unknown action " .. tostring(o.action)

        if not o.action or o.action == "transfer" then
            update_xfer_file(nil, msg)
        end

        return nil, msg
    end
    ----------------

    -- Get bin code and image name
    local code_bin_name = wl.CUP_UPLOAD_FOLDER .. o.ref_name .. "_code.bin"
    local fs_bin_name   = wl.CUP_UPLOAD_FOLDER .. o.ref_name .. "_fs.bin"

    -- Read files
    local code_bin  = read_all_file(code_bin_name)
    local fs_bin    = read_all_file(fs_bin_name)

    -- Transfer file to the mote
    if not o.action or o.action == "transfer" then
        -- Start the transfer of image
        local status, msg = xfer_action_exec(o, code_bin, fs_bin)
        if not status then
            return nil, msg
        end
    end

    -- Write in cfg file to start the code upgrade
    -- Transfer file to the mote
    if not o.action or o.action == "upgrade" then
        local status, msg = upgrade_action_exec(o, code_bin, fs_bin)
        if not status then
            return nil, msg
        end
    end

    p_dbg("DONE")
    return true
end

-- Return the CUP library
return ac_cup
