----------------------------------------
-- Dependencies
----------------------------------------
local p_color  = require "p_color"
local nixio    = require "nixio"
local wl       = require "wl"
----------------------------------------

----------------------------------------------------
-- Print functions
-- Set a empty function to disable a print
----------------------------------------------------
local p_pref = "DP:"
local p_err = function (...) p_color.p_err(p_pref, unpack(arg)) end
local p_dbg = function (...) p_color.p_dbg(p_pref, unpack(arg)) end
local p_inf = function (...) p_color.p_inf(p_pref, unpack(arg)) end
local p_log = function (...) p_color.p_log(p_pref, unpack(arg)) end

----------------------------------------------------
-- Global object
-- To be returned by require function
----------------------------------------------------
ac_dp = {}

local so

-- =================================================
-- ac_dp.init
-- =================================================
-- @brief   Connect with the socket of the display COM port
-- @retval  none
----------------------------------------------------
function ac_dp.init()
    -- Initialize display log
    ac_dp.log_obj = wl.log_init(wl.DISP_LOG_FILE)

    -- Connect with the com port
    so = nixio.connect(localhost, wl.DP_COM_PORT, "inet", "stream")

    if not so then
        error("Could not connect with socket")
    end
end

-- =================================================
-- ac_dp.close
-- =================================================
-- @brief   Close the socket opened by ac_dp.init
-- @retval  none
----------------------------------------------------
function ac_dp.close()
    so:close()
end

-- =================================================
-- ac_dp.set
-- =================================================
-- @brief   Send a message to the display
-- @param   o: The structure with the fields
--              .disp_id
--              .disp_msg
-- @param   raw: if false, add wl.DP_PREPEND and wl.DP_APPEND,
--              otherwise, send the disp_msg as it is
-- @retval  none
----------------------------------------------------
function ac_dp.set(o, raw)
    -- If there is not socket
    -- or display id
    -- or no display message without a previous message
    if not so or not o or not o.disp_id or (not o.disp_msg and not ac_dp.last_msg) then
        return nil, "WIZZI_DISP ERROR"
    end

    -- If there is no disp message, just send the last message
    if not o.disp_msg or #o.disp_msg == 0 then
        o.disp_msg = ac_dp.last_msg
    else
        -- Save last message
        ac_dp.last_msg = o.disp_msg
    end

    p_dbg("SET", o.disp_id, o.disp_msg)

    if wl.DP_CALCULATE_CRC then
        local crc_v = 0
        for c in o.disp_msg:gmatch"." do
            crc_v = bit.bxor(string.byte(c), crc_v)
        end
        p_dbg("CRC", crc_v)
        o.disp_msg = o.disp_msg .. string.format("%02x", crc_v)
    end

    p_inf(o.disp_msg)

    if not raw then
        o.disp_msg = wl.DP_PREPEND .. o.disp_msg .. wl.DP_APPEND
    end

    local sent = so:sendall(o.disp_msg)
    p_dbg("DP SENT", sent)

    -- Log msg
    local ts = os.date("[%y/%m/%d-%Hh%Mm%Ss]", os.time())
    wl.log(ac_dp.log_obj, ts .. wl.pp_data(o.disp_msg) .. "\n")

    return true
end

-----------------------
-- Return the library
-----------------------
return ac_dp
