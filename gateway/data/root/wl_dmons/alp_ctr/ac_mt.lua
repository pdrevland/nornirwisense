----------------------------------------
-- Dependencies
----------------------------------------
local p_color   = require "p_color"
local nixio     = require "nixio"
local nixio_util= require "nixio.util"
local util      = require "luci.util"
local bit       = require "bit"
local alp       = require "alp_parser"
local socket    = require "socket"
local app       = require "app"
----------------------------------------

----------------------------------------------------
-- Print functions
-- Set an empty function to disable a print
----------------------------------------------------
local p_pref = "MT:"
local p_err = function (...) p_color.p_err(p_pref, unpack(arg)) end
local p_dbg = function (...) p_color.p_dbg(p_pref, unpack(arg)) end
local p_inf = function (...) p_color.p_inf(p_pref, unpack(arg)) end
local p_log = function (...) p_color.p_log(p_pref, unpack(arg)) end

----------------------------------------------------
-- Public object
-- To be returned by require function
----------------------------------------------------
ac_mt = {}

----------------------------------------------------
-- Private context object
----------------------------------------------------
local g_ac_mt_ctx = {}

----------------------------------------------------
-- Flag used in nixio.poll to indicate that we will
-- block until there is a socket available for reading
----------------------------------------------------
local POLL_FLAGS = nixio.poll_flags("in")

----------------------------------------------------
-- PRIVATE FUNCTIONS
----------------------------------------------------

-- =================================================
-- clean_sk
-- =================================================
-- @brief   Read all available contents from the COM
--          socket
-- @retval  none
----------------------------------------------------
local function clean_sk()
    -- Read until there is no more pending data to read
    while true do

        -- Read from the COM socket
        local readed, code, err = g_ac_mt_ctx.so:read(1000)

        -- No more data to read
        if readed == false then
            break

        -- An error has occurred
        elseif readed == nil then
            error(tostring(g_ac_mt_ctx.so) .. "\t" .. tostring(code) .. "\t" .. tostring(err))

        -- Connection is lost
        elseif #readed == 0 then
            error("Connection with SR Dispatcher lost")

        -- The socket was not empty, print warning message
        else
            p_err("SK was not clean, outdated ALP responses?", #readed)
        end
    end
end


-- =================================================
-- read_pkt_from_sk
-- =================================================
-- @brief   Try to read a packet from the COM socket
-- @retval  1) The packet sent by the dispatcher process
--          2) nil, msg: in case of error
----------------------------------------------------
local function read_pkt_from_sk(timeout)
    -- Get the time now
    local now = os.time()

    -- Get the limit timestamp we should receive the
    -- a packet
    local resp_before_time = now + timeout

    if timeout <= 0 then
        return nil, "1)Read timeout"
    end

    -- Get the timeout in ms
    local to_ms = timeout*1000

    -- Wait until data is available in socket
    local ok, s_inf = nixio.poll({{fd=g_ac_mt_ctx.so, events=POLL_FLAGS}}, to_ms)

    -- If timeout, return nil
    if not ok then
        return nil, "2)Read timeout"
    end

    -- Decode the returned flags in lua style
    local flags = nixio.poll_flags(s_inf[1].revents)

    -- If the poll returned, but there is no socket
    -- available for reading, raise error
    if not flags["in"] then
        error("Socket error")
    end

    -- Read the length of pkt (it is the first byte sent
    -- by the COM dispatcher)
    local clen, code, err = g_ac_mt_ctx.so:read(1)

    -- Error in socket
    if clen == nil then
        -- Stop on error
        error(tostring(g_ac_mt_ctx.so) .. "\t" .. tostring(code) .. "\t" .. tostring(err))

    -- If the connection is lost
    elseif #clen == 0 then
        -- Stop on error
        error("Socket ALP CTR was disconnected")
    end

    -- Prepare for entering the reading loop
    -- missing: how many byte we still need to read
    local missing = string.byte(clen)

    -- The packet that we are reading
    local pkt = ""

    -- Try reading the rest of the pkt while we are still in time
    -- NOTE: Do not nixio.poll here, data shouldn't take too long
    -- to arrive after len has arrived
    while missing > 0 and os.time() < resp_before_time do

        -- try read the whole pkt
        local rcved, code, err = g_ac_mt_ctx.so:read(missing)

        -- If we read something
        if rcved ~= false then

            -- Error in socket
            if rcved == nil then
                -- Stop on error
                error(tostring(g_ac_mt_ctx.so) .. "\t" .. tostring(code) .. "\t" .. tostring(err))

            -- If the connection is lost
            elseif #rcved == 0 then
                -- Stop on error
                error("Socket ALP CTR was disconnected")
            end

            -- Update the missing variable
            missing = missing - #rcved
            -- Concatenate the received data with the data
            -- previously received
            pkt = pkt .. rcved or ""
        end
    end

    -- If we manage to read the whole packet, return it
    -- Otherwise, return error
    if missing == 0 then
        return pkt
    else
        return nil, "3)Read timeout"
    end
end


-- =================================================
-- get_resp
-- =================================================
-- @brief   Read the alp response from the COM socket
--          sent by the COM dispatcher
-- @param   to_interval: the maximum time to wait for the first response or between two packets
--              If it is a unicast response, it is the timeout to receive one response
--              If it is a multicast response, the time counter is reseted when a packet arrives
--              If the timeout expires from the LAST RECEIVED packet we consider that we won't
--              receive any more pkts
-- @param   fwd (optional, default 0): The forwarding of the response. This is used to determine
--              if this is a local command and to define some error messages
-- @param   uid (optional if fwd = 0): The uid if the sent command was Unicast
--              this is used to update the last communication data of the internal report
-- @param   raw: do not parse the response, just return it raw
--              NOTE: it will return the first received packet, do not use this function
--              with fwd > 0, otherwise we will loose packets
-- @retval 1) All the responses (unpack), raw (if raw mode is true) or a combination
--              of data and error template
--              @see alp.parse in alp_parse.lua file
--         2) nil, msg: if error
----------------------------------------------------
local function get_resp(to_interval, fwd, uid, raw)
    -- loc: true it this is a local command
    local loc = not fwd or fwd == 0

    p_dbg("WAIT RESP", "LOCAL", loc, "TIMEOUT", to_interval)

    -- Declare local variables
    -- The read packet
    local pkt
    -- The returned response list
    local resp_obj
    -- The returned response message in case of error
    local resp_err_msg

    -- Pre-define the response message as no response
    -- from device
    if fwd and fwd > 0 then
        resp_err_msg = "No response from remote device"
    else
        resp_err_msg = "No response from gateway Dash7 modem"
    end

    -- Start the reading loop
    while true do

        -- Read pkt from socket
        pkt, msg = read_pkt_from_sk(to_interval)

        -- If time's up
        if not pkt then
            -- This is an error, because we should receive an
            -- ALP NULL to indicate the end of the command
            p_err("RESP TIME'S UP", msg)
            if fwd and fwd > 0 then
                resp_err_msg = "No control message from gateway Dash7 modem"
            end
            -- Go out the while
            break
        end

        -- If we just read a pkt and this is a raw mode
        -- command, just return it as it is
        if raw then
            return pkt
        end

        -- Parse the received pkt
        local rec, msg = alp.parse(pkt)

        -- Error while parsing
        if not rec then
            p_err("Could not parse ALP PKT", msg)

        -- No errors in parsing
        else

            -- Get data template or error template
            -- A response to a file data cmd contains
            -- one of these templates
            local resp = rec.data_tpl or rec.error_tpl

            -- If this is a response to a file_data cmd
            if resp then

                -- debug print
                p_dbg("RESP", util.serialize_data(resp))

                -- Initialize the response object if it was not already
                -- initialized and insert the response template into it
                resp_obj = resp_obj or {}
                table.insert(resp_obj, resp)

                -- If this is a local cmd we are done
                -- we won't receive anymore responses
                if loc then
                    p_dbg("RESP OK - NO MULT")
                    break
                end

            -- If we received an ALP NULL NACK, then we finished
            -- receiving all responses.
            -- NOTE: Receiving an NACK just means the command is finished.
            -- It does not means that we didn't receive any response
            elseif rec.bid == alp.bid.NULL and rec.op == alp.null_op.NACK then
                p_dbg("RESP END")
                break

            -- If we received an ALP NULL TX FAILED, then we couldn't
            -- send our message
            elseif rec.bid == alp.bid.NULL and rec.op == alp.null_op.TX_FAILED then
                p_err("TX FAILED")
                resp_err_msg = "Could not send the command. Check the CCA level"
                break

            -- Unknown pkt
            else
                p_err("Unknown pkt", util.serialize_data(rec))
            end

        end
    end

    -- If we didn't receive any response
    if not resp_obj then
        p_err("BAD - no resp")
        return nil, resp_err_msg

    -- If we received responses and the INTERNAL_REPORT_ENABLED is true
    -- Then update the last communication time (ts) of this sensor
    elseif app.INTERNAL_REPORT_ENABLED and fwd and fwd > 0 and uid and uid~='FFFFFFFFFFFFFFFF' then
        -- Update the last communication data
        local ret = wl.internal_report({uid = uid, ts = socket.gettime()*1000})

        -- In case of error, log
        if not ret or ret.status == "ERR" then
            p_err("Error in sending obj to sensor_data process")

        -- We are ok
        else
            p_dbg("obj reported",util.serialize_data(obj))
        end

    end

    p_inf("N RESP", table.getn(resp_obj or {}))

    -- Return the list as unpack mode (see lua manual)
    -- So Unicast commands does not need to read a list
    -- or size 1, it will receive directly the response
    return unpack(resp_obj)

end

-- =================================================
-- send_cmd
-- =================================================
-- @brief   Send the cmd and return the response
-- @param   to_interval: the timeout sent to get_resp
-- @retval  return the result of get_resp
-- @see get_resp
----------------------------------------------------
local function send_cmd(cmd, to_interval, fwd, uid)

    -- Clean outdated responses that are still
    -- in the socket queue
    clean_sk()

    -- Send the comand thought socket
    g_ac_mt_ctx.so:sendall(string.char(#cmd))
    g_ac_mt_ctx.so:sendall(cmd)

    -- If the cmd does not require a response
    -- then return success
    if cmd.resp == false then
        return true
    end

    -- Return the response
    return get_resp(to_interval, fwd, uid)
end


-- =================================================
-- set_dst
-- =================================================
-- @brief   Set the destination address of the modem.
--          Write in the general configuration file
--          the requested uid
-- @param   uid: The requested UID (in string format)
-- @retval  1)  true if success
--          2)  nil, msg: in case of error
----------------------------------------------------
local function set_dst(uid)
    p_inf("SET DST UID " .. tostring(uid))

    -- Check if parameter is present
    if not uid then
        return nil, "Missing DST UID"
    end

    -- Check if parameter contains errors
    if #uid ~= wl.S_UID_SIZE then
        return nil, "Wrong size of dst uid"
    end

    -- transform UID as a string of bytes
    local bin_uid = wl.hexstr_to_data(uid)

    -- If we couldn't transform it, it means that
    -- the string containing non hexadecimal characters
    if not bin_uid then
        return nil, "UID in wrong format"
    end

    -- Build the write to file cmd
    local cmd, msg = alp.wf_build({file_id = 0x19, offset = 7, file_data = bin_uid})

    -- If we couldn't build the command, return error
    if not cmd then
        return nil, msg
    end

    -- Send the command and get the response
    local resp = send_cmd(cmd, wl.AC_RESP_DEFAULT_TIMEOUT)

    p_dbg("RESP", util.serialize_data(resp))

    -- In case of error, return nil
    if not resp or resp.error_code ~= 0 then
        return nil, "ERR while writing destination address " .. tostring(uid)
    end

    -- Return true for success
    return true
end

-- =================================================
-- rw
-- =================================================
-- @brief   Read or write file
--          Set destination UID if necessary and if dst_ok is false
--          If fwd is not defined and uid is not the local uid, return error
-- @param   rw_op: 'R' to read, 'W' to write or 'WF' to write flush a file
-- @param   o: The object containing the parameters for read or writing a file
--          @see wf_biuld or rf_build in alp_parser.lua for more informations
--              o = {
--               .uid        (optional if fwd = 0)
--               .offset     (optional, default 0)
--               .file_data  (optional)
--               .resp       (optional, default true)
--               .file_id    (mandatory)
--               .file_block (optional if cmd, default ISFB)
--               .file_len   (mandatory if rw_op = 'R')
--               .fwd        (optional, default 0)
--              }
-- @param   dst_ok: if true, the destination address won't be changed
-- @retval  1) nil, msg in case of error
--          2) The result of send_cmd
--          @see send_cmd
----------------------------------------------------
local function rw(rw_op, o, dst_ok)
    -- Check parameter
    if not o then
        p_err("RW FUNC - WRONG PARAM nil")
        return nil, "RW FUNC - WRONG PARAM nil"
    end

    -- If fwd is not defined and uid is, but o.uid is not our uid,
    -- return error
    if not o.fwd and o.uid then
        local file = wl.get_gw_config() or {}
        if file.uid ~= o.uid then
            return nil, "MISSING FWD PARAMETER"
        end
    end

    -- Set the default timeout if it is not defined
    o.timeout = o.timeout or wl.AC_RESP_DEFAULT_TIMEOUT

    -- If this is a remote cmd, write the
    -- destination id in local file if dst_ok
    -- is false
    if o.fwd and o.fwd ~= 0 and not dst_ok then
        local ret, msg = set_dst(o.uid)
        if not ret then return nil, msg end
    end

    -- Set local variables
    -- The alp command to be send and the error message if any
    local alp_cmd, msg

    -- It this is a read command
    if rw_op == 'R' then
        alp_cmd, msg = alp.rf_build(o)

    -- It this is a write command
    elseif rw_op == 'W' then
        alp_cmd, msg = alp.wf_build(o)

    -- It this is a flush write command
    elseif rw_op == 'WF' then
        alp_cmd, msg = alp.wf_build(o, true)

    else
        error("RW function with unknown cmd " .. tostring(rw_op))
    end

    -- If we couldn't build the command, return error
    if not alp_cmd then
        return nil, msg
    end

    -- debug print
    p_dbg("SENDING", wl.pp_data(alp_cmd))

    -- Send the command and return the responses
    return send_cmd(alp_cmd, o.timeout, o.fwd, o.uid)
end

----------------------------------------------------
-- PUBLIC FUNCTIONS
----------------------------------------------------

-- =================================================
-- ac_mt.init
-- =================================================
-- @brief   Initialize the communication with the mote
--          Connect with the COM dispatcher socket
--          Read the uid of the mote and update it
--          in the gateway config file.
--          Disable force display
-- @retval  none
----------------------------------------------------
function ac_mt.init ()
    p_dbg("INIT")

    -- Create COM socket
    g_ac_mt_ctx.so = nixio.socket("unix", "stream")

    -- Connect with COM Dispatcher socket
    while true do
        local connected = g_ac_mt_ctx.so:connect(wl.SK_ALP_CMD_PATH)

        if not connected then
            p_err("Fail to connect to socket, retrying in 1 sec")
            nixio.nanosleep(1)
        else
            break
        end
    end

    -- Do not block
    g_ac_mt_ctx.so:setblocking(false)

    -- Read gw informations
    local file = wl.get_gw_config() or {}

    -- Update uid
    local uid = ac_mt.get_uid()

    file.uid = uid or file.uid

    p_inf("UID", file.uid)

    -- Disable force display
    file.display_update_off = nil

    -- Save new config
    wl.set_gw_config(file)

    p_dbg("INIT OK")
end

-- =================================================
-- ac_mt.close
-- =================================================
-- @brief   Close the COM socket
-- @retval  none
----------------------------------------------------
function ac_mt.close()
    g_ac_mt_ctx.so:close()
end

-- =================================================
-- ac_mt.read_raw
-- =================================================
-- @brief   Read a packet from the mote without parsing it.
--          This function is used by cup.
-- @param   to_interval:    The timeout to read the packet
-- @retval  1)  The packet read
--          2)  nil, msg: in case of error
----------------------------------------------------
function ac_mt.read_raw(to_interval)
    return get_resp(to_interval, 0, nil, true)
end

-- =================================================
-- ac_mt.get_uid
-- =================================================
-- @brief   Read the uid of the mote in string format
-- @retval  1)  The uid, e.g., "001BC50C70000123"
--          2)  nil, msg: in case of error
----------------------------------------------------
function ac_mt.get_uid()
    local resp, msg = ac_mt.rf({file_id = 1, file_len = 8})

    if not resp then
        return nil, msg
    end

    local uid = resp.file_data

    -- Address in a readable format
    uid = wl.data_to_hexstr(uid)

    return uid
end

-- =================================================
-- ac_mt.wf
-- =================================================
-- @brief   Perform a write file command in the mote
--          Set destination UID if necessary and if dst_ok is false
--          If fwd is not defined and uid is not the local uid, return error
-- @param   o: the object with the parameters to perform a write file
--              o = {
--               .uid        (optional if fwd = 0)
--               .offset     (optional, default 0)
--               .file_data  (optional)
--               .resp       (optional, default true)
--               .file_id    (mandatory)
--               .file_block (optional if cmd, default ISFB)
--               .fwd        (optional, default 0)
--              }
-- @param   dst_ok: if true, the destination address won't be changed
-- @retval  1) nil, msg in case of error
--          2) one or several error templates
--          @see send_cmd
-- @see rw
----------------------------------------------------
function ac_mt.wf(o, dst_ok)
    return rw('W', o, dst_ok)
end

-- =================================================
-- ac_mt.wff
-- =================================================
-- @brief   Perform a write flush file command in the mote
--          Set destination UID if necessary and if dst_ok is false
--          If fwd is not defined and uid is not the local uid, return error
-- @param   o: the object with the parameters to perform a write file
--              o = {
--               .uid        (optional if fwd = 0)
--               .offset     (optional, default 0)
--               .file_data  (optional)
--               .resp       (optional, default true)
--               .file_id    (mandatory)
--               .file_block (optional if cmd, default ISFB)
--               .fwd        (optional, default 0)
--              }
-- @param   dst_ok: if true, the destination address won't be changed
-- @retval  1) nil, msg in case of error
--          2) one or several error templates
--          @see send_cmd
-- @see rw
----------------------------------------------------
function ac_mt.wff(o, dst_ok)
    return rw('WF', o, dst_ok)
end

-- =================================================
-- ac_mt.rf
-- =================================================
-- @brief   Perform a read file command in the mote
--          Set destination UID if necessary and if dst_ok is false
--          If fwd is not defined and uid is not the local uid, return error
-- @param   o: the object with the parameters to perform a write file
--              o = {
--               .uid        (optional if fwd = 0)
--               .offset     (optional, default 0)
--               .resp       (optional, default true)
--               .file_id    (mandatory)
--               .file_block (optional if cmd, default ISFB)
--               .file_len   (mandatory if rw_op = 'R')
--               .fwd        (optional, default 0)
--              }
-- @param   dst_ok: if true, the destination address won't be changed
-- @retval  1) nil, msg in case of error
--          2) one or several data and error templates
--          @see send_cmd
-- @see rw
----------------------------------------------------
function ac_mt.rf(o, dst_ok)
    return rw('R', o, dst_ok)
end

-- Return the public object
return ac_mt
