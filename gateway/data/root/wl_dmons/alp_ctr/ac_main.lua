#!/usr/bin/lua

-- Setting path to look for libraries
-- XXX: this is not a good place, we won't be able to move the code to another file
package.path = (package.path or "") .. ";/root/wl_dmons/alp_ctr/?.lua;/root/wl_dmons/?.lua"

----------------------------------------
-- Dependencies
----------------------------------------
local p_color       = require "p_color"
local nixio         = require "nixio"
local nixio_util    = require "nixio.util"
local ac_mt         = require "ac_mt"
local ac_cup        = require "ac_cup"
local wl            = require "wl"
local util          = require "luci.util"
local alp           = require "alp_parser"
local app           = require "app"
local ac_dp         = app.DP_ENABLED and require "ac_dp" or nil
----------------------------------------

----------------------------------------------------
-- Print functions
-- Set a empty function to disable a print
----------------------------------------------------
local p_pref = "ACM:"
local p_err = function (...) p_color.p_err(p_pref, unpack(arg)) end
local p_dbg = function (...) p_color.p_dbg(p_pref, unpack(arg)) end
local p_inf = function (...) p_color.p_inf(p_pref, unpack(arg)) end
local p_log = function (...) p_color.p_log(p_pref, unpack(arg)) end

-- Declaring the global variable for the listening socket (so)
-- and the client socket (sc)
local so
local sc

-- =================================================
-- init
-- =================================================
-- @brief   Start listening for connections with an Unix
--          socket and initialize libraries
-- @retval  none
----------------------------------------------------
local function init()
    -- Connect to socket
    nixio.fs.unlink(wl.SK_ALP_LUCI_PATH)
    so = nixio.socket("unix", "stream")
    so:bind(wl.SK_ALP_LUCI_PATH)
    so:listen(wl.SK_LISTEN_QUEUE_SIZE)

    ac_mt.init()
    if ac_dp then
        ac_dp.init()
    end
    ac_cup.init()
end

-- =================================================
-- close_all
-- =================================================
-- @brief   Close all sockets and libraries and then
--          raise an error
-- @param   err_msg     The error message to raise
-- @retval  none
----------------------------------------------------
local function close_all(err_msg)
    ac_mt.close()
    if ac_dp then
        ac_dp.close()
    end
    sc:close()
    so:close()
    error(err_msg)
end

-- =================================================
-- main
-- =================================================
-- @brief   The main entry of the program.
--          The program listen for clients to connect and
--          send commands. While it is waiting it does not
--          consume CPU time.
--          Once it is connected, it expects an object
--          serialized by util.serialize_data with at least
--          the field .__cmd and other fields depending on
--          the command.
--
--          The commands can be:
--          'DISP_UPDATE':  Update the display if available
--                          @see at_dp.set for additional parameters
--
--          'ALP+WF':       ALP write to file cmd
--                          @see at_mt.wf for additional parameters
--
--          'ALP+WFF':      ALP write flush to file cmd
--                          @see at_mt.wff for additional parameters
--
--          'ALP+RF':       ALP read from file cmd
--                          @see at_mt.rf for additional parameters
--
--          'CUP':          Code upgrade command
--                          @see at_cup.cup for additional parameters
-- @retval  none
----------------------------------------------------
local function main()
    p_log("Start")

    -- Do not execute if this program is already running
    if not wl.one_prog_running(arg[0]) then
        p_err("This program is already running")
        return
    end

    -- Initialize sockets and libraries
    init()

    -- Start main loop
    while true do

        -- Wait a client to connect
        sc = so:accept()
        p_dbg("CONNECTION ACCEPTED")

        -- If we are connected
        if sc then

            -- Initialize local variables
            local str_obj = ""
            local stat, obj = nil

            -- Stay in the loop If we don't have a decoded obj yet and the timeout has not expired
            while not obj do

                -- Read bytes from socket
                local readed = sc:read(wl.SK_READ_LEN)

                -- If we read something
                if readed then

                    -- Check the connection
                    if #readed == 0 then
                        p_err("Connection closed by client")
                        sc:close()
                        break
                    end

                    -- Concatenate the bytes we just read in the previous
                    -- bytes that we read
                    str_obj = str_obj .. readed

                    p_inf("GOT", str_obj)

                    local resp = nil

                    -- We are supposed to receive a serialized object (function util.serialize_data)
                    -- Unserialize object so we can read its fields
                    stat, obj = pcall(util.restore_data, str_obj)

                    -- If the unserialization was ok
                    if stat and obj then

                        -- Declare the ret variable, where ret[1] will indicate error if false or nil
                        -- and ret[2] is the error message. If ret[1] is not error, then the response
                        -- is OK and data is ret
                        local ret

                        -- Decode the command
                        -- If this is a disp update command
                        if obj.__cmd == 'DISP_UPDATE' and ac_dp then
                            local raw

                            if wl.DP_TYPE_WIRED then
                                local value

                                local d1 = string.sub(obj.disp_msg, 2, 2)


                                local d2 = string.sub(obj.disp_msg, 3, 3)

                                if d1 == ' ' or d2 == ' ' then
                                    value = 10000
                                else
                                    value = tonumber(d1)*10
                                    value = value + tonumber(d2)
                                end

                                --local file_data = wl.pp_data(wl.int_to_bytes(value, false, 2))
                                local file_data = wl.int_to_bytes(value, false, 2)

                                p_inf("Wired Display", file_data)
                                local alp_cmd, msg = alp.wf_build({file_id=0xc1, offset=4, fwd=0, resp=true, file_data=file_data})

                                if not alp_cmd then
                                    p_err("Error building alp command for display", msg)
                                else
                                    -- Encapsulate in Serial format
                                    local pkt = string.char(wl.SERIAL_SYNC0, wl.SERIAL_SYNC1, #alp_cmd, 0, wl.COM_FLOW.ALP_CMD) .. alp_cmd
                                    p_inf("Wired Display Cmd:", wl.data_to_hexstr(pkt))
                                    obj.disp_msg = pkt
                                    raw = true
                                end
                            end

                            ret = {pcall(ac_dp.set, obj, raw)}

                            -- Check for exceptions
                            -- If error, close all sockets before raising the error
                            local pcall_resp = table.remove(ret, 1)
                            if not pcall_resp then
                                close_all(ret[1])
                            end

                        -- If this is a write file command
                        elseif obj.__cmd == 'ALP+WF' then
                            ret = {pcall(ac_mt.wf, obj)}

                            -- Check for exceptions
                            -- If error, close all sockets before raising the error
                            local pcall_resp = table.remove(ret, 1)
                            if not pcall_resp then
                                close_all(ret[1])
                            end

                        -- If this is a write flush file command
                        elseif obj.__cmd == 'ALP+WFF' then
                            ret = {pcall(ac_mt.wff, obj)}

                            -- Check for exceptions
                            -- If error, close all sockets before raising the error
                            local pcall_resp = table.remove(ret, 1)
                            if not pcall_resp then
                                close_all(ret[1])
                            end

                        -- If this is a read file command
                        elseif obj.__cmd == 'ALP+RF' then
                            ret = {pcall(ac_mt.rf, obj)}

                            -- Check for exceptions
                            -- If error, close all sockets before raising the error
                            local pcall_resp = table.remove(ret, 1)
                            if not pcall_resp then
                                close_all(ret[1])
                            end

                        -- If this is a code upgrade command
                        elseif obj.__cmd == 'CUP' then
                            ret = {pcall(ac_cup.cup, obj)}

                            -- Check for exceptions
                            -- If error, close all sockets before raising the error
                            local pcall_resp = table.remove(ret, 1)
                            if not pcall_resp then
                                close_all(ret[1])
                            end
                        else
                            ret = {nil, "Cmd not found"}
                        end


                        -- ret[1] will indicate error if false or nil and ret[2] the error message.
                        -- If ret[1] is not error, then the response is OK and data is ret
                        if ret[1] then
                            resp = {status = "OK", data = ret}
                        else
                            resp = {status = "ERR", msg = ret[2]}
                        end

                        -- Serialize the resp object to send it to the client
                        resp = util.serialize_data(resp)
                        
                        p_dbg("CMD DONE", ret[1], ret[2], resp)

                        -- Sending RESP to client
                        p_dbg("WRITTEN", sc:writeall(resp), "OF", #resp)

                    end
                end
            end

            -- We finished with the client, close the client's socket and go listen for
            -- another connection
            sc:close()
        else
            p_err("Accept failed", sc)
        end

    end
end

-- Start the program
main()
