----------------------------------------
-- Dependencies
----------------------------------------
local util        = require "luci.util"
local http          = require "socket.http"
local protocol      = require "luci.http.protocol"
----------------------------------------

wl = {}

--------------------------------
-- Create metatable to set table
-- wl as a ready-only table
--------------------------------
local _wl = {}

local mt =
{
    __index = _wl,
    __newindex = function() error("Attempt to modify a read-only variable") end,
    __metatable = "protected metatable",
}

setmetatable(wl, mt)

-----------------------------------------
-- DEFINITIONS OF CONSTANT VALUES
-----------------------------------------

-----------------------
-- Serial sync and flow definitions
-----------------------
_wl.SERIAL_SYNC0 = 0X01
_wl.SERIAL_SYNC1 = 0X1F

_wl.FLOWID_TRC = 0x00
_wl.FLOWID_CMD = 0x10
_wl.FLOWID_ALP = 0x20
_wl.FLOWID_SYS = 0x30

------------------------------
-- COM FLOW DEFINITIONS
------------------------------
_wl.COM_FLOW = {
                    PRINTF            = _wl.FLOWID_TRC + 0,
                    PRINTF_COMPRESSED = _wl.FLOWID_TRC + 1,
                    PRINT_HEX         = _wl.FLOWID_TRC + 2,

                    ALP_CMD           = _wl.FLOWID_ALP + 0,
                    ALP_RESP          = _wl.FLOWID_ALP + 1,
                    ALP_UNS           = _wl.FLOWID_ALP + 2,

                    CMD               = _wl.FLOWID_CMD + 0,

                    SYS_RST           = _wl.FLOWID_SYS + 0,
                    SYS_BTN           = _wl.FLOWID_SYS + 1,
                    SYS_DBG           = _wl.FLOWID_SYS + 2,
                    SYS_CUP           = _wl.FLOWID_SYS + 3,
                }

-- Size of the uid in bytes
_wl.UID_SIZE   = 8
-- Size of the uid in hex string
_wl.S_UID_SIZE = wl.UID_SIZE*2

-----------------------
-- General files definitions
-----------------------
_wl.GENERAL_CONFIG_FILE_ID = 0x19
_wl.SLEEP_SCAN_FILE_ID     = 4
_wl.NET_SETTINGS_FILE_ID   = 0

-----------------------
-- General D7A definitions
-----------------------
_wl.D7A_CSMA_RIGD      = 0
_wl.D7A_CSMA_RAIND     = 1
_wl.D7A_CSMA_AIND      = 3
_wl.D7A_CSMA_NONE      = 4

-----------------------
-- CUP definitions
-----------------------
_wl.CUP_RETRIES        = 10
_wl.CUP_CODE_FILE_ID   = 0
_wl.CUP_CFG_FILE_ID    = 0
_wl.CUP_CODE_FILE_BLOCK= 0
_wl.CUP_CFG_FILE_BLOCK = 1
-- CUP CMD is in integer because its interprets 0xC0DEA700 as a negative number
_wl.CUP_CMD            = 3235817216 -- 0xC0DEA700
_wl.CUP_SRC_START      = 0x0800F200
_wl.CUP_CODE_DST_START = 0x08000000
_wl.CUP_FS_DST_START   = 0x08080000
_wl.CUP_CHUNK_SIZE     = 64
_wl.CUP_START_TO       = 10
_wl.CUP_END_TO         = 2*60
_wl.CUP_CHANNEL        = 0x9C
_wl.CUP_CODE_UPLOAD_FORM_NAME  = "cup_code_file"
_wl.CUP_FS_UPLOAD_FORM_NAME    = "cup_fs_file"
_wl.CUP_UPLOAD_FOLDER  = "/etc/dash7/cup_images/"
_wl.CUP_CODE_TMP_FILE  = "/tmp/cup_code"
_wl.CUP_FS_TMP_FILE    = "/tmp/cup_fs"
_wl.CUP_XFER_STATUS_FILE   = "/tmp/xfer"
_wl.CUP_XFER_STATUS_NO_XFER     = "NO_XFER"
_wl.CUP_XFER_STATUS_ON_GOING    = "ON_GOING"
_wl.CUP_XFER_STATUS_DONE_OK     = "DONE_OK"
_wl.CUP_XFER_STATUS_DONE_ERR    = "DONE_ERR"
_wl.CUP_XFER_PROGRESS_UPDATE_TIME    = 3
_wl.CUP_WORD_SIZE      = 4
_wl.CUP_FS_DEVICE_OFFSET   = 0
_wl.CUP_FS_CALIB_OFFSET    = 332
_wl.CUP_FS_TYPE_DEVICE     = 'D'
_wl.CUP_FS_TYPE_CALIB      = 'T'
_wl.CUP_FS_TYPE_CONFIG     = 'C'

-----------------------
-- Debug mode
-----------------------
_wl.DBG_MODE = true
_wl.SENSOR_STORE_DELAY = 2

-----------------------
-- Report definitions
-----------------------
_wl.REPORT_RETRY_DELAY             = 3
_wl.REPORT_FILE_ID                 = 0x1b
_wl.REPORT_COMPR_DBG_FILE_ID       = 0x1c
_wl.REPORT_NON_COMPR_DBG_FILE_ID   = 0x1e
_wl.REPORT_MOTE_VERSION_FILE_ID    = 0x1f
_wl.REPORT_RESTART_FILE_ID         = 0x20
_wl.REPORT_BATTERY_FILE_ID         = 0x21
_wl.REPORT_BTN1_FILE_ID            = 0xe0
_wl.REPORT_BTN2_FILE_ID            = 0xe1
_wl.REPORT_TEMP_FILE_ID            = 0xe2
_wl.REPORT_HUM_FILE_ID             = 0xe3
_wl.REPORT_PRE_FILE_ID             = 0xe4
_wl.REPORT_ADC_FILE_ID             = 0xe5
_wl.REPORT_ACC_FILE_ID             = 0xe6
_wl.RE_HTTP_REQ_TIMEOUT            = 60
_wl.RE_HTTP_RESP_TIMEOUT           = 60*20

-----------------------
-- Calibration definitions
-----------------------
_wl.CALIB_FILE = "/etc/dash7/calib.txt"
_wl.RST_FILE = "/etc/dash7/rst.txt"
_wl.CALIB_FILE_ID = 0x1a

-----------------------
-- Display definitions
-----------------------
_wl.DP_COM_PORT      = 2002
_wl.DP_CALCULATE_CRC = false
_wl.DP_PREPEND       = string.char(0x02)
_wl.DP_APPEND        = string.char(0x03)

-----------------------
-- Serial definitions
-----------------------
_wl.SR_COM_PORT      = 2001
_wl.SR_TS_PER        = 60

-----------------------
-- Display Polling config
-----------------------
_wl.DP_POLL_RETRY_PERIOD   =   3
_wl.DP_POLL_PERIOD         =   30*60
_wl.DP_LOCAL_SET_PER       =   30

-----------------------
-- Display Type:
-- false - Legacy
-- true  - Wired
-----------------------
_wl.DP_TYPE_WIRED          =   false

-----------------------
-- VPN definitions
-----------------------
_wl.VPN_IFACE               = "tun0"
_wl.VPN_KEEPALIVE_PERIOD    = 15*60

-----------------------
-- WatchDog GW definitions
-----------------------
--Minimum required uptime to perform a reboot
_wl.WD_MIN_UPTIME_TO_REBOOT = 30*60
--WatchDog timer for sensor_report process
_wl.WD_RE_ACT_TIMER         = 70*60
_wl.WD_RE_REBOOT_TIMER      = 140*60
--WatchDog timer for sensor_report post to server process
_wl.WD_POST_ACT_TIMER       = 15*60
_wl.WD_POST_REBOOT_TIMER    = 30*60
-- WatchDog timer for poll_display process
_wl.WD_PD_ACT_TIMER         = _wl.DP_POLL_PERIOD + 15*60
_wl.WD_PD_REBOOT_TIMER      = _wl.DP_POLL_PERIOD*2 + 15*60
-- WatchDog timer for set_display
_wl.WD_SD_ACT_TIMER         = _wl.DP_POLL_RETRY_PERIOD*5
_wl.WD_SD_REBOOT_TIMER      = _wl.WD_SD_ACT_TIMER*3
-- WatchDog timer for vpn_keepalive process
_wl.WD_VPN_ACT_TIMER        = _wl.VPN_KEEPALIVE_PERIOD + 5*60
_wl.WD_VPN_REBOOT_TIMER     = _wl.VPN_KEEPALIVE_PERIOD*2 + 5*60
-- Hardware watchdog timer to reboot
_wl.WD_HW_REBOOT_TIMER      = _wl.WD_PD_REBOOT_TIMER + 5*60

-----------------------
-- ALP Controller definitions
-----------------------
_wl.AC_RESP_DEFAULT_TIMEOUT = 15

-----------------------
-- Socket definitions
-----------------------
_wl.SK_ALP_UNS_PATH        = "/tmp/alp_uns.sk"
_wl.SK_ALP_CMD_PATH        = "/tmp/alp_cmd.sk"
_wl.SK_ALP_LUCI_PATH       = "/tmp/alp_luci.sk"
_wl.SK_SENSOR_DATA_PATH    = "/tmp/sensor.sk"
_wl.SK_READ_LEN            = 5000
_wl.SK_PORT_BASE           = 3000
_wl.SK_CONNECT_RETRY_N     = 2
_wl.SK_LISTEN_QUEUE_SIZE   = 100
_wl.SK_TRC_PORT            = 3000

-----------------------
-- Pipe definitions
-----------------------
_wl.PIPE_WD_RE_PATH          = "/tmp/wd_re.pipe"
_wl.PIPE_WD_PD_PATH          = "/tmp/wd_pd.pipe"
_wl.PIPE_WD_SD_PATH          = "/tmp/wd_sd.pipe"
_wl.PIPE_WD_VPN_PATH         = "/tmp/wd_vpn.pipe"
_wl.PIPE_WD_POST_PATH        = "/tmp/wd_post.pipe"
_wl.PIPE_WD_RST_DMONS_PATH   = "/tmp/wd_rst_dmons.pipe"

-----------------------
-- Configuration files
-----------------------
_wl.SR_SENSORS_FILE  = "/tmp/sensors.sr"
_wl.SR_SERVER_FILE   = "/etc/dash7/gw.sr"
_wl.SR_GW_FILE       = "/etc/dash7/gw_cfg.sr"
_wl.OPENVPN_CONF_FILE  = "/etc/openvpn/client.conf"

-----------------------
-- Log file
-----------------------
_wl.LOG_DIR = "/etc/dash7/log/"
_wl.ERR_LOG_FILE    = "err.log"
_wl.MOTE_LOG_FILE   = "mote.log"
_wl.DISP_LOG_FILE   = "disp.log"
_wl.LOG_MAX_SIZE    = 3000000 -- in bytes
--_wl.LOG_MAX_SIZE    = 100 -- in bytes

-----------------------
-- Max coroutines for http reports
-----------------------
_wl.RP_MAX_COROUTINES  =   25
_wl.RP_MEAN_FACTOR     =   0.001

------------------------------------
-- URIs AND DATA FORMATS DEFINITIONS
------------------------------------

-----------------------
-- Dst subnet set command
-----------------------
_wl.HTTP_URI_WF_SUBNET = "http://localhost/cgi-bin/luci/dash7/api/wf?file_id=0x19&offset=6&file_data=%s"
_wl.HTTP_URI_WFF_SUBNET = "http://localhost/cgi-bin/luci/dash7/api/wff?file_id=0x19&offset=6&file_data=%s"

-----------------------
-- Display Polling config
-----------------------
-- Format: protocol, gw_uid, gw_passwd, server
_wl.HTTP_URI_DP_POLL_FM = "%s%s:%s@%s/displays/poll"
-- Format: display id
_wl.HTTP_URI_DP_POST_FM = "http://localhost/cgi-bin/luci/dash7/api/display/%s"

-----------------------
-- Register gateway
-----------------------
-- Format: gateway loging, generated password
_wl.HTTP_BODY_REG_GW_FM   = [[{"login":"%s","password":"%s"}]]
-- Format: protocol, user, password, server
_wl.HTTP_URI_REG_GW_FM    = "%s%s:%s@%s/gateways/register"

-- Format: gateway loging, generated password
_wl.HTTP_BODY_REG_VPN_FM   = [[{"vpn_server":"%s"}]]
-- Format: protocol, user, password, server
_wl.HTTP_URI_REG_VPN_FM    = "%s%s:%s@%s/gateways/register_vpn"

-----------------------
-- Report sensor
-----------------------
_wl.HTTP_URI_REPORT_SENSOR_FM = "%s%s:%s@%s/sensors/report"

-----------------------------------------
-- DEFINITIONS OF COMMON FUNCTIONS
-- ATTENTION: as this functions are used in the LuCI server
-- they should not call the print function
-----------------------------------------

-- TODO: This is not a good place to put functions
-- They are in a read-only table

-- =================================================
-- wl.hrst_gateworks
-- =================================================
-- @brief   Hard Reboot the gateway
-- @retval  none
----------------------------------------------------
function _wl.hrst_gateworks()
    os.execute("/root/wl_scripts/gscreboot.sh")
end

-- =================================================
-- wl.rst_network
-- =================================================
-- @brief   Reset the network
-- @retval  none
----------------------------------------------------
function _wl.rst_network()
    os.execute("/etc/init.d/3g restart && /etc/init.d/network restart && /etc/init.d/openvpn restart")
end

-- =================================================
-- wl.rst_dmons
-- =================================================
-- @brief   Reset the dmons
--          XXX: Do not use this function or bash command outside the watchdog, otherwise
--               the processes will be killed and restarted, but the watchdog
--               will relaunch them again too, and we will have two instances
--               of the same program running at the same time
-- @retval  none
----------------------------------------------------
function _wl.rst_dmons()
    os.execute("/etc/init.d/gateworks wd_rst_dmons")
end

-- =================================================
-- wl.rst_uhttpd
-- =================================================
-- @brief   Reset the http server
-- @retval  none
----------------------------------------------------
function _wl.rst_uhttpd()
    os.execute("/etc/init.d/uhttpd restart")
end

-- =================================================
-- wl.get_file_data
-- =================================================
-- @brief   Read the contents of a file and interpret
--          is as a serialized lua object.
-- @file    The name of the file to be read
-- @retval  1) nil, msg: In case or error
--          2) The unserialized object
----------------------------------------------------
function _wl.get_file_data(file)
    -- Read configuration file to get the uid
    local fd, msg = io.open(file, "r")

    if not fd then
        return nil, "Could not open CFG file"
    end

    -- Read all the contents of the file
    local data = fd:read("*a")
    local stat
    -- Unserialize data
    stat, data = pcall(util.restore_data, data)
    -- We are done with the file, close it
    fd:close()
    -- Check if we could decode the data inside this file
    -- as a lua object
    if not stat or not data then
        return nil, "Could not restore CFG file"
    end

    return data
end

-- =================================================
-- wl.set_file_data
-- =================================================
-- @brief   Serialize the object data and write it in the file
--          ovewritting it.
-- @param   file    The name of the file to save the
--                  object
-- @param   data    The object to be saved
-- @retval  none
----------------------------------------------------
function _wl.set_file_data(file, data)
    local fd = io.open(file, "w+")

    if not fd then
        return nil
    end

    local serial = util.serialize_data(data)
    fd:write(serial)
    fd:close()

    return true
end

-- =================================================
-- get_server_config
-- =================================================
-- @brief   Return the server configuration object
--          stored in the server file
-- @retval  The unserialized server config object
----------------------------------------------------
function _wl.get_server_config()
    return _wl.get_file_data(wl.SR_SERVER_FILE)
end

-- =================================================
-- set_server_config
-- =================================================
-- @brief   Overwrite the current server object stored
--          in the server file
-- @retval  true if success
----------------------------------------------------
function _wl.set_server_config(data)
    return _wl.set_file_data(wl.SR_SERVER_FILE, data)
end

-- =================================================
-- get_gw_config
-- =================================================
-- @brief   Return the gateway configuration object
--          stored in the gateway file
-- @retval  The unserialized gateway config object
----------------------------------------------------
function _wl.get_gw_config()
    return _wl.get_file_data(wl.SR_GW_FILE)
end

-- =================================================
-- set_gw_config
-- =================================================
-- @brief   Overwrite the current gateway configuration
--          object stored in the gateway configuration file
-- @retval  true if success
----------------------------------------------------
function _wl.set_gw_config(data)
    return _wl.set_file_data(wl.SR_GW_FILE, data)
end

-- =================================================
-- wl.bytes_to_int
-- =================================================
-- @brief   Transform an array of bytes in int
-- @param   signed - default false
-- @param   endian - default little
-- @retval  The number
----------------------------------------------------
function _wl.bytes_to_int(str,signed,endian) -- use length of string to determine 8,16,32,64 bits
    local t={str:byte(1,-1)}
    if endian=="big" then --reverse bytes
        local tt={}
        for k=1,#t do
            tt[#t-k+1]=t[k]
        end
        t=tt
    end
    local n=0
    for k=1,#t do
        n=n+t[k]*2^((k-1)*8)
    end
    if signed then
        local nbits = #t*8
        n = (n > 2^(nbits-1) -1) and (n - 2^nbits) or n -- if last bit set, negative.
    end
    return n
end

-- =================================================
-- wl.int_to_bytes
-- =================================================
-- @brief   Transform an array of bytes in int
-- @param   nbytes - The how many to return
-- @param   endian - default little
-- @retval  The byte array
----------------------------------------------------
function _wl.int_to_bytes(num,signed, nbytes, endian)
    if num<0 and not signed then num=-num end

    local res={}
    local n = math.ceil(select(2,math.frexp(num))/8) -- number of bytes to be used.
    n = n > nbytes and n or nbytes
    if signed and num < 0 then
        num = num + 2^n
    end
    for k=n,1,-1 do -- 256 = 2^8 bits per char.
        local mul=2^(8*(k-1))
        res[k]=math.floor(num/mul)
        num=num-res[k]*mul
    end
    assert(num==0)

    if endian == "big" then
        local t={}
        for k=1,n do
            t[k]=res[n-k+1]
        end
        res=t
    end

    return string.sub(string.char(unpack(res)), 1, nbytes)
end

-- =================================================
-- wl.get_byte
-- =================================================
-- @brief   Get the number at the position idx in the
--          str
-- @param   str     The string of bytes to retrieve the
--                  number
-- @param   idx     The index to retrieve the byte in the
--                  str
-- @param   signed  - default false, if the byte is signed
--                  or not
-- @retval  none
----------------------------------------------------
function _wl.get_byte(str, idx, signed)
    return _wl.bytes_to_int(string.sub(str, idx, idx), signed)
end

-- =================================================
-- wl.data_to_hexstr
-- =================================================
-- @brief   Transform an array of byte in a hex string
--          in the format 0005ABF087..., used to compose
--          the UID in string
-- @retval  The hex string
----------------------------------------------------
function _wl.data_to_hexstr(data)
    return (data:gsub('.', function (b)
                                return string.format('%02X', string.byte(b))
                           end))
end

-- =================================================
-- wl.hexstr_to_data
-- =================================================
-- @brief   Transform a hex string in the format
--          0005ABF087... in an array of bytes
--          Used to set a destination UID in the mote
-- @retval  The hex string
----------------------------------------------------
function _wl.hexstr_to_data(data)
    local status, data = pcall(string.gsub, data, '..', function(n) return string.char(tonumber(n,16)) end )
    return status and data
end

function _wl.pp_data(bin)
    -- Address in a readable format
    local t = {}
    for b in string.gfind(bin, ".") do
        table.insert(t, string.format("0x%02X,", string.byte(b)))
    end
    local pp = table.concat(t)

    return pp
end

-- =================================================
-- get_version
-- =================================================
-- @brief   Transform contents of the version file
--          into an integer in reverse order. Example:
--          In the version file: 0x01,0x21,0x05,0x0E
--          Return: 14052101
-- @retval  The version number (integer)
----------------------------------------------------
function _wl.get_version(file_chunk, first, last)
    -- TODO: this is not optimal
    local ver = ""

    for i=last,first,-1 do
        local num = wl.get_byte(file_chunk, i)
        ver = ver .. string.format('%02d', num)
    end

    return tonumber(ver)
end

-- =================================================
-- wl.insert_thread
-- =================================================
-- @brief   Insert a thread to be scheduler by the
--          wl.dispatcher (if used)
-- @param   thread      The thread to be inserted
-- @retval  none
----------------------------------------------------
function _wl.insert_thread(thread)
    _wl.threads = _wl.threads or {}    -- list of all live threads
    table.insert(_wl.threads, thread)
end

-- =================================================
-- wl.dispatcher
-- =================================================
-- @brief   Start the scheduler for the threads
--          inserted with the wl.insert_thread function.
--          This scheduler just get the threads in the
--          list the schedule the next one
-- @retval  none
----------------------------------------------------
function _wl.dispatcher ()
    while true do
        local n = table.getn(_wl.threads)
        if n == 0 then break end   -- no more threads to run
        for i,thread in ipairs(_wl.threads) do
            local status,msg = coroutine.resume(thread)
            -- Removing dead threads
            if not status then
                error(msg)
                table.remove(_wl.threads, i)
            end
        end
    end
end

-- =================================================
-- wl.read_or_yield
-- =================================================
-- @brief   Read len byte from the socket sk, if there
--          is no data available in the socket, or if
--          we didn't complete reading len bytes, yield.
-- @param   sk: The socket to read
-- @param   len: How many sockets to read
-- @retval  The array of bytes (the data read from the
--          socket of size len)
----------------------------------------------------
function _wl.read_or_yield(sk, len)

    -- Get how many bytes we should read
    local missing = len

    local pkt = ""

    while missing > 0 do

        -- try read the whole pkt
        local ret = {sk:read(missing)}

        -- If there is nothing to read then yield
        if ret[1] == false then
            coroutine.yield()

        -- If error
        elseif ret[1] == nil then
            return unpack(ret)

        -- If the connection is lost
        elseif #ret[1] == 0 then
            return nil, tostring(sk), "Connection is lost"

        -- If ok
        else
            missing = missing - #ret[1]
            pkt = pkt .. ret[1]
        end

    end

    return pkt
end

-- =================================================
-- wl.log_init
-- =================================================
-- @brief   Initialize the log_file
-- @param   log_file    The name of the file to log
-- @param   obj         (Optional) Extra options to be used
--                      instead of the default options
--                      .log_fd     (The log file descriptor)
--                      .log_file   (The main log file name)
--                      .log_file_bkp (The back up log file name)
-- @retval  The log object with log informations
--          to be used in wl.log function
----------------------------------------------------
function _wl.log_init(log_file, obj)
    local obj = obj or {}
    -- If a log is already open, close it
    if obj.log_fd then
        obj.log_fd:close()
    end

    -- Build the log file names (the main one and the back up)
    obj.log_file = obj.log_file or _wl.LOG_DIR .. log_file
    obj.log_file_bkp = obj.log_file_bkp or _wl.LOG_DIR .. log_file .. ".bkp"

    -- Create the file if it does not exist
    local exec = "mkdir -p " .. _wl.LOG_DIR .. " && touch " .. obj.log_file
    os.execute(exec)

    -- Open the log file
    obj.log_fd, msg = io.open(obj.log_file, "a+")
    if not obj.log_fd then
        return nil, "Log non initialized"
    end

    return obj
end

-- =================================================
-- fsize
-- =================================================
-- @brief   Return the size of a file
-- @param   The file descriptor to read the size
-- @retval  The file size
----------------------------------------------------
local function fsize (file)
    local current = file:seek()      -- get current position
    local size = file:seek("end")    -- get file size
    file:seek("set", current)        -- restore position
    return size
end

-- =================================================
-- wl.log
-- =================================================
-- @brief   Write a binary array into the log file
--          previously opened by wl.log_init
--          If the log file size is above wl.LOG_MAX_SIZE
--          copy it to the back up file and open a new one
-- @param   obj         The object returned by wl.log_init
-- @param   bin_message The binary array append in the
--                      log file
-- @retval  none
----------------------------------------------------
function _wl.log (obj, bin_message)
    if not obj.log_file or not obj.log_file_bkp or not obj.log_fd then
        return nil, "Log non initialized"
    end
    -- If we reached the max log
    -- rename it to the bkp file and start a new log file
    if fsize(obj.log_fd) > _wl.LOG_MAX_SIZE then

        -- Check if the renaming was not already done by another process
        -- and we just need to reopen the file

        -- XXX we can still have problems if two process moves the file at
        -- the same time, erasing the back up and loosing log, but
        -- the probability that both do this at the same time is low

        -- Re open the file
        _wl.log_init(nil, obj)

        -- Re check the size
        if fsize(obj.log_fd) > _wl.LOG_MAX_SIZE then
            -- Rename it
            os.execute("mv " .. obj.log_file .. " " .. obj.log_file_bkp)
            -- Reopen the file
            _wl.log_init(nil, obj)
         end
    end

    if not bin_message then return end

    -- Write the log message in the file
    local ret = obj.log_fd:write(bin_message or "")

    obj.log_fd:flush()
end

-- =================================================
-- wd_connect
-- =================================================
-- @brief   Open a pipe in write only mode and return
--          its file descriptor
-- @param       pipe_path   The pipe to be opened
-- @err_func    err_func    The print function to be
--                          called when an error occurs
--                          or nil
-- @retval  The file descriptor returned by nixio.open
-- @note    Blocks until the file is opened
----------------------------------------------------
function _wl.wd_connect(pipe_path, err_func)
    local wd

    -- Open the watch dog pipe
    repeat
        wd = nixio.open(pipe_path, nixio.open_flags("wronly"))
        if not wd then
            if err_func then
                err_func("Fail to open watch dog pipe, retrying in 1 sec")
            end
            nixio.nanosleep(1)
        end
    until wd

    return wd
end

-- =================================================
-- exec_bash
-- =================================================
-- @brief   Execute a bash command and return its answer
-- @retval  none
----------------------------------------------------
function _wl.exec_bash(cmd)
    local handler = io.popen(cmd .. " 2>&1")

    local resp = handler:read("*a")

    handler:close()

    return resp
end

-- =================================================
-- one_prog_running
-- =================================================
-- @brief   Return false if there are more then one of
--          the same prog running
-- @retval  none
----------------------------------------------------
function _wl.one_prog_running(prog)
    local is_path = string.find(prog, "/")

    -- If we received a path, get just the name of the program
    if is_path then
        _, _, prog = string.find(prog, ".*/(.*)")
    end

    local resp = _wl.exec_bash("pidof " .. prog)

    local _,_, pid1, pid2 = string.find(resp, "(%d*) (%d*)")

    --if pid1 and pid2 then
    --    return false
    --end

    return true
end

-- =================================================
-- wl.mote_off
-- =================================================
-- @brief   Force the mote off
-- @retval  none
----------------------------------------------------
function _wl.mote_off()
    os.execute("/root/wl_scripts/mote_off.sh")
end

-- =================================================
-- wl.mote_on
-- =================================================
-- @brief   Force the mote on
-- @retval  none
----------------------------------------------------
function _wl.mote_on()
    os.execute("/root/wl_scripts/mote_on.sh")
end

-- =================================================
-- wl.internal_report
-- =================================================
-- @brief   Send the object to the sensor_data process
--          to save the sensor information in a file
--          provided for LuCI internal page
-- @retval  none
----------------------------------------------------
function _wl.internal_report(obj)
    local so = nixio.socket("unix", "stream")
    local resp

    -- Connecting socket
    if so:connect(wl.SK_SENSOR_DATA_PATH) then

        -- Setting command
        obj.__cmd = "SET_OBJ"

        -- Sending the object
        so:write(util.serialize_data(obj))

        -- Removing the command from the object
        obj.__cmd = nil

        -- Get response
        resp = so:readall(wl.SK_READ_LEN)

        -- Done
        so:close()

        -- Unserialize the response
        resp = resp and util.restore_data(resp)

    end

    return resp
end

-----------------------
-- Return the library
-----------------------
return wl
