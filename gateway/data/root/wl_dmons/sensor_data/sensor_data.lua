#!/usr/bin/lua

-- Setting path to look for libraries
-- XXX: this is not a good place, we won't be able to move the code to another file
package.path = (package.path or "") .. ";/root/wl_dmons/?.lua"

----------------------------------------
-- Dependencies
----------------------------------------
local p_color       = require "p_color"
local nixio         = require "nixio"
local nixio_util    = require "nixio.util"
local wl            = require "wl"
local util          = require "luci.util"
local socket        = require "socket"
local json          = require "luci.json"
local app           = require "app"
----------------------------------------

----------------------------------------------------
-- Print functions
-- Set an empty function to disable a print
----------------------------------------------------
local p_pref = "SD:"
local p_err = function (...) p_color.p_err(p_pref, unpack(arg)) end
local p_dbg = function (...) p_color.p_dbg(p_pref, unpack(arg)) end
-- local p_dbg = function (...) end
local p_inf = function (...) p_color.p_inf(p_pref, unpack(arg)) end
local p_log = function (...) p_color.p_log(p_pref, unpack(arg)) end

----------------------------------------------------
-- Private definitions
----------------------------------------------------
local so

-- Main data
local wisense_obj = {ts=0, data={}}

----------------------------------------------------
-- FUNCTIONS
----------------------------------------------------

-- =================================================
-- update_ts
-- =================================================
-- @brief   Increase the timestamp of the file to
--          indicate that the file has being changed.
--          Even this functions is called in the same
--          second, we increase the time by 0.1
-- @retval  none
----------------------------------------------------
local function update_ts()
    local now = socket.gettime()*1000

    -- Ensure that now is diferent of wisense_obj
    if now == wisense_obj then
        now = wisense_obj + 0.1
    end

    wisense_obj.ts = now
end

-- =================================================
-- main
-- =================================================
-- @brief   The main entry.
--          Wait for connection on a socket and receive commands.
--          The program expect to receive an object with a .__cmd
--          field (that won't be save into the file)
--
--          'SET_OBJ': Create or update an object on the data base,
--                      the object must have at least the .uid field.
--                      nil fields won't be errased from the current
--                      record (if it exists) of the object. Non nil
--                      fields will replace the field in the current
--                      record of the object.
--
--          'SET_LIST': Erase all the current data base and
--                      set the new one to be the received object
-- @retval  none
----------------------------------------------------
local function main()
    -- Check if the internal report is enabled
    -- or not. End the program if it is set
    -- to be disabled
    if not app.INTERNAL_REPORT_ENABLED then
        p_log("Internal report is not active")
        return
    end
    p_log("Start")

    -- Connect to socket
    nixio.fs.unlink(wl.SK_SENSOR_DATA_PATH)
    so = nixio.socket("unix", "stream")
    so:bind(wl.SK_SENSOR_DATA_PATH)
    so:listen(wl.SK_LISTEN_QUEUE_SIZE)

    local section_start_time

    while true do
        p_dbg("WAITING FOR CONNECTION")
        local sc = so:accept()
        p_dbg("CONNECTION ACCEPTED")

        if sc then

            -- Initialize local variables
            local str_obj = ""
            local stat, obj = nil

            -- If I don't have a decoded obj yet and the timeout has not passed
            while not obj do

                -- Read data from the socket
                local readed = sc:read(wl.SK_READ_LEN)

                -- If we read something
                if readed then
                    if #readed == 0 then
                        p_err("Connection closed by client")
                        sc:close()
                        break
                    end

                    -- Concatenate the data with the previous read data
                    str_obj = str_obj .. readed

                    p_inf("GOT", str_obj)

                    local resp = nil

                    -- Try to unserialize the data as an object
                    -- If we can not unserialized it, it means
                    -- that we didn't read the entire packet
                    stat, obj = pcall(util.restore_data, str_obj)

                    -- If the serialization was ok
                    if stat and obj then
                        local ret

                        -- If this is an update of an object
                        if obj.__cmd == 'SET_OBJ' then
                            -- Erase the cmd from the serialized obj
                            -- so we won't write this field into the
                            -- file
                            obj.__cmd = nil

                            -- Create or update the wisense object
                            if not wisense_obj.data[obj.uid] then
                                wisense_obj.data[obj.uid] = obj
                            else
                                for k,v in pairs(obj) do
                                    wisense_obj.data[obj.uid][k] = v
                                end
                            end

                            -- Update the date in the file
                            update_ts()

                            -- Write to file
                            -- FIXME: This is a tmp code, LuCI should call GET_LIST
                            -- instead of reading the data from a file
                            wl.set_file_data(wl.SR_SENSORS_FILE, wisense_obj)

                            -- Set the response to OK
                            resp = {status = "OK"}

                        -- If we need to replace the current list of
                        -- objects (this is used by LuCI to erase the
                        -- current list of objects)
                        elseif obj.__cmd == 'SET_LIST' then
                            -- Erase the cmd from the serialized obj
                            obj.__cmd = nil
                            -- Set the entire list
                            wisense_obj = obj

                            -- Write to file
                            -- XXX This is a tmp code, LuCI should call GET_LIST
                            wl.set_file_data(wl.SR_SENSORS_FILE, wisense_obj)

                            -- Set the response to OK
                            resp = {status = "OK"}

                        elseif obj.__cmd == 'GET_LIST' then
                            -- return the wisense_obj
                            resp = wisense_obj
                        else
                            p_err(tostring(obj.__cmd), "Cmd not found")
                            resp = {status = "ERR"}
                        end

                        -- Serialize the response object
                        resp = util.serialize_data(resp)

                        p_dbg("CMD DONE", resp)

                        -- Sending RESP to client
                        p_dbg("WRITTEN", sc:writeall(resp), "OF", #resp)

                    end
                end
            end

            -- We are done with this command, close the socket with the client
            sc:close()
        else
            p_err("Accept failed", sc)
        end

    end
end

-- Return the public object
main()
