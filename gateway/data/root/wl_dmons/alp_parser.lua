-- Setting path to look for libraries
-- XXX: this is not a good place, we won't be able to move the code to another file
package.path = (package.path or "") .. ";/root/wl_dmons/?.lua"

----------------------------------------
-- Dependencies
----------------------------------------
local p_color   = require "p_color"
local nixio     = require "nixio"
local wl        = require "wl"
local bit       = require "bit"
local util      = require "luci.util"
----------------------------------------

----------------------------------------------------
-- Print functions
-- Set a empty function to disable a print
----------------------------------------------------
local p_pref = "ALP:"
local p_err = function (...) p_color.p_err(p_pref, unpack(arg)) end
--local p_dbg = function (...) p_color.p_dbg(p_pref, unpack(arg)) end
local p_dbg = function (...) end
local p_inf = function (...) p_color.p_inf(p_pref, unpack(arg)) end
local p_log = function (...) p_color.p_log(p_pref, unpack(arg)) end


----------------------------------------------------
-- Private ALP definitions
----------------------------------------------------

-- header size of the returned template (file_id, offset, length)
local RETURN_TPL_HEADER_SIZE    = 3
-- header size of the unsolicited template (lb, lqi, uid)
local ALP_UNS_HEADER_SIZE = 10

-- broad cast address
local ALP_BROADCAST_ADDR = "FFFFFFFFFFFFFFFF"

----------------------------------------------------
-- Global object
-- To be returned by require function
----------------------------------------------------
alp_parser = {}

----------------------------------------------------
-- Public ALP definitions
----------------------------------------------------

-- header size of the record structure (flags, len, id, cmd)
alp_parser.HEADER_LEN = 4

-- header size of the file template (file_id, offset, file_len)
alp_parser.FILE_TPL_HEADER_LEN = 5

-- ALP flags (end, beginning, chunked)
alp_parser.flag = {
                    ME = 0x40,
                    MB = 0x80,
                    CC = 0x20,
                }

-- ALP command base id (null, file, query protocol)
alp_parser.bid = {
                    NULL = 0,
                    FILE_DATA = 1,
                    QP = 2,
                }

-- ALP null operands
alp_parser.null_op = {
                    ACK         = 2,
                    TX_FAILED   = 5,
                    NACK        = 6,
                    BOOT        = 7,
                }

-- ALP file operands
alp_parser.op = {
                    READ_PERM   = 0,
                    RETURN_PERM = 1,

                    WRITE_PERM  = 3,
                    READ_DATA   = 4,
                    RETURN_DATA = 5,

                    WRITE_FLUSH_DATA  = 6,

                    WRITE_DATA  = 7,
                    READ_HEADER = 8,
                    RETURN_HEADER   = 9,
                    DELETE_FILE     = 10,
                    CREATE_FILE     = 11,
                    READ_DATA_N_HEADER      = 12,
                    RETURN_DATA_N_HEADER    = 13,
                    RESTORE_FILE    = 14,
                    RETURN_ERR      = 15,
                }

-- ALP query protocol operands
alp_parser.qp_cmd = {
                    INVENTORY_F      = 0,
                    INVENRORY_FS     = 1,
                    ANNOUNCEMENT_F   = 2,
                    ANNOUNCEMENT_FS  = 3,
                    COLLECTION_F_F   = 4,
                    COLLECTION_FS_FS = 5,
                    COLLECTION_F_FS  = 6,
                    COLLECTION_FS_F  = 7,

                    UNS              = 9,

                    ACK              = 10,

                    }

-- ALP response on or off
alp_parser.resp = {
                    ON = 0x80,
                    OFF = 0,
                }

-- ALP file blocks
alp_parser.file_block =  {
                            GFB      = 1,
                            ISFSB    = 2,
                            ISFB     = 3,
                        }

------------------------------
-- PRIVATE FUNCTIONS
------------------------------

-- =================================================
-- rec_pkt_build
-- =================================================
-- @brief   Build the record structure (as a binary string)
--          according with the high level parameters in the o
--          object
-- @param   o: The object containing the parameters for building the structure
--              o = {
--               .fwd        (optional, default 0)
--               .bid        (optional if id, otherwise is mandatory)
--               .id         (optional if bid, otherwise is mandatory)
--               .file_block (optional if cmd, default ISFB)
--               .resp       (optional if cmd, default false)
--               .op         (optional if cmd, otherwise is mandatory)
--               .cmd        (optional if op, otherwise is mandatory)
--               .flags      (optional, default MB AND ME)
--               .pload      (optional)
--              }
--          NOTE: .id has priority over .fwd and .bid since the field
--          id is composed by both.
--          NOTE: .cmd has priority over .file_block, .op and .resp
--          since the field cmd is composed by them.
-- @retval  1)  The built record structure as a string of bytes
--          2)  nil, msg: if error
----------------------------------------------------
local function rec_pkt_build (o)
    -- Auxiliar variable to construct the packet
    local rec = {}

    -- Fill the optional parameters and check of errors

    o.fwd = o.fwd or 0

    if not o.id and not o.bid then
        return nil, "Missing one parameter between id and bid"
    end

    o.id = o.id or (bit.lshift(o.fwd, 6) + bit.lshift(o.fwd, 4) + o.bid)

    o.file_block = o.file_block or alp_parser.file_block.ISFB

    o.resp = o.resp and alp_parser.resp.ON or alp_parser.resp.OFF

    if not o.cmd and not o.op then
        return nil, "Missing one parameter between cmd and op"
    end

    o.cmd = o.cmd or o.resp + bit.lshift(o.file_block, 4) + o.op

    o.flags = o.flags or alp_parser.flag.MB + alp_parser.flag.ME

    local len = string.len(o.pload or "") + alp_parser.HEADER_LEN

    -- Build the pkt header and body
    rec.header = string.char(o.flags, len, o.id, o.cmd)
    rec.body = o.pload

    p_dbg("REC NEW", "FLAGS", o.flags, "LEN", len, "ID", o.id, "CMD" ,o.cmd, "MSG", o.pload)

    -- Return the concatenation of the pkt header and body
    return rec.header .. (rec.body or "")
end

-- =================================================
-- file_data_tpl_build
-- =================================================
-- @brief   Build the file data template as a string of bytes
-- @param   file_id:    the file id in the file_data command
-- @param   offset:     the file offset in the file data command
-- @param   len:        the file len
-- @param   data:       the data
-- @retval  The file data template
----------------------------------------------------
local function file_data_tpl_build(file_id, offset, len, data)
    local t = {}
    table.insert(t, string.char(file_id))
    table.insert(t, string.char(bit.band(offset, 0xff)))
    table.insert(t, string.char(bit.rshift(offset, 8)))
    table.insert(t, string.char(bit.band(len, 0xff)))
    table.insert(t, string.char(bit.rshift(len, 8)))

    p_dbg("ACCESS TPL", "FID", file_id, "OFF", offset, "LEN", len)

    table.insert(t, data)
    return table.concat(t)
end

-- =================================================
-- file_cmd_build
-- =================================================
-- @brief   Build alp command that contains a file data
--          template
-- @param   o: The object describing the command to be built
--              o = {
--               .offset     (optional, default 0)
--               .file_data  (optional)
--               .resp       (optional, default true)
--               .file_id    (mandatory)
--               .file_block (optional if cmd, default ISFB)
--               .file_len   (mandatory)
--               .op         (mandatory)
--               .fwd        (optional, default 0)
--              }
-- @retval  The command as a string of bytes
----------------------------------------------------
local function file_cmd_build(o)

    -- Fill the optional parameters and check of errors

    o.offset = o.offset or 0

    o.file_data = o.file_data or ""

    -- If resp is nil (not false), set to true
    o.resp = (o.resp == false and nil) or true

    if not o.file_id then
        return nil, "Missing file id"
    end

    if not o.file_len then
        return nil, "Missing file len"
    end

    -- Build the file template
    local file_tpl = file_data_tpl_build(o.file_id, o.offset,
                                    o.file_len, o.file_data)

    -- Build record structure
    local cmd = rec_pkt_build({bid          = alp_parser.bid.FILE_DATA,
                               fwd          = o.fwd,
                               op           = o.op,
                               pload        = file_tpl,
                               file_block   = o.file_block,
                               resp         = o.resp})

    return cmd
end


-- =================================================
-- return_tpl_parse
-- =================================================
-- @brief   Parse a return template (that is inside an
--          unsolicited packet) and add the parsed fields
--          in the uns_obj parameter it self
-- @param   uns_obj: The pre-parsed unsolicited object created
--              by the uns_parse function with at least
--              the .body field
-- @retval  1)  nil, msg: if error
--          2)  the uns_obj with the additional fields:
--                  .file_id
--                  .offset
--                  .file_len
--                  .file_chunk
----------------------------------------------------
local function return_tpl_parse(uns_obj)
    if not uns_obj.body or string.len(uns_obj.body) < RETURN_TPL_HEADER_SIZE then
        p_err("RET TPL BAD PARAM")
        return nil, "RET TPL BAD PARAM"
    end

    uns_obj.file_id     = wl.get_byte(uns_obj.body, 1)
    uns_obj.offset      = wl.get_byte(uns_obj.body, 2)
    uns_obj.file_len    = wl.get_byte(uns_obj.body, 3)
    uns_obj.file_chunk  = string.sub(uns_obj.body, RETURN_TPL_HEADER_SIZE+1, -1)

    p_dbg("RET", uns_obj.file_id, uns_obj.file_len, uns_obj.offset, string.len(uns_obj.file_chunk))

    if uns_obj.file_len - uns_obj.offset ~= string.len(uns_obj.file_chunk) then
        local msg = "RCV BAD RETURN TPL " .. tostring(uns_obj.file_len - uns_obj.offset) .. " " .. tostring(string.len(uns_obj.file_chunk))
        p_err(msg)
        return nil, msg
    end

    p_dbg("RET TPL")
    return uns_obj
end

-- =================================================
-- uns_parse
-- =================================================
-- @brief   Parse the body of a record structure as
--          an unsolicited message
-- @param   rec: The pre-parsed rec structure created
--              by the rec_parse function
-- @retval  1) nil,msg: if error
--          2) the rec structure with the additional fields:
--              .uns_tpl = {
--                 .lb
--                 .lqi
--                 .uid
--                 .body
--                 .file_id
--                 .offset
--                 .file_len
--                 .file_chunk
--              }
----------------------------------------------------
local function uns_parse(rec)
    rec.uns_tpl = {}

    if string.len(rec.body or "") < ALP_UNS_HEADER_SIZE then
        local msg = "UNS msg too small"
        p_err(msg)
        return nil, msg
    end

    p_dbg("UNS PARSE")

    -- Saving src uid
    rec.uns_tpl.lb  = wl.get_byte(rec.body, 1)
    rec.uns_tpl.lqi = wl.get_byte(rec.body, 2)
    rec.uns_tpl.uid = string.sub(rec.body, 3, ALP_UNS_HEADER_SIZE)

    -- message without header
    rec.uns_tpl.body = tostring(string.sub(rec.body, ALP_UNS_HEADER_SIZE + 1, -1))

    -- Address in a readable format
    rec.uns_tpl.uid = wl.data_to_hexstr(rec.uns_tpl.uid)

    -- Parse the return template in the body field
    local ok, msg = return_tpl_parse(rec.uns_tpl)

    -- Check for errors
    if not ok then
        return nil, msg
    end

    return rec
end

-- =================================================
-- resp_file_data_parse
-- =================================================
-- @brief   Parse the body of a record structure that
--          contains a return data template
-- @retval  1) nil, msg: if error
--          2) the record structure with additional fields:
--              .data_tpl = {
--                  .file_id
--                  .offset
--                  .file_len
--                  .file_data
--              }
----------------------------------------------------
local function resp_file_data_parse(rec)
    rec.data_tpl = {}
    rec.data_tpl.file_id    = wl.get_byte(rec.body, 1)
    rec.data_tpl.offset     = wl.bytes_to_int(string.sub(rec.body, 2, 3))
    rec.data_tpl.file_len   = wl.bytes_to_int(string.sub(rec.body, 4, 5))
    rec.data_tpl.file_data = string.sub(rec.body, alp_parser.FILE_TPL_HEADER_LEN+1, -1)

    p_dbg("FDATA", wl.data_to_hexstr(rec.data_tpl.file_data))

    if rec.data_tpl.file_len ~= string.len(rec.data_tpl.file_data) then
        local msg = "File len differs of file data len"
        p_err(msg)
        return nil, msg
    end

    return rec
end

-- =================================================
-- resp_error_parse
-- =================================================
-- @brief   Parse the body of a record structure that
--          contains an error template
-- @retval  1) nil, msg: if error
--          2) the record structure with additional fields:
--              .error_tpl = {
--                  .file_id
--                  .error_code
--              }
----------------------------------------------------
local function resp_error_parse(rec)
    rec.error_tpl = {}
    rec.error_tpl.file_id = wl.get_byte(rec.body, 1)
    rec.error_tpl.error_code = wl.get_byte(rec.body, 2)
    return rec
end

-- =================================================
-- rec_parse
-- =================================================
-- @brief   Parse a binary data with an alp record
--          structure in a rec object
-- @param   pkt The binary packet to be parsed
-- @retval  1) nil, msg: in case of error
--          2) A object with the following structure
--              rec = {
--                  .header
--                  .body
--                  .flags
--                  .len
--                  .id
--                  .cmd
--              }
----------------------------------------------------
local function rec_parse(pkt)
    local rec = {}

    rec.header = string.sub(pkt, 1, alp_parser.HEADER_LEN)
    rec.body   = string.sub(pkt, alp_parser.HEADER_LEN+1, -1)

    -- more details
    rec.flags   = wl.get_byte(rec.header, 1)
    rec.len     = wl.get_byte(rec.header, 2)
    rec.id      = wl.get_byte(rec.header, 3)
    rec.cmd     = wl.get_byte(rec.header, 4)

    p_dbg("RX REC", "FLAGS", rec.flags, "LEN", rec.len, "ID", rec.id, "CMD", rec.cmd)

    if rec.len ~= string.len(rec.body) + alp_parser.HEADER_LEN then
        p_err("Rec len field does not math", rec.len, string.len(rec.body))
        return nil, "Rec len field does not math"
    end

    return rec
end


----------------------------------------------------
-- PUBLIC FUNCTIONS
----------------------------------------------------

-- =================================================
-- alp_parse.wf_build
-- =================================================
-- @brief   Build the string of bytes to be send to
--          the mote to perform a write file command
-- @param   o: The object structure containing the
--          write file parameters:
--              o = {
--               .offset     (optional, default 0)
--               .file_data  (optional)
--               .resp       (optional, default true)
--               .file_id    (mandatory)
--               .file_block (optional if cmd, default ISFB)
--               .fwd        (optional, default 0)
--              }
-- @param   flush: set to true to perform a write flush
-- @retval  The binary array with the write file command
----------------------------------------------------
function alp_parser.wf_build (o, flush)
    p_inf("WF")
    p_dbg(luci.util.serialize_data(o))

    if not o.file_data then
        return nil, "Missing file content"
    end

    o.file_len = string.len(o.file_data)

    if flush then
        o.op = alp_parser.op.WRITE_FLUSH_DATA
    else
        o.op = alp_parser.op.WRITE_DATA
    end

    return file_cmd_build(o)
end

-- =================================================
-- alp_parse.rf_build
-- =================================================
-- @brief   Build the string of bytes to be send to
--          the mote to perform a read file command
-- @param   o: The object structure containing the
--          read file parameters:
--              o = {
--               .offset     (optional, default 0)
--               .resp       (optional, default true)
--               .file_id    (mandatory)
--               .file_block (optional if cmd, default ISFB)
--               .file_len   (mandatory)
--               .fwd        (optional, default 0)
--              }
-- @retval  The binary array with the read file command
----------------------------------------------------
function alp_parser.rf_build (o)
    p_inf("RF")
    p_dbg(luci.util.serialize_data(o))

    if not o.file_len then
        return nil, "Invalid file lenth"
    end

    o.op = alp_parser.op.READ_DATA

    return file_cmd_build(o)
end

-- =================================================
-- alp_parser.parse
-- =================================================
-- @brief   General parser. Parse a binary alp packet.
--          Depending on the received pkt, the returned
--          structure may have additional fields.
--
--          All returned packets have the following fields:
--              rec = {
--                  .header
--                  .body
--                  .flags
--                  .len
--                  .id
--                  .cmd
--              }
--
--          Additional fields according with the bid (base
--          id) and op (operand):
--
--          BID: FILE_DATA:
--
--              OP: RETURN_DATA:
--                  .data_tpl = {
--                      .file_id
--                      .offset
--                      .file_len
--                      .file_data
--                  }
--              OP: RETURN_ERR:
--                  .error_tpl = {
--                      .file_id
--                      .error_code
--                  }
--
--          BID: QP:
--
--              OP: UNS:
--                  .uns_tpl = {
--                     .lb
--                     .lqi
--                     .uid
--                     .body
--                     .file_id
--                     .offset
--                     .file_len
--                     .file_chunk
--                  }
--
--          BID: NULL:
--
--              No additional fields
--
-- @retval  1) The parsed structure
--          2) nil, msg: In case of error
----------------------------------------------------
-- General parser
function alp_parser.parse(pkt)
    p_dbg("ALP PARSER")

    -- Parse the record structure (first level)
    local rec, msg = rec_parse(pkt)

    if not rec then
        return nil, msg
    end

    -- Decode the bid and the op
    rec.bid = bit.band(rec.id, 0xf)
    rec.op = bit.band(rec.cmd, 0xf)

    -- Common file data alp packets
    if rec.bid == alp_parser.bid.FILE_DATA then

        if rec.op == alp_parser.op.RETURN_DATA then
            return resp_file_data_parse(rec)
        elseif rec.op == alp_parser.op.RETURN_ERR then
            return resp_error_parse(rec)
        else
            msg = "parsing of FILE DATA pkt OP " .. tostring(rec.op) .. " is not supported"
            p_err(msg)
            return nil, msg
        end

    -- QP packets
    elseif rec.bid == alp_parser.bid.QP then

        if rec.cmd == alp_parser.qp_cmd.UNS then
            return uns_parse(rec)
        else
            msg = "QP pkt cmd " .. tostring(rec.cmd) .. " is not supported"
            p_err(msg)
            return nil, msg
        end

    -- ALP NULL
    elseif rec.bid == alp_parser.bid.NULL then

        return rec

    else
        msg = "Unknown bid " .. tostring(rec.bid)
        p_err(msg)
        return nil, msg
    end
end

return alp_parser
