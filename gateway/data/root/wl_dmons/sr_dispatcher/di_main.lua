#!/usr/bin/lua

-- Setting path to look for libraries
-- XXX: this is not a good place, we won't be able to move the code to another file
package.path = (package.path or "") .. ";/root/wl_dmons/sr_dispatcher/?.lua;/root/wl_dmons/?.lua"

----------------------------------------
-- Dependencies
----------------------------------------
local p_color   = require "p_color"
local di_sr     = require "di_sr"
local wl        = require "wl"
----------------------------------------

----------------------------------------------------
-- Print functions
-- Set an empty function to disable a print
----------------------------------------------------
local p_pref = "DIM:"
local p_err = function (...) p_color.p_err(p_pref, unpack(arg)) end
local p_dbg = function (...) p_color.p_dbg(p_pref, unpack(arg)) end
local p_inf = function (...) p_color.p_inf(p_pref, unpack(arg)) end
local p_log = function (...) p_color.p_log(p_pref, unpack(arg)) end

-- Poll is equivalent to select to wait for new data
-- in the socket. Set the poll flags to in to indicate
-- that we are waiting for data to arrive
local POLL_FLAGS = nixio.poll_flags("in")

-- =================================================
-- dispatch_on_rdy
-- =================================================
-- @brief   The scheduler of the program.
--          Sleep until there is some data coming from
--          the mote or coming from the alp control
--          process
-- @param   pollcond The parameters for nixio.poll to
--                      with the sockets to wait for
--                      new data
-- @param   thread_map  The threads to be resumed
--                      depending on the socket with
--                      available data
-- @retval  none
----------------------------------------------------
local function dispatch_on_rdy (pollcond, thread_map)
    while true do
        -- Wait for data to be available in one
        -- of the sockets
        local ok, s_inf = nixio.poll(pollcond, -1)

        if not ok then
            error("Unexpected timeout")
        end

        if ok ~= 1 then
            p_inf("MULT SCH", ok)
        end

        -- For each socket check its flag
        for i,inf in ipairs(s_inf) do
            local flags = nixio.poll_flags(inf.revents)

            -- If connection is lost
            if flags.hup then
                error("Socket connection is lost.")
            end

            -- If an error has occurred
            if flags.err then
                error("Socket Error.")
            end

            -- If there are data available to read
            -- Resume the associated thread
            if flags["in"] then

                -- Get the thread associated with
                -- this socket
                local thread = thread_map[inf.fd]

                if not thread then
                    error("Unknown file descriptor")
                end

                -- Resume the thread
                local status,msg = coroutine.resume(thread)

                -- Stop in case of error
                if not status then
                    error(msg)
                end
            end
        end

    end
end


-- =================================================
-- main
-- =================================================
-- @brief   The main entry of the program.
--          Initialize the sockets and threads and
--          launch the scheduler (dispatch_on_rdy)
-- @retval  none
----------------------------------------------------
local function main()
    p_log("Start")

    -- Do not execute if this program is already running
    if not wl.one_prog_running(arg[0]) then
        p_err("This program is already running")
        return
    end

    -- Initializing serial connection and get fds to wait for
    local fd_com, fd_sk = di_sr.init()

    -- Get COM and SK threads and organize by file descriptor
    local thread_map = {[fd_com] = di_sr.thread_com(), [fd_sk] = di_sr.thread_sk()}

    -- Prepare poll condition
    local pollcond = {{fd=fd_com, events=POLL_FLAGS},{fd=fd_sk, events=POLL_FLAGS} }


    -- Launching threads
    dispatch_on_rdy(pollcond, thread_map)
end

main()
