----------------------------------------
-- Dependencies
----------------------------------------
local p_color   = require "p_color"
local nixio     = require "nixio"
local nixio_util= require "nixio.util"
local wl        = require "wl"
local bit       = require "bit"
----------------------------------------

----------------------------------------------------
-- Print functions
-- Set an empty function to disable a print
----------------------------------------------------
local p_pref = "SR:"
local p_err = function (...) p_color.p_err(p_pref, unpack(arg)) end
local p_dbg = function (...) p_color.p_dbg(p_pref, unpack(arg)) end
--local p_dbg = function() end
local p_inf = function (...) p_color.p_inf(p_pref, unpack(arg)) end
local p_log = function (...) p_color.p_log(p_pref, unpack(arg)) end
local p_trc = function (str) io.write(color.onmagenta("TRC:") .. str) end

----------------------------------------------------
-- Public object
-- To be returned by require function
----------------------------------------------------
di_sr = {}

-- Defined "macros"

-- Sync bytes in a string
local SERIAL_CSYNC0 = string.char(wl.SERIAL_SYNC0)
local SERIAL_CSYNC1 = string.char(wl.SERIAL_SYNC1)

-- The size of the serial encapsulation (syn1 syn2 len flow seq)
local SERIAL_HEADER_SIZE = 5

-- The sequence number modulo (1 byte)
local SEQ_MOD = 256

------------------------------
-- PRIVATE FUNCTIONS
------------------------------

-- =================================================
-- ts_pkt
-- =================================================
-- @brief   Create a time stamp packet. It is a time
--          stamp string encapsulated in a serial packet
--          for logging purpose (to be easily read by
--          external tools as monitor.rb)
-- @retval  The time stamp packet
----------------------------------------------------
local function ts_pkt()
    local payload = os.date("GW:%Y%m%d-%Hh%Mm%Ss\n", os.time()) .. string.char(0)
    local l = string.char(wl.SERIAL_SYNC0, wl.SERIAL_SYNC1, string.len(payload), 0, wl.COM_FLOW.PRINTF) .. payload
    return l
end

-- =================================================
-- write_trc_sk
-- =================================================
-- @brief   Send data to the trace socket if there is
--          any external connection (by WizziCom or
--          monitor.rb for example)
--          If there is no connections, close the socket
--          and retry to connect.
--          If a connections is already opened, pipe
--          the data through it
-- @param   buf     The data to be sent
-- @retval  none
----------------------------------------------------
local function write_trc_sk(buf)
    -- If we are not connected with a client or the client has close its connection
    if not di_sr.trc_cli_sk or not di_sr.trc_cli_sk:getpeername() then

        -- Server side close in socket closed by client
        if di_sr.trc_cli_sk then
            p_inf("CLOSING LOST CONNECTION")
            di_sr.trc_cli_sk:close()
            di_sr.trc_cli_sk = nil
        end

        -- Check for pending connections and accept it
        p_dbg("SK TRC TRY")
        di_sr.trc_cli_sk, from, port = di_sr.trc_sk:accept()
        if di_sr.trc_cli_sk then
            p_inf("SK TRC CONNECTED", from, port)
            di_sr.trc_cli_sk:setblocking(false)
        end
    end

    -- If we are connected, pipe buf through trace socket
    if di_sr.trc_cli_sk then
        local written = di_sr.trc_cli_sk:sendall(buf)
        p_dbg("TRC WRITE", #buf, written)
    end
end

-- =================================================
-- log
-- =================================================
-- @brief   Save the mote output into a file and add
--          time stamps from time to time
-- @retval  none
----------------------------------------------------
local function log(msg)
    p_dbg("LOG LEN", #msg)
    -- Save TS in log if its time
    if (not di_sr.last_ts) or os.time() - di_sr.last_ts > wl.SR_TS_PER then
        local ts_p = ts_pkt()
        wl.log(di_sr.log_obj, ts_p)
        di_sr.last_ts = os.time()
        p_inf("LOG TS SAVED")
    end

    -- Log the msg
    wl.log(di_sr.log_obj, msg)
end

-- =================================================
-- close_all_sockets
-- =================================================
-- @brief   Close all open sockets (this function is
--          called in case of errors)
-- @retval  none
----------------------------------------------------
local function close_all_sockets()
    di_sr.com_sk:close()
    if di_sr.trc_cli_sk then di_sr.trc_cli_sk:close() end
    di_sr.trc_sk:close()
    di_sr.alp_uns_cli_sk:close()
    di_sr.alp_cmd_cli_sk:close()
end

-- =================================================
-- read_com
-- =================================================
-- @brief   Read all data available in the COM socket
--          (data coming from the mote) limited by a
--          maximum of bytes defined by wl.SK_READ_LEN
--          Log this data and send it through the trace
--          connection (WizziCom or monitor.rb for example)
-- @retval  The bytes read from the COM socket
----------------------------------------------------
local function read_com()
    local buf, code, err

    while not buf do
        buf, code, err = di_sr.com_sk:read(wl.SK_READ_LEN)

        -- If there is nothing to read then yield
        if buf == false then
            coroutine.yield()

        -- If error then raise error
        elseif buf == nil then
            p_err("Com socket error")
            -- Stop on error
            close_all_sockets()
            error(tostring(di_sr.com_sk) .. "\t" .. tostring(code) .. "\t" .. tostring(err))

        -- If connection is lost
        elseif #buf == 0 then
            p_err("Com socket disconnected")
            -- Stop on error
            close_all_sockets()
            error("Socket COM was disconnected")
        end
    end

    -- Send data though trace socket
    write_trc_sk(buf)

    -- Save data in log
    log(buf)

    return buf
end

-- =================================================
-- write_com
-- =================================================
-- @brief   Send data to the mote (COM socket)
-- @param   pkt     The array of bytes to send
-- @retval  none
----------------------------------------------------
local function write_com(pkt)
    di_sr.com_sk:sendall(pkt)
end

-- =================================================
-- write_alp_cmd_sk
-- =================================================
-- @brief   Send data to the socket connected with
--          the alp_control process
-- @param   pkt     The array of bytes to send
-- @retval  none
----------------------------------------------------
local function write_alp_cmd_sk(pkt)
    di_sr.alp_cmd_cli_sk:sendall(string.char(#pkt))
    di_sr.alp_cmd_cli_sk:sendall(pkt)
end

-- =================================================
-- write_alp_uns_sk
-- =================================================
-- @brief   Send data to the socket connected with
--          the sensor_report process
-- @param   pkt     The array of bytes to send
-- @retval  none
----------------------------------------------------
local function write_alp_uns_sk(pkt)
    di_sr.alp_uns_cli_sk:sendall(string.char(#pkt))
    di_sr.alp_uns_cli_sk:sendall(pkt)
end


-- =================================================
-- dispatch
-- =================================================
-- @brief   Send a packet to the appropriated process
--          according with its flowid
-- @retval  none
----------------------------------------------------
local function dispatch(flowid, pload)
    p_dbg("DISPATCH:", flowid, #pload)
    wl.pp_data(pload)

    flowid = bit.band(flowid,0x7F) 
    -- If this is a command (ALP NULL), duplicate the pkt to both process
    if flowid == wl.COM_FLOW.ALP_CMD then
        write_alp_cmd_sk(pload)
        write_alp_uns_sk(pload)
        p_dbg("DISPATCHED:", flowid, #pload)
    elseif flowid == wl.COM_FLOW.ALP_RESP or flowid == wl.COM_FLOW.SYS_CUP then
        write_alp_cmd_sk(pload)
        p_dbg("DISPATCHED:", flowid, #pload)
    elseif flowid == wl.COM_FLOW.ALP_UNS then
        write_alp_uns_sk(pload)
        p_dbg("DISPATCHED:", flowid, #pload)
    end
end


-- =================================================
-- run_com
-- =================================================
-- @brief   The function running in a coroutine that
--          reads data from the mote (COM socket) and
--          dispatch it to the appropriated process
-- @retval  none
----------------------------------------------------
local function run_com()
    -- Variable to hold the expected sequence number
    local seq_exp

    -- Initialize the buffer for receiving data
    local buf = ""

    -- Start the infinity loop
    while true do
        buf = buf .. read_com()
        p_dbg("BUF", #buf)
        --wl.pp_data(buf)

        -- Consume all data
        while true do

            -- Look for the beginning of the serial packet
            -- (header index)
            local h_idx = string.find(buf, SERIAL_CSYNC0)

            -- If we found the beginning of the packet
            if h_idx then
                p_dbg("H_IDX", h_idx)

                -- Remove data from buffer before the syn0
                buf = string.sub(buf, h_idx, -1)

                -- If we have enough data for a complete header
                if #buf >= SERIAL_HEADER_SIZE then

                    if wl.get_byte(buf, 2) == wl.SERIAL_SYNC1 then
                        -- We found a packet
                        -- Extract the header data
                        local len       = wl.get_byte(buf, 3)
                        local seq       = wl.get_byte(buf, 4)
                        local flowid    = wl.get_byte(buf, 5)

                        p_dbg("PKT FOUND", len, seq, flowid)

                        -- If the body of the pkt is within buf
                        if #buf >= SERIAL_HEADER_SIZE + len then
                            -- Check sequence number and log a message
                            -- if the sequence number was not the one we
                            -- were expecting
                            if seq_exp and seq_exp ~= seq then
                                p_err(string.format("Expecting SEQ %d but got %d", seq_exp, seq))
                            end

                            -- Set the next expected sequence number
                            seq_exp = (seq + 1) % SEQ_MOD

                            -- Dispatch pkt
                            dispatch(flowid, string.sub(buf, SERIAL_HEADER_SIZE + 1, SERIAL_HEADER_SIZE + len))

                            -- Remove the parsed data from the buffer
                            buf = string.sub(buf, SERIAL_HEADER_SIZE + len + 1, -1)
                        else
                            --Not enough data for complete pkt
                            break
                        end

                    else
                        -- If we didn't find the syn1, remove syn0 and go to next iteration
                        buf = string.sub(buf, 2, -1)
                    end
                else
                    -- Not enough data for header
                    break
                end
            else
                -- If pkt start not found

                -- Empty buffer
                buf = ""
                -- Read more data from serial
                break

            end
        end
    end
end

-- =================================================
-- run_sk
-- =================================================
-- @brief   The function running in a coroutine that
--          reads data from the alp socket and sends
--          it to the mote
-- @retval  none
----------------------------------------------------
local function run_sk()
    while true do
        -- Try read the length of pkt
        local clen, code, err = di_sr.alp_cmd_cli_sk:read(1)

        -- If we read something
        if clen ~= false then

            -- If socket error
            if clen == nil then
                p_err("Alp socket error")
                close_all_sockets()
                error(tostring(di_sr.alp_cmd_cli_sk) .. "\t" .. tostring(code) .. "\t" .. tostring(err))

            -- If the connection is lost
            elseif #clen == 0 then
                p_err("Alp socket disconnected")
                close_all_sockets()
                error("Socket ALP CTR was disconnected")
            end

            -- Get how many bytes we should read
            local len = string.byte(clen)

            p_inf("ALP RCV CMD", len)

            local pload, code, err = wl.read_or_yield(di_sr.alp_cmd_cli_sk, len)

            -- If the connection is lost
            if not pload then
                p_err("Alp socket lost connection")
                close_all_sockets()
                error("Socket ALP CTR was disconnected\t" .. tostring(code) .. "\t" .. tostring(err))
            end

            p_inf("ALP CMD", wl.pp_data(pload))

            -- Here we already read the whole pkt
            -- Send it throught COM
            write_com(string.char(wl.SERIAL_SYNC0, wl.SERIAL_SYNC1, #pload, di_sr.seq, wl.COM_FLOW.ALP_CMD) .. pload)
        end

        -- Yield if we read nothing or if we already finished dealing with a pkt
        coroutine.yield()
    end
end


------------------------------
-- PUBLIC FUNCTIONS
------------------------------

-- =================================================
-- di_sr.thread_com
-- =================================================
-- @brief   Create the coroutine that read packets
--          from the mote
-- @retval  The created coroutine
----------------------------------------------------
function di_sr.thread_com()
    return coroutine.create(run_com)
end

-- =================================================
-- di_sr.thread_sk
-- =================================================
-- @brief   Create the coroutine that wait for data
--          from the alp process and send it to the
--          mote
-- @retval  The created coroutine
----------------------------------------------------
function di_sr.thread_sk()
    return coroutine.create(run_sk)
end

-- =================================================
-- di_sr.init
-- =================================================
-- @brief   Initialize the serial library
--          Prepare the sockets and the connections
-- @retval  none
----------------------------------------------------
function di_sr.init()
    local from

    -- Trace Socket
    di_sr.trc_sk = nixio.bind("*", wl.SK_PORT_BASE)
    di_sr.trc_sk:setblocking(false)
    di_sr.trc_sk:listen(0)

    -- Alp Cmd Socket listen
--  Reference code to allow for connection in TCP mode
----[[    di_sr.alp_cmd_sk = nixio.bind("*", wl.SK_PORT_BASE + wl.COM_FLOW.ALP_RESP)
--    di_sr.alp_cmd_sk:listen(0)
--    di_sr.alp_cmd_cli_sk, from, port = di_sr.alp_cmd_sk:accept()
--    di_sr.alp_cmd_cli_sk:setblocking(false)]]
--
    nixio.fs.unlink(wl.SK_ALP_CMD_PATH)
    local alp_cmd_sk = nixio.socket("unix", "stream")
    alp_cmd_sk:bind(wl.SK_ALP_CMD_PATH)
    alp_cmd_sk:listen(0)


    -- Alp Uns Socket listen
--  Reference code to allow for connection in TCP mode
--[[    di_sr.alp_uns_sk = nixio.bind("*", wl.SK_PORT_BASE + wl.COM_FLOW.ALP_UNS)
    di_sr.alp_uns_sk:listen(0)
    di_sr.alp_uns_cli_sk, from, port = di_sr.alp_uns_sk:accept()
    di_sr.alp_uns_cli_sk:setblocking(false)]]

    nixio.fs.unlink(wl.SK_ALP_UNS_PATH)
    local alp_uns_sk = nixio.socket("unix", "stream")
    alp_uns_sk:bind(wl.SK_ALP_UNS_PATH)
    alp_uns_sk:listen(0)

    -- Wait connection in alp port to start
    p_inf("WAITING SK ALP CMD TO CONNECT")
    di_sr.alp_cmd_cli_sk, from, port = alp_cmd_sk:accept()
    alp_cmd_sk:close()
    di_sr.alp_cmd_cli_sk:setblocking(false)
    p_inf("SK ALP CMD CONNECTED")

    -- Wait connection in alp port to start
    p_inf("WAITING SK ALP UNS TO CONNECT")
    di_sr.alp_uns_cli_sk, from, port = alp_uns_sk:accept()
    alp_uns_sk:close()
    di_sr.alp_uns_cli_sk:setblocking(false)
    p_inf("SK ALP UNS CONNECTED")

    -- Connecting in serial port
    di_sr.com_sk = assert(nixio.connect(localhost, wl.SR_COM_PORT, "inet", "stream"), "Unable to connect with ser2net socket")
    -- Setting non blocking connection
    di_sr.com_sk:setblocking(false)

    -- Initialize sequence number
    di_sr.seq = 0

    -- Initialize log system
    di_sr.log_obj = wl.log_init(wl.MOTE_LOG_FILE)

    -- Return sockets we are listen
    return di_sr.com_sk, di_sr.alp_cmd_cli_sk
end


-- Return the public object
return di_sr
