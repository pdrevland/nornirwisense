#!/usr/bin/lua

-- Setting path to look for libraries
-- XXX: this is not a good place, we won't be able to move the code to another file
package.path = (package.path or "") .. ";/root/wl_dmons/sensor_report/?.lua;/root/wl_dmons/?.lua"

----------------------------------------
-- Dependencies
----------------------------------------
local p_color     = require "p_color"
local util        = require "luci.util"
local socket      = require "socket"
local wl          = require "wl"
local rp_lib      = require "rp_lib"
local rp_com      = require "rp_com"
local rp_post     = require "rp_post"
local app         = require "app"
----------------------------------------

----------------------------------------------------
-- Print functions
-- Set an empty function to disable a print
----------------------------------------------------
local p_pref = "RPM:"
local p_err = function (...) p_color.p_err(p_pref, unpack(arg)) end
local p_dbg = function (...) p_color.p_dbg(p_pref, unpack(arg)) end
-- local p_dbg = function (...) end
local p_inf = function (...) p_color.p_inf(p_pref, unpack(arg)) end
local p_log = function (...) p_color.p_log(p_pref, unpack(arg)) end

----------------------------------------------------
-- Private context
----------------------------------------------------
local g_ctx = {}

-- Initialize context

-- The delay to wait when a http post fails
g_ctx.back_off_delay = 0
-- Time stamp of the last http post fail
g_ctx.post_fail_ts = 0
-- How many free coroutines we have
g_ctx.free_coroutines = wl.RP_MAX_COROUTINES

-- =================================================
-- post_fail
-- =================================================
-- @brief   Call this function when a post fail.
--          Send error to the soft watchdog to start a
--          time count down if no post_success occurs
--          to reset the network and then reset the
--          entire gateway.
--          Update the back_off_delay with the application
--          function app.back_off_fail defined in app.lua
--          if the function exists
-- @retval  none
----------------------------------------------------
local function post_fail()
    -- Warn the watch dog we failed
    if g_ctx.wd_post then g_ctx.wd_post:write('e') end

    -- Increase the back_off_delay
    if app.back_off_fail then
        g_ctx.back_off_delay = app.back_off_fail(g_ctx.back_off_delay)
        p_inf("BACK OFF (FAIL) DELAY", tostring(g_ctx.back_off_delay))
    end

    -- Save the time when we failed
    g_ctx.post_fail_ts = socket.gettime()
end

-- =================================================
-- post_success
-- =================================================
-- @brief   Call this function when a post succeeds.
--          Send tick to the soft watchdog to reset the
--          time count down if it is active.
--          Update the back_off_delay with the application
--          function app.back_off_success defined in app.lua
--          if the function exists
-- @retval  none
----------------------------------------------------
local function post_success()
    -- Deactivate the watch dog's timer
    if g_ctx.wd_post then g_ctx.wd_post:write('r') end

    -- Decrease the back_off_delay
    if app.back_off_success then
        g_ctx.back_off_delay = app.back_off_success(g_ctx.back_off_delay)
        p_inf("BACK OFF (SUCCESS) DELAY", tostring(g_ctx.back_off_delay))
    end

end

-- =================================================
-- free_coroutine
-- =================================================
-- @brief   Check if there is a free coroutine to
--          perform a http post
-- @retval  true if there is a free coroutine
----------------------------------------------------
local function free_coroutine()
    return g_ctx.free_coroutines ~= 0
end

-- =================================================
-- deallocate_coroutine
-- =================================================
-- @brief   Release a coroutine that ended
--          Update the free_coroutine counter, remove
--          it and its socket from the rp_lib.sel_table
--          so we won't wait for any more data from it
--          anymore.
-- @param   sk  The socket associated with the coroutine
-- @retval  none
----------------------------------------------------
local function deallocate_coroutine(sk)
    -- close the socket if is was not closed yet
    sk:close()

    -- Free coroutine in counter
    g_ctx.free_coroutines = g_ctx.free_coroutines + 1

    p_inf("Coroutine deallocated", sk, "N coroutines:",
            wl.RP_MAX_COROUTINES - g_ctx.free_coroutines, "max coroutines", g_ctx.max_coroutines)

    -- Remove socket from select table
    rp_lib.sel_table[sk] = nil

    if g_ctx.free_coroutines > wl.RP_MAX_COROUTINES then
        rp_lib.close_all_sockets()
        error("Deallocating more coroutines then the max allowed")
    end
end

-- =================================================
-- add_to_select
-- =================================================
-- @brief   Add socket and its associated coroutine
--          into the rp_lib.sel_table
-- @param   sk: The socket to perform the select
-- @param   co: The coroutine to be resumed when data
--              is available for reading in sk
-- @param   sensors: The objects that the coroutine co
--                  is responsible to post
-- @param   creation_time: The creation time of the coroutine
-- @retval  none
----------------------------------------------------
local function add_to_select(sk, co, sensors, creation_time)
    if rp_lib.sel_table[sk] then
        rp_lib.close_all_sockets()
        error("Attempt to insert an already inserted socket in select table")
    end

    if not co then
        error("Trying to insert socket into sel_table without coroutine")
    end

    rp_lib.sel_table[sk] = {co=co, sensors=sensors, creation_time=creation_time}
end

-- =================================================
-- select_sockets
-- =================================================
-- @brief   Perform a select for reading in the sockets
--          inserted in the rp_lib.sel_table and return
--          an iterator on the selected sockets.
--
--          If the socket of the COM data is on the list,
--          it will be the last element returned by the
--          iterator (because it has lower priority)
-- @retval  The iterator for the selected sockets
----------------------------------------------------
-- Iterator in selects
local function select_sockets(to)

    -- Create list of sockets to select
    local sel_param = {}
    for sk in pairs(rp_lib.sel_table) do
        table.insert(sel_param, sk)
    end

    -- Perform the select
    p_dbg("SELECT TO", to, "BACK OFF DELAY", g_ctx.back_off_delay, "LAST", g_ctx.post_fail_ts, "NOW", socket.gettime())
    local ok_list = socket.select(sel_param, nil, to)
    p_dbg("SEL DONE", #ok_list)

    local com_sk = nil
    local i = 0

    -- Create the iterator
    return function()
        i = i + 1

        -- Get the next element of the list
        local sk = ok_list[i]

        -- If this is a com_sk, save it to return it
        -- as the last element (because it has lower priority)
        -- and get the next element
        if sk == g_ctx.com_sk then
            com_sk = sk
            i = i + 1
            sk = ok_list[i]
        end

        -- If not sk, return com_sk if it exist
        -- and set com_sk to nil, thus it won't
        -- be returned twice
        if not sk then
            local ret = com_sk
            com_sk = nil
            return ret
        end

        return sk

    end
end


-- =================================================
-- get_uid_list
-- =================================================
-- @brief   Build a list of uid from a list of sensors
--          Used for debug
-- @param   sensors The list of sensors to retrieve
--          the UIDs
-- @retval  A list of UIDs
----------------------------------------------------
local function get_uid_list(sensors)
    local uid_list = {}
    for i,sensor in ipairs(sensors) do
        p_inf("UID LIST:", i, sensor.uid)
        table.insert(uid_list, sensor.uid)
    end

    return uid_list
end

-- =================================================
-- post_http
-- =================================================
-- @brief   Retrieve sensors from the pending list
--          and launch a coroutine to perform the
--          http post
-- @retval  none
----------------------------------------------------
local function post_http()
    -- Get a sensors list from the pending table
    local sensors = rp_lib.rm_from_pending_list()

    -- Create a http coroutine
    local co = rp_post.http_coroutine_new()

    p_dbg("Generating new coroutine", co)


    -- Dispatch the request
    local ok, sk, msg = coroutine.resume(co, sensors)

    p_dbg("HTTP POST RESUME", ok, sk, msg)

    -- If we couldn't create the coroutine, reinsert the sensors objects in the pending list
    if not ok or not sk then
        p_err("Could not create http post coroutine " .. tostring(util.serialize_data(sk)) .. " " ..  tostring(util.serialize_data(msg)))

        -- Reinsert sensors in pending list
        p_log("Reinserting:", table.concat(get_uid_list(sensors), "\n"))
        rp_lib.reinsert_pending_list_if_necessary(sensors)
        -- Warn the watch dog we failed and check the backoff factor
        post_fail()
    else
        -- Add socket/coroutine pair in the select config
        add_to_select(sk, co, sensors, socket.gettime())

        -- Allocate a coroutine
        g_ctx.free_coroutines = g_ctx.free_coroutines - 1


        local n_coroutines = wl.RP_MAX_COROUTINES - g_ctx.free_coroutines

        -- Calculate the max worker
        if not g_ctx.max_coroutines or n_coroutines > g_ctx.max_coroutines then
            g_ctx.max_coroutines = n_coroutines
        end

        p_inf("Coroutine generated", sk, "N coroutines:", n_coroutines, "max coroutines", g_ctx.max_coroutines)
    end

end

-- =================================================
-- coroutine_garbage_collec
-- =================================================
-- @brief   Check for dead coroutines, if any, deallocate
--          the coroutine and reinsert the sensor
--          in the pending list
-- @retval  none
----------------------------------------------------
local function coroutine_garbage_collec()
    local time_now = socket.gettime()

    for sk, info in pairs(rp_lib.sel_table) do
        -- If this socket is not the COM socket
        if sk ~= g_ctx.com_sk then

            -- Check if coroutine is dead
            -- or if we are waiting too long time for socket
            if coroutine.status(info.co) == "dead" or (info.creation_time and time_now - info.creation_time > wl.RE_HTTP_RESP_TIMEOUT) then
                local msg = os.date("%c", time_now)

                if coroutine.status(info.co) == "dead" then
                    msg = msg .. " Coroutine is dead before receiving from socket " .. tostring(sk) .. " " .. tostring(info.co) .. " " .. util.serialize_data(info.sensor) .. "\n"
                else
                    msg = msg .. " Socket http resp timeout " .. tostring(sk) .. " " .. tostring(info.co) .. " " .. util.serialize_data(info.sensor) .. "\n"
                end

                p_err(msg)

                -- Warn the watch dog we failed and check the backoff factor
                post_fail()

                -- reinsert sensor in pending list
                p_log("Reinserting:", table.concat(get_uid_list(info.sensors), "\n"))
                rp_lib.reinsert_pending_list_if_necessary(info.sensors)

                -- close the socket and deallocate the coroutine
                deallocate_coroutine(sk)
            end
        end
    end
end

-- =================================================
-- scheduler
-- =================================================
-- @brief   This is the main task of the process.
--          Launch new coroutines when there are pending
--          reports and free coroutines.
--          Perform a select for receiving data in all
--          the active sockets and re-schedule associated
--          coroutine to read the data
-- @retval  none
----------------------------------------------------
local function scheduler()
    while true do
        -- If we have pending reports, free coroutines and we don't need to wait for a back off delay
        -- try to post this report to the server
        if rp_lib.pending_reports() and free_coroutine() and (g_ctx.back_off_delay == 0 or (socket.gettime() - g_ctx.post_fail_ts >= g_ctx.back_off_delay)) then
            -- Post report if we have pending reports and
            -- a free coroutine
            post_http()
        else

            p_inf("------------------ Selecting in sockets.", "Free coroutines:", g_ctx.free_coroutines, "Pending:", rp_lib.pending_reports())

            -- We don't have a free_coroutine or a pending report
            -- wait for a socket to become ready

            -- Calculate the back off retry time if any
            local back_off_to = nil
            if g_ctx.back_off_delay > 0 and rp_lib.pending_reports() and free_coroutine() then

                -- Get the remaining time to retry the http post
                back_off_to = g_ctx.post_fail_ts + g_ctx.back_off_delay - socket.gettime()

                -- If we already passed the time to retry, then set the timeout to 0
                if back_off_to < 0 then
                    back_off_to = 0
                end
            end

            -- Iterate over the sockets that are ready
            for sk in select_sockets(back_off_to) do

                p_dbg("ITER SK", sk, "IS COM SK", sk==g_ctx.com_sk)

                -- Resume the corresponding thread
                local ok, resp, msg = coroutine.resume(rp_lib.sel_table[sk].co)

                p_dbg("CO RESP " .. tostring(ok) .. tostring(resp) .. tostring(msg))

                -- If this is a http socket
                -- and it has already finished
                -- the execution
                if coroutine.status(rp_lib.sel_table[sk].co) == "dead" then

                    p_dbg("Coroutine has finished", rp_lib.sel_table[sk].co)

                    if sk == g_ctx.com_sk then
                        rp_lib.close_all_sockets()
                        error("COM coroutine is dead" .. tostring(util.serialize_data(resp)) .. " " ..  tostring(util.serialize_data(msg)))
                    end

                    -- In case of error, reinsert the report in the
                    -- pending list if there is no new data from this
                    -- sensors
                    if not ok or not resp then

                        -- If we could not resume the thread
                        -- for some reason, it is an error
                        if not ok then
                            p_err("Could not resume coroutine", util.serialize_data(resp), util.serialize_data(msg))
                        else
                            p_err(util.serialize_data(msg))
                        end

                        p_log("Reinserting:", table.concat(get_uid_list(rp_lib.sel_table[sk].sensors), "\n"))
                        rp_lib.reinsert_pending_list_if_necessary(rp_lib.sel_table[sk].sensors)

                        -- Warn the watch dog we failed and check the backoff factor
                        post_fail()
                    else
                        -- Send the success to the WatchDog process and reset the backoff factor
                        post_success()
                    end

                    -- Deallocate coroutine
                    deallocate_coroutine(sk)

                    -- Print stats of lost packets
                    p_inf("PKT STATS:", rp_lib.pkts_stat())
                end
            end

            -- Check for dead coroutines (that are dead before receiving something in socket)
            -- and for timeouted sockets
            coroutine_garbage_collec()
        end
    end
end

-- =================================================
-- init
-- =================================================
-- @brief   Initialize the sub libs, prepare the sockets
--          launch the COM coroutine.
-- @retval  none
----------------------------------------------------
local function init()

    -- XXX: This resets the tboot counter of rst_dmons of watch dog
    --      This is done because the tboot in rst_dmons is not used
    --      Check a better way to deactivate tboot
    if app.SOFT_WATCH_DOG_ENABLED then
        local rst_wd = wl.wd_connect(wl.PIPE_WD_RST_DMONS_PATH, p_err)
        rst_wd:write('r')
        rst_wd:close()
    end

    -- Initializing COM
    local com_sk_nixio = rp_com.init()

    -- connect with watch dog for post
    if app.SOFT_WATCH_DOG_ENABLED and app.REPORT_TO_REMOTE_SERVER then
        g_ctx.wd_post = wl.wd_connect(wl.PIPE_WD_POST_PATH, p_err)
    end

    -- Encapsulate nixio socket in luasocket socket
    -- To allow the select to be made in the Unix
    -- socket too
    g_ctx.com_sk = socket.tcp()
    g_ctx.com_sk:setfd(com_sk_nixio:fileno())

    p_inf("COM SK", g_ctx.com_sk)

    -- Start com coroutine
    local com_co = rp_com.thread()

    -- Insert com socket in select list
    add_to_select(g_ctx.com_sk, com_co)
end

-- =================================================
-- main
-- =================================================
-- @brief   Read all available contents from the COM
-- @retval  none
----------------------------------------------------
local function main()
    p_log("Start")

    -- Do not execute if this program is already running
    if not wl.one_prog_running(arg[0]) then
        p_err("This program is already running")
        return
    end

    init()
    scheduler()
end

-- Start the program
main()
