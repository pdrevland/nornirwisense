----------------------------------------
-- Dependencies
----------------------------------------
local p_color     = require "p_color"
local util        = require "luci.util"
local json        = require "luci.json"
local nixio       = require "nixio"
local nixio_util  = require "nixio.util"
local socket      = require "socket"
local http        = require "socket.http"
local wl          = require "wl"
local alp         = require "alp_parser"
local bit         = require "bit"
local rp_lib      = require "rp_lib"
local app         = require "app"
----------------------------------------

----------------------------------------------------
-- Print functions
-- Set an empty function to disable a print
----------------------------------------------------
local p_pref = "RE:"
local p_err = function (...) p_color.p_err(p_pref, unpack(arg)) end
local p_dbg = function (...) p_color.p_dbg(p_pref, unpack(arg)) end
local p_inf = function (...) p_color.p_inf(p_pref, unpack(arg)) end
local p_log = function (...) p_color.p_log(p_pref, unpack(arg)) end

----------------------------------------------------
-- Public object
-- To be returned by require function
----------------------------------------------------
rp_com = {}

-- =================================================
-- set_dst_subnet
-- =================================================
-- @brief   Set the destination subnet of the mote
-- @param   subnet  The subnet to be set
-- @param   flush   Set to true to perform an write flush
-- @retval  none
----------------------------------------------------
local function set_dst_subnet(subnet, flush)

    local uri

    -- Build the command to write the subnet
    if flush then
        -- This is a write file flush command
        uri = string.format(wl.HTTP_URI_WFF_SUBNET, subnet)
    else
        -- This is a common write file command
        uri = string.format(wl.HTTP_URI_WF_SUBNET, subnet)
    end

    p_dbg("SET DST SUBNET", uri)
    p_log("SET DST SUBNET ", subnet, flush)

    local resp_s, code, _, res = http.request(uri)

    if not resp_s or code ~= 200 then
        error("Set dst subnet http request error " .. tostring(resp_s) .. " " .. tostring(code) .. " " .. tostring(res))

    else
        p_dbg("LUCI RESP: ", resp_s, code, res)

        local resp = resp_s and json.decode(resp_s)

        if resp then
            if resp.status == "OK" then
                p_inf("Dst subnet to", subnet)
            else
                error("Could not set dst subnet to", subnet)
            end
        else
            error("Could not decode json obj", resp_s, code, res)
        end

    end

end


----------------------------------------------------
-- Private context object
----------------------------------------------------
local g_ctx = {}

---------------------------
-- Parsing Functions
---------------------------

-- =================================================
-- save_calib
-- =================================================
-- @brief   Append the calibration data in the CALIB
--          FILE
-- @retval  none
----------------------------------------------------
local function save_calib(obj)
    p_dbg("SAVE CALIB")
    local fd, msg = io.open(wl.CALIB_FILE, "a")
    if not fd then
        p_err(msg)
        return
    end

    fd:write(util.serialize_data(obj) .. "\n")
    fd:close()
end

-- =================================================
-- parse_calib
-- =================================================
-- @brief   Parse the unsolicited report content as
--          being the calibration file
-- @param   The unsolicited object
--
-- @retval  Return an object with the following fields:
--              .uid
--              .cal_val
--              .cal_var
--              .ts
----------------------------------------------------
local function parse_calib(pkt_parsed)
    --p_dbg("PARSE CALIB", #pkt_parsed.file_chunk)

    if string.len(pkt_parsed.file_chunk) < 4 then
        return nil
    end

    local obj = {uid = pkt_parsed.uid}
    local cal_val = wl.get_byte(pkt_parsed.file_chunk, 1) + bit.lshift(wl.get_byte(pkt_parsed.file_chunk, 2), 8)
    local cal_var = wl.get_byte(pkt_parsed.file_chunk, 3) + bit.lshift(wl.get_byte(pkt_parsed.file_chunk, 4), 8)
    obj.cal_val = cal_val
    obj.cal_var = cal_var
    obj.ts = os.time()
    return obj
end

-- =================================================
-- parse_mote_version
-- =================================================
-- @brief   Parse the unsolicited report content as
--          being the version file.
-- @param   The unsolicited object
-- @retval  An object with the following fields:
--              .uid
--              .lb (link budget)
--              .ts (time stamp)
--              .mv (mote version)
--              .mfs(file system crc)
----------------------------------------------------
local function parse_mote_version(pkt_parsed)

    local obj = {uid = pkt_parsed.uid}
    obj.lb    = pkt_parsed.lb
    obj.ts    = os.time()

    obj.mv = wl.get_version(pkt_parsed.file_chunk, 1, 4)
    obj.mfs = wl.get_version(pkt_parsed.file_chunk, 5, 8)

    p_inf("VERSION ", obj.mv, obj.mfs)


    return obj
end

-- =================================================
-- parse_generic
-- =================================================
-- @brief   Parse the unsolicited report content as
--          being the any otherwise treated file
-- @param   The unsolicited object
-- @retval  An object with the following fields:
--              .uid
--              .lb (link budget)
--              .ts (time stamp)
--              .fid (file id)
--              .dat(data content)
--              .off(offset in file)
----------------------------------------------------
local function parse_generic(pkt_parsed)
  local obj  = {uid = pkt_parsed.uid}
  obj.lb     = pkt_parsed.lb
  -- time in ms
  obj.ts    = socket.gettime()*1000
  
  obj.fid    = pkt_parsed.file_id
  obj.off    = pkt_parsed.offset
  
  -- Seqnum
    obj.sn    = wl.get_byte(pkt_parsed.file_chunk, 1)
  -- Retry
    obj.rt    = bit.rshift(wl.get_byte(pkt_parsed.file_chunk, 2),4)
  
  obj.ser = tostring(string.sub(pkt_parsed.file_chunk,3,5))
  -- make the serviceid readable
  obj.ser = wl.data_to_hexstr(obj.ser)
  obj.dat = nixio.bin.b64encode(tostring(string.sub(pkt_parsed.file_chunk, 6, -1)))

  p_inf("GENERIC ", obj.uid, obj.fid, obj.ser, obj.dat)


  return obj
end

---------------------------
-- Main thread
---------------------------

-- =================================================
-- run_listener
-- =================================================
-- @brief   Thread that listen for data in the COM
--          socket and parse the packets being received.
--          The thread tries to read data from the socket,
--          if there is not data it yields and wait for
--          the rp_main scheduler to resume it again when
--          there is new data.
--
--          After reading the packet and parsing it with
--          alp_parser.lua (alp.parse() function), it calls
--          another parser depending on the file id that may take
--          individual actions, for example, if a calib file
--          is reported, it saves the values into a Unix file.
--
--          When a report is received, it sends a tick to
--          the soft watchdog (if it is enabled).
--
--          If the received report is not a calibration or
--          version file, the application parser (in app.lua
--          file) is called. If this parsing is successful and
--          the wl.REPORT_TO_REMOTE_SERVER is true, the
--          report is added into the pending list to be
--          post to a remote server by another thread.
--          If app.INTERNAL_REPORT_ENABLED, it will send
--          the report to the sensor_data process that will
--          provide information for the internal page.
--
--          ALP NULL packets are being forward to this process
--          too. If we receive a BOOT message, we re-write a valid
--          Subnet, otherwise we ignore the message.
--
-- @retval  none
----------------------------------------------------
local function run_listener()
    --p_dbg("LISTENER START")

    while true do

        local clen = wl.read_or_yield(g_ctx.so, 1)

        -- If the connection is lost
        if not clen then
            rp_lib.close_all_sockets()
            error("Socket COM was disconnected")
        end

        -- Get how many bytes we should read
        local len = string.byte(clen)

        --p_dbg("RCV LEN", len)

        local pload = wl.read_or_yield(g_ctx.so, len)

        -- If the connection is lost
        if not pload then
            rp_lib.close_all_sockets()
            error("Socket COM was disconnected")
        end


        --p_dbg("COM PKT", wl.pp_data(pload))

        -- Here we already read the whole pkt
        -- Parse pkt
        local rec, msg = alp.parse(pload)

        if rec and rec.uns_tpl then

            local pkt_parsed = rec.uns_tpl

            -- If this is a calibration announcement from the sensors
            if pkt_parsed.file_id == wl.CALIB_FILE_ID then
                -- Say something to watch dog
                if app.SOFT_WATCH_DOG_ENABLED then g_ctx.wd:write('C') end

                local calib = parse_calib(pkt_parsed)
                save_calib(calib)

            -- It this is a mote version report
            elseif pkt_parsed.file_id == wl.REPORT_MOTE_VERSION_FILE_ID then
                -- Say something to watch dog
                if app.SOFT_WATCH_DOG_ENABLED then g_ctx.wd:write('M') end

                local sensor = parse_mote_version(pkt_parsed)

                --p_dbg("SENSOR", util.serialize_data(sensor))

                -- Add new_obj in the pending list
                if app.REPORT_TO_REMOTE_SERVER then
                    rp_lib.add_to_pending_list(sensor)
                end

                -- Internal report the sensor to generate the local page
                if app.INTERNAL_REPORT_ENABLED then
                    local ret = wl.internal_report(sensor)
                    -- Check errors and save it in the log file if any
                    if not ret or ret.status == "ERR" then
                        p_err("Error in sending obj to sensor_data process")
                    else
                        p_dbg("obj reported",util.serialize_data(sensor))
                    end
                end

            -- This is probably an application file
            else
                -- Say something to watch dog
                if app.SOFT_WATCH_DOG_ENABLED then g_ctx.wd:write('R') end

                -- Call the application's parser
                local sensor = app.parse_sensor and app.parse_sensor(pkt_parsed)
                local gensensor = parse_generic(pkt_parsed)
                p_dbg("sensor parsed")

                -- Add the sensor in the report-to-remove-server pending list
                if app.REPORT_TO_REMOTE_SERVER then
                    rp_lib.add_to_pending_list(gensensor)
                end
                
                if not sensor then
                    -- We don't know this file id
                    p_err("UNS PARSING ERROR", pkt_parsed.file_id)
                else
                
  

                    -- Internal report the sensor to generate the local page
                    if app.INTERNAL_REPORT_ENABLED then
                        local ret = wl.internal_report(sensor)
                        -- Check errors and save it in the log file if any
                        if not ret or ret.status == "ERR" then
                            p_err("Error in sending obj to sensor_data process")
                        else
                            p_dbg("obj reported",util.serialize_data(sensor))
                        end
                    end
                end
            end

        -- If this is a alp null
        elseif rec and rec.bid == alp.bid.NULL then

            -- If this is a boot message
            -- Set the subnet to a valid one
            if rec.op == alp.null_op.BOOT then
                p_err("RCV ALP NULL BOOT")
                set_dst_subnet(app.SUBNET_VALID)
            else
                p_inf("Ignoring alp null op:", rec.op)
            end


        -- If error
        else
            p_err("Failed to parse UNSOLICITED", msg)

        end
    end
end


----------------------------------------------------
-- PUBLIC FUNCTIONS
----------------------------------------------------


-- =================================================
-- rp_com.thread
-- =================================================
-- @brief   Create the thread that will listen for
--          reports in the COM socket
-- @retval  The thread to be resumed with coroutine.resume
----------------------------------------------------
function rp_com.thread()
    return coroutine.create(run_listener)
end

-- =================================================
-- rp_com.init
-- =================================================
-- @brief   Initialize the library.
--          Connects with the watchdog and with the
--          serial dispatcher process.
--          Write flush an invalid subnet and write
--          a valid subnet
-- @retval  The socket connected with the serial
--          dispatcher used by the scheduler to check
--          if there is new data and re-schedule the
--          thread
----------------------------------------------------
function rp_com.init()
    -- Connect with watch dog
    g_ctx.wd = app.SOFT_WATCH_DOG_ENABLED and wl.wd_connect(wl.PIPE_WD_RE_PATH, p_err)

    -- Connect with com port
    g_ctx.so = nixio.socket("unix", "stream")

    while true do
        local connected = g_ctx.so:connect(wl.SK_ALP_UNS_PATH)

        if not connected then
            p_err("Fail to connect to socket, retrying in 1 sec")
            nixio.nanosleep(1)
        else
            break
        end
    end

    -- Set com socket to non blocking
    -- If there is nothing to read
    -- The coroutine will yield and it will be re-scheduled
    -- in the select in rp_main when some data is available to be read
    g_ctx.so:setblocking(false)

    p_dbg("ALP UNS SK", g_ctx.so)

    -- Write flush un invalid subnet to the mode
    set_dst_subnet(app.SUBNET_INVALID, true)

    -- Write a valid subnet to the mote
    set_dst_subnet(app.SUBNET_VALID)

    return g_ctx.so
end

-- Return the public object
return rp_com
