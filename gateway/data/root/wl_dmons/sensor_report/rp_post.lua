----------------------------------------
-- Dependencies
----------------------------------------
local p_color     = require "p_color"
local util        = require "luci.util"
local httpclient  = require "luci.httpclient"
local nixio       = require "nixio"
local nixio_util  = require "nixio.util"
local socket      = require "socket"
local wl          = require "wl"
local alp         = require "alp_parser"
local bit         = require "bit"
local rp_lib      = require "rp_lib"
----------------------------------------

----------------------------------------------------
-- Print functions
-- Set an empty function to disable a print
----------------------------------------------------
local p_err = function (...) p_color.p_err("PT:", unpack(arg)) end
--local p_dbg = function (...) p_color.p_dbg("PT:", unpack(arg)) end
local p_dbg = function (...) end
local p_inf = function (...) p_color.p_inf("PT:", unpack(arg)) end

---------------------------
-- Public object
---------------------------
rp_post = {}

----------------------------------------------------
-- Private context
----------------------------------------------------
local g_ctx = {}

-- =================================================
-- http_post
-- =================================================
-- @brief   Perform an http post by calling the
--          app.http_request in the app.lua file
--          This functions should yield while waiting
--          for the response, passing the socket that
--          we are waiting for data to arrive
--          This function is meant to be integrated in
--          a coroutine. The first time this coroutine
--          is resumed, a list of sensors should be
--          passed as a parameter
-- @param   sensors: The list of sensors to be posted
-- @retval  1) nil,msg: If error
--          2) The http response
----------------------------------------------------
local function http_post(sensors)

    -- Save first post time
    if not g_ctx.start_time then
        g_ctx.start_time = os.time()
    end

    -- Save a time stamp before doing the request
    local t_http = socket.gettime()

    local response_body = {}


    -- Request and yield while waiting the server response
    local ret, code, _, res = app.http_request(sensors, response_body)


    -- Get the elapsed time of the request
    t_http = socket.gettime() - t_http

    -- Update max time
    if not g_ctx.t_http_max or t_http > g_ctx.t_http_max then
        g_ctx.t_http_max = t_http
    end

    -- Calculate the mean http request time
    if not g_ctx.t_http_mean then
        g_ctx.t_http_mean = t_http
    else
        g_ctx.t_http_mean = g_ctx.t_http_mean*(1-wl.RP_MEAN_FACTOR) + wl.RP_MEAN_FACTOR*t_http
    end

    -- Check for errors
    if not ret or code ~= 200 then
        p_err("http_post failed " .. tostring(ret) .. " " .. tostring(code) .. " " .. tostring(res) .. " request etime " .. tostring(t_http) .. " mean time " .. tostring(g_ctx.t_http_mean) .. " max time " .. tostring(g_ctx.t_http_max) )

        return nil, code
    end

    p_inf("http_post OK", ret, code, res, "request etime", tostring(t_http), "mean time", tostring(g_ctx.t_http_mean), "max time", tostring(g_ctx.t_http_max), "since", tostring((os.time() - g_ctx.start_time)/60) .. " min ago")

    for k,v in pairs(response_body) do
        p_dbg("CHUNK", k, v)
    end

    return table.concat(response_body)
end

-- =================================================
-- rp_post.http_coroutine_new
-- =================================================
-- @brief   Create a coroutine that will perform a
--          post to an external server
-- @retval  The new coroutine
----------------------------------------------------
function rp_post.http_coroutine_new()
    return coroutine.create(http_post)
end

-- Start the program
return rp_post
