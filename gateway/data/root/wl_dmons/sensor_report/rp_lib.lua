----------------------------------------
-- Dependencies
----------------------------------------
local util      = require "luci.util"
local p_color   = require "p_color"
local app       = require "app"
----------------------------------------

----------------------------------------------------
-- Print functions
-- Set an empty function to disable a print
----------------------------------------------------
local p_pref = "LIB:"
local p_err = function (...) p_color.p_err(p_pref, unpack(arg)) end
local p_dbg = function (...) p_color.p_dbg(p_pref, unpack(arg)) end
local p_inf = function (...) p_color.p_inf(p_pref, unpack(arg)) end
local p_log = function (...) p_color.p_log(p_pref, unpack(arg)) end


----------------------------------------------------
-- Public object
-- To be returned by require function
----------------------------------------------------
rp_lib = {} -- packet

---------------------------
-- Context Initialization
---------------------------
-- Table containing the pending reports
-- indexed by their UIDs
rp_lib.pending = {}

-- Table containing the last sequence number received by a sensor
-- indexed by their UIDs
rp_lib.last_sn = {}

-- Table containing all the sockets that we are waiting for data and their
-- associated coroutine and other informations as the associated sensor obj
-- and the creation time of the coroutine
rp_lib.sel_table = {}

-- Received packets counter
rp_lib.rcv_pkts = 0

-- Discarded packets counter
rp_lib.lost_pkts = 0


-- =================================================
-- rp_lib.pending_reports
-- =================================================
-- @brief   Check if there are pending reports to send
--          to a remote server
-- @retval  true if there are pending reports
----------------------------------------------------
function rp_lib.pending_reports()
    return next(rp_lib.pending) ~= nil
end

-- =================================================
-- rp_lib.add_to_pending_list
-- =================================================
-- @brief   Add an object to be send to a remote server
--          This object must contain at least the .uid
--          field and the .sn field (sequence number)
-- @param   sensor  The object to be reported
-- @retval  none
----------------------------------------------------
function rp_lib.add_to_pending_list(sensor)

    -- Add this sensor in the rcv counter
    rp_lib.rcv_pkts = rp_lib.rcv_pkts + 1

    p_dbg("PKT RCVED", rp_lib.rcv_pkts, "UID", sensor.uid, "SN", sensor.sn)

    -- Add the former data from this sensor.uid
    -- in the lost count if we are replacing it
    if rp_lib.pending[sensor.uid] and  rp_lib.pending[sensor.uid].sn ~= sensor.sn then
        p_err("DISCARDING", "UID", rp_lib.pending[sensor.uid].uid, "WITH SN", rp_lib.pending[sensor.uid].sn, "REPLACED BY", sensor.sn)
        rp_lib.lost_pkts = rp_lib.lost_pkts + 1
    end

    rp_lib.pending[sensor.uid] = sensor
    -- Save last sequence number
    rp_lib.last_sn[sensor.uid] = sensor.sn
end

-- =================================================
-- rp_lib.reinsert_pending_list_if_necessary
-- =================================================
-- @brief   If a http post has failed, call this function
--          to reinsert the report objects into the pending
--          list if non new report to this uid has arrived,
--          otherwise it will discard the object.
-- @param   sensor  A list of sensors to be re-inserted
--                  in the pending list
-- @retval  none
----------------------------------------------------
function rp_lib.reinsert_pending_list_if_necessary(sensors)
    for i, sensor in ipairs(sensors) do
        -- If the slot is empty, we maybe able to re-insert it
        if not rp_lib.pending[sensor.uid] then

            -- Check if this is not a past report
            if rp_lib.last_sn[sensor.uid] == sensor.sn then
                rp_lib.pending[sensor.uid] = sensor

            -- Add this sensor in the lost counter
            -- if we are not re-adding it in the pending reports
            else
                p_err("DISCARDING (past)", "UID", sensor.uid, "WITH SN", tostring(sensor.sn), "LAST SN", tostring(rp_lib.last_sn[sensor.uid]))
                -- Add this sensor in the lost counter
                rp_lib.lost_pkts = rp_lib.lost_pkts + 1
            end

        -- The slot is not empty
        -- if we are not re-adding it in the pending reports
        elseif rp_lib.pending[sensor.uid].sn ~= sensor.sn then
            p_err("DISCARDING", "UID", sensor.uid, "WITH SN", sensor.sn, "CURRENT SN", rp_lib.pending[sensor.uid].sn)
            -- Add this sensor in the lost counter
            rp_lib.lost_pkts = rp_lib.lost_pkts + 1
        end
    end
end

-- =================================================
-- rp_lib.rm_from_pending_list
-- =================================================
-- @brief   Retrieve and remove a list of sensors from
--          the pending list.
-- @retval  A list of sensors to be posted
----------------------------------------------------
function rp_lib.rm_from_pending_list()
    local sensors

    for i=1,app.RP_GROUP_N do
        -- Get sensor from list
        local uid, sensor = next(rp_lib.pending)

        if not sensor then
            break
        end

        -- Initialize the list if it is not
        -- initialized yet
        sensors = sensors or {}

        -- Insert sensor in return list
        table.insert(sensors, sensor)

        -- Remove this sensor from the list
        rp_lib.pending[uid] = nil
    end

    return sensors
end

-- =================================================
-- rp_lib.close_all_sockets
-- =================================================
-- @brief   Close all sockets in the sel_table (select
--          table) used in the main scheduler to
--          wait for new available data
-- @retval  none
----------------------------------------------------
function rp_lib.close_all_sockets()
    -- XXX:  Do more tests to enable this
    --for sk in pairs(rp_lib.sel_table) do
    --    sk:close()
    --end
end

-- =================================================
-- rp_lib.pkts_stat
-- =================================================
-- @brief   Get a string with the status of lost packets
-- @retval  A status string
----------------------------------------------------
function rp_lib.pkts_stat()
    return "Received pkts: " .. tostring(rp_lib.rcv_pkts) .. " Lost pkts: " .. tostring(rp_lib.lost_pkts) .. " Percentage: " .. tostring(rp_lib.lost_pkts / rp_lib.rcv_pkts)
end

-- Return the public object
return rp_lib
