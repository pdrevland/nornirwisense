-- Setting path to look for libraries
-- XXX: this is not a good place, we won't be able to move the code to another file
package.path = (package.path or "") .. ";/root/wl_dmons/?.lua"

----------------------------------------
-- Dependencies
----------------------------------------
local color    = require "ansicolors"
local wl       = require "wl"
----------------------------------------

----------------------------------------------------
-- Public object
-- To be returned by require function
----------------------------------------------------
p_color = {}

-- =================================================
-- buil_msg
-- =================================================
-- @brief   Concatenate the prefix and list of arguments
-- @retval  The concatenated string
----------------------------------------------------
local function build_msg(pref, arg)
    local msg = pref or ""

    for i=1,arg.n do
        msg = msg .. "\t" .. tostring(arg[i])
    end

    return msg
end

-- =================================================
-- p_color.p_log
-- =================================================
-- @brief   Print a log type message and save it in
--          the wl.ERR_LOG_FILE
-- @retval  none
----------------------------------------------------
function p_color.p_log(pref, ...)
    local ts = os.date("[%y/%m/%d-%Hh%Mm%Ss]", os.time())

    local msg = ts .. build_msg(pref, arg) .. "\n"

    print(color.onblue("LOG:") ..  color.white .. msg)
    io.write(tostring(color.reset))

    -- Log message to file with time stamp
    wl.log(p_color.log_obj, msg)
end


-- =================================================
-- p_color.p_err
-- =================================================
-- @brief   Print an error type message and save it in
--          the wl.ERR_LOG_FILE
-- @retval  none
----------------------------------------------------
function p_color.p_err(pref, ...)
    local ts = os.date("[%y/%m/%d-%Hh%Mm%Ss]", os.time())

    local msg = ts .. build_msg(pref, arg) .. "\n"

    print(color.onred("ERR:") ..  color.red, msg)
    io.write(tostring(color.reset))

    -- Log message to file with time stamp
    wl.log(p_color.log_obj, msg)
end

-- =================================================
-- p_color.p_dbg
-- =================================================
-- @brief   Print a debug type message
-- @retval  none
----------------------------------------------------
function p_color.p_dbg(pref, ...)
    print(color.yellow("DBG:") ..  color.yellow .. pref, unpack(arg))
    io.write(tostring(color.reset))
end

-- =================================================
-- p_color.p_inf
-- =================================================
-- @brief   Print an information type message
-- @retval  none
----------------------------------------------------
function p_color.p_inf(pref, ...)
    print(color.onblue("INF:") ..  color.white .. pref, unpack(arg))
    io.write(tostring(color.reset))
end

-- =================================================
-- under_pcall
-- =================================================
-- @brief   Check if a function was called under
--          pcall lua function
-- @retval  true if pcall is in the stack of caller
--          function
----------------------------------------------------
local function under_pcall()
    local i = 3

    local calling_func = debug.getinfo(i)

    while calling_func do
        if calling_func.name == 'pcall' then
            return true
        end

        i = i + 1
        calling_func = debug.getinfo(i)
    end
end

-- =================================================
-- error
-- =================================================
-- @brief   Override the current error function to
--          log the message before raising the error
-- @retval  none
----------------------------------------------------
local old_error_func = error
error = function(msg, lvl)
            lvl = lvl and lvl + 1 or 2
            local info = debug.getinfo(lvl, "Sl")
            local pref = string.format("[%s]:%d", info and info.short_src, info and info.currentline)
            p_color.p_err(pref, msg)

            -- If this error was not under a protected call
            -- then assert and reset the dmons
            if not under_pcall() then
                p_color.p_err(pref, "ASSERT. RESETING DMONS ...")

                local rst_wd = wl.wd_connect(wl.PIPE_WD_RST_DMONS_PATH, p_err)
                rst_wd:write('e')
                rst_wd:close()
            end

            old_error_func(msg, lvl)
        end

-- Open log file
p_color.log_obj = wl.log_init(wl.ERR_LOG_FILE)

return p_color
