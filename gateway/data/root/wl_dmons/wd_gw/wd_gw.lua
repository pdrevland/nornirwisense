#!/usr/bin/lua

-- Setting path to look for libraries
-- XXX: this is not a good place, we won't be able to move the code to another file
package.path = (package.path or "") .. ";/root/wl_dmons/?.lua"

-- Dependencies
----------------------------------------
local p_color     = require "p_color"
local util        = require "luci.util"
local httpclient  = require "luci.httpclient"
local json        = require "luci.json"
local nixio       = require "nixio"
local wl          = require "wl"
local socket      = require "socket"
local app         = require "app"
----------------------------------------

----------------------------------------------------
-- Print functions
-- Set an empty function to disable a print
----------------------------------------------------
local p_pref = "WD:"
local p_err = function (...) p_color.p_err(p_pref, unpack(arg)) end
local p_dbg = function (...) p_color.p_dbg(p_pref, unpack(arg)) end
local p_inf = function (...) p_color.p_inf(p_pref, unpack(arg)) end
local p_log = function (...) p_color.p_log(p_pref, unpack(arg)) end


-----------------------------------------
-- Global private context
-----------------------------------------
local g_ctx = {}
-- The list of pipes we are watching for new data
-- This variable is used to perform a select
g_ctx.watch_sk_list = {}
-- The tick counter for ticking the hardware watchdog
g_ctx.ticks = 0
-- Mode definition
-- The watch dog can work in two modes:
--
--      0- CLASSIC: Wait for ticks, execute act if no ticks were made
--                  between the last tick time and tact time. Execute
--                  reboot if no ticks were made between the last tick
--                  time and tboot time.
--
--      1- ON_ERR:  The character 'e' is used to signalize an error and
--                  the character 'r' is used to reset the errors.
--                  When a character 'e' is received, the watchdog save
--                  the error time and waits for the  'r' character
--                  ignoring any other character (even 'e').
--                  If no 'r' character is received between the error
--                  time and the tact time, act is executed.
--                  It no 'r' character is received between the error
--                  time and the tboot time, reboot is executed.
g_ctx.mode = {CLASSIC=0, ON_ERR=1}

-- The name of the post to serv process to turn off the mote when the action
-- is taken and to turn it on again when the timer is reseted
local POST_TO_SERV_PROCESS = "post_to_serv"

-- =================================================
-- reboot
-- =================================================
-- @brief   Reboot the gateway
-- @retval  none
----------------------------------------------------
local function reboot()
    nixio.nanosleep(1)

    wl.hrst_gateworks()

    while true do end
end

-- =================================================
-- close_all_fds
-- =================================================
-- @brief   close all opened file descriptors
-- @retval  none
----------------------------------------------------
local function close_all_fds()
    for _,el in ipairs(app.watch_table) do
        if el.fd then el.fd:close() end
    end
end

----------------------------------------------------
-- FUNCTIONS
----------------------------------------------------

-- =================================================
-- time_since_boot()
-- =================================================
-- @brief   Get the time in seconds since boot from
--          /proc/uptime file
-- @retval  none
----------------------------------------------------
local function time_since_boot()
    local fd
    local time

    repeat
        -- Open the uptime file
        fd = io.open('/proc/uptime')

        if fd then
            -- Read a number
            local str = fd:read("*number")
            -- CLose the file descriptor
            fd:close()
            -- Transform the string that we read into
            -- a number
            time = tonumber(str)
            p_dbg("Uptime", time, str)
        else
            p_err("Fail to read uptime, retrying in 1 sec")
            nixio.nanosleep(1)
        end

    until time


    return time
end

-- =================================================
-- tick_hw_wd
-- =================================================
-- @brief   Tick the hardware watch dog
-- @retval  none
----------------------------------------------------
local function tick_hw_wd()

    p_log("HW WATCH DOG TICK")

    -- Send the tick to the watch dog file
    -- TODO: do not open a bash to tick the watchdog
    -- open the file instead
    os.execute("echo -n T > /dev/watchdog")

end

-- =================================================
-- configure_hw_wd
-- =================================================
-- @brief   Configure the hardware watchdog reboot
--          timer
----------------------------------------------------
local function configure_hw_wd()
    -- Kill the current watchdog soft process
    -- restart a new watchdog process reprogramming the timer
    -- kill it again to stop auto ticks
    local cmd = "killall watchdog; watchdog -T %d /dev/watchdog; sleep 1; killall watchdog"
    cmd = string.format(cmd, wl.WD_HW_REBOOT_TIMER)
    p_log("Executing cmd", cmd)
    p_log("Uptime", time_since_boot())
    os.execute(cmd)

    -- Initial tick
    tick_hw_wd()
end

-- =================================================
-- how_many_classic_el
-- =================================================
-- @brief   Counts how many watched elements in the
--          watch table are of CLASSIC mode
--          Each time a classic mode ticks we update
--          the g_ctx.ticks variable, when it reaches
--          the quantity of CLASSIC modes, we tich
--          the hardware watchdog
-- @retval  none
----------------------------------------------------
local function how_many_classic_el()
    local n = 0

    for _,el in ipairs(app.watch_table) do
        if el.mode == g_ctx.mode.CLASSIC then
            n = n + 1
        end
    end

    return n
end

-- =================================================
-- check_hw_wd_tick
-- =================================================
-- @brief   Check if it is time to tick the hardware
--          WatchDog and ticks it if so.
--          If all CLASSIC modes elements have ticked,
--          Then we tick the hw WatchDog and reset the
--          g_ctx.ticks counter
-- @retval  none
----------------------------------------------------
local function check_hw_wd_tick()
    p_dbg("CHECK HW TICK", g_ctx.ticks, table.getn(app.watch_table))

    -- If all the process (in CLASSIC mode) have ticked at least once
    -- then tick the hardware watchdog
    if g_ctx.ticks == g_ctx.classic_el then
        tick_hw_wd()

        -- Clean all ticked flags
        for _,el in ipairs(app.watch_table) do
            el.ticked = nil
        end

        g_ctx.ticks = 0

        p_dbg("HW TICK CLEAR", g_ctx.ticks, table.getn(app.watch_table))
    end

end

-- =================================================
-- get_remaining_time
-- =================================================
-- @brief   Get the remaining time a tick must occur
--          before the next action for a given process
-- @param   el  table   The element in the watch_table
--                      to calculate the remaining time
-- @param   now number  The reference Unix time to
--                      calculate the remaining time
-- @return  number      The remaining time for the element
--          nil         Return nil if the remaining time
--                      is not relevant (non active
--                      ON_ERR elements)
----------------------------------------------------
local function get_remaining_time(el, now)

    -- If el is in ON_ERR mode and it is not active
    -- return nil
    if el.mode == g_ctx.mode.ON_ERR and not el.last then
        return nil
    end

    -- Initialize el.last if it is the first time we execute this function
    el.last = el.last or now

    local etime = now - el.last

    -- Get remaining time to execute the action
    local remaining = el.tact - etime

    -- If we still have time, just return this remaining time
    if remaining > 0 then
        return remaining
    end

    -- If we don't have time, check if the action was already
    -- done between the last tick
    if el.act_done then
        -- If it was already done, return the remaining time to reboot

        -- If the uptime is not enough to reboot
        -- and reference time to reboot is smaller then min_uptime_to_reboot
        -- return the effective remaining time considering the min time to
        -- reboot
        if now < wl.WD_MIN_UPTIME_TO_REBOOT and el.tboot + el.last < wl.WD_MIN_UPTIME_TO_REBOOT then
            remaining = wl.WD_MIN_UPTIME_TO_REBOOT - now
            p_dbg("Remaining time to reboot is before the minimum uptime allowed to reboot.\nChanging the remaining time to", remaining, "instead of", el.tboot - etime)
        else
            remaining = el.tboot - etime
        end

        if remaining < 0 then
            return 0
        end

        return remaining
    else
        -- If not, the action must be done now, return 0 so the poll
        -- won't block and we will execute the action just after the
        -- poll in the main loop
        return 0
    end

end

-- =================================================
-- get_min_to
-- =================================================
-- @brief   Get the minimum remaining time from all
--          watched process. This is the maximum time
--          we can sleep before taking any action if
--          no tick is made
-- @return  number  The max time we can sleep without
--                  receiving a tick from any watched
--                  processes in seconds
----------------------------------------------------
local function get_min_to()
    local now = time_since_boot()

    local to

    -- For all process, calculate the remaining time
    -- to take an action and get the lowest value
    for _,el in ipairs(app.watch_table) do

        -- Get how much time the process has to report
        local remaining = get_remaining_time(el, now)

        p_dbg("REMAINING", tostring(remaining), el.name, el.act_done and "REBOOT" or "ACT", el.mode == g_ctx.mode.CLASSIC and "CLASSIC" or "ON_ERR")

        -- If el mode is CLASSIC or a ON_ERR with an active timer
        -- the remaining won't be nil.
        -- If el mode is ON_ERR with an inactive timer, the
        -- remaining will be nil
        if remaining then

            -- If the remaining is zero, we can return. There
            -- is no smaller number then 0 for remaining time
            if remaining == 0 then
                return 0
            end

            -- First case when to is nil
            if not to then
                to = remaining

            -- Get the lowest to value
            else
                to = to < remaining and to or remaining
            end
        end
    end

    p_dbg("NOW", now, "TO MIN", to)
    return to
end

-- =================================================
-- check_err_sig_in_str
-- =================================================
-- @brief   Check for 'e' or 'r' characters in a ON_ERR
--          mode element.
--          If we receive an 'e', it means that we have
--          an error and we need to activate its timer:
--          save the time of the first non reseted error
--          to activate the WD timer and increment the
--          error counter of this element.
--          If we receive an 'r', it means that we can
--          deactivate the timer: Reset the errors counter,
--          the action to be taken (if it was waiting to
--          reboot) and erase the last error time.
-- @param   el: The element in ON_ERR mode that we are
--              analyzing.
-- @param   str: The string to look for the 'e' or 'r'
--                  characters
-- @retval  none
----------------------------------------------------
local function check_err_sig_in_str(el, str)

    -- Iterate through all characters in str
    for c in str:gmatch"." do
        -- If we found an 'e' character
        if c == 'e' then

            -- If this is the first error 
            -- Save the error time to activate the timer
            -- Now, the get_remaining_time won't return
            -- nil anymore for this element
            if not el.last then

                -- Save the time of the first error
                el.last = time_since_boot()

                p_log("ON ERR TIMER STARTED", el.name)
            end

            -- Count the number of errors
            -- for log and debug
            el.err_n = (el.err_n or 0) + 1

        -- If we found a 'r' character
        elseif c == 'r' then

            -- If we have an active timer, remove it, otherwise
            -- do nothing
            if el.last then
                p_log("ON ERR TIMER RESETED", el.name)

                -- Erase the error time, we are good
                el.last = nil

                if el.act_done then

                    -- Reset the action
                    -- Now, the get_remaining_time will return nil
                    el.act_done = nil

                    -- XXX: This is a special case, the post_to_serv
                    -- turns off the mote if we fail, then we need to
                    -- turn it on again here
                    if el.name == POST_TO_SERV_PROCESS then
                        p_log("Turning mote on")
                        wl.mote_on()
                    end
                end

            end

            -- Clear the errors counter
            el.err_n = 0

        end
    end

end

-- =================================================
-- check_err_sig
-- =================================================
-- @brief   Read the pipe and look for error or reset
--          signals for el.mode.ON_ERR
-- @return  true if we the timer is active (we are
--          still waiting for the reset command to
--          reset the error timer
----------------------------------------------------
local function check_err_sig(el)
    local msg

    repeat
        msg = el.fd:read(wl.SK_READ_LEN)

        p_dbg("GOT", msg)

        if msg and #msg ~= 0 then
            check_err_sig_in_str(el, msg)
        end

    until not msg

    p_dbg("ON_ERR", el.name, el.last, "ERR COUNT", el.err_n)

    return el.last and true or false
end

-- =================================================
-- check_ticked
-- =================================================
-- @brief   Read the pipe and check if a given entry
--          of the watch_table in CLASSIC mode was ticked.
-- @return  bool    true if it was ticked, false otherwise
----------------------------------------------------
local function check_ticked(el)
    local msg
    local ticked = false

    repeat
        msg = el.fd:read(wl.SK_READ_LEN)

        p_dbg("GOT", msg)

        if msg and #msg ~= 0 then
            ticked = true
        end
    until not msg

    -- Set the ticked flag for the hw watch dog
    if ticked and not el.ticked then
        el.ticked = true
        g_ctx.ticks = g_ctx.ticks + 1
        p_dbg("UPDATING TICK", el.name, g_ctx.ticks)
    end


    return ticked
end

-- =================================================
-- check_trigger_time
-- =================================================
-- @brief   Check the timing of a given entry of the
--          watch_table and trigger the appropriate
--          actions if necessary
----------------------------------------------------
local function check_trigger_time(el)
    local now = time_since_boot()

    -- If el.last does not exist (ON_ERR not active)
    -- etime is zero
    local etime = now - (el.last or now)

    p_dbg(el.name, "didn't respond. Remaining time reboot", el.tboot - etime, "Remaining time tact", el.tact - etime, "NOW", now)

    -- Check boot timeout
    if etime >= el.tboot and time_since_boot() > wl.WD_MIN_UPTIME_TO_REBOOT then
        p_err(el.name .. " REBOOT TIMEOUT. REBOOTING ...")
        -- Reboot
        reboot()
    -- Check tact timeout
    elseif etime >= el.tact and not el.act_done then
        p_err(el.name .. " ACTION TIMEOUT. EXECUTING ACTION ... " .. tostring(el.err_msg) )
        -- Execute the action
        el.act()
        -- Set the flags that says the action has being executed
        el.act_done = true

        -- Turn the mote off if we are executing the action of post_to_serv
        -- XXX: This is a special process that we turn off the mote when the
        -- action is take until the reset of the timer (until the post is successful)
        if el.name == POST_TO_SERV_PROCESS then
            p_log("Turning mote off")
            wl.mote_off()
        end
    end
end

-- =================================================
-- open_pipes
-- =================================================
-- @brief   Open the pipes configured in the watch_table
--          in app.lua file
-- @retval  none
----------------------------------------------------
local function open_pipes()
    for _,el in ipairs(app.watch_table) do

        -- Create the pipe if it does not exist
        local created, code, msg = nixio.fs.mkfifo(el.pipe_path, "777")

        -- Check errors
        if not created and msg ~= "File exists" then
            close_all_fds()
            error("Could not create pipe " .. tostring(el.pipe_path))
        end

        -- Open pipe
        -- Open in read AND WRITE mode. We need the write mode
        -- to not block when open and to enable the select function
        -- to return just if there is something different of EOF.
        -- For that, the pipe must be keep opened for writing
        -- by someone, the OS won't send EOF as a new character
        -- to be read all the time.
        el.fd = nixio.open(el.pipe_path, nixio.open_flags("rdwr", "nonblock"))

        if not el.fd then
            error("Could not open pipe " .. tostring(el.pipe_path))
        end

        -- Encapsulate the file descriptor in a tcp socket
        -- to allow the usage of the select function
        el.so = socket.tcp()
        el.so:setfd(el.fd:fileno())

        -- Insert in the select list
        table.insert(g_ctx.watch_sk_list, el.so)
    end
end



-- =================================================
-- main
-- =================================================
-- @brief   main entry.
--          Configure the hardware watchdog. Open the
--          pipes, count how many elements in CLASSIC
--          mode we have and start the main loop.
--          In the main loop we calculate the minimum
--          time that we can sleep before taking an
--          action.
--          We perform a socket select in the pipes to
--          wait for data to become available. Depending
--          on the mode (CLASSIC or ON_ERR) and on the
--          time, we take the proper actions
-- @retval  none
----------------------------------------------------
local function main()
    -- Check if the watch dog should be enabled
    -- or not. End the program if it is set
    -- to be disabled
    if not app.SOFT_WATCH_DOG_ENABLED then
        p_log("WatchDog is not active")
        return
    end

    p_log("Start")

    -- Do not execute if this program is already running
    if not wl.one_prog_running(arg[0]) then
        p_err("This program is already running")
        return
    end

    -- Configure hardware watchdog
    configure_hw_wd()

    -- Open the pipes
    open_pipes()

    -- Get how many classic mode elements we have
    g_ctx.classic_el = how_many_classic_el()

    p_dbg("WD Start")

    while true do
        -- Get the timeout from the watch_table with shortest response time
        -- This is the maximum time we can block in the socket.select function
        local to = get_min_to()

        -- Wait until there is something to read in one of the pipes
        -- that we are listen to
        local ok = socket.select(g_ctx.watch_sk_list, nil, to)

        p_dbg("Pipe select", ok, to)

        -- If none sockets responded
        -- if not ok then
            -- XXX: Can we optimize here? We still need to iterate in all the entries
            -- One of the sockets has ran out of time
        -- end

        -- Iterate through every watcher process in watch_table
        for i,el in ipairs(app.watch_table) do

            -- If this is a on error mode
            -- Check for error or rst signal
            if el.mode == g_ctx.mode.ON_ERR then

                local timer_active = check_err_sig(el)
                -- If the timer is active, check the timer and actions
                -- to be taken
                if timer_active then
                    check_trigger_time(el)
                end

            -- If this is the classic mode element
            else
                local ticked = check_ticked(el)

                -- We receive something with this socket
                if ticked then
                    -- Check if we need to tick the hardware watchdog
                    check_hw_wd_tick()

                    -- Reset its timer
                    el.last = time_since_boot()

                    -- Reset the action flag
                    el.act_done = nil

                    p_inf(el.name, "is alive!")

                -- If we didn't receive a tick from this element, check if it is
                -- still in time and take the appropriate action if timeout
                else
                    check_trigger_time(el)
                end

            end

        end
    end
end


----------------------------------------------------
-- Start the program
----------------------------------------------------
main()
