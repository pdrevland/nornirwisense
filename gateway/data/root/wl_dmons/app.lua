package.path = (package.path or "") .. ";/root/wl_dmons/?.lua"

----------------------------------------
-- Dependencies
----------------------------------------
local ssl         = require "ssl"
local json        = require "luci.json"
local socket      = require "socket"
local ltn12       = require "ltn12"
local nixio       = require "nixio"
local nixio_util  = require "nixio.util"
local async_https = require "async_https"

----------------------------------------

  ----------------------------------------------------
  -- Print functions
  -- Set an empty function to disable a print
  ----------------------------------------------------
  local p_pref = "APP:"
  local p_err = function (...) p_color.p_err(p_pref, unpack(arg)) end
  local p_dbg = function (...) p_color.p_dbg(p_pref, unpack(arg)) end
  local p_inf = function (...) p_color.p_inf(p_pref, unpack(arg)) end
  local p_log = function (...) p_color.p_log(p_pref, unpack(arg)) end

----------------------------------------------------
-- Set the library object
----------------------------------------------------
app = {}

-- =================================================
-- INTERNAL_REPORT_ENABLED
-- =================================================
-- Enable or disable the internal reports
-- If it is enable, the last data of the
-- sensors will be saved into a file
-- so the internal LuCI page can be populated
----------------------------------------------------
app.INTERNAL_REPORT_ENABLED = true

-- =================================================
-- SOFT_WATCH_DOG_ENABLED
-- =================================================
-- Enable or disable the software watchdog
-- @see app.watch_table below
----------------------------------------------------
app.SOFT_WATCH_DOG_ENABLED = true

-- =================================================
-- REPORT_TO_REMOTE_SERVER
-- =================================================
-- Enable or disable the http post to
-- an external server
----------------------------------------------------
app.REPORT_TO_REMOTE_SERVER = false

-- =================================================
-- REMOTE_REPORT_USE_SSL
-- =================================================
-- Enable or disable SSL when reporting to
-- an external server
----------------------------------------------------
app.REMOTE_REPORT_USE_SSL = false

-- =================================================
-- RP_GROUP_N
-- =================================================
-- If REPORT_TO_REMOTE_SERVER is enabled
-- set the max number or elements in the
-- sensor list when calling the app.http_request
-- function
-- @see app.http_request below
----------------------------------------------------
app.RP_GROUP_N =   1

----------------------------------------------------
-- DST SUBNET DEFINITIONS
----------------------------------------------------
-- Set SUBNET_INVALID to zero for subnet protection
-- to work
-- When the program starts it writes permanently a
-- invalid subnet and writes a volatile valid subnet,
-- thus if the program crashes or if the gateway restarts,
-- the invalid subnet takes place preventing the mote
-- to communicate with the remote sensors
app.SUBNET_VALID    =   0xD4
app.SUBNET_INVALID  =   app.SUBNET_VALID

----------------------------------------------------
-- WATCHDOG PROCESS TABLE
----------------------------------------------------
-- tboot, tact and act are mandatory
-- tboot > tact must always be true
-- if you want just to reboot with timeout to, you can
-- set tboot = to+1, tack = to and act = reboot
-- err_msg is the message to be printed when act is executed
--
-- The watch dog can work in two modes:
--
--      0- CLASSIC: Wait for ticks, execute act if no ticks were made
--                  between the last tick time and tact time. Execute
--                  reboot if no ticks were made between the last tick
--                  time and tboot time.
--
--      1- ON_ERR:  The character 'e' is used to signalize an error and
--                  the character 'r' is used to reset the errors.
--                  When a character 'e' is received, the watchdog save
--                  the error time and waits for the  'r' character
--                  ignoring any other character (even 'e').
--                  If no 'r' character is received between the error
--                  time and the tact time, act is executed.
--                  It no 'r' character is received between the error
--                  time and the tboot time, reboot is executed.
app.watch_table = {
  {name = "sensor_report",    pipe_path = wl.PIPE_WD_RE_PATH,     tboot = wl.WD_RE_REBOOT_TIMER,  tact = wl.WD_RE_ACT_TIMER,  act = wl.rst_dmons,     err_msg = "RST_DMONS",  mode = 0},
  {name = "rst_dmons",        pipe_path = wl.PIPE_WD_RST_DMONS_PATH, tboot = 99999999,            tact = 1,                   act = wl.rst_dmons,     err_msg = "RST_DMONS",  mode = 1},
  -- XXX: The post_to_serv is a special case that turn the mote on and off, do not change its name
  app.REPORT_TO_REMOTE_SERVER and {name = "post_to_serv",     pipe_path = wl.PIPE_WD_POST_PATH,   tboot = wl.WD_POST_REBOOT_TIMER,tact = wl.WD_POST_ACT_TIMER,act = wl.rst_network,   err_msg = "RST_NET",    mode = 1} or nil,
}

-- =================================================
-- parse_sensor
-- =================================================
-- @brief   Function called to parse notifications
-- @retval  Return the parsed object or nil if the
--          notification file is unknown
----------------------------------------------------
function app.parse_sensor(pkt_parsed)
  local obj = {uid = pkt_parsed.uid}
  obj.lb    = pkt_parsed.lb
  -- time in ms
  obj.ts    = socket.gettime()*1000
  -- Seqnum
  obj.sn    = wl.get_byte(pkt_parsed.file_chunk, 1)
  -- Retry
  obj.rt    = wl.get_byte(pkt_parsed.file_chunk, 2)

  -- If this is a button 1 file
  if pkt_parsed.file_id == wl.REPORT_BTN1_FILE_ID then
    obj.btn1 = socket.gettime()*1000

  elseif pkt_parsed.file_id == wl.REPORT_BTN2_FILE_ID then
    obj.btn2 = socket.gettime()*1000

  elseif pkt_parsed.file_id == wl.REPORT_TEMP_FILE_ID then
    obj.tmp    = wl.bytes_to_int(string.sub(pkt_parsed.file_chunk, 6, 7), true)

  elseif pkt_parsed.file_id == wl.REPORT_HUM_FILE_ID then
    obj.hum    = wl.get_byte(pkt_parsed.file_chunk, 6)

  elseif pkt_parsed.file_id == wl.REPORT_PRE_FILE_ID then
    obj.pre    = wl.bytes_to_int(string.sub(pkt_parsed.file_chunk, 6, 7), true)

  elseif pkt_parsed.file_id == wl.REPORT_ADC_FILE_ID then
    obj.lig    = wl.bytes_to_int(string.sub(pkt_parsed.file_chunk, 6, 7), true)

  elseif pkt_parsed.file_id == wl.REPORT_ACC_FILE_ID then
    obj.x    = wl.bytes_to_int(string.sub(pkt_parsed.file_chunk, 6, 7), true)
    obj.y    = wl.bytes_to_int(string.sub(pkt_parsed.file_chunk, 8, 9), true)
    obj.z    = wl.bytes_to_int(string.sub(pkt_parsed.file_chunk, 10, 11), true)

  else
    -- Unknown wisense file
    return nil
  end

  return obj
end

-- =================================================
-- http_request
-- =================================================
-- @brief   Function called to perform a post to a
--          remote server
-- @retval  The return of the socket.http.trequest
--          function
----------------------------------------------------
--- Change this parameters to your own: ip, path, port, protocol and the data objectID
function app.http_request(sensors, response_body)
  -- Assemble the POST parameters
  local ip = "xx.xx.xx.xx"
  local path = "/path/to/DOMAIN/SERVICE"
  local port
  local protocol
  if app.REMOTE_REPORT_USE_SSL then
    protocol= "https"
    port = "8888"
  else
    protocol="http"
    port = "8080"
  end
  -- Assemble the uri to post the data
  local uri = protocol .. "://" .. ip .. ":" .. port .. path
  local file = wl.get_gw_config() or {}
  if file then
    uid =  tostring(file.uid)
  end
  -- objectID HARD CODED
  local data = "objectID=1000"..
    "&GATEWAYID=" .. tostring(uid)..
    "&WISENSEID=" .. tostring(sensors[1].uid)..
    "&SERVICEID=" .. tostring(sensors[1].ser) ..
    "&LB=" .. tostring(sensors[1].lb)..
    "&SN=" .. tostring(sensors[1].sn)..
    "&RT=" .. tostring(sensors[1].rt)..
    "&FILEID=" .. tostring(sensors[1].fid)..
    "&OFFSET=" .. tostring(sensors[1].offset)..
    "&TIMESTAMP=" .. tostring(sensors[1].ts)..
    "&DATA=" .. tostring(sensors[1].dat)
  -- Informative print
  p_inf("http_post to ", uri, " DATA ", data)

  local req_params = {
    url = uri,
    headers = {
      ["content-type"] = "application/x-www-form-urlencoded",
      ["content-length"] = tostring(#data),
      ["Host"] = ip .. ":" .. port,
      ["Synx-Cat"] = "1",
    },
    method = "POST",
    source = ltn12.source.string(data),
    sink = ltn12.sink.table(response_body)
  }

  -- Request and yield while waiting the server response
  if app.REMOTE_REPORT_USE_SSL then
    return async_https.https_request(req_params)
  else
    return socket.http.trequest(req_params, true)
  end
end

-- =================================================
-- back_off_fail
-- =================================================
-- @brief   Function called to calculate the new back off delay
--          when a post to the remote server fails
-- @param   The current back off delay being used by the report
--          daemong in seconds
-- @retval  The new backoff delay in seconds
----------------------------------------------------
local BACK_OFF_MAX_DELAY =   0
function app.back_off_fail(current_delay)
  local ret = current_delay+1
  if ret > BACK_OFF_MAX_DELAY then
    return BACK_OFF_MAX_DELAY
  else
    return ret
  end
end

-- =================================================
-- back_off_success
-- =================================================
-- @brief   Function called to calculate the new back off delay
--          when a post to the remote server sucecced
-- @param   The current back off delay being used by the report
--          daemong in seconds
-- @retval  The new backoff delay in seconds
----------------------------------------------------
function app.back_off_success(current_delay)
  return 0
end

----------------------------------------------------
-- Return the defined library object
----------------------------------------------------
return app
