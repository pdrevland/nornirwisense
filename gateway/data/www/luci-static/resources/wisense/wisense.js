var FEEDBACK_PER = 4000;
var WISENSE_BASE_ADDR = "/cgi-bin/luci/wisense/";

function bytes_to_number(bytes, signed)
{
    if (bytes[bytes.length-1] == ',')
    {
        bytes = bytes.substring(0, bytes.length-1);
    }

    var arr = bytes.split(",");
    var num = 0;
    var negative = signed ? arr[arr.length - 1] & 0x80 : 0;

    for (var i = arr.length; i >= 0; i--)
    {
        num = num << 8;
        num = num + Number(arr[i]);

    }

    if(negative)
    {
        console.log("Negative");
        num = num - Math.pow(2, (arr.length*8));
    }

    return num;
}

function number_to_bytes(number, lengh)
{
    var ret = ""

    for (var i = 0; i < lengh-1; i++)
    {
        var val = number & 0xff;
        ret = ret + val.toString() + ",";
        number = number >> 8;
    }

    var val = number & 0xff;
    ret = ret + val.toString();

    return ret;
}

// Get unix_ts (in miliseconds) in a formated string
function get_unix_formatted_time(unix_ts)
{
    if (unix_ts == undefined)
    {
        return UNDEFINED_STR + ':' + UNDEFINED_STR + ':' + UNDEFINED_STR;
    }

    // create a new javascript Date object based on the timestamp
    // multiplied by 1000 so that the argument is in milliseconds, not seconds
    var date = new Date(unix_ts);
    // hours part from the timestamp
    var hours = date.getHours();
    // minutes part from the timestamp
    var minutes = date.getMinutes();
    // seconds part from the timestamp
    var seconds = date.getSeconds();

    // will display time in 10:30:23 format
    var formattedTime = hours + ':' + minutes + ':' + seconds;

    return formattedTime;
}

function btn_set_color(circle, pressed, gw_time)
{
    var btn_color;

    if(!pressed || gw_time - pressed >= BTN_DUR)
    {
        console.log("White");
        btn_color = "white";

    }
    else
    {
        console.log("Green");
        // Set the pressed color
        btn_color = "green";

        // Set timer to unset this color
        setTimeout(function() {btn_set_color(circle, false); }, BTN_DUR);
    }
    circle.css("background-color", btn_color);
}

function create_button_div(title, pressed, div_class, gw_time)
{
    // Create content div
    var div = $('<div class="' + div_class + '"></div>');

    // Create table for tabular data
    var table = $('<table></table>');
    div.append(table);

    table.append($('<col width="70">'))
    table.append($('<col width="80">'))
    table.append($('<col width="100">'))

    // Create sub circle
    var circle = $('<div class="circle" style="width:35px;height:35px;float:left"></div>');
    var row = $('<tr></tr>');
    table.append(row);
    row.append($('<td style="border-top:none; padding: 0;"></td>').append(circle));

    // Set sub circle color
    btn_set_color(circle, pressed, gw_time);

    // Pressed on date label
    var pressed_label = $('<div style="float:left"> Pressed at:</div>');
    row.append($('<td style="border-top:none; padding: 0;"></td>').append(pressed_label));

    // Pressed on date value
    var pressed_value = ($('<div style="float:left" class="pressed_on"></div>').text(get_unix_formatted_time(pressed)));
    row.append($('<td style="border-top:none; padding: 0;"></td>').append(pressed_value));

    return sensor_template(title, div);
}

function update_button(btn_table, pressed, gw_time)
{
    var circle = btn_table.find("div.circle");

    btn_set_color(circle, pressed, gw_time);

    var pressed_on = btn_table.find(".pressed_on");
    pressed_on.text(get_unix_formatted_time(pressed));
}

function sensor_template(sen_title, content)
{
    // Create the returned div
    var div = $('<div class="xmlfield" data-toggle="tooltip"></div>');

    // Title
    var title = $('<div class="xmlfield-name">' + sen_title + '</div>');
    div.append(title);

    // Action line
    var action_line = $('<div class="xmlfield-action-line"></div>');
    div.append(action_line);

    // Action
    var action = $('<div class="xmlfield-action"></div>');
    action_line.append(action);

    action.append(content);

    return div;
}

function create_info_div(title, value, div_class)
{
    if (value == undefined)
    {
        value = UNDEFINED_STR;
    }
    var content = $('<div class="' + div_class + '"></div>').text(value);
    return sensor_template(title, content);
}

function update_info(info_obj, new_value)
{
    info_obj.text(new_value);
}

function create_acc_div(title, x, y, z)
{
    if (x == undefined)
    {
        x = UNDEFINED_STR;
    }
    if (y == undefined)
    {
        y = UNDEFINED_STR;
    }
    if (z == undefined)
    {
        z = UNDEFINED_STR;
    }

    // Create content div
    var div = $('<div class="acc"></div>');

    // Create table
    var table = $('<table></table>');
    div.append(table);

    table.append($('<col width="20">'))
    table.append($('<col width="50">'))
    table.append($('<col width="20">'))
    table.append($('<col width="50">'))
    table.append($('<col width="20">'))
    table.append($('<col width="50">'))

    //// Create line
    row = $('<tr></tr>');
    table.append(row);

    //// Create X column
    row.append($('<td style="border-top:none; padding: 0;"></td>').append('X:'));
    row.append($('<td class="X" style="border-top:none; padding: 0;"></td>').text(x));
    //// Create Y column
    row.append($('<td style="border-top:none; padding: 0;"></td>').append('Y:'));
    row.append($('<td class="Y" style="border-top:none; padding: 0;"></td>').text(y));
    //// Create Z column
    row.append($('<td style="border-top:none; padding: 0;"></td>').append('Z:'));
    row.append($('<td class="Z" style="border-top:none; padding: 0;"></td>').text(z));

    return sensor_template(title, div);

}

function update_acc(acc_obj, x, y, z)
{
    acc_obj.find(".X").text(x);
    acc_obj.find(".Y").text(y);
    acc_obj.find(".Z").text(z);
}

function read_file(uid, error_feedback, ok_feedback, busy_feedback, file_id, offset, file_len, fwd, cb)
{
    ok_feedback.hide();
    error_feedback.hide();
    busy_feedback.show();

    var url = WISENSE_BASE_ADDR + "../dash7/api/rf?file_id=" + file_id + "&offset=" + offset + "&file_len=" + file_len +"&fwd=" + fwd + "&uid=" + uid;

    $.getJSON(url)
            .done(function(data, status)
                    {
                        busy_feedback.hide();
                        if (data.status == "OK" && data.data[0].file_data)
                        {
                            ok_feedback.show();
                            setTimeout(function () {ok_feedback.hide();}, FEEDBACK_PER);
                            cb(data.data[0].file_data);
                        }
                        else
                        {
                            error_feedback.show();
                            setTimeout(function () {error_feedback.hide();}, FEEDBACK_PER);
                        }

                        console.log("getJSON read " + JSON.stringify(data) );
                    })
            .fail(function(jqxhr, textStatus, error)
                    {
                        busy_feedback.hide();
                        error_feedback.show();
                        setTimeout(function () {error_feedback.hide();}, FEEDBACK_PER);
                        console.error("getJSON read failed, status: " + textStatus + ", error: "+error);
                    });
}


function write_file(uid, error_feedback, ok_feedback, busy_feedback, file_id, offset, file_len, fwd, file_data)
{
    ok_feedback.hide();
    error_feedback.hide();
    busy_feedback.show();

    var url = WISENSE_BASE_ADDR + "../dash7/api/wf?file_id=" + file_id + "&offset=" + offset + "&file_len=" + file_len +"&fwd=" + fwd + "&uid=" + uid + "&file_data=" + file_data;

    $.getJSON(url)
            .done(function(data, status)
                    {
                        busy_feedback.hide();
                        if (data.status == "OK" && data.data[0].error_code == 0)
                        {
                            ok_feedback.show();
                            setTimeout(function () {ok_feedback.hide();}, FEEDBACK_PER);
                        }
                        else
                        {
                            error_feedback.show();
                            setTimeout(function () {error_feedback.hide();}, FEEDBACK_PER);
                        }

                        console.log("getJSON write " + JSON.stringify(data) );
                    })
            .fail(function(jqxhr, textStatus, error)
                    {
                        busy_feedback.hide();
                        error_feedback.show();
                        setTimeout(function () {error_feedback.hide();}, FEEDBACK_PER);
                        console.error("getJSON write failed, status: " + textStatus + ", error: "+error);
                    });
}

function led_change(uid, led, state, error_feedback, ok_feedback, busy_feedback)
{
    var file_data;
    var offset = led-1;

    if(state)
    {
        file_data = 1;
    }
    else
    {
        file_data = 0;
    }

    write_file(uid, error_feedback, ok_feedback, busy_feedback, 0xd1, offset, 1, 2, file_data);
}

function create_led_div(title, led, uid, loading_gif_path)
{
    var table = $('<table></table>');
    var row = $('<tr></tr>');
    table.append(row);

    // Create sub contents (ON/OFF buttons)
    var btn_group = $('<div class="btn-group"></div>');
    var on_button = $('<button type="button" class="btn btn-success" >Turn ON</button>');
    var off_button = $('<button type="button" class="btn btn-danger default" >Turn OFF</button>');
    btn_group.append(on_button);
    btn_group.append(off_button);
    row.append($('<td style="border-top:none; padding: 0;"></td>').append(btn_group));


    // Create error feedback icon
    var error_feedback = $('<font hidden size=8 color="red">✗</font>');
    row.append($('<td style="border-top:none; padding: 0;"></td>').append(error_feedback));

    // Create ok feedback icon
    var ok_feedback = $('<font hidden size=8 color="green">✓</font>');
    row.append($('<td style="border-top:none; padding: 0;"></td>').append(ok_feedback));

    var busy_feedback = $('<img hidden src="' + loading_gif_path + '" />');
    row.append($('<td style="border-top:none; padding: 0;"></td>').append(busy_feedback));


    // Adding actions to button
    off_button.click(function() { led_change(uid, led, false, error_feedback, ok_feedback, busy_feedback); });
    on_button.click(function() { led_change(uid, led, true, error_feedback, ok_feedback, busy_feedback); });

    return sensor_template(title, table);
}

function create_wr_div(title, id, uid, file_id, offset, file_len, fwd, info, read, write, signed, loading_gif_path, select)
{
    var table = $('<table></table>');
    var row = $('<tr></tr>');
    table.append(row);

    var form = $('<div class="form-inline"></div>')
    row.append(form);

    var size = 10;
    // Set a smaller size for input if we have read and write buttons
    if (read && write)
    {
        size = 7
    }

    var input;

    if (select)
    {
        var input = $('<select id="' + id + '" class="xmlfd-select form-control input-sm" name="operating_mode_combo"></select>');

        input.append($("<option selected disabled hidden value=''></option>"));
        for (var i = 0; i < select.length; i++)
        {
            input.append($('<option value="' + i + '">' + select[i] + '</option>'));
        }
    }
    else
    {
        input = $('<input id="' + id + '" class="xmlfd-input form-control input-sm" type="number" value="" placeholder="' + info + '" size=' + size + ' >')
    }

    form.append(input);

    // Create error feedback icon
    var error_feedback = $('<font hidden size=8 color="red">✗</font>');

    // Create ok feedback icon
    var ok_feedback = $('<font hidden size=8 color="green">✓</font>');

    // Create busy feedback icon
    var busy_feedback = $('<img hidden src="' + loading_gif_path + '" />');

    if (write)
    {
        var button = $('<button type="button" class="btn btn-primary btn-sm" >Send</button>');

        form.append(button);

        button.click(function()
            {
                file_data = number_to_bytes(input.val(), 2);
                console.log("File data:" + file_data);
                write_file(uid, error_feedback, ok_feedback, busy_feedback, file_id, offset, file_len, fwd, file_data);
            });
    }

    if (read)
    {
        var button = $('<button type="button" class="btn btn-info btn-sm" >Read</button>');

        form.append(button);

        button.click(function()
            {
                read_file(uid, error_feedback, ok_feedback, busy_feedback, file_id, offset, file_len, fwd, function(file_data)
                    {
                        var num = bytes_to_number(file_data, signed);
                        input.val(num);
                    });
            });
    }

    row.append($('<td style="border-top:none; padding: 0;"></td>').append(error_feedback));
    row.append($('<td style="border-top:none; padding: 0;"></td>').append(ok_feedback));
    row.append($('<td style="border-top:none; padding: 0;"></td>').append(busy_feedback));

    return sensor_template(title, table);

}

function create_wisense_panel(panel_group, id, before_el, title, content)
{
    // Creating panel
    var panel = $('<div class="panel panel-default entry" id="' + id + '"></div>');

    // Add this new entry to the page
    if (before_el)
    {
        panel.insertBefore(before_el);
    }
    else
    {
        // if before is not defined, just append
        panel_group.append(panel);
    }

    // Heading
    var panel_heading = $('<div class="panel-heading"></div>');
    panel.append(panel_heading);
    var panel_title = $('<h4 class="panel-title"></h4>');
    panel_heading.append(panel_title);
    var panel_ref = $('<a data-toggle="collapse" href="#col-' + id + '"> ' + title + '</a>');
    panel_title.append(panel_ref);


    // Body
    var panel_colapse = $('<div id="col-' + id + '" class="panel-collapse collapse"></div>');
    panel.append(panel_colapse);
    var panel_body = $('<div class="panel-body"></div>');
    panel_colapse.append(panel_body);

    panel_body.append(content);
}
