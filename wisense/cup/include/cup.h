/// @copyright
/// ========================================================================={{{
/// Copyright (c) 2012-2013 WizziLab                                           /
/// All rights reserved                                                        /
/// =========================================================================}}}
/// @endcopyright

//  =======================================================================
/// @file           cup.h
/// @brief          Code Upgrade public header 
//  =======================================================================

#ifndef __CUP_H__
#define __CUP_H__

/// @ingroup CUP
/// @defgroup Code Upgrade function prototype
/// @{
#include "hal.h"

#define CUP_DBG
#define CUP_DBG_SERIAL

// Code version to be defined in application
extern const u8 _code_version[4];

// Use this define in application code to define the code version
#define CUP_SET_CODE_VERSION(version) const u8 _code_version[4] = version

#define CUP_GET_CODE_VERSION() _code_version

//======================================================================
// cup_dbg_cfg_t
//======================================================================
typedef struct
{
    u32 *led_on_addr;
    u32 *led_off_addr;
    u32 led_mask;
    u8 print_string[15];
} cup_dbg_cfg_t;

//======================================================================
// cup_cfg_t
// WARN: Be careful when modifing this structure
// It is used to calculate the CUP CFG SIZE and may change
// the current file system position
//======================================================================
TYPEDEF_STRUCT_PACKED
{
    u32 cmd;
    u16 fs_size;
    u16 fs_offset;
    u32 signature;
    u32 *src_start;
    u32 code_size;
    u32 *code_dst_start;
} cup_cfg_t;


//======================================================================
// cup_ctx_t
//======================================================================
typedef struct
{
    u8 com_cnt;
    u8 sig_string[6];
    cup_cfg_t cfg;
} cup_ctx_t;

//======================================================================
// cup_state_t
//======================================================================
typedef struct
{
    s16 cfg_fd;
    kal_timer_t tim;
    cup_cfg_t cfg;
} cup_state_t;


extern u8 _cup_cfg_base;
extern u8 _cup_code_base;
extern u8 _eeprom_start;
extern u8 _eeprom_size;

#define CUP_CMD_UPGRADE_DONE 0xc0dea7f1
#define CUP_CMD_UPGRADE_START 0xc0dea700
#define CUP_CMD_UPGRADE_CONFIRMED 0xc0dea800

#define CUP_CFG_STRUCT_ADDR (((u32) &_cup_cfg_base) + 8)

// TODO Use the linker to generate this
// 124k is 128k - 4k reserved for CUP
// Only half of the size can be used for the new code
#define CUP_CODE_MAXSIZE (121*1024/2)
// CUP_START defines where the new Code + FS are located
//#define CUP_START ((u32) &_cup_code_base)
#define CUP_START ((u32) (FLASH_BASE + CUP_CODE_MAXSIZE))

// File system max size
#define CUP_FS_MAXSIZE ((u32) &_eeprom_size)
// File system address
#define CUP_FS_START ((u32) &_eeprom_start)
// File system base
#define FS_BASE EEPROM_BASE

#define CUP_RAMCODE     IN_SECTION(".cup_ramcode")
#define CUP_FLASHCODE   IN_SECTION(".cup_flashcode")
#define CUP_ENTRYCODE   IN_SECTION(".cup_entrycode")
#define CUP_GLOBALS     IN_SECTION(".cup_globals")
#define CUP_CFG         IN_SECTION(".cup_cfg")
#define CUP_CODE        IN_SECTION(".cup_code")

#define CUP_CFG_FILE_ID     0x0
#define CUP_CODE_FILE_ID    0x0

//======================================================================
// cup_entry
//----------------------------------------------------------------------
/// @brief  Start upgrade process from boot loader
//======================================================================
public void cup_entry(void);

//======================================================================
// cup_open
//----------------------------------------------------------------------
/// @brief  Starts Code Upgrade daemon
//======================================================================
public void cup_open();

//======================================================================
// cup_get_fs_crc
//----------------------------------------------------------------------
/// @brief  Calculate and return the crc of the basic file system
//======================================================================
public u32 cup_get_fs_crc(void);


#endif // __CUP_H__
/// @}
// vim:fdm=marker:fdc=2
