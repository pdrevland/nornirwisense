/// @copyright
/// ========================================================================={{{
/// Copyright (c) 2014 WizziLab                                                /
/// All rights reserved                                                        /
///                                                                            /
/// IMPORTANT: This Software may not be modified, copied or distributed unless /
/// embedded on a WizziLab product. Other than for the foregoing purpose, this /
/// Software and/or its documentation may not be used, reproduced, copied,     /
/// prepared derivative works of, modified, performed, distributed, displayed  /
/// or sold for any purpose. For the sole purpose of embedding this Software   /
/// on a WizziLab product, copy, modification and distribution of this         /
/// Software is granted provided that the following conditions are respected:  /
///                                                                            /
/// *  Redistributions of source code must retain the above copyright notice,  /
///    this list of conditions and the following disclaimer                    /
///                                                                            /
/// *  Redistributions in binary form must reproduce the above copyright       / 
///    notice, this list of conditions and the following disclaimer in the     /
///    documentation and/or other materials provided with the distribution.    /
///                                                                            /
/// *  The name of WizziLab can not be used to endorse or promote products     /
///    derived from this software without specific prior written permission.   /
///                                                                            /
/// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS        /
/// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED  /
/// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR /
/// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR          /
/// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,      /
/// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,        /
/// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,            /
/// OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY     /
/// OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING    /
/// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS         /
/// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.               /
/// WIZZILAB HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,       /
/// ENHANCEMENTS OR MODIFICATIONS.                                             /
///                                                                            /
/// Should you have any questions regarding your right to use this Software,   /
/// contact WizziLab at www.wizzilab.com.                                      /
///                                                                            /
/// =========================================================================}}}
/// @endcopyright

//  =======================================================================
/// @file           temp-rh_si7020_drv.c
/// @brief          Driver of the SI7020 temperature and relative humidity sensor
//  =======================================================================

#include "hal.h"
#include "kal.h"
#include "hal_defs.h"
#include "drv_events.h"
#include "temp_rh_si7020_drv.h"

//==========================================================================================
// Debug
//==========================================================================================
#define SI7020_NO_PRINT
#ifndef SI7020_NO_PRINT
#define DPRINT(...) kal_dbg_printf(__VA_ARGS__)
#else
#define DPRINT(...)
#endif // SI7020_NO_PRINT

//==========================================================================================
// Enumerators and definitions
//==========================================================================================

// SI7020 I2C address
#define SI7020_I2C_ADDR             0x40


// =======================================================================
/// SI7020 commands
// =======================================================================

// Measure Relative Humidity, Hold Master Mode
#define SI7020_MEASURE_RH_HMM       0xE5
// Measure Relative Humidity, No Hold Master Mode
#define SI7020_MEASURE_RH_NHMM      0xF5
// Measure Temperature, Hold Master Mode 
#define SI7020_MEASURE_TEMP_HMM     0xE3
// Measure Temperature, No Hold Master Mode
#define SI7020_MEASURE_TEMP_NHMM    0xF3
// Read Temperature Value from Previous RH Measurement
#define SI7020_READ_TEMP_FROM_PREV  0xE0
// Reset
#define SI7020_RESET                0xFE
// Write RH/T User Register 1
#define SI7020_WR_USR_REG           0xE6
// Read RH/T User Register 1
#define SI7020_RD_USR_REG           0xE7
// Read Electronic ID 1st Byte
#define SI7020_RD_ID1               { 0xFA, 0x0F }
// Read Electronic ID 2nd Byte
#define SI7020_RD_ID2               { 0xFC, 0xC9 }
// Read Firmware Revision
#define SI7020_RD_FIRMWARE          { 0x84, 0xB8 }


// =======================================================================
/// Differents states indicating which process is running
// =======================================================================
enum {
    SI7020_STATE_CLOSED,
    SI7020_STATE_OPEN,
    SI7020_STATE_READING,
    SI7020_STATE_READING_BOTH,
    SI7020_STATE_WRITING,
};


// =======================================================================
//  si7020_dev_t
//------------------------------------------------------------------------
/// @brief This structure store the data for the process
// =======================================================================
typedef struct {
    u8                  state;
    u8                  cmd;
    u8                  crc_mode;
    s8*                 temp;
    u8*                 rh;
} si7020_dev_t;

si7020_dev_t             g_si7020;

// timer for waiting times
kal_timer_t             g_si7020_tim;

// I2C device
hal_i2c_dev_t*          g_si7020_dev;

// Process target
process_t*              g_si7020_tgt;

// =======================================================================
/// Driver processes
// =======================================================================
KAL_PROCESS(si7020_p, DRV_RH_TEMP_PROCESS_ID);

//======================================================================
// si7020_conv
//----------------------------------------------------------------------
/// @brief  convert the mesure read from the device
/// @param                  void
/// @retval                 void        
//======================================================================
private void si7020_conv( void )
{
    //~ kal_dbg_printf( "SI7020: 0x%02X%02X.\n", g_si7020_dev->buf[0], g_si7020_dev->buf[1] );
    
    if ( g_si7020.cmd == SI7020_MEASURE_RH_NHMM )
    {
        u32 tmp = 0;
        tmp |= ((u32)g_si7020_dev->buf[0] << 8 );
        tmp |= g_si7020_dev->buf[1];
        tmp = (125*tmp)/65536;
        tmp = (tmp <= 6)? 0 : tmp - 6;
        if ( tmp > 100 )
        {
            tmp = 100;
        }
        
        g_si7020.rh[0] = (u8)tmp;
    }
    else if ( g_si7020.cmd == SI7020_MEASURE_TEMP_NHMM )
    {
        s32 tmp = 0;
        tmp |= ((s32)g_si7020_dev->buf[0] << 8 );
        tmp |= g_si7020_dev->buf[1];
        tmp = ((17572*tmp)/65536)-4685;
        if ( tmp >= 0 )
        {
            tmp = (tmp%100 >= 50)? (tmp/100)+1 : tmp/100;
        }
        else
        {
            tmp = (tmp%100 <= -50)? (tmp/100)-1 : tmp/100;
        }
        
        g_si7020.temp[0] = (s8)tmp;
    }
}


//======================================================================
// si7020_conv
//----------------------------------------------------------------------
/// @brief  check the read crc
/// @param                  void
/// @retval                 void        
//======================================================================
//~ private u8 si7020_crc_check( u8* data, u8 size )
//~ {
    //~ u8 crc = 0x00;
    //~ i = size;

    //~ while(i--)
    //~ {
        //~ u8 j;
        //~ crc ^= (u8)*data++;

        //~ for(j=0; j<8; j++)
        //~ {
            //~ if (crc & 1)
                //~ crc = (crc >> 1) ^ 0x131;
            //~ else
                //~ crc >>= 1;
        //~ }
    //~ }

    //~ crc ^= 0xff;
    
    //~ return crc;
//~ }


// =======================================================================
// si7020_p
//------------------------------------------------------------------------
/// @brief SI7020 process
// =======================================================================
KAL_PROC_THREAD(si7020_p, ev, evp)
{
    (void)ev;
    (void)evp;
    
    KAL_PROC_BEGIN();
    // user memory addressing
    hal_i2c_new(g_si7020_dev, SI7020_I2C_ADDR, I2C_MASTER_MODE, 150);
    
    while(1)
    {
        KAL_PROC_WAIT_EVENT();
        
        // check event
        if ( DRV_IEVT_RH_TEMP_MEASURE_NHMM == ev )
        {
            DPRINT( "SI7020: NHMMode.." );
            
            hal_i2c_rd_regs( HAL_I2C_SI7020, g_si7020_dev, g_si7020.cmd, 2 );
            KAL_PROC_WAIT_EVENT();
            
            do
            {
                // check event
                if ( HAL_EVT_I2C_XFER_DONE == ev )
                {
                    
                }
                else if ( HAL_EVT_I2C_XFER_ARB_LOST == ev )
                    
                {
                    kal_dbg_printf( "SI7020: I2C ARB lost.\n" );
                }
                else if ( HAL_EVT_I2C_XFER_NACK == ev )
                {
#if 1
                    // Use a timer to save power and I2C ressource
                    // TODO: this still lets SCL low during the delay
                    kal_timer_start(&g_si7020_tim, KAL_ETIMER, &si7020_p, 0,
                    // Measuring relative Humidity take around 17ms
                    // Measuring temperature takes around 9ms
                    (g_si7020.cmd == SI7020_MEASURE_TEMP_NHMM)?9:17);
                    KAL_PROC_WAIT_UNTIL_EVENT(KAL_EVT_TIMER);
#endif

                    // DPRINT( "SI7020: NACK.\n" );
                    hal_i2c_rd_bytes( HAL_I2C_SI7020, g_si7020_dev, 2 );
                    //~ DPRINT( "SI7020: Wait rd evt.\n" );
                    KAL_PROC_WAIT_EVENT();
                }
                else
                {
                    kal_dbg_printf( "SI7020: Unknown evt %x in SI7020 process.\n", ev );
                }
            } while ( HAL_EVT_I2C_XFER_DONE != ev );
            
            // convert the measure
            si7020_conv();
            
            if( g_si7020.state == SI7020_STATE_READING_BOTH )
            {
                g_si7020.cmd = SI7020_MEASURE_TEMP_NHMM;
                g_si7020.state = SI7020_STATE_READING;
                // post reading event
                kal_proc_post_evt( &si7020_p, DRV_IEVT_RH_TEMP_MEASURE_NHMM, (evt_param_t)NULL );
                DPRINT( "MEASURE_NHMM\n" );
            }
            else
            {
                // finished
                g_si7020.state = SI7020_STATE_OPEN;
                
                // post the ready event
                kal_proc_post_evt( g_si7020_tgt, DRV_EVT_RH_TEMP_AVAILABLE, (evt_param_t)NULL );
                DPRINT( "Done.\n" );
            }
        }
        else if ( DRV_IEVT_RH_TEMP_RESET == ev )
        {
            DPRINT( "SI7020: Reset... " );
            
            hal_i2c_wr_bytes( HAL_I2C_SI7020, g_si7020_dev, 1 );
            KAL_PROC_WAIT_EVENT();
            
            // check event
            if ( HAL_EVT_I2C_XFER_DONE == ev )
            {
                // finished
                g_si7020.state = SI7020_STATE_OPEN;
                
                // post the ready event
                kal_proc_post_evt( g_si7020_tgt, DRV_EVT_RH_TEMP_AVAILABLE, (evt_param_t)NULL );
                DPRINT( "Done.\n" );
            }
            else if ( HAL_EVT_I2C_XFER_ARB_LOST == ev )
            {
                kal_dbg_printf( "SI7020: I2C ARB lost.\n" );
            }
            else if ( HAL_EVT_I2C_XFER_NACK == ev )
            {
                kal_dbg_printf( "SI7020: NACK.\n" );
            }
            else
            {
                kal_dbg_printf( "SI7020: Unknown evt %x in SI7020 process.\n", ev );
            }
        }
        else if ( DRV_IEVT_RH_TEMP_RES == ev )
        {
            DPRINT( "SI7020: Writing register... " );
            
            hal_i2c_rd_regs( HAL_I2C_SI7020, g_si7020_dev, SI7020_RD_USR_REG, 1 );
            KAL_PROC_WAIT_EVENT();
            
            // check event
            if ( HAL_EVT_I2C_XFER_DONE == ev )
            {
                if ( g_si7020.cmd == SI7020_HEATER_ENABLE || g_si7020.cmd == SI7020_HEATER_DISABLE )
                {
                    if ( g_si7020.cmd == SI7020_HEATER_DISABLE )
                    {
                        g_si7020.cmd = 0x00;
                    }
                    g_si7020_dev->buf[0] &= ~SI7020_HEATER_ENABLE;
                    g_si7020_dev->buf[0] |= g_si7020.cmd;
                }
                else
                {
                    g_si7020_dev->buf[0] &= ~SI7020_RH_TEMP_RES_11_11;
                    g_si7020_dev->buf[0] |= g_si7020.cmd;
                }
                
                hal_i2c_wr_reg( HAL_I2C_SI7020, g_si7020_dev, SI7020_WR_USR_REG, 1 );
                KAL_PROC_WAIT_EVENT();
                
                // check event
                if ( HAL_EVT_I2C_XFER_DONE == ev )
                {
                    // finished
                    g_si7020.state = SI7020_STATE_OPEN;
                    
                    // post the ready event
                    kal_proc_post_evt( g_si7020_tgt, DRV_EVT_RH_TEMP_AVAILABLE, (evt_param_t)NULL );
                    DPRINT( "Done.\n" );
                }
                else if ( HAL_EVT_I2C_XFER_ARB_LOST == ev )
                {
                    kal_dbg_printf( "SI7020: I2C ARB lost.\n" );
                }
                else if ( HAL_EVT_I2C_XFER_NACK == ev )
                {
                    kal_dbg_printf( "SI7020: NACK.\n" );
                }
                else
                {
                    kal_dbg_printf( "SI7020: Unknown evt %x in SI7020 process.\n", ev );
                }
            }
            else if ( HAL_EVT_I2C_XFER_ARB_LOST == ev )
            {
                kal_dbg_printf( "SI7020: I2C ARB lost.\n" );
            }
            else if ( HAL_EVT_I2C_XFER_NACK == ev )
            {
                kal_dbg_printf( "SI7020: NACK.\n" );
            }
            else
            {
                kal_dbg_printf( "SI7020: Unknown evt %x in SI7020 process.\n", ev );
            }
        }
        else
        {
            kal_dbg_printf( "SI7020: Unknown evt %x in SI7020 process.\n", ev );
        }
    }
    
    KAL_PROC_END();
}



//======================================================================
// si7020_open
//----------------------------------------------------------------------
/// @brief  initialize the SI7020 module (first function to use)
/// @param  tgt_process     process_t   target process
/// @retval                 void        
//======================================================================
public void si7020_open( process_t* tgt_process )
{
    
    if( g_si7020.state != SI7020_STATE_CLOSED )
    {
        kal_dbg_printf("SI7020: Device already open.\n");
    }
    else
    {
        // init I2C
        hal_i2c_open(HAL_I2C_SI7020);
        
        // memory allocation
        g_si7020_dev = (hal_i2c_dev_t*)kal_malloc( sizeof(hal_i2c_dev_t) + 3 );
        
        // store target process
        g_si7020_tgt = tgt_process;
    
        // Timer init
        kal_timer_init(&g_si7020_tim);
        
        // start SI7020 process
        kal_proc_start(&si7020_p, (evt_param_t)NULL);
        
        // change status
        g_si7020.state = SI7020_STATE_OPEN;
        
        // post the ready event
        kal_proc_post_evt( g_si7020_tgt, DRV_EVT_RH_TEMP_INIT_DONE, (evt_param_t)NULL );
        
        DPRINT( "SI7020: Device opened.\n" );
    }
}


//======================================================================
// si7020_close
//----------------------------------------------------------------------
/// @brief  close the SI7020 session
/// @param                  void
/// @retval                 void        
//======================================================================
public void si7020_close( void )
{
    if( g_si7020.state == SI7020_STATE_CLOSED )
    {
        kal_dbg_printf("SI7020: Device already closed.\n");
    }
    else if ( g_si7020.state != SI7020_STATE_OPEN )
    {
        kal_dbg_printf("SI7020: Wait for operations to finish before closing the device.\n");
    }
    else
    {
        // stop process
        kal_proc_exit(&si7020_p);
        
        // close I2C port
        hal_i2c_close(HAL_I2C_SI7020);
        
        // free memory
        kal_free( g_si7020_dev );
        
        g_si7020.state = SI7020_STATE_CLOSED;
        
        DPRINT( "SI7020: Device closed.\n" );
    }
}


//======================================================================
// si7020_set_heater
//----------------------------------------------------------------------
/// @brief  change the resolution parameters
/// @param  param           u8              parameter
/// @retval                 void        
//======================================================================
public void si7020_set_heater( u8 param )
{
    if ( g_si7020.state != SI7020_STATE_OPEN )
    {
        kal_dbg_printf( "SI7020: Wait for operation to finish before continuing:\nSI7020: Use: KAL_PROC_WAIT_UNTIL_EVENT(DRV_EVT_RH_TEMP_AVAILABLE);\n" );
    }
    else if ( g_si7020.state == SI7020_STATE_OPEN )
    {
        // save param
        g_si7020.cmd = param;
        
        // change state
        g_si7020.state = SI7020_STATE_WRITING;
        
        // post change event
        kal_proc_post_evt( &si7020_p, DRV_IEVT_RH_TEMP_RES, (evt_param_t)NULL );
        DPRINT( "SI7020: Heater evt posted.\n" );
    }
}

//======================================================================
// si7020_change_res
//----------------------------------------------------------------------
/// @brief  change the resolution parameters
/// @param  res             u8              resolution
/// @retval                 void        
//======================================================================
public void si7020_change_res( u8 res )
{
    if ( g_si7020.state != SI7020_STATE_OPEN )
    {
        kal_dbg_printf( "SI7020: Wait for operation to finish before continuing:\nSI7020: Use: KAL_PROC_WAIT_UNTIL_EVENT(DRV_EVT_RH_TEMP_AVAILABLE);\n" );
    }
    else if ( g_si7020.state == SI7020_STATE_OPEN )
    {
        // save param
        g_si7020.cmd = res;
        
        // change state
        g_si7020.state = SI7020_STATE_WRITING;
        
        // post change event
        kal_proc_post_evt( &si7020_p, DRV_IEVT_RH_TEMP_RES, (evt_param_t)NULL );
        DPRINT( "SI7020: Res evt posted.\n" );
    }
}

//======================================================================
// si7020_rd_rh_nhmm
//----------------------------------------------------------------------
/// @brief  read the relative humidity value
/// @param  data            u8*             data read
/// @retval                 void        
//======================================================================
public void si7020_rd_rh_nhmm( u8* data )
{
    if ( g_si7020.state != SI7020_STATE_OPEN )
    {
        kal_dbg_printf( "SI7020: Wait for operation to finish before continuing:\nSI7020: Use: KAL_PROC_WAIT_UNTIL_EVENT(DRV_EVT_RH_TEMP_AVAILABLE);\n" );
    }
    else if ( g_si7020.state == SI7020_STATE_OPEN )
    {
        // initialize param
        g_si7020.rh = data;
        g_si7020.cmd = SI7020_MEASURE_RH_NHMM;
        
        // change state
        g_si7020.state = SI7020_STATE_READING;
        
        // post reading event
        kal_proc_post_evt( &si7020_p, DRV_IEVT_RH_TEMP_MEASURE_NHMM, (evt_param_t)NULL );
        DPRINT( "SI7020: Read rh evt posted.\n" );
    }
}


//======================================================================
// si7020_rd_temp_nhmm
//----------------------------------------------------------------------
/// @brief  read the temperature value
/// @param  data            s8*             data read
/// @retval                 void        
//======================================================================
public void si7020_rd_temp_nhmm( s8* data )
{
    if ( g_si7020.state != SI7020_STATE_OPEN )
    {
        kal_dbg_printf( "SI7020: Wait for operation to finish before continuing:\nSI7020: Use: KAL_PROC_WAIT_UNTIL_EVENT(DRV_EVT_RH_TEMP_AVAILABLE);\n" );
    }
    else if ( g_si7020.state == SI7020_STATE_OPEN )
    {
        // initialize param
        g_si7020.temp = data;
        g_si7020.cmd = SI7020_MEASURE_TEMP_NHMM;
        
        // change state
        g_si7020.state = SI7020_STATE_READING;
        
        // post reading event
        kal_proc_post_evt( &si7020_p, DRV_IEVT_RH_TEMP_MEASURE_NHMM, (evt_param_t)NULL );
        DPRINT( "SI7020: Read temp evt posted.\n" );
    }
}


//======================================================================
// si7020_rd_rh_temp_nhmm
//----------------------------------------------------------------------
/// @brief  read the temperature value
/// @param  rh              u8*             humidity
/// @param  temp            s8*             temperature
/// @retval                 void        
//======================================================================
public void si7020_rd_rh_temp_nhmm( u8* rh, s8* temp )
{
    if ( g_si7020.state != SI7020_STATE_OPEN )
    {
        kal_dbg_printf( "SI7020: Wait for operation to finish before continuing:\nSI7020: Use: KAL_PROC_WAIT_UNTIL_EVENT(DRV_EVT_RH_TEMP_AVAILABLE);\n" );
    }
    else if ( g_si7020.state == SI7020_STATE_OPEN )
    {
        // initialize param
        g_si7020.rh = rh;
        g_si7020.temp = temp;
        g_si7020.cmd = SI7020_MEASURE_RH_NHMM;
        
        // change state
        g_si7020.state = SI7020_STATE_READING_BOTH;
        
        // post reading event
        kal_proc_post_evt( &si7020_p, DRV_IEVT_RH_TEMP_MEASURE_NHMM, (evt_param_t)NULL );
        DPRINT( "SI7020: Read rh temp evt posted.\n" );
    }
}


//======================================================================
// si7020_reset
//----------------------------------------------------------------------
/// @brief  read the temperature value
/// @param                  void
/// @retval                 void        .
//======================================================================
public void si7020_reset( void )
{
    if ( g_si7020.state != SI7020_STATE_OPEN )
    {
        kal_dbg_printf( "SI7020: Wait for operation to finish before continuing:\nSI7020: Use: KAL_PROC_WAIT_UNTIL_EVENT(DRV_EVT_RH_TEMP_AVAILABLE);\n" );
    }
    else if ( g_si7020.state == SI7020_STATE_OPEN )
    {
        // fill i2c buffer
        g_si7020_dev->buf[0] = SI7020_RESET;
        
        // change state
        g_si7020.state = SI7020_STATE_WRITING;
        
        // post reset event
        kal_proc_post_evt( &si7020_p, DRV_IEVT_RH_TEMP_RESET, (evt_param_t)NULL );
        DPRINT( "SI7020: Reset evt posted.\n" );
    }
}





