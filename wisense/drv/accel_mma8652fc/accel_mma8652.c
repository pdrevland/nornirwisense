/// @copyright
/// ========================================================================={{{
/// Copyright (c) 2012-2013 WizziLab                                           /
/// All rights reserved                                                        /
///                                                                            /
/// IMPORTANT: This Software may not be modified, copied or distributed unless /
/// embedded on a WizziLab product. Other than for the foregoing purpose, this /
/// Software and/or its documentation may not be used, reproduced, copied,     /
/// prepared derivative works of, modified, performed, distributed, displayed  /
/// or sold for any purpose. For the sole purpose of embedding this Software   /
/// on a WizziLab product, copy, modification and distribution of this         /
/// Software is granted provided that the following conditions are respected:  /
///                                                                            /
/// *  Redistributions of source code must retain the above copyright notice,  /
///    this list of conditions and the following disclaimer                    /
///                                                                            /
/// *  Redistributions in binary form must reproduce the above copyright       / 
///    notice, this list of conditions and the following disclaimer in the     /
///    documentation and/or other materials provided with the distribution.    /
///                                                                            /
/// *  The name of WizziLab can not be used to endorse or promote products     /
///    derived from this software without specific prior written permission.   /
///                                                                            /
/// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS        /
/// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED  /
/// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR /
/// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR          /
/// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,      /
/// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,        /
/// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,            /
/// OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY     /
/// OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING    /
/// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS         /
/// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.               /
/// WIZZILAB HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,       /
/// ENHANCEMENTS OR MODIFICATIONS.                                             /
///                                                                            /
/// Should you have any questions regarding your right to use this Software,   /
/// contact WizziLab at www.wizzilab.com.                                      /
///                                                                            /
/// =========================================================================}}}
/// @endcopyright

//  =======================================================================
/// @file           accel_mma8652.c
/// @brief          Accelerometer Driver
//  =======================================================================

#include "hal.h"
#include "kal.h"
#include "hal_defs.h"
#include "accel_mma8652.h"
#include "drv_events.h"

//==========================================================================================
// DEBUG
//==========================================================================================
#if 0
#define DPRINT(...) kal_dbg_printf(__VA_ARGS__)
#else
#define DPRINT(...)
#endif

//==========================================================================================
// Enumerators and definitions
//==========================================================================================
enum {
    MMA_STATE_CLOSED,
    MMA_STATE_INIT,
    MMA_STATE_OPEN,
    MMA_STATE_CLOSING
};

#define MMA_I2C_ADDR        0x1D

#define MMA_REG_STAT                0x0             // R0
#define MMA_REG_XOUT_MSB            0x1             // R0
#define MMA_REG_XOUT_LSB            0x2             // R0
#define MMA_REG_YOUT_MSB            0x3             // R0
#define MMA_REG_YOUT_LSB            0x4             // R0
#define MMA_REG_ZOUT_MSB            0x5             // R0
#define MMA_REG_ZOUT_LSB            0x6             // R0

// Fifo Setup                       
#define MMA_REG_F_SETUP             0x9             // RW
    #define MMA_FIFO_DIS            (0x00 << 6)
    #define MMA_FIFO_CIRC           (0x01 << 6)
    #define MMA_FIFO_STOP           (0x02 << 6)
    #define MMA_FIFO_TRIG           (0x03 << 6)
    #define MMA_FIFO_WTMRK(n)       ((n) & 0x3f)
    #define MMA_FIFO_WTMRK_DIS      (0)

// Fifo Trigger config              
#define MMA_REG_TRIG_CFG            0xa             // RW
    #define MMA_TRIG_TRANS          (1<<5)
    #define MMA_TRIG_LANDS          (1<<4)
    #define MMA_TRIG_PULSE          (1<<3)
    #define MMA_TRIG_MOTIO          (1<<2)

// System Status                    
#define MMA_REG_SYS_MOD             0xb             // RO
    #define MMA_FGERR               (1<<7)          // Fifo gate error RO
    #define MMA_FGERR_NUM(x)        (((x)>>2)&0x1f) // Fifo gate error number RO
    #define MMA_STANBY              0x0
    #define MMA_AWAKE               0x1
    #define MMA_SLEEP               0x2

// Interrupt source
#define MMA_REG_INT_SRC             0xc             // RO
    #define MMA_SRC_ASLP            (1<<7)          // Triggerred by auto SLEEP to WAKE or WAKE to SLEEP
    #define MMA_SRC_FIFO            (1<<6)          // Fifo Irq
    #define MMA_SRC_TRANS           (1<<5)          // Transcient Acceleration
    #define MMA_SRC_LNDPRT          (1<<4)          // Landscape/Portrait
    #define MMA_SRC_PULSE           (1<<3)          // Single or double pulse EVT
    #define MMA_SRC_MOTION          (1<<2)          // Motion or Free Fall detected
    #define MMA_SRC_DRDY            (1<<0)          // Data ready to be read

// Constant ID chip model
#define MMA_REG_WHO_AM_I            0xd             // RO

// Data format configuration
#define MMA_REG_XYZ_DATA_CFG        0xe             // RW
    // High pass filter
    #define MMA_HPF_OUT_EN             (1<<4)
    #define MMA_HPF_OUT_DIS            (0<<4)
    // Full scale format 2G, 4G or 8G
    #define MMA_FSFORMAT_2G            (0<<0)   // 0.98 mg (12bits)
    #define MMA_FSFORMAT_4G            (1<<0)   // 1.96 mg (12bits)
    #define MMA_FSFORMAT_8G            (2<<0)   // 3.90 mg (12bits)
    
// High pass filter cutoff
#define MMA_REG_HP_FILTER_CUTOFF    0xf             // RW
    #define MMA_PULSE_HPF_EN           (0<<5)
    #define MMA_PULSE_HPF_DIS          (1<<5)
    #define MMA_PULSE_LPF_EN           (1<<4)
    #define MMA_PULSE_LPF_DIS          (0<<4)
    #define MMA_HPF_CUTOFF_DEF         (0<<0)
    #define MMA_HPF_CUTOFF_DEF_DIV2    (1<<0)
    #define MMA_HPF_CUTOFF_DEF_DIV4    (2<<0)
    #define MMA_HPF_CUTOFF_DEF_DIV8    (3<<0)

// Portrait / Landscape configuration
#define MMA_REG_PL_STATUS           0x10
#define MMA_REG_PL_CFG              0x11
#define MMA_REG_PL_COUNT            0x12
#define MMA_REG_PL_BF_ZCOMP         0x13
#define MMA_REG_P_L_THS             0x14

// Motion / FreeFall Detection Configuration
#define MMA_REG_FF_MT_CFG           0x15
#define MMA_REG_FF_MT_SRC           0x16
#define MMA_REG_FF_MT_THS           0x17
#define MMA_REG_FF_MT_COUNT         0x18

// Transcient Detection Configuration
#define MMA_REG_TRANS_CFG           0x1d
#define MMA_REG_TRANS_SRC           0x1e
#define MMA_REG_TRANS_THS           0x1f
#define MMA_REG_TRANS_COUNT         0x20

// Pulse Detection Configuration
#define MMA_REG_PULSE_CFG           0x21
#define MMA_REG_PULSE_SRC           0x22
#define MMA_REG_PULSE_THSX          0x23
#define MMA_REG_PULSE_THSY          0x24
#define MMA_REG_PULSE_THSZ          0x25
#define MMA_REG_PULSE_TMLT          0x26
#define MMA_REG_PULSE_LTCY          0x27
#define MMA_REG_PULSE_WIND          0x28

// Auto Wakeup / Sleep Configuration
#define MMA_REG_ASLP_COUNT          0x29

// System Configuration
#define MMA_REG_CTRL1               0x2a
    #define MMA_ASLP_RATE(n)            (((n)&0x3)<<6)
    #define MMA_DATA_RATE(n)            (((n)&0x7)<<3)
    #define MMA_DATA_RATE_800           MMA_DATA_RATE(0)
    #define MMA_DATA_RATE_400           MMA_DATA_RATE(1)
    #define MMA_DATA_RATE_200           MMA_DATA_RATE(2)
    #define MMA_DATA_RATE_100           MMA_DATA_RATE(3)
    #define MMA_DATA_RATE_50            MMA_DATA_RATE(4)
    #define MMA_DATA_RATE_12_5          MMA_DATA_RATE(5)
    #define MMA_DATA_RATE_6_25          MMA_DATA_RATE(6)
    #define MMA_DATA_RATE_1_56          MMA_DATA_RATE(7)
    #define MMA_FAST_READ_EN            (1<<1)
    #define MMA_MODE_STDBY              (0<<0)
    #define MMA_MODE_ACTIV              (1<<0)


// Power control
#define MMA_REG_CTRL2               0x2b
    #define MMA_SELF_TST            (1<<7)
    #define MMA_RST                 (1<<6)
    // to be used with sleep_mode or active mode
    #define MMA_MODE_NORM             (0)
    #define MMA_MODE_LNOISE_LPOW      (1)
    #define MMA_MODE_HIGH_RES         (2)
    #define MMA_MODE_LPOW             (3)
    #define MMA_SLEEP_PMODE(n)       ((n)<<3) // Sleep power Mode
    #define MMA_ACTIV_PMODE(n)       ((n)<<3) // Active Power Mode
    #define MMA_AUTO_SLEEP      (1<<2)

// Wake up and IRQ lines control 
#define MMA_REG_CTRL3               0x2c // RW
    #define MMA_IRQ_FIFO_GATE_EN    (1<<7)
    #define MMA_IRQ_WAKE_TRAN       (1<<6)
    #define MMA_IRQ_WAKE_ORIENT     (1<<5)
    #define MMA_IRQ_WAKE_PULSE      (1<<4)
    #define MMA_IRQ_WAKE_MOTION     (1<<3)
    // IRQ line polarity
    #define MMA_IRQ_ACTIVE_HIGH     (1<<1)
    #define MMA_IRQ_ACTIVE_LOW      (0<<1)
    // Irq line drive mode
    #define MMA_IRQ_PUSH_PULL       (0<<0)
    #define MMA_IRQ_OPEN_DRAIN      (1<<0)


// Interrupt Enable
#define MMA_REG_CTRL4               0x2d
    #define MMA_IRQ_ASLP_EN         (1<<7)
    #define MMA_IRQ_FIFO_EN         (1<<6)
    #define MMA_IRQ_TRANS_EN        (1<<5)
    #define MMA_IRQ_ORIENT_EN       (1<<4)
    #define MMA_IRQ_PULSE_EN        (1<<3)
    #define MMA_IRQ_MOTION_EN       (1<<2)
    #define MMA_IRQ_DREADY_EN       (1<<0)

// Interrupt Control
#define MMA_REG_CTRL5               0x2e
    #define MMA_IRQ_ASLP_INT1       (1<<7)
    #define MMA_IRQ_FIFO_INT1       (1<<6)
    #define MMA_IRQ_TRANS_INT1      (1<<5)
    #define MMA_IRQ_ORIENT_INT1     (1<<4)
    #define MMA_IRQ_PULSE_INT1      (1<<3)
    #define MMA_IRQ_MOTION_INT1     (1<<2)
    #define MMA_IRQ_DREADY_INT1     (1<<0)

// Data Calibration
// 1.96 mg/LSB
#define MMA_REG_OFF_X               0x2f // RW 
#define MMA_REG_OFF_Y               0x30 // RW
#define MMA_REG_OFF_Z               0x31 // RW

#define FRAC_2d1 5000
#define FRAC_2d2 2500
#define FRAC_2d3 1250
#define FRAC_2d4 625
#define FRAC_2d5 313
#define FRAC_2d6 156
#define FRAC_2d7 78
#define FRAC_2d8 39
#define FRAC_2d9 20

typedef struct {
    process_t*          tgt_process;
    mma_rate_t          rate;
    hal_i2c_dev_t(8)    dev;
    u8                  state;
} mmadev_t;

typedef struct {
    u8    addr;
    u8    data;
} mma_init_cmd_t;

// ------------------------------
// Initialization sequence
// ------------------------------
const mma_init_cmd_t g_mma_init[] = 
{
    // { MMA_REG_STAT              , XX},   // Status register - RO
	// { MMA_REG_XOUT_MSB          , XX},   // XOUT_MSB register - RO
	// { MMA_REG_XOUT_LSB          , XX},   // XOUT_LSB register - RO
	// { MMA_REG_YOUT_MSB          , XX},   // YOUT_MSB register - RO
	// { MMA_REG_YOUT_LSB          , XX},   // YOUT_LSB register - RO
	// { MMA_REG_ZOUT_MSB          , XX},   // ZOUT_MSB register - RO
	// { MMA_REG_ZOUT_LSB          , XX},   // ZOUT_LSB register - RO
	// { MMA_REG_F_SETUP           , MMA_FIFO_DIS},
	// { MMA_REG_TRIG_CFG          , },
	// { MMA_REG_SYS_MOD           , },
	// { MMA_REG_INT_SRC           , },
	// { MMA_REG_WHO_AM_I          , },
	// { MMA_REG_XYZ_DATA_CFG      , },
	// { MMA_REG_HP_FILTER_CUTOFF  , },
	// { MMA_REG_PL_STATUS         , },
	// { MMA_REG_PL_CFG            , },
	// { MMA_REG_PL_COUNT          , },
	// { MMA_REG_PL_BF_ZCOMP       , },
	// { MMA_REG_P_L_THS           , },
	// { MMA_REG_FF_MT_CFG         , },
	// { MMA_REG_FF_MT_SRC         , },
	// { MMA_REG_FF_MT_THS         , },
	// { MMA_REG_FF_MT_COUNT       , },
	// { MMA_REG_TRANS_CFG         , },
	// { MMA_REG_TRANS_SRC         , },
	// { MMA_REG_TRANS_THS         , },
	// { MMA_REG_TRANS_COUNT       , },
	// { MMA_REG_PULSE_CFG         , },
	// { MMA_REG_PULSE_SRC         , },
	// { MMA_REG_PULSE_THSX        , },
	// { MMA_REG_PULSE_THSY        , },
	// { MMA_REG_PULSE_THSZ        , },
	// { MMA_REG_PULSE_TMLT        , },
	// { MMA_REG_PULSE_LTCY        , },
	// { MMA_REG_PULSE_WIND        , },
	// { MMA_REG_ASLP_COUNT        , },
	// { MMA_REG_CTRL2             , MMA_SLEEP_PMODE(MMA_MODE_LPOW)|MMA_ACTIV_PMODE(MMA_MODE_LPOW)|MMA_AUTO_SLEEP },
	{ MMA_REG_CTRL3             , MMA_IRQ_ACTIVE_HIGH | MMA_IRQ_PUSH_PULL},
	{ MMA_REG_CTRL4             , MMA_IRQ_DREADY_EN},
	{ MMA_REG_CTRL5             , MMA_IRQ_DREADY_INT1},
	{ MMA_REG_CTRL1             , MMA_MODE_STDBY}, // To allow CTRL1 CFG
};

mmadev_t g_mma;

#if 0
void SCI_s12frac_Out (tword data)
{
    dword result;
    byte a, b, c, d;
    word r;
    data.DWord = data.Word >> 4;
    if (data.DWord >= 0x1000)
    data.DWord = data.DWord - 0x2000;
    result = (1000 * data.DWord + 512) >> 10;
    /*** Determine sign and output */
    if (result > 0x80000000)
    {
    SCI_CharOut ('-');
    result = ~result + 1;
    }
    else
    {
    SCI_CharOut ('+');
    }
    /*** Convert mantissa value to 4 decimal places */
    r = result % 1000;
    a = (byte)(result / 1000);
    b = (byte)(r / 100);
    r %= 100;
    c = (byte)(r / 10);
    d = (byte)(r%10);
    /*** Output mantissa */
    SCI_NibbOut (a);
    SCI_NibbOut (b);
    SCI_NibbOut (c);
    SCI_NibbOut (d);
    SCI_CharOut ('mg')
}


void SCI_s12frac_Out (s32 data)
{
    BIT_FIELD value;
    word result;
    byte a, b, c, d;
    word r;

    data &= 0xFFF0;
    /*** Determine sign and output */
    if (data & 0x800000)
    {
        //SCI_CharOut ('-');
        data = ~data + 1;
    }
    else
    {
        //SCI_CharOut ('+');
    }
    /*** Determine integer value and output */
    SCI_NibbOut((data.Byte.hi & 0x40) >>6);
    data.Word = data.Word <<2;

    SCI_CharOut ('.');
    /*** Determine mantissa value */
    result = 0;
    value.Byte = data.Byte.hi;
    if (value.Bit._7 == 1)
        result += FRAC_2d1;
    if (value.Bit._6 == 1)
        result += FRAC_2d2;
    if (value.Bit._5 == 1)
        result += FRAC_2d3;
    if (value.Bit._4 == 1)
        result += FRAC_2d4;
    //
        data.Word = data.Word <<4;
        value.Byte = data.Byte.hi;
    //
    if (value.Bit._7 == 1)
        result += FRAC_2d5;
    if (value.Bit._6 == 1)
        result += FRAC_2d6;
    if (value.Bit._5 == 1)
        result += FRAC_2d7;
    if (value.Bit._4 == 1)
        result += FRAC_2d8;
    //
    if (full_scale != FULL_SCALE_8G)
    {
        if (value.Bit._3 == 1)
            result += FRAC_2d9;
    }
    if (full_scale == FULL_SCALE_2G)
    {
        if (value.Bit._2 == 1)
            result += FRAC_2d10;
    }
}


#endif

KAL_PROCESS(drv_mma_p, DRV_MMA_PROCESS_ID);
KAL_PROC_THREAD(drv_mma_p, ev, evp)
{
    (void) evp;
    accel_res_t* res;
    static int i;

    KAL_PROC_BEGIN();

    while(1) {
        if (ev==HAL_EVT_I2C_XFER_DONE)
        {
            if (g_mma.state == MMA_STATE_OPEN)
            {
                if ((evp.data==HAL_EVT_I2C_XFER_ARB_LOST) || (evp.data==HAL_EVT_I2C_XFER_NACK))
                {
                    // TODO Implement error recovery
                    kal_dbg_assert(FALSE, "ASSERT: MMA I2C Error %d\n",evp.data);
                }

                res = DRV_MALLOC(sizeof(accel_res_t));
                res->stat = g_mma.dev.buf[0];
                // Compute X data
                res->xout = (g_mma.dev.buf[2] | (g_mma.dev.buf[1]<<8));
                // Result is on 12 bits
                res->xout=(res->xout)>>4;
                // Compute Y data
                res->yout = (g_mma.dev.buf[4] | (g_mma.dev.buf[3]<<8));
                // Result is on 12 bits
                res->yout=(res->yout)>>4;
                // Compute Z data
                res->zout = (g_mma.dev.buf[6] | (g_mma.dev.buf[5]<<8));
                // Result is on 12 bits
                res->zout=(res->zout)>>4;
                kal_proc_post_evt(g_mma.tgt_process,DRV_EVT_ACCEL_RES_AVAILABLE,(evt_param_t)(void*)res);
            }

        }
        else if (ev==HAL_EVT_I2C_XFER_NACK || ev==HAL_EVT_I2C_XFER_ARB_LOST)
        {
            kal_proc_post_evt(g_mma.tgt_process,DRV_EVT_ACCEL_ERROR,(evt_param_t)NULL);
        }
        else if (ev==KAL_EVT_INIT)
        {
            DPRINT("MMA Init\n");
            for (i=0;i<(int) (sizeof(g_mma_init)/sizeof(mma_init_cmd_t));i++)
            {
                hal_i2c_wr_reg(HAL_I2C_ACC, &g_mma.dev,g_mma_init[i].addr,g_mma_init[i].data);
                KAL_PROC_WAIT_UNTIL_EVENT(HAL_EVT_I2C_XFER_DONE);
                if ((evp.data==HAL_EVT_I2C_XFER_ARB_LOST) || (evp.data==HAL_EVT_I2C_XFER_NACK))
                {
                    // TODO Implement error recovery
                    kal_dbg_assert(FALSE, "ASSERT: MMA I2C Error %d\n",evp.data);
                }
            }
            hal_i2c_wr_reg(HAL_I2C_ACC, &g_mma.dev,MMA_REG_CTRL1,((u8) g_mma.rate)|MMA_MODE_ACTIV);
            KAL_PROC_WAIT_UNTIL_EVENT(HAL_EVT_I2C_XFER_DONE);
            if ((evp.data==HAL_EVT_I2C_XFER_ARB_LOST) || (evp.data==HAL_EVT_I2C_XFER_NACK))
            {
                // TODO Implement error recovery
                kal_dbg_assert(FALSE, "ASSERT: MMA I2C Error %d\n",evp.data);
            }

            DPRINT("MMA Init Done\n");
            hal_gpio_irq_en(HAL_GPIO_ACCEL_INT1);
            hal_gpio_irq_en(HAL_GPIO_ACCEL_INT2);

            g_mma.state = MMA_STATE_OPEN;
            kal_proc_post_evt(g_mma.tgt_process,DRV_EVT_ACCEL_INIT_DONE,(evt_param_t)NULL);
        }
        else if (ev==DRV_EVT_ACCEL_CHECK_CFG)
        {
            DPRINT("MMA Readback Config\n");
            for (i=0;i<(int) (sizeof(g_mma_init)/sizeof(mma_init_cmd_t));i++)
            {
                hal_i2c_rd_regs(HAL_I2C_ACC, &g_mma.dev,g_mma_init[i].addr,1);
                KAL_PROC_WAIT_UNTIL_EVENT(HAL_EVT_I2C_XFER_DONE);
                if ((evp.data==HAL_EVT_I2C_XFER_ARB_LOST) || (evp.data==HAL_EVT_I2C_XFER_NACK))
                {
                    // TODO Implement error recovery
                    kal_dbg_assert(FALSE, "ASSERT: MMA I2C Error %d\n",evp.data);
                }

                if (g_mma.dev.buf[0]!=g_mma_init[i].data)
                {
                    DPRINT("MMA Reg %02x Read %02x Expected %02x\n",
                        g_mma_init[i].addr,
                        g_mma.dev.buf[0], 
                        g_mma_init[i].data);
                }

            }
            DPRINT("MMA Readback CTRL in block\n");
            hal_i2c_rd_regs(HAL_I2C_ACC, &g_mma.dev, MMA_REG_CTRL1, 5);
            KAL_PROC_WAIT_UNTIL_EVENT(HAL_EVT_I2C_XFER_DONE);
            if ((evp.data==HAL_EVT_I2C_XFER_ARB_LOST) || (evp.data==HAL_EVT_I2C_XFER_NACK))
            {
                // TODO Implement error recovery
                kal_dbg_assert(FALSE, "ASSERT: MMA I2C Error %d\n",evp.data);
            }

            DPRINT("MMA CTRL1 %02x CTRL2 %02x CTRL3 %02x CTRL4 %02x CTRL5 %02x\n",
                g_mma.dev.buf[0],
                g_mma.dev.buf[1],
                g_mma.dev.buf[2],
                g_mma.dev.buf[3],
                g_mma.dev.buf[4]
                );
            kal_proc_post_evt(g_mma.tgt_process,DRV_EVT_ACCEL_CHECK_CFG_DONE,(evt_param_t)NULL);
        }
        else if (ev==DRV_EVT_ACCEL_GETXYZ)
        {
            DPRINT("MMA Read XYY\n");
            hal_i2c_rd_regs(HAL_I2C_ACC, &g_mma.dev, MMA_REG_STAT, 7);
        }
        else if (ev==KAL_EVT_EXIT)
        {
            hal_gpio_irq_dis(HAL_GPIO_ACCEL_INT1);
            hal_gpio_irq_dis(HAL_GPIO_ACCEL_INT2);
            hal_i2c_wr_reg(HAL_I2C_ACC, &g_mma.dev, MMA_REG_CTRL1, 0);
            KAL_PROC_WAIT_UNTIL_EVENT(HAL_EVT_I2C_XFER_DONE);
            if ((evp.data==HAL_EVT_I2C_XFER_ARB_LOST) || (evp.data==HAL_EVT_I2C_XFER_NACK))
            {
                // TODO Implement error recovery
                kal_dbg_assert(FALSE, "ASSERT: MMA I2C Error %d\n",evp.data);
            }

            hal_i2c_free(HAL_I2C_ACC);
            g_mma.state = MMA_STATE_CLOSED;
            KAL_PROC_EXIT();
        }
        KAL_PROC_WAIT_EVENT();
    }
    KAL_PROC_END();
}

protected void mma_int_isr(uint status)
{
    (void) status;
    DPRINT("MMA IRQ\n");
    accel_mma8652_get_xyz();
}

//======================================================================
// accel_mma8652_open 
//----------------------------------------------------------------------
/// @brief  Open Accelerometer. Interactions with the accelerometer are
///         all interrupt-based and results are available through callback
///         calls.
/// @param  mode: bitmap selection of enabled interrupts among mma_int_mode_t.
///         Any combination of interrupts can be selected.
/// @param  refresh_rate: number of sample updates per seconds among mma_rate_t.
///         Note: Tap detection requires 120 samples/sec rate.
/// @param  tgt_process : event receiving process
//======================================================================
public void accel_mma8652_open( u8 mode, u8 refresh_rate, process_t* tgt_process)
{
    (void) mode;

    DPRINT("MMA Open\n");
    g_mma.tgt_process  = tgt_process;


    hal_gpio_irq_set(HAL_GPIO_ACCEL_INT1,GPIO_IRQ_RISE,mma_int_isr);
    hal_gpio_irq_set(HAL_GPIO_ACCEL_INT2,GPIO_IRQ_RISE,mma_int_isr);

    hal_i2c_new(&g_mma.dev,MMA_I2C_ADDR, I2C_MASTER_MODE, 100);
    ((hal_i2c_dev_t*)&g_mma.dev)->p    = &drv_mma_p;

    g_mma.state = MMA_STATE_INIT;

    // Select most appropriate rate
    switch (refresh_rate)
    {
        case MMA_RATE_1_SPS:
            g_mma.rate = MMA_DATA_RATE_1_56;
            break;
        case MMA_RATE_2_SPS:
        case MMA_RATE_5_SPS:
            g_mma.rate = MMA_DATA_RATE_6_25;
            break;
        case MMA_RATE_10_SPS:
            g_mma.rate = MMA_DATA_RATE_12_5;
            break;
        case MMA_RATE_15_SPS:
        case MMA_RATE_20_SPS:
        case MMA_RATE_25_SPS:
        case MMA_RATE_50_SPS:
            g_mma.rate = MMA_DATA_RATE_50;
            break;
        case MMA_RATE_100_SPS:
            g_mma.rate = MMA_DATA_RATE_100;
            break;
        case MMA_RATE_200_SPS:
            g_mma.rate = MMA_DATA_RATE_200;
            break;
        case MMA_RATE_300_SPS:
        case MMA_RATE_400_SPS:
            g_mma.rate = MMA_DATA_RATE_400;
            break;
        case MMA_RATE_500_SPS:
        case MMA_RATE_700_SPS:
        case MMA_RATE_900_SPS:
            g_mma.rate = MMA_DATA_RATE_800;
            break;

    }
    kal_proc_start(&drv_mma_p,(evt_param_t)NULL);
}

//======================================================================
// accel_mma8652_close 
//----------------------------------------------------------------------
/// @brief  Disables accel_mma8652 sensor
/// @param  
//======================================================================
public void accel_mma8652_close()
{
    DPRINT("MMA Close\n");
    if (g_mma.state == MMA_STATE_CLOSED) return;
    kal_proc_post_evt(&drv_mma_p,KAL_EVT_EXIT,(evt_param_t)NULL);
}

//======================================================================
// accel_mma8652_check_cfg
//----------------------------------------------------------------------
/// @brief  Check configuration registers
/// @param  none
//======================================================================
public void accel_mma8652_check_cfg()
{
    kal_proc_post_evt(&drv_mma_p,DRV_EVT_ACCEL_CHECK_CFG,(evt_param_t)NULL);
}

//======================================================================
// accel_mma8652_get_xyz
//----------------------------------------------------------------------
/// @brief  Get XYZ values
/// @param  none
//======================================================================
public void accel_mma8652_get_xyz()
{
    kal_proc_post_evt(&drv_mma_p,DRV_EVT_ACCEL_GETXYZ,(evt_param_t)NULL);
}


