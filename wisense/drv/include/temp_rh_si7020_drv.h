/// @copyright
/// ========================================================================={{{
/// Copyright (c) 2012-2013 WizziLab                                           /
/// All rights reserved                                                        /
///                                                                            /
/// IMPORTANT: This Software may not be modified, copied or distributed unless /
/// embedded on a WizziLab product. Other than for the foregoing purpose, this /
/// Software and/or its documentation may not be used, reproduced, copied,     /
/// prepared derivative works of, modified, performed, distributed, displayed  /
/// or sold for any purpose. For the sole purpose of embedding this Software   /
/// on a WizziLab product, copy, modification and distribution of this         /
/// Software is granted provided that the following conditions are respected:  /
///                                                                            /
/// *  Redistributions of source code must retain the above copyright notice,  /
///    this list of conditions and the following disclaimer                    /
///                                                                            /
/// *  Redistributions in binary form must reproduce the above copyright       / 
///    notice, this list of conditions and the following disclaimer in the     /
///    documentation and/or other materials provided with the distribution.    /
///                                                                            /
/// *  The name of WizziLab can not be used to endorse or promote products     /
///    derived from this software without specific prior written permission.   /
///                                                                            /
/// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS        /
/// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED  /
/// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR /
/// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR          /
/// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,      /
/// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,        /
/// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,            /
/// OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY     /
/// OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING    /
/// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS         /
/// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.               /
/// WIZZILAB HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,       /
/// ENHANCEMENTS OR MODIFICATIONS.                                             /
///                                                                            /
/// Should you have any questions regarding your right to use this Software,   /
/// contact WizziLab at www.wizzilab.com.                                      /
///                                                                            /
/// =========================================================================}}}
/// @endcopyright

//  =======================================================================
///  @file  temp_rh_si7020_drv.h
///  @brief Functions to handle the SI7020 Read and Write to registers
//  =======================================================================

#ifndef __SI7020_DRV_H___
#define __SI7020_DRV_H___

#include "hal.h"
#include "kal.h"
#include "drv_events.h"

//======================================================================
// System parameter


// measurement resolution
#define SI7020_RH_TEMP_RES_14_12    0x00
#define SI7020_RH_TEMP_RES_12_08    0x01
#define SI7020_RH_TEMP_RES_13_10    0x80
#define SI7020_RH_TEMP_RES_11_11    0x81

// Heater
#define SI7020_HEATER_ENABLE        0x04
#define SI7020_HEATER_DISABLE       0x05

// VDD status
#define SI7020_VDD_STATUS           0x40


//======================================================================
// Application events
//======================================================================
typedef enum
{
    // internal events
    DRV_IEVT_RH_TEMP_MEASURE_NHMM = DRV_EVT_BASE(DRV_ID_RH_TEMP),
    DRV_IEVT_RH_TEMP_MEASURE_BOTH_NHMM,
    DRV_IEVT_RH_TEMP_RESET,
    DRV_IEVT_RH_TEMP_RES,
    // external events
    DRV_EVT_RH_TEMP_INIT_DONE,
    DRV_EVT_RH_TEMP_AVAILABLE,
    
} si7020_events_t;


//======================================================================
// Application processes
//======================================================================
KAL_PROCESS_DECLARE(si7020_p);



//======================================================================
// si7020_open
//----------------------------------------------------------------------
/// @brief  initialize the SI7020 module (first function to use)
/// @param  tgt_process     process_t   target process
/// @retval                 void        
//======================================================================
public void si7020_open( process_t* tgt_process );


//======================================================================
// si7020_close
//----------------------------------------------------------------------
/// @brief  close the SI7020 session
/// @param                  void
/// @retval                 void        
//======================================================================
public void si7020_close( void );


//======================================================================
// si7020_set_heater
//----------------------------------------------------------------------
/// @brief  change the resolution parameters
/// @param  param           u8              parameter
/// @retval                 void        
//======================================================================
public void si7020_set_heater( u8 param );


//======================================================================
// si7020_change_res
//----------------------------------------------------------------------
/// @brief  change the resolution parameters
/// @param  res             u8              resolution
/// @retval                 void        
//======================================================================
public void si7020_change_res( u8 res );


//======================================================================
// si7020_rd_rh_nhmm
//----------------------------------------------------------------------
/// @brief  read the relative humidity value
/// @param  data            u8*             data read
/// @retval                 void        
//======================================================================
public void si7020_rd_rh_nhmm( u8* data );


//======================================================================
// si7020_rd_temp_nhmm
//----------------------------------------------------------------------
/// @brief  read the temperature value
/// @param  data            s8*             data read
/// @retval                 void        
//======================================================================
public void si7020_rd_temp_nhmm( s8* data );


//======================================================================
// si7020_rd_rh_temp_nhmm
//----------------------------------------------------------------------
/// @brief  read the temperature value
/// @param  rh              u8*             humidity
/// @param  temp            s8*             temperature
/// @retval                 void        
//======================================================================
public void si7020_rd_rh_temp_nhmm( u8* rh, s8* temp );


//======================================================================
// si7020_reset
//----------------------------------------------------------------------
/// @brief  read the temperature value
/// @param                  void
/// @retval                 void        
//======================================================================
public void si7020_reset( void );




#endif //__M24LR_DRV_H__
