/// @copyright
/// ========================================================================={{{
/// Copyright (c) 2012-2013 WizziLab                                           /
/// All rights reserved                                                        /
///                                                                            /
/// IMPORTANT: This Software may not be modified, copied or distributed unless /
/// embedded on a WizziLab product. Other than for the foregoing purpose, this /
/// Software and/or its documentation may not be used, reproduced, copied,     /
/// prepared derivative works of, modified, performed, distributed, displayed  /
/// or sold for any purpose. For the sole purpose of embedding this Software   /
/// on a WizziLab product, copy, modification and distribution of this         /
/// Software is granted provided that the following conditions are respected:  /
///                                                                            /
/// *  Redistributions of source code must retain the above copyright notice,  /
///    this list of conditions and the following disclaimer                    /
///                                                                            /
/// *  Redistributions in binary form must reproduce the above copyright       / 
///    notice, this list of conditions and the following disclaimer in the     /
///    documentation and/or other materials provided with the distribution.    /
///                                                                            /
/// *  The name of WizziLab can not be used to endorse or promote products     /
///    derived from this software without specific prior written permission.   /
///                                                                            /
/// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS        /
/// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED  /
/// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR /
/// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR          /
/// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,      /
/// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,        /
/// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,            /
/// OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY     /
/// OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING    /
/// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS         /
/// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.               /
/// WIZZILAB HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,       /
/// ENHANCEMENTS OR MODIFICATIONS.                                             /
///                                                                            /
/// Should you have any questions regarding your right to use this Software,   /
/// contact WizziLab at www.wizzilab.com.                                      /
///                                                                            /
/// =========================================================================}}}
/// @endcopyright

//  =======================================================================
/// @file           accel_mma7660.h
/// @brief          Accelerometer Driver API
//  =======================================================================

/// @ingroup DRV
/// @defgroup ACCEL_API       Accelerometer API
/// @{

#ifndef __ACCEL_MMA8652_H__
#define __ACCEL_MMA8652_H__

#include "drv_events.h"
//==========================================================================================
// Enumerators and definitions
//==========================================================================================
//======================================================================
// MMA driver events
//======================================================================
typedef enum
{
    _DRV_ACCEL_EVENT_BASE=DRV_EVT_BASE(DRV_ID_ACCEL),

    DRV_EVT_ACCEL_RES_AVAILABLE=_DRV_ACCEL_EVENT_BASE,
    DRV_EVT_ACCEL_INIT_DONE,
    DRV_EVT_ACCEL_CHECK_CFG_DONE,
    DRV_EVT_ACCEL_ERROR,
    // Internal driver events
    DRV_EVT_ACCEL_CHECK_CFG,
    DRV_EVT_ACCEL_GETXYZ,
    
} drv_accel_events_t;


typedef enum {
    MMA_RATE_1_SPS      = 0,
    MMA_RATE_2_SPS,
    MMA_RATE_5_SPS,
    MMA_RATE_10_SPS,
    MMA_RATE_15_SPS,
    MMA_RATE_20_SPS,
    MMA_RATE_25_SPS,
    MMA_RATE_50_SPS,
    MMA_RATE_100_SPS,
    MMA_RATE_200_SPS,
    MMA_RATE_300_SPS,
    MMA_RATE_400_SPS,
    MMA_RATE_500_SPS,
    MMA_RATE_700_SPS,
    MMA_RATE_900_SPS,
} mma_rate_t;


typedef struct {
    /// X-axis acceleration in ~1mG/unit (signed)
    s16  xout;
    /// Y-axis acceleration in ~1mG/unit (signed)
    s16  yout;
    /// Z-axis acceleration in ~1mG/unit (signed)
    s16  zout;
    /// Compound status containing :
    u8  stat;
} accel_res_t;

//======================================================================
// accel_mma8652_open 
//----------------------------------------------------------------------
/// @brief  Open Accelerometer. Interactions with the accelerometer are
///         all event-based and results are available in event message.
/// @param  mode : bitmap selection of enabled interrupts among mma_int_mode_t.
///         Any combination of interrupts can be selected.
/// @param  refresh_rate : number of sample updates per seconds among mma_rate_t.
/// @param  tgt_process : event receiving process
//======================================================================
public void accel_mma8652_open( u8 mode, mma_rate_t refresh_rate, process_t* tgt_process);

//======================================================================
// accel_mma8652_close 
//----------------------------------------------------------------------
/// @brief  Disables accel_mma8652 sensor
/// @param  
//======================================================================
public void accel_mma8652_close();

//======================================================================
// accel_mma8652_check_cfg
//----------------------------------------------------------------------
/// @brief  Check configuration registers
/// @param  none
//======================================================================
public void accel_mma8652_check_cfg();

//======================================================================
// accel_mma8652_get_xyz
//----------------------------------------------------------------------
/// @brief  Get XYZ values
/// @param  none
//======================================================================
public void accel_mma8652_get_xyz();


#endif // __ACCEL_MMA8652_H__

