/// @copyright
/// ========================================================================={{{
/// Copyright (c) 2012-2013 WizziLab                                           /
/// All rights reserved                                                        /
///                                                                            /
/// IMPORTANT: This Software may not be modified, copied or distributed unless /
/// embedded on a WizziLab product. Other than for the foregoing purpose, this /
/// Software and/or its documentation may not be used, reproduced, copied,     /
/// prepared derivative works of, modified, performed, distributed, displayed  /
/// or sold for any purpose. For the sole purpose of embedding this Software   /
/// on a WizziLab product, copy, modification and distribution of this         /
/// Software is granted provided that the following conditions are respected:  /
///                                                                            /
/// *  Redistributions of source code must retain the above copyright notice,  /
///    this list of conditions and the following disclaimer                    /
///                                                                            /
/// *  Redistributions in binary form must reproduce the above copyright       / 
///    notice, this list of conditions and the following disclaimer in the     /
///    documentation and/or other materials provided with the distribution.    /
///                                                                            /
/// *  The name of WizziLab can not be used to endorse or promote products     /
///    derived from this software without specific prior written permission.   /
///                                                                            /
/// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS        /
/// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED  /
/// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR /
/// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR          /
/// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,      /
/// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,        /
/// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,            /
/// OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY     /
/// OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING    /
/// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS         /
/// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.               /
/// WIZZILAB HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,       /
/// ENHANCEMENTS OR MODIFICATIONS.                                             /
///                                                                            /
/// Should you have any questions regarding your right to use this Software,   /
/// contact WizziLab at www.wizzilab.com.                                      /
///                                                                            /
/// =========================================================================}}}
/// @endcopyright

//  =======================================================================
/// @file           baro_mpl115.h
/// @brief          Pressure Sensor Driver API
//  =======================================================================

/// @ingroup DRV
/// @defgroup PRESS_API       Pressure Sensor API
/// @{

#ifndef __BARO_MPL_H
#define  __BARO_MPL_H

#include "drv_events.h"

//==========================================================================================
// Enumerators and definitions
//==========================================================================================

//======================================================================
// MMA driver events
//======================================================================
typedef enum
{
    _DRV_BARO_EVENT_BASE=DRV_EVT_BASE(DRV_ID_BARO),

    DRV_EVT_BARO_RES_AVAILABLE=_DRV_BARO_EVENT_BASE,
    DRV_EVT_BARO_INIT_DONE,
    DRV_EVT_BARO_ERROR,
    // Internal driver events
    DRV_EVT_BARO_GET_PRESS,
    
} drv_baro_events_t;

// Evt params used by baro driver timers
#define BARO_READ_DATA 0xa
#define BARO_REQ_DATA  0xb

//======================================================================
// baro_mpl115_open 
//----------------------------------------------------------------------
/// @brief  Open Pressure Sensor. Interactions with the pressure sensor are
///         all event-based and results are available in event message.
/// @param  mode : bitmap selection of enabled interrupts among mpl_int_mode_t.
///         Any combination of interrupts can be selected.
/// @param  refresh_rate : number of sample updates per seconds among mpl_rate_t.
/// @param  tgt_process : event receiving process
//======================================================================
public void baro_mpl115_open(u16 refresh_rate, process_t* tgt_process);

//======================================================================
// baro_mpl115_close 
//----------------------------------------------------------------------
/// @brief  Close Pressure Sensor
/// @param  none
//======================================================================
public void baro_mpl115_close();

//======================================================================
// baro_mpl115_get_press
//----------------------------------------------------------------------
/// @brief  Get Pressure value
/// @param  none
//======================================================================
public void baro_mpl115_get_press();

#endif // __BARO_MPL_H
