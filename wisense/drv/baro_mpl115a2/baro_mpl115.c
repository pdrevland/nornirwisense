/// @copyright
/// ========================================================================={{{
/// Copyright (c) 2012-2013 WizziLab                                           /
/// All rights reserved                                                        /
///                                                                            /
/// IMPORTANT: This Software may not be modified, copied or distributed unless /
/// embedded on a WizziLab product. Other than for the foregoing purpose, this /
/// Software and/or its documentation may not be used, reproduced, copied,     /
/// prepared derivative works of, modified, performed, distributed, displayed  /
/// or sold for any purpose. For the sole purpose of embedding this Software   /
/// on a WizziLab product, copy, modification and distribution of this         /
/// Software is granted provided that the following conditions are respected:  /
///                                                                            /
/// *  Redistributions of source code must retain the above copyright notice,  /
///    this list of conditions and the following disclaimer                    /
///                                                                            /
/// *  Redistributions in binary form must reproduce the above copyright       / 
///    notice, this list of conditions and the following disclaimer in the     /
///    documentation and/or other materials provided with the distribution.    /
///                                                                            /
/// *  The name of WizziLab can not be used to endorse or promote products     /
///    derived from this software without specific prior written permission.   /
///                                                                            /
/// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS        /
/// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED  /
/// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR /
/// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR          /
/// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,      /
/// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,        /
/// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,            /
/// OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY     /
/// OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING    /
/// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS         /
/// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.               /
/// WIZZILAB HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,       /
/// ENHANCEMENTS OR MODIFICATIONS.                                             /
///                                                                            /
/// Should you have any questions regarding your right to use this Software,   /
/// contact WizziLab at www.wizzilab.com.                                      /
///                                                                            /
/// =========================================================================}}}
/// @endcopyright

//  =======================================================================
/// @file           baro_mpl115.c
/// @brief          Pressure Sensor Driver
//  =======================================================================

#include "hal.h"
#include "kal.h"
#include "hal_defs.h"
#include "baro_mpl115.h"
#include "drv_events.h"

//==========================================================================================
// Debug
//==========================================================================================
#if 0
#define DPRINT(...) kal_dbg_printf(__VA_ARGS__)
#else
#define DPRINT(...)
#endif
//==========================================================================================
// Enumerators and definitions
//==========================================================================================
enum {
    BARO_STATE_CLOSED,
    BARO_STATE_INIT,
    BARO_STATE_OPEN,
    BARO_STATE_WAIT_CONVERSION,
    BARO_STATE_WAIT_DATA,
    BARO_STATE_CONV_REQ,
    BARO_STATE_CLOSING
};

#define BARO_CONVERSION_TIME 20
typedef struct {
    process_t*          tgt_process;
    hal_i2c_dev_t(8)    dev;
    u8                  state;
    u16                 rate;
    s32                 a0;
    s32                 b1;
    s32                 b2;
    s32                 c12;
    kal_timer_t         tim;
} mpldev_t;

mpldev_t g_mpl;

#define MPL_I2C_ADDR        0x60

// MPL Register map
#define MPL_REG_PADC_MSB        0x0
#define MPL_REG_PADC_LSB        0x1
#define MPL_REG_TADC_MSB        0x2
#define MPL_REG_TADC_LSB        0x3
#define MPL_REG_A0_MSB          0x4
#define MPL_REG_A0_LSB          0x5
#define MPL_REG_B1_MSB          0x6
#define MPL_REG_B1_LSB          0x7
#define MPL_REG_B2_MSB          0x8
#define MPL_REG_B2_LSB          0x9
#define MPL_REG_C12_MSB         0xa
#define MPL_REG_C12_LSB         0xb
#define MPL_REG_RESERVED0       0xc
#define MPL_REG_RESERVED1       0xd
#define MPL_REG_RESERVED2       0xe
#define MPL_REG_RESERVED3       0xf
#define MPL_REG_RESERVED4       0x10
#define MPL_REG_RESERVED5       0x11
#define MPL_REG_CONVERT         0x12

// Pcomp = a0 + (b1+c12*Tadc)*Padc + b2*Tadc
// 

KAL_PROCESS(drv_mpl_p, DRV_MPL_PROCESS_ID);
KAL_PROC_THREAD(drv_mpl_p, ev, evp)
{
    (void) evp;
    u16 padc, tadc;
    int pcomp,a2x2;
    u32 pressure;

    KAL_PROC_BEGIN();

    while(1) {
        if (ev==KAL_EVT_TIMER)
        {
            // If repetitive measure, launch timer for next measure
            if ((g_mpl.rate>0) && (g_mpl.state == BARO_STATE_OPEN))
            {
                kal_timer_start(&(g_mpl.tim),KAL_ETIMER,&drv_mpl_p , BARO_REQ_DATA, g_mpl.rate);
            }
            // Request conversion
            baro_mpl115_get_press();
        }
        else if (ev==DRV_EVT_BARO_GET_PRESS)
        {
            // Request conversion
            DPRINT("BARO Request Conversion\n");
            hal_i2c_wr_reg(HAL_I2C_BARO, &g_mpl.dev,MPL_REG_CONVERT,0);
            KAL_PROC_WAIT_UNTIL_EVENT(HAL_EVT_I2C_XFER_DONE);
            if ((evp.data==HAL_EVT_I2C_XFER_ARB_LOST) || (evp.data==HAL_EVT_I2C_XFER_NACK))
            {
                // TODO Implement error recovery
                kal_dbg_assert(FALSE, "ASSERT: MMA I2C Error %d\n",evp.data);
            }

            // Conversion command sent - result ready in BARO_CONVERSION_TIME ms
            DPRINT("BARO Conversion command sent - Wait for conversion\n");
            kal_timer_start(&(g_mpl.tim),KAL_ETIMER,&drv_mpl_p , BARO_READ_DATA, BARO_CONVERSION_TIME);
            KAL_PROC_WAIT_UNTIL_EVENT(KAL_EVT_TIMER);
            DPRINT("BARO Read Conversion Result\n");
            // Read Results
            hal_i2c_rd_regs(HAL_I2C_BARO, &g_mpl.dev,MPL_REG_PADC_MSB,4);
            KAL_PROC_WAIT_UNTIL_EVENT(HAL_EVT_I2C_XFER_DONE);
            if ((evp.data==HAL_EVT_I2C_XFER_ARB_LOST) || (evp.data==HAL_EVT_I2C_XFER_NACK))
            {
                // TODO Implement error recovery
                kal_dbg_assert(FALSE, "ASSERT: MMA I2C Error %d\n",evp.data);
            }

            padc = ((g_mpl.dev.buf[0]<<8) | (g_mpl.dev.buf[1]))>>6;
            tadc = ((g_mpl.dev.buf[2]<<8) | (g_mpl.dev.buf[3]))>>6;
            // ----------------------------------------------
            // Pcomp = a0 + (b1+c12*Tadc)*Padc + b2*Tadc
            // Compute the result of 2^14 Pcomp
            // 2^14 Pcomp = a0*2^11 + (2*b1+c12*Tadc/2^8)*Padc + b2*Tadc
            // ----------------------------------------------
            pcomp = (g_mpl.c12*tadc)>>8;   
            pcomp = (pcomp+2*g_mpl.b1);   
            pcomp = (pcomp*padc);       
            pcomp = pcomp+(g_mpl.a0<<11);
            a2x2 = g_mpl.b2*tadc;
            pcomp = pcomp+a2x2;    
            DPRINT("Pcomp 0x%04x%04x in .14 format\n",(u16)((u32) (pcomp)>>16),pcomp&0xffff);
            DPRINT("Pcomp %d \n",pcomp>>14);
            // reduce number of bits before last computation
            pcomp=pcomp>>6;

            pressure = 500+((pcomp*10*(115-50))>>8)/1023;
            DPRINT("Pressure %d hPa\n",pressure);

            kal_proc_post_evt(g_mpl.tgt_process,DRV_EVT_BARO_RES_AVAILABLE,(evt_param_t)(int)pressure);
        }
        else if (ev==HAL_EVT_I2C_XFER_NACK || ev==HAL_EVT_I2C_XFER_ARB_LOST)
        {
            kal_proc_post_evt(g_mpl.tgt_process,DRV_EVT_BARO_ERROR,(evt_param_t)NULL);
        }
        else if (ev==KAL_EVT_INIT)
        {
            DPRINT("MPL Init\n");

            // Set device in active mode (release shutdown and reset)
            hal_gpio_set(HAL_GPIO_BARO_SHDN);
            hal_gpio_set(HAL_GPIO_BARO_RST);


            // Read calibration coefficients
            hal_i2c_rd_regs(HAL_I2C_BARO, &g_mpl.dev,MPL_REG_A0_MSB,8);
            KAL_PROC_WAIT_UNTIL_EVENT(HAL_EVT_I2C_XFER_DONE);
            if ((evp.data==HAL_EVT_I2C_XFER_ARB_LOST) || (evp.data==HAL_EVT_I2C_XFER_NACK))
            {
                // TODO Implement error recovery
                kal_dbg_assert(FALSE, "ASSERT: MMA I2C Error %d\n",evp.data);
            }

            // Save calibration parameters in pressure sensor context
            g_mpl.a0 =  (g_mpl.dev.buf[0]<<8) | (g_mpl.dev.buf[1]);
            g_mpl.b1 =  (g_mpl.dev.buf[2]<<8) | (g_mpl.dev.buf[3]);
            g_mpl.b2 =  (g_mpl.dev.buf[4]<<8) | (g_mpl.dev.buf[5]);
            g_mpl.c12 = (g_mpl.dev.buf[6]<<8) | (g_mpl.dev.buf[7]);
            DPRINT("Raw Coeff a0 %x, b1 %x, b2 %x, c12 %x\n",g_mpl.a0,g_mpl.b1,g_mpl.b2,g_mpl.c12);
            // a0 is 12.3 SIIIIIIIIIIII.DDD
            if (g_mpl.a0 & 0x8000)
            {
                // extend sign on 32 bits
                g_mpl.a0 |= 0xffff0000;
            } 
            // b1 is 2.13 SII.DDDDDDDDDDDDD
            if (g_mpl.b1 & 0x8000)
            {
                // extend sign on 32 bits
                g_mpl.b1 |= 0xffff0000;
            } 
            // b2 is 1.14 SI.DDDDDDDDDDDDDD
            if (g_mpl.b2 & 0x8000)
            {
                // extend sign on 32 bits
                g_mpl.b2 |= 0xffff0000;
            } 
            // c12 is is represented as SDDDDDDDDDDDDD00 (9 zero after decimal implicit)
            // c12 is S0.000000000DDDDDDDDDDDDD00
            if (g_mpl.c12 & 0x8000)
            {
                // extend sign on 32 bits
                g_mpl.c12 |= 0xffff0000;
            }
            // Remove 2 trailing 0
            g_mpl.c12 = g_mpl.c12>>2;
            g_mpl.state = BARO_STATE_OPEN;
            if (g_mpl.rate!=0)
            {
                //lauch timer for data reporting
                kal_timer_start(&(g_mpl.tim),KAL_ETIMER,&drv_mpl_p , BARO_REQ_DATA, g_mpl.rate);
            }
            DPRINT("Raw Coeff a0 %d, b1 %d, b2 %d, c12 %d\n",g_mpl.a0,g_mpl.b1,g_mpl.b2,g_mpl.c12);
            kal_proc_post_evt(g_mpl.tgt_process,DRV_EVT_BARO_INIT_DONE,(evt_param_t)NULL);
        }
        else if (ev==KAL_EVT_EXIT)
        {
            hal_i2c_free(HAL_I2C_BARO);
            // Put device in shutdown and reset
            hal_gpio_clr(HAL_GPIO_BARO_SHDN);
            hal_gpio_clr(HAL_GPIO_BARO_RST);
            g_mpl.state = BARO_STATE_CLOSED;
            KAL_PROC_EXIT();
        }
        KAL_PROC_WAIT_EVENT();
    }
    KAL_PROC_END();
}

//======================================================================
// baro_mpl115_open 
//----------------------------------------------------------------------
/// @brief  Open Pressure Sensor. Interactions with the pressure sensor are
///         all event-based and results are available in event message.
/// @param  mode : bitmap selection of enabled interrupts among mpl_int_mode_t.
///         Any combination of interrupts can be selected.
/// @param  refresh_rate : number of sample updates per seconds among mpl_rate_t.
/// @param  tgt_process : event receiving process
//======================================================================
public void baro_mpl115_open(u16 refresh_rate, process_t* tgt_process)
{

    DPRINT("MPL Open\n");
    g_mpl.tgt_process  = tgt_process;
    g_mpl.rate = refresh_rate;
    kal_timer_init(&(g_mpl.tim));

    
    hal_i2c_new(&g_mpl.dev,MPL_I2C_ADDR, I2C_MASTER_MODE, 100);
    ((hal_i2c_dev_t*)&g_mpl.dev)->p    = &drv_mpl_p;

    g_mpl.state = BARO_STATE_INIT;
    kal_proc_start(&drv_mpl_p,(evt_param_t)NULL);
}

//======================================================================
// baro_mpl115_close 
//----------------------------------------------------------------------
/// @brief  Close Pressure Sensor
/// @param  none
//======================================================================
public void baro_mpl115_close()
{
    DPRINT("MPL Close\n");
    if (g_mpl.state == BARO_STATE_CLOSED) return;
    kal_proc_post_evt(&drv_mpl_p,KAL_EVT_EXIT,(evt_param_t)NULL);
}

//======================================================================
// baro_mpl115_get_press
//----------------------------------------------------------------------
/// @brief  Get XYZ values
/// @param  none
//======================================================================
public void baro_mpl115_get_press()
{
    kal_proc_post_evt(&drv_mpl_p,DRV_EVT_BARO_GET_PRESS,(evt_param_t)NULL);
}
