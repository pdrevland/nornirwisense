/// @copyright
/// ========================================================================={{{
/// Copyright (c) 2012-2013 WizziLab                                           /
/// All rights reserved                                                        /
///                                                                            /
/// IMPORTANT: This Software may not be modified, copied or distributed unless /
/// embedded on a WizziLab product. Other than for the foregoing purpose, this /
/// Software and/or its documentation may not be used, reproduced, copied,     /
/// prepared derivative works of, modified, performed, distributed, displayed  /
/// or sold for any purpose. For the sole purpose of embedding this Software   /
/// on a WizziLab product, copy, modification and distribution of this         /
/// Software is granted provided that the following conditions are respected:  /
///                                                                            /
/// *  Redistributions of source code must retain the above copyright notice,  /
///    this list of conditions and the following disclaimer                    /
///                                                                            /
/// *  Redistributions in binary form must reproduce the above copyright       /
///    notice, this list of conditions and the following disclaimer in the     /
///    documentation and/or other materials provided with the distribution.    /
///                                                                            /
/// *  The name of WizziLab can not be used to endorse or promote products     /
///    derived from this software without specific prior written permission.   /
///                                                                            /
/// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS        /
/// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED  /
/// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR /
/// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR          /
/// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,      /
/// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,        /
/// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,            /
/// OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY     /
/// OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING    /
/// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS         /
/// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.               /
/// WIZZILAB HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,       /
/// ENHANCEMENTS OR MODIFICATIONS.                                             /
///                                                                            /
/// Should you have any questions regarding your right to use this Software,   /
/// contact WizziLab at www.wizzilab.com.                                      /
///                                                                            /
/// =========================================================================}}}
/// @endcopyright

//  =======================================================================
/// @file           alp.h
/// @defgroup       ALP
//  =======================================================================

#ifndef __ALP_H__
#define __ALP_H__

/// Disable execution of QP commands
#define ALP_QP_CMD_DISABLE

#include "hal.h"
#include "kal_events.h"

/// shift applied to create the groups of events
#define _ALP_EVENT_GROUP_SHIFT                      (8)

/// mask used to select a group of events
#define _ALP_EVENT_MASK                             (0xFFFF << _ALP_EVENT_GROUP_SHIFT)

/// relative offset of the ALP reception events
#define _ALP_EVENT_OFFSET_RX                        (0 << _ALP_EVENT_GROUP_SHIFT)

/// relative offset of the ALP transmission events
#define _ALP_EVENT_OFFSET_TX                        (1 << _ALP_EVENT_GROUP_SHIFT)

/// base (absolute offset) of the ALP reception events
#define _ALP_EVENT_BASE_RX                          (_ALP_EVENT_BASE + _ALP_EVENT_OFFSET_RX)

/// base (absolute offset) of the ALP transmission events
#define _ALP_EVENT_BASE_TX                          (_ALP_EVENT_BASE + _ALP_EVENT_OFFSET_TX)

/// wait until a ALP reception event is received
#define ALP_WAIT_RX_EVENT()                         KAL_PROC_WAIT_UNTIL_EVENT_RANGE(_ALP_EVENT_BASE_RX,_ALP_EVENT_MASK)

/// wait until a ALP transmission event is received
#define ALP_WAIT_TX_EVENT()                         KAL_PROC_WAIT_UNTIL_EVENT_RANGE(_ALP_EVENT_BASE_TX,_ALP_EVENT_MASK)

#define ALP_REC_HEADER_LEN              ((u16)(sizeof(alp_rec_t) - sizeof(u8)))

#define ALP_RESPOND                     0x80
#define ALP_RESPOND_IS_SET(cmd_byte)    ((cmd_byte)&ALP_RESPOND)

#define ALP_ACCESS_TPL_SIZE sizeof(alp_data_access_tpl_t)

#define ALP_FILE_BLOCK_GET(cmd_byte) ((cmd_byte) & 0x70)
#define ALP_FILE_OP_GET(cmd_byte) ((cmd_byte) & 0xf)

#define ALP_FILE_IS_RESP(cmd_byte) (((cmd_byte) & 0x3) == 1 || \
                                    ((cmd_byte) & 0xf) == 0xf)

#define ALP_IFACE_NONE 0xff

typedef enum
{
    ALP_IFACE_EVT_RX_PKT            = _ALP_EVENT_BASE_RX,
    ALP_IFACE_EVT_RX_NULL_CMD       ,
    ALP_IFACE_EVT_RX_TIMEOUT        ,
    ALP_IFACE_EVT_TX                = _ALP_EVENT_BASE_TX,
    ALP_IFACE_EVT_TX_TIMEOUT        ,

} alp_iface_events_t;

typedef enum
{
    ALP_FLAG_MB                 = 0x80,
    ALP_FLAG_ME                 = 0x40,
    ALP_FLAG_CC                 = 0x20,

} alp_flag_t;


typedef enum
{
    ALP_BID_NULL                = 0,
    ALP_BID_FILE_DATA           = 1,
    ALP_BID_QP                  = 2,

} alp_base_id_t;


typedef enum
{
    ALP_FILE_BLOCK_CUP_CODE     = (0 << 4),
    ALP_FILE_BLOCK_CUP_CFG      = (1 << 4),
    ALP_FILE_BLOCK_ISFSB        = (2 << 4),
    ALP_FILE_BLOCK_ISFB         = (3 << 4),

} alp_file_block_t;

typedef enum
{
    ALP_OP_NULL_ACK             = 2,
    ALP_OP_NULL_TX_FAILED       = 5,
    ALP_OP_NULL_NACK            = 6,
    ALP_OP_NULL_BOOT            = 7,
    ALP_OP_NULL_NFC_START       = 8,
    ALP_OP_NULL_NFC_DONE        = 9,

} alp_null_op_t;


typedef enum
{
    ALP_OP_READ_PERM            = 0,
    ALP_OP_RETURN_PERM          = 1,

    ALP_OP_WRITE_PERM           = 3,
    ALP_OP_READ_DATA            = 4,
    ALP_OP_RETURN_DATA          = 5,

    ALP_OP_WRITE_FLUSH_DATA     = 6,

    ALP_OP_WRITE_DATA           = 7,
    ALP_OP_READ_HEADER          = 8,
    ALP_OP_RETURN_HEADER        = 9,
    ALP_OP_DELETE_FILE          = 10,
    ALP_OP_CREATE_FILE          = 11,
    ALP_OP_READ_DATA_N_HEADER   = 12,
    ALP_OP_RETURN_DATA_N_HEADER = 13,
    ALP_OP_RESTORE_FILE         = 14,
    ALP_OP_RETURN_ERR           = 15,


} alp_op_t;

typedef enum
{
    // Alp file errors
    ALP_ERR_NO                  = 0,
    ALP_ERR_FILE_NOT_FOUND      = 1,
    ALP_ERR_FILE_EXIST          = 2,
    ALP_ERR_FILE_NOT_RESTORABLE = 3,
    ALP_ERR_PERMISSION_DENIED   = 4,
    ALP_ERR_CREATE_LEN_BEYOND_LIM = 5,
    ALP_ERR_CREATE_ALC_BEYOND_LIM = 6,
    ALP_ERR_WRITE_OFFSET_OUT_OF_BOUNDS = 7,
    ALP_ERR_WRITE_BEYOND_ALC    = 8,
    ALP_ERR_UNKNOWN             = 0xff,

    // Internal errors
    ALP_ERR_NULL_POINTER = 0x81,
    ALP_ERR_ID_UNKNOWN   = 0x82,
    ALP_ERR_WRONG_FORMAT = 0x83,
    ALP_ERR_TX_FAILED    = 0x84,
    ALP_ERR_NOT_A_PACKET = 0x85,
    ALP_ERR_SERIAL_ID_UNKNOWN   = 0x86,
    ALP_ERR_SERIAL_NOT_PKT      = 0x87,
    ALP_ERR_WRITE_FILE          = 0x88,
    ALP_ERR_CMD_UNKNOWN         = 0x89,

} alp_err_t;


TYPEDEF_STRUCT_PACKED
{
    u8 flags;
    u8 len;
    u8 id;
    u8 cmd;
    u8 data[1];

} alp_rec_t;


TYPEDEF_STRUCT_PACKED
{
    u8 iface_id;
    u8 len;
    u8 data[1];
} alp_iface_rx_msg_t;


typedef struct
{
    /// Maximum number of retries
    u8 retry_nb;

} alp_retry_policy_t;

TYPEDEF_STRUCT_PACKED
{
    u8 file_id;
} alp_file_id_tpl_t;

// ==============================
// File permissions templates
// ==============================
typedef u8 alp_read_perm_tpl_t;

TYPEDEF_STRUCT_PACKED
{
    u8 file_id;
    u8 perm;
} alp_write_perm_tpl_t;

typedef alp_write_perm_tpl_t alp_return_perm_tpl_t;


// ==============================
// File data access templates
// ==============================
TYPEDEF_STRUCT_PACKED
{
    u8 file_id;
    u16 offset;
    u16 nbytes;
} alp_data_access_tpl_t;

typedef alp_data_access_tpl_t alp_read_tpl_t;

TYPEDEF_STRUCT_PACKED
{
    alp_data_access_tpl_t access_tpl;
    u8 data[1];
} alp_write_tpl_t;

typedef alp_write_tpl_t alp_return_file_tpl_t;

// ==============================
// File header access templates
// ==============================
typedef alp_file_id_tpl_t alp_read_header_tpl_t;

TYPEDEF_STRUCT_PACKED
{
    u8 file_id;
    u8 perm;
    u16 len;
    u16 alloc;
} alp_header_tpl_t;

typedef alp_header_tpl_t alp_return_header_tpl_t;

// ==============================
// Delete file templates
// ==============================
typedef alp_file_id_tpl_t alp_delete_tpl_t;

// ==============================
// Create file templates
// ==============================
typedef alp_return_header_tpl_t alp_create_tpl_t;

// ==============================
// File header and data
// ==============================
typedef alp_read_tpl_t alp_read_header_n_file_tpl_t;

TYPEDEF_STRUCT_PACKED
{
    u8 file_id;
    u8 perm;
    u16 len;
    u16 alloc;
    u16 offset;
    u16 nbytes;
    u8 data[1];
} alp_return_header_n_file_tpl_t;

// ==============================
// Error template
// ==============================
TYPEDEF_STRUCT_PACKED
{
    u8 file_id;
    u8 error_code;
} alp_return_error_tpl_t;

// ==============================
// API Functions
// ==============================

//======================================================================
// alp_open
//----------------------------------------------------------------------
/// @brief Initialize ALP mode
/// @param    uns_fwd_if      Interface to forward the unsolicited to - use ALP_IFACE_NONE if no forwarding wanted
/// @param    uns_fwd_nb      Number of forward on unsollicited forwarding
/// @param    cli_proc        The client process that will
///                                         receive alp responses
/// @retval                 void
//======================================================================
public void alp_open(u8 uns_fwd_if, u8 uns_fwd_nb, process_t *cli_proc);

// =======================================================================
// alp_iface_open_com
// -----------------------------------------------------------------------
/// @brief open COM interface
/// @param iface_id     u8                  identifier of the interface
/// @param dev_id       u8                  device identifier
/// @param group        kal_file_group_t    kal user group
// =======================================================================
public void alp_iface_open_com(u8 iface_id, u8 dev_id, kal_file_group_t group);

// =======================================================================
// alp_iface_open_d7a
// -----------------------------------------------------------------------
/// @brief open D7A interface
/// @param iface_id     u8                  identifier of the interface
/// @param group        kal_file_group_t    kal user group
// =======================================================================
public void alp_iface_open_d7a(u8 iface_id, kal_file_group_t group, alp_retry_policy_t* policy);



/// @} ALP_API

#endif // __ALP_H__
