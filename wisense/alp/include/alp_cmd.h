/// @copyright
/// ========================================================================={{{
/// Copyright (c) 2012-2013 WizziLab                                           /
/// All rights reserved                                                        /
///                                                                            /
/// Should you have any questions regarding your right to use this Software,   /
/// contact WizziLab at www.wizzilab.com.                                      /
///                                                                            /
/// =========================================================================}}}

#ifndef __ALP_CMD_H__
#define __ALP_CMD_H__

#include "alp.h"

public void alp_cmd_open(u8 iface_id);

public void alp_cmd_wf(const u8 fwd, const u8 file_block, const bool resp,
                       const u8 file_id, const u16 offset, const u16 file_size,
                       const u8 * const content);

public void alp_cmd_null(const u8 fwd, const bool resp, alp_null_op_t param);

#endif // __ALP_CMD_H__
