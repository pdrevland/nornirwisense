#!/bin/sh
# openocd can be controlled via GDB

arm-none-eabi-gdb -tui -x ../env/gdb/gdb.cmd \
 -ex "mon flash write_image erase $1" \
 -ex "b main" \
 -ex "b cup_entry" \
 -ex "mon reset halt" \
 -ex "mon wait_halt 200" \
 -ex "cont" \
 $1


