#!/usr/bin/perl
# openocd can be controlled via telnet
#   telnet localhost 4444
#   reset halt
#   flash probe 0 flash write_image erase <path_to_your_elf>/your_elf.elf
#   verify_image <path_to_your_elf>/your_elf.elf
#   reset halt -> can be replaced by "reset run" to actually run the code
#   exit

use Net::Telnet;
$ip = "127.0.0.1";
$port = 4444;
$telnet = new Net::Telnet (
Port => $port,
Timeout=>30,
Errmode=>'die',
Prompt =>'/>/');
$telnet->open($ip);
print $telnet->cmd('reset');
print $telnet->cmd('exit');
print "\n";
