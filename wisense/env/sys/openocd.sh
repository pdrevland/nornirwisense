#!/bin/sh
TARGET=$1
PROJ_BASE="../"
cd $PROJ_BASE./
echo "PROJ_BASE:" `pwd`

DEBUGGER="none"

# Look for a valid debugger
lsusb > .tmp_lsusb;
if [ `cat .tmp_lsusb | grep -c "ST-LINK/V2"` = 1 ]
then 
    DEBUGGER="stl"
elif [ `cat .tmp_lsusb | grep -c "ARM-USB-TINY-H"` = 1 ]
then 
    DEBUGGER="olimex_tiny"
fi

rm -f .tmp_lsusb
if [ "$TARGET" = "F" ] || [ "$TARGET" = "stm32f1x" ]
then
    echo "OpenOCD Target = stm32f1x"
    TARGET=stm32f1x
else
    echo "OpenOCD Target = stm32lx"
    TARGET=stm32lx
fi

if [ "$DEBUGGER" = "olimex_ocd" ] 
then
    # OLIMEX ARM-USB-OCD
    echo "---- Using ARM-USB-OCD debugger"
    openocd -f ./env/openocd/openocd.cfg \
        -f ./env/openocd/interface/olimex-arm-usb-ocd.cfg \
        -f ./env/openocd/target/$TARGET".cfg"
elif [ "$DEBUGGER" = "stl" ]
then
    # STLink V2
    echo "---- Using STLink V2 debugger"
    openocd -f ./env/openocd/openocd.cfg \
        -f ./env/openocd/interface/stlink-v2.cfg \
        -f ./env/openocd/target/$TARGET"_stlink.cfg"
elif [ "$DEBUGGER" = "olimex_tiny" ] 
then
    # ARM-USB-TINY-H
    echo "---- Using ARM-USB-TINY-H debugger"
    openocd -f ./env/openocd/openocd.cfg \
        -f ./env/openocd/interface/olimex-arm-usb-tiny-h.cfg \
        -f ./env/openocd/target/$TARGET".cfg"
else
    echo "Could not find a valid debugger"
fi
exit

