#!/usr/bin/env ruby
$LOAD_PATH.unshift File.dirname(__FILE__)
require 'optparse'
require 'pp'
require "fileutils"
require "net/telnet"
require 'digest/crc32'

DEFAULT_UID     = "deadbeef8badf00d"
WIZZILAB_OUI    = "001BC50C70000000"
FLASH_BASE      = "0x8000000"
CUP_BASE        = "0x801f000"
CUPCODE_BASE    = "0x800f300"

$target = "stm32lx"
$opt = {}
class  Net::Telnet
    def vcmd(options)
        if $opt[:debug]
            if options.kind_of?(Hash)
                string   = options["String"]
            else
                string = options
            end
            STDOUT << string
            STDOUT << "\n"
        end
        cmd(options)
    end
end

# Create new file from 'file' replacing 'duid' by 'uid'
def create_eeprom_image(file,duid,uid) #{{{
    ref_eeprom = open(file, "rb") {|io| io.read }
    ref_eeprom = ref_eeprom.unpack("H*")
    if ref_eeprom[0].sub!(duid,uid) == nil then raise "No matching UID in #{file}" end

    new_eeprom = ref_eeprom.pack("H*")

    # Remove the old crc at the end of file to append the new one
    new_eeprom = new_eeprom[0..-5]

    # Calculate the new crc
    new_crc = Digest::CRC32.checksum(new_eeprom)

    # Append at the end of the new_eeprom
    new_eeprom = new_eeprom + [new_crc].pack("l<")
    
    e2p_tmp = file.sub('eeprom.bin','') + uid + "_eeprom.bin"
    puts "Creating #{File.basename(e2p_tmp)}"
    open(e2p_tmp,"wb"){|f| f.write(new_eeprom)}
    e2p_tmp
end #}}}

# Burn relevant Sections
def burn_memory(e2p,bin,cup,cupcode) #{{{
    if $opt[:dry]== true then
        puts "dry-run : Flash/EEPROM won't be touched"
    end
    oocd = Net::Telnet::new("Host" => "127.0.0.1",
                           "Port" => 4444,
                           "Telnetmode" => false,
                           "Output_log" => "telnel_out.log",
                           #"Waittime" => 2,
                           "Prompt" => />/n)
	oocd.vcmd("adapter_khz 200")

    if ($opt[:burn].include?("F"))
        puts "Burning FLASH"
        if $opt[:dry]==false
            openOCD_write_verify_image(oocd,bin,FLASH_BASE)
        end
    end
    if ($opt[:burn].include?("C"))
        puts "Burning CUP"
        if $opt[:dry]==false
            openOCD_write_verify_image(oocd,cup,CUP_BASE)
        end
    end
    if ($opt[:burn].include?("U"))
        puts "Burning CUP code"
        if $opt[:dry]==false
            openOCD_write_verify_image(oocd,cupcode,CUPCODE_BASE)
        end
    end

    if ($opt[:burn].include?("E"))
        puts "Burning EEPROM"
        if $opt[:dry]==false
            if $target=="stm32lx"
                openOCD_write_eeprom_wordwise(oocd,e2p,$EEPROM_BASE)
            else
                openOCD_write_verify_image(oocd,e2p,$EEPROM_BASE)
            end
        end
    end
    openOCD_reset(oocd)
end #}}}

def openOCD_reset_halt(oocd,speed) #{{{
	# Make sure we will be able to reset
	oocd.vcmd("adapter_khz 100")

    STDOUT << "--- reset "
    begin
        r = oocd.vcmd("String"   => "reset halt",
                     "Match"    => /Not halted|ftdi_write_data|xPSR:/)
        if r=~ /ftdi_write_data/
            raise "JTAG Error: #{r}"
        elsif r=~ /Not halted/
            STDOUT <<  "\"Not Halted\" Retrying !"
        elsif r=~ /xPSR:/
            STDOUT << "OK\n"
            r = NIL
        else
            raise "Unknown Error: #{r}"
        end
    end while r!=NIL

    if speed > 300
	    #puts "STM32L: Enabling HSI"
	    # Set HSION in RCC_CR
	    oocd.vcmd("mww 0x40023800 0x00000101")
	    # Set HSI as SYSCLK
	    oocd.vcmd("mww 0x40023808 0x00000001")
        sleep(0.1)
    end
	# Change JTAG speed
    puts "--- adapter #{speed} KHz"
	oocd.vcmd("adapter_khz #{speed}")
end #}}}

# Burn/verify image via JTAG
def openOCD_write_verify_image(oocd,file,addr) #{{{

    openOCD_reset_halt(oocd,1000)
    r = oocd.vcmd("String"   => "flash probe 0",
                 "Match"    => /flash '#{$target}' found/)
    puts "--- probe OK"

    STDOUT << "--- write "
    r = oocd.vcmd("String"   => "flash write_image erase #{file} #{addr}",
                 "Match"    => /wrote/,
                 "Timeout"  => 15)
    if r=~ /couldn't open\s(.*)/
        raise "#{$1} not found"
    end
    STDOUT << "OK\n"

    openOCD_reset_halt(oocd,1000)

    STDOUT << "--- verif "
    r = oocd.vcmd("String"   => "verify_image #{file} #{addr}",
                 "Match"    => /verified|mismatch/)
    if r=~ /couldn't open\s(.*)/
        raise "#{$1} not found"
    elsif r=~ /mismatch/
        raise "Verify error:\n #{r}"
    end
    STDOUT << "OK\n"
end #}}}

def openOCD_reset(oocd) #{{{
	r = oocd.vcmd("adapter_khz 200")
    r = oocd.vcmd("reset")
    r = oocd.vcmd("exit")
end #}}}

# Burn/verify eeprom via JTAG
def openOCD_write_eeprom(oocd,file,addr) #{{{
    openOCD_reset_halt(oocd,4)

   fsize = File.size(file) - 1 # -1 for EOF ?
    # Unlock EEPROM
    r = oocd.vcmd("String"   => "mww 0x40023C0C 0x89ABCDEF")
    r = oocd.vcmd("String"   => "mww 0x40023C0C 0x02030405")
    STDOUT << "--- load EEPROM "
    r = oocd.vcmd("String"   => "load_image #{file} #{addr}", "Match"    => /downloaded|MEM_AP_TAR/)
    if r=~ /bytes written at address/
        STDOUT << "OK\n"
    elsif r=~ /MEM_AP_TAR 0x(.*)/
        if $1.to_i(16) == (fsize + $EEPROM_BASE.to_i(16))
            STDOUT << "Error but should be OK...\n"
        else
            STDOUT << "Error. Lets verify anyway...\n"
        end
    else
        raise "ERROR while burning EEPROM: #{r}"
    end
    # Lock EEPROM
    r = oocd.vcmd("String"   => "mww 0x40023C04 0x00000001")
    openOCD_reset_halt(oocd,200)
    STDOUT << "--- verif "
    r = oocd.vcmd("String"   => "verify_image #{file} #{addr}",
                 "Match"    => /differences|verified/)
    if r=~ /couldn't open\s(.*)/
        raise "#{$1} not found"
    elsif r=~ /mismatch/
        raise "Verify error:\n #{r}"
    end
    STDOUT << "OK\n"
end #}}}

def openOCD_write_eeprom_wordwise(oocd,file,addr) #{{{
    openOCD_reset_halt(oocd,200)

    fsize = File.size(file)# -1 for EOF ?
    e2p_code = open(file, "rb") {|io| io.read }
    tab = []
    offset = 0
    # build table, handle non multiple of 4Bytes sizes
    while offset < fsize do
        val=0
        begin
            val +=  (e2p_code[offset].unpack("H*")[0].to_i(16)) << (8 * (offset%4))
            offset += 1
        end while offset < fsize and offset%4 != 0
        tab << val
    end
    # format and handle signed stuff
    tab.map! {|i| "0x" + ("%08X" % i).sub(/\.\.F/,"")}

    # Unlock EEPROM
    r = oocd.vcmd("String"   => "mww 0x40023C0C 0x89ABCDEF")
    r = oocd.vcmd("String"   => "mww 0x40023C0C 0x02030405")

    # Burn word by word
    STDOUT << "--- load EEPROM\n"
    at = addr.to_i(16)
    offset = 0
    while offset < (fsize+3)/4 do
        cmd = "mww " + "0x%08X " % (at+ 4*offset) + tab[offset]
        r = oocd.vcmd(cmd)
        sleep(0.01)
        offset += 1
    end
    
    # Lock EEPROM
    r = oocd.vcmd("String"   => "mww 0x40023C04 0x00000001")
    sleep(0.01)

    #openOCD_reset_halt(oocd,200)
    STDOUT << "--- verif "
    r = oocd.vcmd("String"   => "verify_image #{file} #{addr}",
                 "Match"    => /differences|verified/)
    if r=~ /couldn't open\s(.*)/
        raise "#{$1} not found"
    elsif r=~ /mismatch/
        raise "Verify error:\n #{r}"
    end
    STDOUT << "OK\n"
end #}}}

##############################################################################
# Main Code
if $0 == __FILE__ then

# Commandline Options parser {{{
optparse = OptionParser.new {|opts|
    opts.banner = "Burns a binary (flash and eeprom), optionaly changing the UID
Usage: burn.rb [options] NAME"

    opts.separator ""
    opts.separator "Options:"

    $opt[:burn] = ["F"]
    opts.on( '-b', '--burn F,E,C,U', Array, "Memory(ies) to burn F:main Flash code (default)
                                                         E:EEPROM 
                                                         C:CUP
                                                         U:CUP code
                                                         A:all" ) do |l|
      l.each { |a| a.upcase! }
      if l.include?("A")
          l = ["F","E","C"]
      end
      $opt[:burn] = l
    end

    $opt[:eeprom] = NIL
    opts.on( '-e', '--eeprom F,CAL,CFG', 'Eeeprom sections to burn 
                            F:Full filesystem (default)
                            CAL: keep calib 
                            CFG: keep config and calib
                            ' ) do |eetype|
      if eetype=="F"
        $opt[:eeprom] = NIL
      elsif eetype=="CAL"
        $opt[:eeprom] = "_noconfig"
      elsif eetype=="CFG"
        $opt[:eeprom] = "_noconfig_nocalib"
      end
    end

    $opt[:uid  ]= NIL
    opts.on('-u', '--uid [UID]', 'UID to be burned') do |uid|
        $opt[:uid] = uid
    end

    $opt[:duid ]= DEFAULT_UID
    opts.on('-d', '--default-uid [DUID]', "Overloads default UID (0x#{DEFAULT_UID}) to be searched/modified") do |duid|
        $opt[:duid] = duid
    end

    $opt[:path  ]= NIL
    opts.on('-p', '--path [PATH TO BIN]', 'Path to binary (defaults here)') do |path|
        $opt[:path] = path
    end

    $opt[:mode ]= "ONESHOT"
    opts.on('-m', '--multiple-burn', 'Queued burn operations, with either new or auto-incremented UID') do
        $opt[:mode] = "AUTO_INC"
    end

    $opt[:build ]= false
    opts.on('-M', '--build-binary', 'Call Make to rebuild binary before burning') do
        $opt[:build] = true
    end

    $opt[:dry ]= false
    opts.on('-n', '--dry-run', 'Do not burn (debug only)') do
        $opt[:dry] = true
    end


    $opt[:debug]= false
    opts.on('-v','--verbose', '') do
        $opt[:debug]=true
    end
    
    $target = "stm32lx"
    $EEPROM_BASE = "0x8080000"

    opts.on('-t', '--target TARGET', "The target to perform the 
                                            L STM32L series
                                            F STM32F series"
                                            ) do |v|

        if v == 'L'
            $target = "stm32lx"
            $EEPROM_BASE = "0x8080000"
        else
            $target = "stm32f1x"
            $EEPROM_BASE = "0x801E000"
        end
    end
                                    
    opts.on( '-h', '--help', 'Display this screen' ) do
        puts opts
        exit
    end
}

params=ARGV.dup
begin
    optparse.parse!
rescue OptionParser::InvalidOption
    puts "burn: Invalid Option"
    puts optparse
end
$opt[:name] = NIL
$opt[:name] = ARGV.shift

if $opt[:name] == NIL 
    puts "binary name is missing !"
    puts optparse
    exit
end

# }}} Comandline Parser

# Cleanup bin name
$opt[:name] = File.basename($opt[:name], File.extname($opt[:name]))

# Get env stuff
$opt[:proj_base]  = "../"
openocd_on = %x(ps au | grep openocd.sh | grep -v grep)

# Build binaries if requested
if $opt[:build] == true
    puts "Building #{$opt[:name]}.elf"
    if system("make #{$opt[:name]}.elf") == false
        raise "Error while building #{$opt[:name]}.elf"
    end
end

# Pathes to binaries
if $opt[:path]==NIL
    binpath = `pwd`.chomp
else
    binpath = $opt[:path].chomp
end
bin = %x(find #{binpath} -name #{$opt[:name]}.bin)
e2p = %x(find #{binpath} -name #{$opt[:name]}_eeprom#{$opt[:eeprom]}.bin)
cup = %x(find #{binpath} -name #{$opt[:name]}_cup.bin)
cup_code = %x(find #{binpath} -name #{$opt[:name]}.bin)
bin=File.expand_path(bin).chomp
e2p=File.expand_path(e2p).chomp
cup=File.expand_path(cup).chomp
cup_code=File.expand_path(cup_code).chomp
if File.file?(bin)==false then raise "Main binary not found" end
if File.file?(e2p)==false then raise "EEPROM binary not found" end
if File.file?(cup)==false then raise "CUP binary not found" end
if File.file?(cup_code)==false then raise "CUPCODE binary not found" end

if $opt[:debug]== true then
    pp "options:", $opt
    puts "openocd_on:#{openocd_on}"
    puts "bin:#{bin}"
    puts "e2p:#{e2p}"
    puts "cup_code:#{cup_code}"
end

# Launch OpenOCD if needed
if openocd_on==""
    # launch in a new process
    system("xterm -xrm '*hold: true' -e '#{$opt[:proj_base]}/env/sys/openocd.sh #{$target}' &")
    %x(sleep 1)
end

# Init UID
if $opt[:uid] == NIL
    uid = "0"
else
    uid = "%016X" % (WIZZILAB_OUI.to_i(16) + $opt[:uid].to_i(16))
end

# One shot burn ---------------------------------------------------------------
if $opt[:mode] == "ONESHOT"
    if $opt[:uid] then
        e2p_tmp = create_eeprom_image(e2p,$opt[:duid],uid)
    else
        uid = $opt[:duid]
        e2p_tmp = e2p.dup
    end
    puts "BURNING  bin:#{File.basename(bin)} UID:#{uid}"
    burn_memory(e2p_tmp,bin,cup,cup_code)

else
# Multiple burn ---------------------------------------------------------------
    begin
        puts "READY TO BURN   bin:#{File.basename(bin)} UID:#{uid}"
        puts " - Enter new UID (7 hex char max, no quotes) if needed"
        puts " - Press <Enter> to burn"
        puts " - <CTRL-C> to exit"
        begin
            strin = gets
            strin.chomp!
            if strin.length == 0 then break end
            if strin.length <= 7 and !strin[/\H/]
                #uid = strin.upcase
                uid = "%016X" % (strin.to_i(16) + WIZZILAB_OUI.to_i(16))
                break
            end
            puts "Invalid UID !: #{strin}"
        end while true

        # Modify the EEPROM with new UID
        e2p_tmp = create_eeprom_image(e2p,$opt[:duid],uid)
        puts "BURNING  bin:#{File.basename(bin)} UID:#{uid}"
        burn_memory(e2p_tmp,bin,cup,cup_code)

        # Auto increment UID for next burn
        uid = "%016X" % (uid.to_i(16) + 1)
    end while true
end

end
# vim:fdm=marker:fdc=2
