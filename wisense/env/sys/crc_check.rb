#!/usr/bin/env ruby
$LOAD_PATH.unshift File.dirname(__FILE__)
require 'digest/crc32'

# Main Code
if $0 == __FILE__ then
    f = ARGV.shift
    cup_code = open(f, "rb") {|io| io.read }
    sig = Digest::CRC32.file(f).checksum
    puts "CRC32:" + "0x%08X" % sig
end
