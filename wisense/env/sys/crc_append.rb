#!/usr/bin/env ruby
$LOAD_PATH.unshift File.dirname(__FILE__)
require 'digest/crc32'

# Main Code
if $0 == __FILE__ then
    f = ARGV.shift
    sig = Digest::CRC32.file(f).checksum
    File.write(f, [sig].pack("l<") , File.size(f), mode: 'a')
    puts "CRC32 0x%08X appended to file #{f}" % sig
end
