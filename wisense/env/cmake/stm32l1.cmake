INCLUDE(CMakeForceCompiler)
#######################################
# Toolchain setup
#######################################
SET(CMAKE_SYSTEM_NAME Generic)  
SET(CMAKE_SYSTEM_VERSION 1)

# specify the cross compiler
SET(CROSS arm-none-eabi)  
SET(CMAKE_C_COMPILER ${CROSS}-gcc)  
SET(CMAKE_CXX_COMPILER ${CROSS}-g++)  
SET(AS "arm-none-eabi-gcc -x -assembler-with-cpp")  
SET(AR ${CROSS}-ar)  
SET(LD ${CROSS}-ld)  
SET(NM ${CROSS}-nm)  
SET(OBJCOPY ${CROSS}-objcopy)  
SET(OBJDUMP ${CROSS}-objdump)  
SET(READELF ${CROSS}-readelf)  
SET(SIZE    ${CROSS}-size)  
  
CMAKE_FORCE_C_COMPILER(${CROSS}-gcc GNU)  
CMAKE_FORCE_CXX_COMPILER(${CROSS}-g++ GNU)  
  
# where is the target environment 
SET(CMAKE_FIND_ROOT_PATH  /usr/arm-none-eabi)

 
# adjust the default behaviour of the FIND_XXX() commands:  
# search headers and libraries in the target environment, search   
# programs in the host environment  
SET(CMAKE_FIND_ROOT_PATH_MODE_PROGRAM NEVER)  
SET(CMAKE_FIND_ROOT_PATH_MODE_LIBRARY ONLY)  
SET(CMAKE_FIND_ROOT_PATH_MODE_INCLUDE ONLY)  

# Wizzilab's usual warning level
SET(WFLAGS "-Wall -Wextra -Wno-missing-field-initializers -Wno-missing-braces -Wno-main -Wno-pointer-sign -Wreturn-type")

SET(MCU cortex-m3)  
SET(MCFLAGS "-march=armv7-m -mtune=${MCU} -mthumb -mlittle-endian -nostdlib")  
SET(MCFLAGS "-march=armv7-m -mtune=cortex-m3 -mthumb -mlittle-endian -nostdlib")  
SET(CMAKE_ASM_FLAGS "${MCFLAGS}")  
SET(CMAKE_C_FLAGS "${MCFLAGS} ${WFLAGS} -ffunction-sections -fdata-sections -fverbose-asm")  
SET(CMAKE_CXX_FLAGS "${MCFLAGS} ${WFLAGS} -ffunction-sections -std=c++98 -fno-builtin -fdata-sections -fverbose-asm -fno-rtti -fno-exceptions")  
  
SET(CMAKE_C_FLAGS_DEBUG "-O0 -g -gstabs+")  
SET(CMAKE_CXX_FLAGS_DEBUG "-O0 -g -gstabs+")  
SET(CMAKE_ASM_FLAGS_DEBUG "-g -gstabs+")  
  
SET(CMAKE_C_FLAGS_RELEASE "-Os")  
SET(CMAKE_CXX_FLAGS_RELEASE "-Os")  
SET(CMAKE_ASM_FLAGS_RELEASE "")  
  
#SET(CMAKE_EXE_LINKER_FLAGS "${MCFLAGS} -T${LDSCRIPT} -Wl,-Map=$(PRJ).map,--cref,--no-warn-mismatch -Wl,--gc-sections -Wl,--defsym=malloc_getpagesize_P=0x1000")  
# LD script is now set at executable level
SET(CMAKE_EXE_LINKER_FLAGS "${MCFLAGS} -Wl,--cref,--no-warn-mismatch -Wl,--gc-sections -flto ")  
SET(CMAKE_MODULE_LINKER_FLAGS "${MCFLAGS} -flto")  
SET(CMAKE_SHARED_LINKER_FLAGS "${MCFLAGS} -flto")  

