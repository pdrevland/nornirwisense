set remotetimeout 90
target remote localhost:3333
set remote hardware-breakpoint-limit 6
set remote hardware-watchpoint-limit 4
mon reset halt
mon wait_halt 200
mon flash probe 0
