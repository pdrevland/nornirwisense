/// @copyright
/// ========================================================================={{{
/// Copyright (c) 2012-2014 WizziLab                                           /
/// All rights reserved                                                        /
///                                                                            /
/// IMPORTANT: This Software may not be modified, copied or distributed unless /
/// embedded on a WizziLab product. Other than for the foregoing purpose, this /
/// Software and/or its documentation may not be used, reproduced, copied,     /
/// prepared derivative works of, modified, performed, distributed, displayed  /
/// or sold for any purpose. For the sole purpose of embedding this Software   /
/// on a WizziLab product, copy, modification and distribution of this         /
/// Software is granted provided that the following conditions are respected:  /
///                                                                            /
/// *  Redistributions of source code must retain the above copyright notice,  /
///    this list of conditions and the following disclaimer                    /
///                                                                            /
/// *  Redistributions in binary form must reproduce the above copyright       / 
///    notice, this list of conditions and the following disclaimer in the     /
///    documentation and/or other materials provided with the distribution.    /
///                                                                            /
/// *  The name of WizziLab can not be used to endorse or promote products     /
///    derived from this software without specific prior written permission.   /
///                                                                            /
/// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS        /
/// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED  /
/// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR /
/// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR          /
/// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,      /
/// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,        /
/// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,            /
/// OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY     /
/// OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING    /
/// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS         /
/// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.               /
/// WIZZILAB HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,       /
/// ENHANCEMENTS OR MODIFICATIONS.                                             /
///                                                                            /
/// Should you have any questions regarding your right to use this Software,   /
/// contact WizziLab at www.wizzilab.com.                                      /
///                                                                            /
/// =========================================================================}}}
/// @endcopyright

//  ====================================================================
//  @file           hal_defs_csm_v1.h
//  @brief          hal board definition header
//  ====================================================================

#ifndef __HAL_WISENSE_V1_CFG_H__
#define __HAL_WISENSE_V1_CFG_H__

// Wisense is based on a STM32L100 microcontroler
#define HAL_USE_STM32L

//======================================================================
// BOARD CONFIGURATION
//======================================================================

// External Slow Oscillator (32.768KHz)
#define HAL_PLATFORM_HAS_LSE    1

// External Fast Oscillator (Typ 8.00MHz)
#define HAL_PLATFORM_HAS_HSE    0
#define HAL_PLATFORM_HSE_FREQ   80000000

// Lowest Power mode that will be targeted in sleep
// We have to keep this to LPM5 because of its absolute timer.
// If RAC is not used LPM6 can be used
#define HAL_MIN_POWER_MODE      LPM6

//======================================================================
// IO PORTS
//======================================================================
// ----------------------- Port A
// PA.0  BARO_SHDN
// PA.1  BARO_RST
// PA.2  MOTE_DIN
// PA.3  MOTE_DOUT
// PA.4  DAC1
// PA.5  DAC2
// PA.6  MOTE_IRQ_IN
// PA.7  NC
// PA.8  LED1
// PA.9  RX_STRACE
// PA.10 TX_STRACE
// PA.11 USB_DM
// PA.12 USB_DP
// PA.13 JTMS (keep rst value: ALT/PU)
// PA.14 JTCK (keep rst value: ALT/PD)
// PA.15 JTDI (keep rst value: ALT/PU)
#define PA_UNCONNECT            0x0080
#define PA_GPIO_IN              0x1800
#define PA_GPIO_ALT             0xE60C
#define PA_GPIO_ANA             0x0030
#define PA_GPIO_OUT             0x0143
#define PA_GPIO_OUT_INIT        0x0000
#define PA_GPIO_FAST            0x0000
#define PA_GPIO_OPEN_DRAIN      0x0000
#define PA_GPIO_PULL_DOWN       0x4000
#define PA_GPIO_PULL_UP         0xA000

#define PA_AFRL0                0
#define PA_AFRL1                0
#define PA_AFRL2                GPIO_AF_USART2
#define PA_AFRL3                GPIO_AF_USART2
#define PA_AFRL4                0
#define PA_AFRL5                0
#define PA_AFRL6                0
#define PA_AFRL7                0
#define PA_AFRH8                0
#define PA_AFRH9                GPIO_AF_USART1
#define PA_AFRH10               GPIO_AF_USART1
#define PA_AFRH11               GPIO_AF_USB
#define PA_AFRH12               GPIO_AF_USB
#define PA_AFRH13               0
#define PA_AFRH14               0
#define PA_AFRH15               0

// ----------------------- Port B
// PB.0  NC
// PB.1  NC
// PB.2  MOTE_PWR_REQ
// PB.3  JTDO  (keep rst value: ALT/NOPU_NOPD/FAST)
// PB.4  JTRST (keep rst value: ALT/PU)
// PB.5  SW2
// PB.6  I2C1_SCL
// PB.7  I2C1_SDA
// PB.8  ACCEL_INT2
// PB.9  ACCEL_INT1
// PB.10 EXT8
// PB.11 EXT7
// PB.12 EXT6
// PB.13 EXT5
// PB.14 EXT4
// PB.15 EXT3
#define PB_UNCONNECT            0x0003
#define PB_GPIO_IN              0xFF20
#define PB_GPIO_ALT             0x00C0
#define PB_GPIO_ANA             0x0000
#define PB_GPIO_OUT             0x0004
#define PB_GPIO_OUT_INIT        0x0000
#define PB_GPIO_FAST            0x0000
#define PB_GPIO_OPEN_DRAIN      0x00C0
#define PB_GPIO_PULL_DOWN       0xFC00
#define PB_GPIO_PULL_UP         0x0020

#define PB_AFRL0                0
#define PB_AFRL1                0
#define PB_AFRL2                0
#define PB_AFRL3                0
#define PB_AFRL4                0
#define PB_AFRL5                0
#define PB_AFRL6                GPIO_AF_I2C1
#define PB_AFRL7                GPIO_AF_I2C1
#define PB_AFRH8                0
#define PB_AFRH9                0
#define PB_AFRH10               0
#define PB_AFRH11               0
#define PB_AFRH12               0
#define PB_AFRH13               0
#define PB_AFRH14               0
#define PB_AFRH15               0

// ----------------------- Port C
// PC.0 PHOTO_PWR
// PC.1 PHOTO
// PC.2 NC
// PC.3 SW1
// PC.4 MOTE_IRQ_OUT
// PC.5 NC
// PC.6 STAT2
// PC.7 STAT1
// PC.8 PROG2
// PC.9 LED2
// PC.10 NC
// PC.11 NC
// PC.12 NC
// PC.13 NC
// PC.14 OSC 32K IN
// PC.15 OSC 32K OUT
#define PC_UNCONNECT            0x3C24
#define PC_GPIO_IN              0x01D8
#define PC_GPIO_ALT             0x0000
#define PC_GPIO_ANA             0x0002
#define PC_GPIO_OUT             0x0301
#define PC_GPIO_OUT_INIT        0x0000
#define PC_GPIO_FAST            0x0000
#define PC_GPIO_OPEN_DRAIN      0x0000
#define PC_GPIO_PULL_DOWN       0x0000
#define PC_GPIO_PULL_UP         0x0008

#define PC_AFRL0                0
#define PC_AFRL1                0
#define PC_AFRL2                0
#define PC_AFRL3                0
#define PC_AFRL4                0
#define PC_AFRL5                0
#define PC_AFRL6                0
#define PC_AFRL7                0
#define PC_AFRH8                0
#define PC_AFRH9                0
#define PC_AFRH10               0
#define PC_AFRH11               0
#define PC_AFRH12               0
#define PC_AFRH13               0
#define PC_AFRH14               0
#define PC_AFRH15               0

// ----------------------- Port D
// PD.0->1 N/A
// PD.2  NC
// PD.3->15 N/A
#define PD_UNCONNECT            0xFFFF
#define PD_GPIO_IN              0x0000
#define PD_GPIO_ALT             0x0000
#define PD_GPIO_ANA             0x0000
#define PD_GPIO_OUT             0x0000
#define PD_GPIO_OUT_INIT        0x0000
#define PD_GPIO_FAST            0x0000
#define PD_GPIO_OPEN_DRAIN      0x0000
#define PD_GPIO_PULL_DOWN       0x0000
#define PD_GPIO_PULL_UP         0x0000

#define PD_AFRL0                0
#define PD_AFRL1                0
#define PD_AFRL2                0
#define PD_AFRL3                0
#define PD_AFRL4                0
#define PD_AFRL5                0
#define PD_AFRL6                0
#define PD_AFRL7                0
#define PD_AFRH8                0
#define PD_AFRH9                0
#define PD_AFRH10               0
#define PD_AFRH11               0
#define PD_AFRH12               0
#define PD_AFRH13               0
#define PD_AFRH14               0
#define PD_AFRH15               0

// ----------------------- Ports E->G : N/A
#define NO_PORT_E
#define NO_PORT_F
#define NO_PORT_G

// ----------------------- Port H
// PH.0  MOTE_RESET (OD+PU init High)
// PH.1  NC 
// PH.2->15 N/A
#define PH_UNCONNECT            0xFFFE
#define PH_GPIO_IN              0x0000
#define PH_GPIO_OUT             0x0001
#define PH_GPIO_OUT_INIT        0x0001
#define PH_GPIO_FAST            0x0000
#define PH_GPIO_OPEN_DRAIN      0x0001
#define PH_GPIO_PULL_DOWN       0x0000
#define PH_GPIO_PULL_UP         0x0000
#define PH_GPIO_ALT             0x0000
#define PH_GPIO_ANA             0x0000

#define PH_AFRL0                0
#define PH_AFRL1                0
#define PH_AFRL2                0
#define PH_AFRL3                0
#define PH_AFRL4                0
#define PH_AFRL5                0
#define PH_AFRL6                0
#define PH_AFRL7                0
#define PH_AFRH8                0
#define PH_AFRH9                0
#define PH_AFRH10               0
#define PH_AFRH11               0
#define PH_AFRH12               0
#define PH_AFRH13               0
#define PH_AFRH14               0
#define PH_AFRH15               0

//======================================================================
// GPIOs
//======================================================================

#define GPIO_LED_RED            IOPACK(PA, 8 )
#define GPIO_LED_GREEN          IOPACK(PC, 9 )
#define GPIO_BARO_SHDN          IOPACK(PA, 0 )
#define GPIO_BARO_RST           IOPACK(PA, 1 )
#define GPIO_MOTE_PWR_REQ       IOPACK(PB, 2 )
#define GPIO_MOTE_IRQ_OUT       IOPACK(PC, 4 )
#define GPIO_MOTE_IRQ_IN        IOPACK(PA, 6 )
#define GPIO_MOTE_RST           IOPACK(PH, 0 )
#define GPIO_I2C1_SCL           IOPACK(PB, 6)
#define GPIO_I2C1_SDA           IOPACK(PB, 7)
#define GPIO_ACCEL_INT1         IOPACK(PB, 9)
#define GPIO_ACCEL_INT2         IOPACK(PB, 8)
#define GPIO_PHOTO_PWR          IOPACK(PC, 0)
#define GPIO_SW1                IOPACK(PC, 3)
#define GPIO_SW2                IOPACK(PB, 5)
#define GPIO_STAT1              IOPACK(PC, 7)
#define GPIO_STAT2              IOPACK(PC, 6)
#define GPIO_PROG2              IOPACK(PC, 8)

enum {
    HAL_GPIO_LED_RED = 0,
    HAL_GPIO_LED_GREEN,
    HAL_GPIO_BARO_SHDN,
    HAL_GPIO_BARO_RST,
    HAL_GPIO_MOTE_PWR_REQ,
    HAL_GPIO_MOTE_IRQ_OUT,
    HAL_GPIO_MOTE_IRQ_IN,
    HAL_GPIO_MOTE_RST,
    HAL_GPIO_I2C1_SCL,
    HAL_GPIO_I2C1_SDA,
    HAL_GPIO_ACCEL_INT1,
    HAL_GPIO_ACCEL_INT2,
    HAL_GPIO_PHOTO_PWR,
    HAL_GPIO_SW1,
    HAL_GPIO_SW2,
    HAL_GPIO_STAT1,
    HAL_GPIO_STAT2,
    HAL_GPIO_PROG2,
    HAL_GPIO_QTY
};

#define HAL_GPIOS { \
    GPIO_LED_RED,   \
    GPIO_LED_GREEN, \
    GPIO_BARO_SHDN, \
    GPIO_BARO_RST,  \
    GPIO_MOTE_PWR_REQ,  \
    GPIO_MOTE_IRQ_OUT,  \
    GPIO_MOTE_IRQ_IN,   \
    GPIO_MOTE_RST,  \
    GPIO_I2C1_SCL,  \
    GPIO_I2C1_SDA,  \
    GPIO_ACCEL_INT1,    \
    GPIO_ACCEL_INT2,    \
    GPIO_PHOTO_PWR, \
    GPIO_SW1,   \
    GPIO_SW2,   \
    GPIO_STAT1, \
    GPIO_STAT2, \
    GPIO_PROG2, \
}

//======================================================================
// I2Cs
//======================================================================

// I2C static context indexes
enum 
{
    HAL_I2C_SENSOR = 0,
    HAL_I2C_EXT = 1,
};

#define HAL_I2C_SI7020  HAL_I2C_SENSOR

// I2Cx to index mapping
#define HAL_I2C_QTY                 (2)
#define HAL_I2C_MAP                 {1,2}

#define HAL_I2C_ACC     HAL_I2C_SENSOR
#define HAL_I2C_BARO    HAL_I2C_SENSOR
//======================================================================
// LEDs
//======================================================================

//                                    PORT | PIN | POLARITY
#define LED_RED                     { GPIOA , 8 , LED_NORMAL_POLARITY }
#define LED_GREEN                   { GPIOC , 9 , LED_NORMAL_POLARITY }
#define HAL_LEDS                    { LED_RED , LED_GREEN }

enum {
    HAL_LED_RED = 0,
    HAL_LED_GREEN,
    HAL_LED_QTY
};

//======================================================================
// COM 
//======================================================================

// COM static context indexes
enum {
    /// Debug trace channel
    DBG_COM = 0,
    /// MOTE channel
    MOTE_COM,
};

// 2 COM channels:
#define KAL_COM_NB                  (2)
#define KAL_COM0_TX_FIFO_LEN        (512)
#define KAL_COM1_TX_FIFO_LEN        (64)

// COM0 : UART1 / DMA Tx/Rx / 128B Tx Fifo
#define KAL_COM0_DEF                { COM_TYPE_UART, 1, COM_BAUDRATE_115200, TX_DMA, RX_DMA, COM_MODE_WL_PACKET, \
                                            KAL_COM0_TX_FIFO_LEN, 0, COM_NO_WU, COM_NO_WU}
// COM1 : UART2 / DMA Tx/Rx / 64B Tx Fifo / 2 ms put_delay
#define KAL_COM1_DEF                { COM_TYPE_UART, 2, COM_BAUDRATE_115200, TX_DMA, RX_DMA, COM_MODE_WL_PACKET, \
                                            KAL_COM1_TX_FIFO_LEN, 2, HAL_GPIO_MOTE_IRQ_IN, HAL_GPIO_MOTE_IRQ_OUT}

// Export
#define KAL_COM_COMS_DEF            { KAL_COM0_DEF, KAL_COM1_DEF }
#define KAL_COM_TOTAL_TXBUF_LEN     ( KAL_COM0_TX_FIFO_LEN + KAL_COM1_TX_FIFO_LEN )

//======================================================================
// CUP Debug definitions
//======================================================================

#define CUP_DBG_LED                 LED_RED
#define CUP_DBG_PIN_RX              10
#define CUP_DBG_PIN_TX              9
#define CUP_DBG_PIN_PORT            GPIOA
#define CUP_DBG_UART                USART1

#endif // __HAL_WISENSE_V1_CFG_H__
