/// @copyright
/// ========================================================================={{{
/// Copyright (c) 2012-2013 WizziLab                                           /
/// All rights reserved                                                        /
///                                                                            /
/// IMPORTANT: This Software may not be modified, copied or distributed unless /
/// embedded on a WizziLab product. Other than for the foregoing purpose, this /
/// Software and/or its documentation may not be used, reproduced, copied,     /
/// prepared derivative works of, modified, performed, distributed, displayed  /
/// or sold for any purpose. For the sole purpose of embedding this Software   /
/// on a WizziLab product, copy, modification and distribution of this         /
/// Software is granted provided that the following conditions are respected:  /
///                                                                            /
/// *  Redistributions of source code must retain the above copyright notice,  /
///    this list of conditions and the following disclaimer                    /
///                                                                            /
/// *  Redistributions in binary form must reproduce the above copyright       / 
///    notice, this list of conditions and the following disclaimer in the     /
///    documentation and/or other materials provided with the distribution.    /
///                                                                            /
/// *  The name of WizziLab can not be used to endorse or promote products     /
///    derived from this software without specific prior written permission.   /
///                                                                            /
/// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS        /
/// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED  /
/// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR /
/// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR          /
/// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,      /
/// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,        /
/// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,            /
/// OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY     /
/// OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING    /
/// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS         /
/// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.               /
/// WIZZILAB HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,       /
/// ENHANCEMENTS OR MODIFICATIONS.                                             /
///                                                                            /
/// Should you have any questions regarding your right to use this Software,   /
/// contact WizziLab at www.wizzilab.com.                                      /
///                                                                            /
/// =========================================================================}}}
/// @endcopyright

//  =======================================================================
/// @file           hal_dac.h
/// @brief          API header of the DAC driver
// @ingroup HAL
/// @defgroup HAL_DAC      HAL DAC
/// @{
//  =======================================================================

#ifndef __HAL_DAC_H__
#define __HAL_DAC_H__

#include "hal_types.h"
#include "hal_error.h"
/// @ingroup HAL_DAC
/// @defgroup HAL_DAC_STR       HAL DAC Structures
/// @{

#define HAL_DAC_NB_CH           (2)

/// @} HAL_DAC_STR

/// @ingroup HAL_DAC
/// @defgroup HAL_DAC_API       HAL DAC API
/// @{

#define HAL_DAC1    (1) // On PA4
#define HAL_DAC2    (2) // On PA5

typedef enum {
    HAL_DAC_MODE_NORMAL     = 0x00,
    HAL_DAC_MODE_NOISE      = 0x01,
    HAL_DAC_MODE_TRIANGLE   = 0x02,
    HAL_DAC_MODE_DMA        = 0x04,
    HAL_DAC_MODE_OUTBUF_EN  = 0x80,
} hal_dac_mode_t;

typedef enum {
    HAL_DAC_WAMP_1BIT   = 0,//0000: Unmask bit0 of LFSR/ Triangle Amplitude equal to 1
    HAL_DAC_WAMP_2BIT,      //0001: Unmask bits[1:0] of LFSR/ Triangle Amplitude equal to 3
    HAL_DAC_WAMP_3BIT,      //0010: Unmask bits[2:0] of LFSR/ Triangle Amplitude equal to 7
    HAL_DAC_WAMP_4BIT,      //0011: Unmask bits[3:0] of LFSR/ Triangle Amplitude equal to 15
    HAL_DAC_WAMP_5BIT,      //0100: Unmask bits[4:0] of LFSR/ Triangle Amplitude equal to 31
    HAL_DAC_WAMP_6BIT,      //0101: Unmask bits[5:0] of LFSR/ Triangle Amplitude equal to 63
    HAL_DAC_WAMP_7BIT,      //0110: Unmask bits[6:0] of LFSR/ Triangle Amplitude equal to 127
    HAL_DAC_WAMP_8BIT,      //0111: Unmask bits[7:0] of LFSR/ Triangle Amplitude equal to 255
    HAL_DAC_WAMP_9BIT,      //1000: Unmask bits[8:0] of LFSR/ Triangle Amplitude equal to 511
    HAL_DAC_WAMP_10BIT,     //1001: Unmask bits[9:0] of LFSR/ Triangle Amplitude equal to 1023
    HAL_DAC_WAMP_11BIT,     //1010: Unmask bits[10:0] of LFSR/ Triangle Amplitude equal to 2047
    HAL_DAC_WAMP_12BIT,     //≥ 1011: Unmask bits[11:0] of LFSR/ Triangle Amplitude equal to 4095
} hal_dac_wave_amp_t;


//======================================================================
// hal_dac_open
//----------------------------------------------------------------------
/// @brief  
//======================================================================
void hal_dac_open(void);
vu32* hal_dac_setup(u8 ch, u8 mode, u8 wamp);
void hal_dac_enable(u8 ch, u8 enable);
void hal_dac_set_value(u8 ch, u16 val);
void hal_dac_sw_trigger(u8 ch);
void hal_dac_close(void);

/// @} HAL_DAC_API
/// @} HAL_DAC

#endif // __HAL_DAC_H__
