/// @copyright
/// ========================================================================={{{
/// Copyright (c) 2012-2013 WizziLab                                           /
/// All rights reserved                                                        /
///                                                                            /
/// IMPORTANT: This Software may not be modified, copied or distributed unless /
/// embedded on a WizziLab product. Other than for the foregoing purpose, this /
/// Software and/or its documentation may not be used, reproduced, copied,     /
/// prepared derivative works of, modified, performed, distributed, displayed  /
/// or sold for any purpose. For the sole purpose of embedding this Software   /
/// on a WizziLab product, copy, modification and distribution of this         /
/// Software is granted provided that the following conditions are respected:  /
///                                                                            /
/// *  Redistributions of source code must retain the above copyright notice,  /
///    this list of conditions and the following disclaimer                    /
///                                                                            /
/// *  Redistributions in binary form must reproduce the above copyright       / 
///    notice, this list of conditions and the following disclaimer in the     /
///    documentation and/or other materials provided with the distribution.    /
///                                                                            /
/// *  The name of WizziLab can not be used to endorse or promote products     /
///    derived from this software without specific prior written permission.   /
///                                                                            /
/// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS        /
/// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED  /
/// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR /
/// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR          /
/// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,      /
/// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,        /
/// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,            /
/// OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY     /
/// OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING    /
/// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS         /
/// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.               /
/// WIZZILAB HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,       /
/// ENHANCEMENTS OR MODIFICATIONS.                                             /
///                                                                            /
/// Should you have any questions regarding your right to use this Software,   /
/// contact WizziLab at www.wizzilab.com.                                      /
///                                                                            /
/// =========================================================================}}}
/// @endcopyright

//  =======================================================================
/// @file           hal_led.h
/// @brief          Hardware Abstraction Layer API
//  =======================================================================

#ifndef __HAL_LED_H__
#define __HAL_LED_H__

#include "hal_types.h"

/// @ingroup HAL
/// @defgroup HAL_LED_API       HAL LED API
/// @{

// =======================================================================
/// Defines for LEDs
// =======================================================================

// LED Structure
// -----------------------------------------------------
#define LED_NORMAL_POLARITY     0
#define LED_INVERTED_POLARITY   1
#define HAL_LED_ALL 0 // XXX TODO implement this

//======================================================================
// hal_led_on
//----------------------------------------------------------------------
/// @brief  Turn On selected LEDs
/// @param  leds : OR of leds to be turned on 
//======================================================================
public void hal_led_on(u8 led);

//======================================================================
// hal_led_off
//----------------------------------------------------------------------
/// @brief  Turn Off selected LEDs
/// @param  leds : OR of leds to be turned off 
//======================================================================
public void hal_led_off(u8 led);


//======================================================================
// hal_led_toggle
//----------------------------------------------------------------------
/// @brief  Toggle selected LEDs
/// @param  leds : OR of leds to be toggled
//======================================================================
public void hal_led_toggle(u8 led);


//======================================================================
// hal_led_get
//----------------------------------------------------------------------
/// @brief  Get status of LEDs
/// @retval OR of leds that are currently turned ON
//======================================================================
public u8 hal_led_get();

//======================================================================
// hal_led_invert
//----------------------------------------------------------------------
/// @brief  Invert the state of all LEDs
//======================================================================
public void hal_led_invert();

/// @} HAL_LED_API
#endif // __HAL_LED_H__
