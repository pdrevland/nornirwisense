/// @copyright
/// ========================================================================={{{
/// Copyright (c) 2012-2013 WizziLab                                           /
/// All rights reserved                                                        /
///                                                                            /
/// IMPORTANT: This Software may not be modified, copied or distributed unless /
/// embedded on a WizziLab product. Other than for the foregoing purpose, this /
/// Software and/or its documentation may not be used, reproduced, copied,     /
/// prepared derivative works of, modified, performed, distributed, displayed  /
/// or sold for any purpose. For the sole purpose of embedding this Software   /
/// on a WizziLab product, copy, modification and distribution of this         /
/// Software is granted provided that the following conditions are respected:  /
///                                                                            /
/// *  Redistributions of source code must retain the above copyright notice,  /
///    this list of conditions and the following disclaimer                    /
///                                                                            /
/// *  Redistributions in binary form must reproduce the above copyright       / 
///    notice, this list of conditions and the following disclaimer in the     /
///    documentation and/or other materials provided with the distribution.    /
///                                                                            /
/// *  The name of WizziLab can not be used to endorse or promote products     /
///    derived from this software without specific prior written permission.   /
///                                                                            /
/// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS        /
/// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED  /
/// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR /
/// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR          /
/// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,      /
/// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,        /
/// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,            /
/// OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY     /
/// OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING    /
/// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS         /
/// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.               /
/// WIZZILAB HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,       /
/// ENHANCEMENTS OR MODIFICATIONS.                                             /
///                                                                            /
/// Should you have any questions regarding your right to use this Software,   /
/// contact WizziLab at www.wizzilab.com.                                      /
///                                                                            /
/// =========================================================================}}}
/// @endcopyright

#ifndef __HAL_GPIO_H__
#define __HAL_GPIO_H__

#include "hal_types.h"
#include "hal_defs.h"
#include "hal_isr.h"

#define IOPACK(port,bit)    ((((u8)(port)&0xF)<<4)|((u8)(bit)&0xF))

//======================================================================
// hal_gpio_set
//----------------------------------------------------------------------
/// @brief  Turn On selected IO
/// @param  io Index of the gpio
//======================================================================
public void hal_gpio_set(u8 io);

//======================================================================
// hal_gpio_clr
//----------------------------------------------------------------------
/// @brief  Turn On selected IO
/// @param  io Index of the gpio
//======================================================================
public void hal_gpio_clr(u8 io);


//======================================================================
// hal_gpio_toggle
//----------------------------------------------------------------------
/// @brief  Toggle selected io
/// @param  io Index of the gpio
//======================================================================
public void hal_gpio_toggle(u8 io);

//======================================================================
// hal_gpio_read
//----------------------------------------------------------------------
/// @brief  Get status of Gpio
/// @param  io Index of the gpio
/// @retval 0 or 1 depending on gpio status
//======================================================================
public u8 hal_gpio_read(u8 io);

enum {
    GPIO_IRQ_RISE = 1,
    GPIO_IRQ_FALL = 2
};

//======================================================================
// hal_gpio_irq_set
//----------------------------------------------------------------------
/// @brief  Setup an IRQ on a GPIO. (Disabled by default)
/// @param  io Index of the gpio
/// @param  trigger selects triggering edge amongst GPIO_IRQ_RISE/FALL
/// @param  handler IRQ handler function
//======================================================================
public void hal_gpio_irq_set(u8 io, u8 trigger, hal_isr_t handler);

//======================================================================
// hal_gpio_irq_en
//----------------------------------------------------------------------
/// @brief  Enable an IRQ on a GPIO.
/// @param  io Index of the gpio
//======================================================================
public void hal_gpio_irq_en(u8 io);

//======================================================================
// hal_gpio_irq_dis
//----------------------------------------------------------------------
/// @brief  Disable an IRQ on a GPIO.
/// @param  io Index of the gpio
//======================================================================
public void hal_gpio_irq_dis(u8 io);

//======================================================================
// hal_gpio_irq_clr
//----------------------------------------------------------------------
/// @brief  Clear an IRQ on a GPIO.
/// @param  io Index of the gpio
//======================================================================
public void hal_gpio_irq_clr(u8 io);

//======================================================================
// hal_gpio_line
//----------------------------------------------------------------------
/// @brief  return bitmap of actual EXTI line of the GPIO.
///         This bitmap is what is send in ISR status.
/// @param  io Index of the gpio
//======================================================================
public u16 hal_gpio_line(u8 io);

typedef enum {
    HAL_IOMODE_IN  = 0x01,
    HAL_IOMODE_OUT = 0x02,
    HAL_IOMODE_ALT = 0x04,
    HAL_IOMODE_ANA = 0x08,
    HAL_IOMODE_OD  = 0x10,
    HAL_IOMODE_PU  = 0x20,
    HAL_IOMODE_PD  = 0x40,
    HAL_IOMODE_FAST= 0x80,
} hal_iomode_t;

//======================================================================
// hal_gpio_setup
//----------------------------------------------------------------------
/// @brief  overrides initial IO setting. Works only on IOs listed in
///         HAL_GPIO_xxx list.
/// @param  io Index of the gpio
/// @param  mode bitmap desribing the IO mode using hal_iomode_t
//======================================================================
public void hal_gpio_setup(u8 io, u8 mode);

#endif // __HAL_GPIO_H__ 
// vim:fdm=marker:fdc=2
