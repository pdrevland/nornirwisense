/// @copyright
/// ========================================================================={{{
/// Copyright (c) 2012-2013 WizziLab                                           /
/// All rights reserved                                                        /
///                                                                            /
/// IMPORTANT: This Software may not be modified, copied or distributed unless /
/// embedded on a WizziLab product. Other than for the foregoing purpose, this /
/// Software and/or its documentation may not be used, reproduced, copied,     /
/// prepared derivative works of, modified, performed, distributed, displayed  /
/// or sold for any purpose. For the sole purpose of embedding this Software   /
/// on a WizziLab product, copy, modification and distribution of this         /
/// Software is granted provided that the following conditions are respected:  /
///                                                                            /
/// *  Redistributions of source code must retain the above copyright notice,  /
///    this list of conditions and the following disclaimer                    /
///                                                                            /
/// *  Redistributions in binary form must reproduce the above copyright       / 
///    notice, this list of conditions and the following disclaimer in the     /
///    documentation and/or other materials provided with the distribution.    /
///                                                                            /
/// *  The name of WizziLab can not be used to endorse or promote products     /
///    derived from this software without specific prior written permission.   /
///                                                                            /
/// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS        /
/// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED  /
/// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR /
/// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR          /
/// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,      /
/// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,        /
/// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,            /
/// OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY     /
/// OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING    /
/// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS         /
/// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.               /
/// WIZZILAB HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,       /
/// ENHANCEMENTS OR MODIFICATIONS.                                             /
///                                                                            /
/// Should you have any questions regarding your right to use this Software,   /
/// contact WizziLab at www.wizzilab.com.                                      /
///                                                                            /
/// =========================================================================}}}
/// @endcopyright

#include "hal_types.h"
#include "hal_isr.h"

//======================================================================
// hal_timer_open
//----------------------------------------------------------------------
/// @brief  Open a general purpose timer
/// @param  timer irq function
/// @retval none
//======================================================================
public void hal_timer_open(hal_isr_t irq_fun);

//======================================================================
// hal_timer_close
//----------------------------------------------------------------------
/// @brief  Close timer event generation
/// @param  none
/// @retval none
//======================================================================
public void hal_timer_close();

//======================================================================
// hal_timer_start
//----------------------------------------------------------------------
/// @brief  Start timer for the given period
/// @param  period : period at which this event should occur
/// @retval none
//======================================================================
public void hal_timer_start(u16 period);

//======================================================================
// hal_timer_update
//----------------------------------------------------------------------
/// @brief  Update expiration time of a running timer
/// @param  period : new period at which this event should occur
/// @retval none
//======================================================================
public void hal_timer_update(u16 period);

//======================================================================
// hal_timer_stop
//----------------------------------------------------------------------
/// @brief  Stop timer 
/// @param  none
/// @retval none
//======================================================================
public void hal_timer_stop();

//======================================================================
// hal_timer_gettime
//----------------------------------------------------------------------
/// @brief  returns current timer value
/// @param  none
/// @retval none
//======================================================================
public u16 hal_timer_gettime();


//======================================================================
// hal_dbgtim_open
//----------------------------------------------------------------------
/// @brief  Opens the debug free-running counter
///         The debug counter is a virtual 32-bits free-running @8.192K
///         giving a 524288 second (145,63 hours) wrap time / 122us reso
//======================================================================
#define DBGTIM_TO_SEC(t)    ((t)/8192)
#define DBGTIM_TO_MSEC(t)   (((t)*125)/1024)
#define DBGTIM_TO_MSEC_OVFL(t)   ((t)>=34359738)
#define DBGTIM_TO_USEC(t)   ((t)*122)
#define DBGTIM_TO_SMU(t,s,m,u) do { \
    u32 tmp; \
    s = ((t)/8192); \
    tmp = (t) - s * 8192; \
    m = (tmp*125)/1024; \
    tmp -= ((m*1024)/125); \
    u = tmp*122; \
}while (0)
public void hal_dbgtim_open(void);

//======================================================================
// hal_dbgtim_gettime
//----------------------------------------------------------------------
/// @brief  Gets time elapsed since reference time.
/// @param  offset reference time for period calculation
/// @retval date (if offset=0) or period (if offset=reference time)
//======================================================================
public u32 hal_dbgtim_gettime(u32 offset);

//======================================================================
// hal_dbgtim2_open
//----------------------------------------------------------------------
/// @brief  Open a simple dbg timer counting at SYSCLK
/// @param  u16         psc         clock prescaler
/// @retval void
//======================================================================
public void hal_dbgtim2_open(u16 psc);

//======================================================================
// hal_dbgtim2_getcnt
//----------------------------------------------------------------------
/// @brief  Gets counter elapsed since reference counter.
/// @param  offset reference counter for period calculation
/// @retval timer counter (if offset=0) or delta (if offset=reference counter)
//======================================================================
public u32 hal_dbgtim2_getcnt(u32 offset);

//======================================================================
// hal_wait_ms
//----------------------------------------------------------------------
/// @brief  generate a SW delay in 1/1024 s
/// @param  n               u16             delay in 1/1024s
/// @retval                 void
//======================================================================
public void hal_wait_ms(u16 n);

