/// @copyright
/// ========================================================================={{{
/// Copyright (c) 2012-2013 WizziLab                                           /
/// All rights reserved                                                        /
///                                                                            /
/// IMPORTANT: This Software may not be modified, copied or distributed unless /
/// embedded on a WizziLab product. Other than for the foregoing purpose, this /
/// Software and/or its documentation may not be used, reproduced, copied,     /
/// prepared derivative works of, modified, performed, distributed, displayed  /
/// or sold for any purpose. For the sole purpose of embedding this Software   /
/// on a WizziLab product, copy, modification and distribution of this         /
/// Software is granted provided that the following conditions are respected:  /
///                                                                            /
/// *  Redistributions of source code must retain the above copyright notice,  /
///    this list of conditions and the following disclaimer                    /
///                                                                            /
/// *  Redistributions in binary form must reproduce the above copyright       /
///    notice, this list of conditions and the following disclaimer in the     /
///    documentation and/or other materials provided with the distribution.    /
///                                                                            /
/// *  The name of WizziLab can not be used to endorse or promote products     /
///    derived from this software without specific prior written permission.   /
///                                                                            /
/// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS        /
/// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED  /
/// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR /
/// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR          /
/// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,      /
/// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,        /
/// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,            /
/// OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY     /
/// OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING    /
/// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS         /
/// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.               /
/// WIZZILAB HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,       /
/// ENHANCEMENTS OR MODIFICATIONS.                                             /
///                                                                            /
/// Should you have any questions regarding your right to use this Software,   /
/// contact WizziLab at www.wizzilab.com.                                      /
///                                                                            /
/// =========================================================================}}}
/// @endcopyright

//  =======================================================================
/// @file           hal_spi.h
/// @brief          API header of the SPI driver
/// @ingroup HAL
/// @defgroup HAL_SPI   HAL SPI
/// @{
//  =======================================================================

#ifndef __HAL_SPI_H__
#define __HAL_SPI_H__

#include "hal_types.h"

typedef enum
{
    SPI_ERR_NO,
    SPI_ERR_BSY,
} hal_spi_error_t;

//======================================================================
// hal_spi_open
//----------------------------------------------------------------------
/// @brief  Open spi module
/// @param  tgt_process process_t*              Ponter to the target process
/// @retval             hal_spi_error_t         See hal_spi_error_t
//======================================================================
public void hal_spi_open(process_t *proc);

//======================================================================
// hal_spi_on
//----------------------------------------------------------------------
/// @brief  Set the NSS low, enable spi and set internal status as busy
//======================================================================
public void hal_spi_on(void);

//======================================================================
// hal_spi_off
//----------------------------------------------------------------------
/// @brief  Set the NSS high and disable SPI until the next send/receive/io
///         function is called.
//          Call this function after reveiving a HAL_EVT_SPI_TRANSFER_DONE event
//          or after hal_spi_wait_done()
//======================================================================
public void hal_spi_off(void);

//======================================================================
// hal_spi_io
//----------------------------------------------------------------------
/// @brief  Performs a full duplex transfer
/// @param  buffer_in   u8*     The buffer to save the incoming data
/// @param  data_out    u8*     The data to be sent
/// @param  size        u8      How many bytes to send/receive
/// @retval             hal_spi_error_t         See hal_spi_error_t
//======================================================================
public hal_spi_error_t hal_spi_io(u8 *buffer_in, u8 *data_out, u8 size);

//======================================================================
// hal_spi_send
//----------------------------------------------------------------------
/// @brief  Performs a full duplex transfer ignoring the received data
/// @param  data        u8*     The data to be sent
/// @param  size        u8      How many bytes to send
/// @retval             hal_spi_error_t         See hal_spi_error_t
//======================================================================
public hal_spi_error_t hal_spi_send(const u8 *data, u8 size);

//======================================================================
// hal_spi_send_ch
//----------------------------------------------------------------------
/// @brief  Send a caracter. This function is blocking and it does not start a DMA.
///         Spi must be turned of with hal_spi_off after this function or
///         calling hal_spi_send/receive/io will turn the spi off automaticaly after
///         the transaction.
/// @param  data        u8     The data to be sent
/// @retval             hal_spi_error_t         See hal_spi_error_t
//======================================================================
public hal_spi_error_t hal_spi_send_ch(const u8 data);

//======================================================================
// hal_spi_receive
//----------------------------------------------------------------------
/// @brief  Performs a full duplex transfer sending a dummy data
/// @param  data        u8*     The buffer to save the incoming data
/// @param  size        u8      How many bytes to receive
/// @retval             hal_spi_error_t         See hal_spi_error_t
//======================================================================
public hal_spi_error_t hal_spi_receive(u8 *data, u8 size);

/// @} HAL_SPI_API
/// @} HAL_SPI
#endif // __HAL_SPI_H__
