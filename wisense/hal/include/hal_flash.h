/// @copyright
/// ========================================================================={{{
/// Copyright (c) 2012-2013 WizziLab                                           /
/// All rights reserved                                                        /
///                                                                            /
/// IMPORTANT: This Software may not be modified, copied or distributed unless /
/// embedded on a WizziLab product. Other than for the foregoing purpose, this /
/// Software and/or its documentation may not be used, reproduced, copied,     /
/// prepared derivative works of, modified, performed, distributed, displayed  /
/// or sold for any purpose. For the sole purpose of embedding this Software   /
/// on a WizziLab product, copy, modification and distribution of this         /
/// Software is granted provided that the following conditions are respected:  /
///                                                                            /
/// *  Redistributions of source code must retain the above copyright notice,  /
///    this list of conditions and the following disclaimer                    /
///                                                                            /
/// *  Redistributions in binary form must reproduce the above copyright       /
///    notice, this list of conditions and the following disclaimer in the     /
///    documentation and/or other materials provided with the distribution.    /
///                                                                            /
/// *  The name of WizziLab can not be used to endorse or promote products     /
///    derived from this software without specific prior written permission.   /
///                                                                            /
/// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS        /
/// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED  /
/// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR /
/// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR          /
/// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,      /
/// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,        /
/// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,            /
/// OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY     /
/// OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING    /
/// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS         /
/// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.               /
/// WIZZILAB HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,       /
/// ENHANCEMENTS OR MODIFICATIONS.                                             /
///                                                                            /
/// Should you have any questions regarding your right to use this Software,   /
/// contact WizziLab at www.wizzilab.com.                                      /
///                                                                            /
/// =========================================================================}}}
/// @endcopyright

//  =======================================================================
/// @file           hal.h
/// @brief          Hardware Abstraction Layer API
/// @defgroup       HAL
//  =======================================================================

#ifndef __HAL_FLASH_H__
#define __HAL_FLASH_H__

#include "hal_types.h"
#include "kal.h"

extern u8 _flash_pagesize;

// Defining the NULL address
#define HAL_FLASH_NULL          ((u8*) 0x0)
#define HAL_FLASH_EMPTY         (0x0)
#define HAL_FLSH_PAGESIZE       (((int) &_flash_pagesize))

typedef enum 
{
    HAL_FLASH_OK = 0,
    HAL_FLASH_TIMEOUT,
    HAL_FLASH_ERROR,

} hal_flash_status_t;

//======================================================================
// hal_flash_open
//----------------------------------------------------------------------
/// @brief  Initialise hal_flash
//======================================================================
public void hal_flash_open(void);

//======================================================================
// hal_flash_write
//----------------------------------------------------------------------
/// @brief  Write data into flash using a virtual address that starts
///         at 0x00
/// @param  vaddr   int     The virtual address
/// @param  data    void*   The pointer to the data to be stored
/// @param  length  u16     The length of the data to be stored (in bytes)
/// @return How many bytes were written
//======================================================================
public int hal_flash_write(const u8* vaddr, const void* data, const u16 length);

//======================================================================
// hal_flash_read
//----------------------------------------------------------------------
/// @brief  Read data within the indicated virtual address.
///         If the virtual address is invalid, zero is read.
/// @param  vaddr   int     The virtual address
//======================================================================
public u8 hal_flash_read(const u8* vaddr);

//======================================================================
// hal_flash_read_int
//----------------------------------------------------------------------
/// @brief  Read data within the indicated virtual address.
///         If the virtual address is invalid, zero is read.
/// @param  vaddr   int     The virtual address
//======================================================================
public int hal_flash_read_int(const u8* vaddr);

//======================================================================
// hal_flash_die_on_error
//----------------------------------------------------------------------
/// @brief  For debug only - Triggers assert in case of error on memory
/// @param  none
/// @return none
//======================================================================
public void hal_flash_die_on_error(void);

//======================================================================
// hal_pflash_write
//----------------------------------------------------------------------
/// @brief  Write to program flash
///         If it is the first byte of a page, the page will be erased
///         Will only work if dest is aligned on a half page
///         length must be a multiple of 4 (dword)
/// @param src_addr start address of the source half page 
/// @param dest_addr start address of the destination half page 
/// @retval none
//======================================================================
public u32 hal_pflash_write(u8* dest_addr, const u8* src_addr,u16 length);

//======================================================================
// hal_pflash_read
//----------------------------------------------------------------------
/// @brief  Read data within the indicated virtual address.
///         If the virtual address is invalid, zero is read.
/// @param  vaddr   u16     The virtual address
//======================================================================
public u8 hal_pflash_read(const u8* vaddr);

#endif // __HAL_FLASH_H__
