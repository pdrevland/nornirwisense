/// @copyright
/// ========================================================================={{{
/// Copyright (c) 2012-2013 WizziLab                                           /
/// All rights reserved                                                        /
///                                                                            /
/// IMPORTANT: This Software may not be modified, copied or distributed unless /
/// embedded on a WizziLab product. Other than for the foregoing purpose, this /
/// Software and/or its documentation may not be used, reproduced, copied,     /
/// prepared derivative works of, modified, performed, distributed, displayed  /
/// or sold for any purpose. For the sole purpose of embedding this Software   /
/// on a WizziLab product, copy, modification and distribution of this         /
/// Software is granted provided that the following conditions are respected:  /
///                                                                            /
/// *  Redistributions of source code must retain the above copyright notice,  /
///    this list of conditions and the following disclaimer                    /
///                                                                            /
/// *  Redistributions in binary form must reproduce the above copyright       / 
///    notice, this list of conditions and the following disclaimer in the     /
///    documentation and/or other materials provided with the distribution.    /
///                                                                            /
/// *  The name of WizziLab can not be used to endorse or promote products     /
///    derived from this software without specific prior written permission.   /
///                                                                            /
/// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS        /
/// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED  /
/// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR /
/// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR          /
/// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,      /
/// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,        /
/// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,            /
/// OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY     /
/// OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING    /
/// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS         /
/// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.               /
/// WIZZILAB HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,       /
/// ENHANCEMENTS OR MODIFICATIONS.                                             /
///                                                                            /
/// Should you have any questions regarding your right to use this Software,   /
/// contact WizziLab at www.wizzilab.com.                                      /
///                                                                            /
/// =========================================================================}}}
/// @endcopyright

//  =======================================================================
/// @file           hal_adc.h
/// @brief          API header of the ADC driver
// @ingroup HAL
/// @defgroup HAL_ADC      HAL ADC
/// @{
//  =======================================================================

#ifndef __HAL_ADC_H__
#define __HAL_ADC_H__

#include "hal_types.h"
#include "hal_defs.h"
#include "hal_error.h"
#include "kal.h"
/// @ingroup HAL_ADC
/// @defgroup HAL_ADC_STR       HAL ADC Structures
/// @{

// Used for indexing
#define HAL_ADC1    (0)
#define HAL_ADC2    (1)
#define HAL_ADC3    (2)

// Helpers
#define HAL_EVP2ADC(evp)    (evp.data & 0xFF)
#define HAL_EVP2CFG(evp)    ((evp.data>>8) & 0xFF)
#define HAL_ADCCFG2EVP(adc,cfg) ((((cfg) & 0xFF) << 8)|((adc) & 0xFF))

typedef enum {
    ADC_MODE_NORMAL = 0,
    ADC_MODE_DUAL,
} hal_adc_mode_t;

typedef enum {
    ADC_CH_0 = 0,
    ADC_CH_1,
    ADC_CH_2,
    ADC_CH_3,
    ADC_CH_4,
    ADC_CH_5,
    ADC_CH_6,
    ADC_CH_7,
    ADC_CH_8,
    ADC_CH_9,
    ADC_CH_10,
    ADC_CH_11,
    ADC_CH_12,
    ADC_CH_13,
    ADC_CH_14,
    ADC_CH_15,
    ADC_CH_TEMP,    // (only ADC1)Temperature reco samp. time = 10us (F) / 17.1us (L)
    ADC_CH_VREFINT, // (only ADC1)Internal Vref
    ADC_CH_18,
    ADC_CH_19,
    ADC_CH_20,
    ADC_CH_21,
    ADC_CH_22,
    ADC_CH_23,
    ADC_CH_24,
    ADC_CH_25,
    ADC_CH_26,
} hal_adc_ch_id_t;

typedef struct {
    /// Channel ID among hal_adc_ch_id_t
    u8  ch_id;
    /// Sampling time in ns
    u16 tsamp_ns;
} hal_adc_ch_t;


// =======================================================================
// hal_adc_cfg_t
// -----------------------------------------------------------------------
/// Template structure of the ADC setup
/// This structure is used as a template by the adc service
/// - it can be used to access hal_adc_cfg_t objects through dereferencing
/// - it should not be instanciated directly (except for single channel).
// =======================================================================
typedef struct {
    /// Mode
    u8              mode;
    /// Status
    u8              status;
    /// Target process
    process_t*      tgt_proc;
    /// Timer for periodic measurements
    kal_timer_t     tim;
    /// Measurement period
    u16             period;
    /// Number of samples for each active channel (only with DMA)
    u16             nb_samp;
    /// Output buffer (Samples will be stored via DMA whenever buf != NULL)
    u16*            buf;
    /// Number of channels
    u8              nb_ch;
    /// Active channel Id
    u8              ch_id;
    /// Channel list
    hal_adc_ch_t    ch[1];
} hal_adc_cfg_t;

// =======================================================================
// hal_adc_cfg_t(n)
// -----------------------------------------------------------------------
/// Structure of the ADC setup.
/// This macro allows static instanciation of hal_adc_cfg_t with variable
/// channel list size. The parameter n indicates the maximum number of
/// channels that will be handled by the ADC.
// =======================================================================
#define hal_adc_cfg_t(n)    struct { \
    u8              mode; \
    u8              status; \
    process_t*      tgt_proc; \
    kal_timer_t     tim; \
    u16             period; \
    u16             nb_samp; \
    u16*            buf; \
    u8              nb_ch; \
    u8              ch_id; \
    hal_adc_ch_t    ch[n]; \
}

/// @} HAL_ADC_STR

/// @ingroup HAL_ADC
/// @defgroup HAL_ADC_API       HAL ADC API
/// @{


//======================================================================
// hal_adc_open
//----------------------------------------------------------------------
//======================================================================
public void hal_adc_open(void);
public u8 hal_adc_add_cfg(hal_adc_cfg_t* cfg);
public void hal_adc_rem_cfg(u8 cfg_id);
public void hal_adc_start_cfg(u8 cfg_id);
public void hal_adc_stop_cfg(u8 cfg_id);
public u16 hal_adc_get_value(u8 cfg_id,u8 ch_id);


/// @} HAL_ADC_API

/// @ingroup HAL_ADC
/// @defgroup HAL_ADC_SINGLE_API       HAL ADC SINGLE SHOT API
/// @{

//======================================================================
// hal_adc_single_open
//----------------------------------------------------------------------
/// @brief  Open ADC driver in single mode. IO setup must be done separately
/// @param  void
/// @return void
//======================================================================
public void hal_adc_single_open(void);

//======================================================================
// hal_adc_single_run
//----------------------------------------------------------------------
/// @brief  One shot ADC measurement. Should not be called on ISR.
/// @param  channel: channel input selection among hal_adc_ch_t list.
/// @param  handler_proc: process to which the events will be sent after each new sampling. To be set
///         to NULL if not required.
/// @return void
//======================================================================
public void hal_adc_single_run(u8 channel, process_t* handler_proc);

//======================================================================
// hal_adc_single_close
//----------------------------------------------------------------------
/// @brief  close ADC channel. Should not be called on ISR.
/// @param  void
/// @return void
//======================================================================
void hal_adc_single_close(void);


/// @} HAL_ADC_SINGLE_API
/// @} HAL_ADC

#endif // __HAL_ADC_H__
