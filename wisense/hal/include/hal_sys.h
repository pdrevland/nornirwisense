/// @copyright
/// ========================================================================={{{
/// Copyright (c) 2012-2013 WizziLab                                           /
/// All rights reserved                                                        /
///                                                                            /
/// IMPORTANT: This Software may not be modified, copied or distributed unless /
/// embedded on a WizziLab product. Other than for the foregoing purpose, this /
/// Software and/or its documentation may not be used, reproduced, copied,     /
/// prepared derivative works of, modified, performed, distributed, displayed  /
/// or sold for any purpose. For the sole purpose of embedding this Software   /
/// on a WizziLab product, copy, modification and distribution of this         /
/// Software is granted provided that the following conditions are respected:  /
///                                                                            /
/// *  Redistributions of source code must retain the above copyright notice,  /
///    this list of conditions and the following disclaimer                    /
///                                                                            /
/// *  Redistributions in binary form must reproduce the above copyright       / 
///    notice, this list of conditions and the following disclaimer in the     /
///    documentation and/or other materials provided with the distribution.    /
///                                                                            /
/// *  The name of WizziLab can not be used to endorse or promote products     /
///    derived from this software without specific prior written permission.   /
///                                                                            /
/// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS        /
/// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED  /
/// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR /
/// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR          /
/// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,      /
/// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,        /
/// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,            /
/// OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY     /
/// OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING    /
/// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS         /
/// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.               /
/// WIZZILAB HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,       /
/// ENHANCEMENTS OR MODIFICATIONS.                                             /
///                                                                            /
/// Should you have any questions regarding your right to use this Software,   /
/// contact WizziLab at www.wizzilab.com.                                      /
///                                                                            /
/// =========================================================================}}}
/// @endcopyright

//  =======================================================================
/// @file           hal_sys.h
/// @brief          Hardware Abstraction Layer API - System functions
/// @ingroup HAL
/// @defgroup HAL_SYS   HAL SYS
/// @{
//  =======================================================================

#ifndef __HAL_SYS_H__
#define __HAL_SYS_H__

#include "hal_types.h"
#include "hal_isr.h"

/// @ingroup HAL_SYS
/// @defgroup HAL_SYS_API   HAL SYS API
/// @{

#define HAL_SYS_USE_LSI (0x1 << 0)
#define HAL_SYS_USE_LSE (0x1 << 1)
#define HAL_SYS_USE_HSI (0x1 << 2)
#define HAL_SYS_USE_HSE (0x1 << 3)
enum {LPM0 = 0,LPM1,LPM2,LPM3,LPM4,LPM5,LPM6, HAL_LPM_QTY};

//======================================================================
// hal_open
//----------------------------------------------------------------------
/// @brief  Initializes platform
/// @param  assert_handler      assert handler (may be NULL)
/// @param  max_pm              requested maximum power level
/// @retval status
//======================================================================
public u16 hal_open(hal_isr_t assert_handler, u8 max_pm);

//======================================================================
// hal_close
//----------------------------------------------------------------------
/// @brief  close down HAL
//======================================================================
public void hal_close(void);

//======================================================================
// hal_rand
//----------------------------------------------------------------------
/// @brief  generate pseudo random 32-bit value
/// @retval                 pseudo random value
//======================================================================
public u32 hal_rand(void);

//======================================================================
// hal_rand_range
//----------------------------------------------------------------------
/// @brief  generate pseudo random 16-bit value in given range
/// @param  max             u16                 maximum range
/// @retval                 u16                 pseudo random value in [0; max [
//======================================================================
public u16 hal_rand_range(u16 max);

//======================================================================
// memcpy
//----------------------------------------------------------------------
/// @brief  standard memcopy.The memory areas must not overlap.
///         This function/symbol is mandatory for GNU/GCC
/// @param  dest pointer to the destination buffer
/// @param  src  pointer to the source buffer
/// @param  n    copy length in bytes
/// @retval pointer to dest
//======================================================================
public void* memcpy(void *dest, const void *src, size_t n);
#define hal_memcpy(dest,src,len) memcpy(dest,src,len)

//======================================================================
// memset
//----------------------------------------------------------------------
/// @brief  standard memset.
///         This function/symbol is mandatory for GNU/GCC
/// @param  s pointer to the destination buffer
/// @param  c  pointer to the source buffer
/// @param  n  number of bytes to be written
/// @retval pointer to dest
//======================================================================
public void* memset(void *s, int c, size_t n);

//======================================================================
// memmove
//----------------------------------------------------------------------
/// @brief  standard memmove. Copy the first n bytes pointed to by src
/// to the buffer pointed to by dest. Source and destination may overlap
///         This function/symbol is mandatory for GNU/GCC
/// @param  dest pointer to the destination buffer
/// @param  src  pointer to the source buffer
/// @param  n    copy length in bytes
/// @retval pointer to dest
//======================================================================
public void* memmove(void *dest, const void *src, size_t n);

//======================================================================
// hal_get_die_id
//----------------------------------------------------------------------
/// @brief  Retrieves 8 first bytes of the unique Chip ID made of Lot/Waffer
/// @param  die_id      u8*                 pointer to a 8-bytes buffer
/// @retval             void
//======================================================================
public void hal_get_die_id(u8* die_id);

//======================================================================
//  hal_get_full_die_id
//----------------------------------------------------------------------
/// @brief Get the 12-bytes (3-word) Unique ID
/// @param  die_id      u32*                 pointer to a 3-word buffer
/// @retval             void
//======================================================================
public void hal_get_full_die_id(u32* uid);

//======================================================================
// hal_get_die_temp
//----------------------------------------------------------------------
/// @brief  Get die temperature reference 
/// @param  temp_30c    u16*                pointer to the 30c ref
/// @param  temp_gain   u16*                pointer to the gain 
/// @retval             void
//======================================================================
public void hal_get_die_temp(u16* temp_30c, u16* temp_gain);

//======================================================================
// hal_sleep
//----------------------------------------------------------------------
/// @brief  go to the deepest sleep mode allowed by currently active
///         resources.
///         Re-enable interrupts in any case to allow wake-up.
//======================================================================
public void hal_sleep(void);

//======================================================================
// hal_software_reset
//----------------------------------------------------------------------
/// @brief  Generate an immediate software reset using the watchdog
/// @param  status          uint        unused, kep function type compatible with hal_isr_t
/// @retval                 void
//======================================================================
public void hal_software_reset(uint status); 

//======================================================================
// hal_watchdog_start
//----------------------------------------------------------------------
/// @brief  Start the watchdog
/// @param  period          u16         max update period in ms
/// @retval                 void
//======================================================================
public void hal_watchdog_start(u16 period); 

//======================================================================
// hal_watchdog_reload
//----------------------------------------------------------------------
/// @brief  Reload the watchdog to prevent reset
/// @param                  void 
/// @retval                 void
//======================================================================
public void hal_watchdog_reload(void);

//======================================================================
// hal_crc32
//----------------------------------------------------------------------
/// @brief Computes a CRC on a datastream using CRC-32 (Ethernet) polynomial: 0x4C11DB7
/// But unfortunatelly endianness,shifting and initial value are
/// all the opposite of all "standard" implementations... So some
/// bit-fiddl'ing is necessary 
/// Can work with any alignment and size
/// @param   buffer pointer to the data
/// @param   size data size in bytes
/// @retval  CRC value
/// @todo Unresolved "hardfault" issue
/// @todo Doesn't work in Debug compile?
//======================================================================
public u32 hal_crc32(u8 *buffer, size_t size);

//======================================================================
// Sleep Related functions
//----------------------------------------------------------------------
// The system is send to sleep through the call to hal_sleep.
// Depending on currently used resources, the lowest Power Mode is
// selected as target.
// Up to X "resources" can be defined. They must then be added to the
// relevant HAL_RESOURCE_NEED_x bitmap depending on the minimum power
// mode they require.
//======================================================================
#define HAL_RESOURCE_COMRX(n)   (1 << (n)) // Up to 4 COMs
#define HAL_RESOURCE_COMTX(n)   (1 << (4+(n))) // Up to 4 COMs
#define HAL_RESOURCE_ANYCOM     (0xFF) // Up to 4 COMs
#define HAL_RESOURCE_TIMER      (1 << 8) // HAL Timer
#define HAL_RESOURCE_RADIO      (1 << 9) // RAC Timers + SPI ?
#define HAL_RESOURCE_I2C(n)     (1 << (10+(n))) // Up to 2 I2C
#define HAL_RESOURCE_ANYI2C     (3 << 10) // Up to 2 I2C
#define HAL_RESOURCE_SPI        (1 << 12) // TODO: multiple SPI
#define HAL_RESOURCE_ADC        (1 << 16) // ADCs
#define HAL_RESOURCE_DAC        (1 << 17) // DACs
#define HAL_RESOURCE_PWM_SLOW   (1 << 18) // PWM on LSE
#define HAL_RESOURCE_PWM_FAST   (1 << 19) // PWM on SYSCLK
#define HAL_RESOURCE_DBGTIM     (1 << 30) // HAL DBG Timer
#define HAL_RESOURCE_FULL       (1 << 31) // Everything ON except CPU


//======================================================================
// hal_use_resource
//----------------------------------------------------------------------
/// @brief  Mark a resource as "used".
///         Resource flag is used to derive achievable low-power mode.
/// @param  resource: bitmap of resources to be considered active.
//======================================================================
public void hal_use_resource(uint resource);

//======================================================================
// hal_release_resource
//----------------------------------------------------------------------
/// @brief  Release a resource flag.
///         Resource flag is used to derive achievable low-power mode.
/// @param  resource: bitmap of resources to be released
//======================================================================
public void hal_release_resource(uint resource);

//======================================================================
// __get_MSP
//----------------------------------------------------------------------
/// @brief  Return the Main Stack Pointer
/// @param                  void 
/// @retval                 uint32_t        Main Stack Pointer of the
///                                         Cortex processor register
//======================================================================
public u32 hal_get_msp(void) __attribute__( ( naked ) );

public u32 hal_get_pfreq(void);
public u32 hal_get_hfreq(void);

//======================================================================
// HAL_SLEEP_UNTIL
//----------------------------------------------------------------------
/// @brief  Continuouly goes to sleep until the condition is met.
///         To be used only if the condition is altered by an IRQ.
/// @param  cond: conditional statement
//======================================================================
#if 1 // TODO
#define HAL_SLEEP_WHILE(cond) do{ \
                            hal_set_primask(1); \
                            if (cond) { \
                                hal_sleep(); \
                            } \
                            hal_set_primask(0); \
                        }while(0)
#else

#define HAL_SLEEP_WHILE(cond)

#endif


/// @} HAL_SYS_API
/// @} HAL_SYS
#endif // __HAL_SYS_H__
