/// @copyright
/// ========================================================================={{{
/// Copyright (c) 2012-2013 WizziLab                                           /
/// All rights reserved                                                        /
///                                                                            /
/// IMPORTANT: This Software may not be modified, copied or distributed unless /
/// embedded on a WizziLab product. Other than for the foregoing purpose, this /
/// Software and/or its documentation may not be used, reproduced, copied,     /
/// prepared derivative works of, modified, performed, distributed, displayed  /
/// or sold for any purpose. For the sole purpose of embedding this Software   /
/// on a WizziLab product, copy, modification and distribution of this         /
/// Software is granted provided that the following conditions are respected:  /
///                                                                            /
/// *  Redistributions of source code must retain the above copyright notice,  /
///    this list of conditions and the following disclaimer                    /
///                                                                            /
/// *  Redistributions in binary form must reproduce the above copyright       / 
///    notice, this list of conditions and the following disclaimer in the     /
///    documentation and/or other materials provided with the distribution.    /
///                                                                            /
/// *  The name of WizziLab can not be used to endorse or promote products     /
///    derived from this software without specific prior written permission.   /
///                                                                            /
/// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS        /
/// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED  /
/// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR /
/// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR          /
/// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,      /
/// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,        /
/// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,            /
/// OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY     /
/// OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING    /
/// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS         /
/// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.               /
/// WIZZILAB HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,       /
/// ENHANCEMENTS OR MODIFICATIONS.                                             /
///                                                                            /
/// Should you have any questions regarding your right to use this Software,   /
/// contact WizziLab at www.wizzilab.com.                                      /
///                                                                            /
/// =========================================================================}}}
/// @endcopyright

//  =======================================================================
/// @file           hal_isr.h
/// @brief          header of the hal_isr.c
/// @ingroup HAL
/// @defgroup HAL_ISR       HAL Interrupts
/// @{
//  =======================================================================

#ifndef __HAL_ISR_H__
#define __HAL_ISR_H__

#include "hal_types.h"

/// @typedef hal_callback_t
// =======================================================================
// hal_isr_vectors_t
// -----------------------------------------------------------------------
/// Isr vector function type
/// looks like: void isrVector(uint cause)
// =======================================================================
typedef void (*hal_isr_t)(uint);

// =======================================================================

/// @ingroup HAL_ISR
/// @defgroup HAL_ISR_API       HAL Interrupts API
/// @{

//======================================================================
// hal_get_primask
//----------------------------------------------------------------------
/// @brief  Return the Priority Mask value
/// @param                  void
/// @retval PriMask         uint32_t Return state of the priority mask bit from the priority mask register
//======================================================================
__attribute__( ( always_inline ) ) static inline uint32_t hal_get_primask(void)
{
  uint32_t result;

  __asm volatile ("MRS %0, primask" : "=r" (result) );
  return(result);
}

//======================================================================
// hal_set_primask
//----------------------------------------------------------------------
/// @brief  Set the Priority Mask value
/// @param  PriMask         uint32_t Set the priority mask bit in the priority mask register
/// @retval                 void
//======================================================================
__attribute__( ( always_inline ) ) static inline void hal_set_primask(uint32_t priMask)
{
  __asm volatile ("MSR primask, %0" : : "r" (priMask) );
}

//======================================================================
// hal_enable_irq
//----------------------------------------------------------------------
/// @brief  Enable all IRq
/// @param                  void 
/// @retval                 void
//======================================================================
static inline void hal_enable_irq()               
{ 
    __asm volatile ("cpsie i"); 
}

//======================================================================
// hal_enable_irq
//----------------------------------------------------------------------
/// @brief  Enable all IRq
/// @param                  void 
/// @retval                 void
//======================================================================
static inline void hal_disable_irq()
{ 
    __asm volatile ("cpsid i"); 
}

//======================================================================
// hal_isr_enter_critical_section
//----------------------------------------------------------------------
/// @brief  Enter critical section (ie disable interrupts)
/// @retval status of the interrupt bit before the irq
//======================================================================
static inline u32 hal_isr_enter_critical_section(void)
{
    u32 status = hal_get_primask();
    hal_set_primask(1); // TODO: __disable_irq can only be called in priviledged mode
    return status;
}

//======================================================================
// hal_isr_exit_critical_section
//----------------------------------------------------------------------
/// @brief  Exit critical section (ie restore interrupt enable state). Use it with the return value of hal_isr_enter_critical_section
/// @param  status of the interrupt bit before the irq
//======================================================================
static inline void hal_isr_exit_critical_section(unsigned int status)
{
    hal_set_primask(status);
}

/// @} HAL_ISR_API

/// @} HAL_ISR

#endif // __HAL_ISR_H__
