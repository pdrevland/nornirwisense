/// @copyright
/// ========================================================================={{{
/// Copyright (c) 2012-2013 WizziLab                                           /
/// All rights reserved                                                        /
///                                                                            /
/// IMPORTANT: This Software may not be modified, copied or distributed unless /
/// embedded on a WizziLab product. Other than for the foregoing purpose, this /
/// Software and/or its documentation may not be used, reproduced, copied,     /
/// prepared derivative works of, modified, performed, distributed, displayed  /
/// or sold for any purpose. For the sole purpose of embedding this Software   /
/// on a WizziLab product, copy, modification and distribution of this         /
/// Software is granted provided that the following conditions are respected:  /
///                                                                            /
/// *  Redistributions of source code must retain the above copyright notice,  /
///    this list of conditions and the following disclaimer                    /
///                                                                            /
/// *  Redistributions in binary form must reproduce the above copyright       / 
///    notice, this list of conditions and the following disclaimer in the     /
///    documentation and/or other materials provided with the distribution.    /
///                                                                            /
/// *  The name of WizziLab can not be used to endorse or promote products     /
///    derived from this software without specific prior written permission.   /
///                                                                            /
/// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS        /
/// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED  /
/// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR /
/// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR          /
/// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,      /
/// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,        /
/// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,            /
/// OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY     /
/// OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING    /
/// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS         /
/// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.               /
/// WIZZILAB HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,       /
/// ENHANCEMENTS OR MODIFICATIONS.                                             /
///                                                                            /
/// Should you have any questions regarding your right to use this Software,   /
/// contact WizziLab at www.wizzilab.com.                                      /
///                                                                            /
/// =========================================================================}}}
/// @endcopyright

//  ====================================================================
//  @file           hal_defs_p.h
//  @brief          hal definition header
//  ====================================================================

#ifndef __HAL_CFG_H
#define __HAL_CFG_H

#include "hal_led.h"

//======================================================================
// GENERAL PURPOSE DEFINES USED IN THE BOARD CONFIGURATION
//======================================================================


//======================================================================
// COM Ports definition
// --------------------------------------------------------------
// XXX TODO: not where it belongs but prevents to include kal_com...
#define COM_TYPE_UART               0
#define COM_TYPE_USB                1
#define TX_DMA                      1
#define TX_NODMA                    0
#define RX_DMA                      1
#define RX_NODMA                    0
#define COM_NO_WU                   0xFF

typedef enum
{
    ///  No Serial
    COM_MODE_OFF                     = 0,
    /// Raw (no header)
    COM_MODE_RAW                     = 1,
    /// WizziCom packet
    COM_MODE_WL_PACKET               = 2,
    /// Bluegiga packet
    COM_MODE_BG_PACKET               = 3,
    /// Generic packet
    COM_MODE_GEN_PACKET              = 4,

} kal_com_mode_t;


typedef enum
{
    COM_BAUDRATE_9600                = 0,
    COM_BAUDRATE_28800               = 1,
    COM_BAUDRATE_57600               = 2,
    COM_BAUDRATE_115200              = 3

} kal_com_baudrate_t;



//======================================================================
// BOARD CONFIGURATION
//======================================================================
#if ( defined(PLATFORM_LEV) )
//  #warning "compiling for PLATFORM: LEV"
    #include "../defs/hal_defs_lev.h"
#elif ( defined(PLATFORM_MOTE) )
//  #warning "compiling for PLATFORM: MOTE"
    #include "../defs/hal_defs_mote_v1.h"
#elif ( defined(PLATFORM_CSM) )
//  #warning "compiling for PLATFORM: CSM"
    #include "../defs/hal_defs_csm_v1.h"
#elif ( defined(PLATFORM_CSMR_V1) )
//  #warning "compiling for PLATFORM: CSMR_V1"
    #include "../defs/hal_defs_csmr_v1.h"
#elif ( defined(PLATFORM_CSMR) )
//  #warning "compiling for PLATFORM: CSMR"
    #include "../defs/hal_defs_csmr_v2.h"
#elif ( defined(PLATFORM_WISENSE) )
//  #warning "compiling for PLATFORM: WISENSE"
    #include "../defs/hal_defs_wisense.h"
#elif ( defined(PLATFORM_WM1001_CSM) )
//  #warning "compiling for PLATFORM: WM1001_CSM"
    #include "../defs/hal_defs_wm1001_csm.h"
#elif ( defined(PLATFORM_WM1001_WISENSE) )
//  #warning "compiling for PLATFORM: WM1001_WISENSE"
    #include "../defs/hal_defs_wm1001_wisense.h"
#elif ( defined(PLATFORM_WM1001_WIPI1) )
//  #warning "compiling for PLATFORM: WM1001_WIPI1"
    #include "../defs/hal_defs_wm1001_wipi1.h"
#elif ( defined(PLATFORM_WM1001_WIPI2) )
//  #warning "compiling for PLATFORM: WM1001_WIPI2"
    #include "../defs/hal_defs_wm1001_wipi2.h"
#elif ( defined(PLATFORM_GW1101) )
//  #warning "compiling for PLATFORM: GW1101"
    #include "../defs/hal_defs_gw1101.h"
#elif ( defined(PLATFORM_OLIMEX) )
//  #warning "compiling for PLATFORM: OLIMEX stm32h152"
    #include "../defs/hal_defs_stm32h152.h"
#elif ( defined(PLATFORM_LCDPARK) )
//  #warning "compiling for PLATFORM: LCDPARK"
    #include "../defs/hal_defs_lcdpark.h"
#elif ( defined(PLATFORM_WM1001_LCDPARK) )
//  #warning "compiling for PLATFORM: WM1001_LCDPARK"
    #include "../defs/hal_defs_wm1001_lcdpark.h"
#elif ( defined(PLATFORM_WM1001_CSMR_V1) )
//  #warning "compiling for PLATFORM: WM1001_CSMR_V1"
    #include "../defs/hal_defs_wm1001_csmr_v1.h"
#elif ( defined(PLATFORM_WM1001_CSMR) )
//  #warning "compiling for PLATFORM: WM1001_CSMR"
    #include "../defs/hal_defs_wm1001_csmr_v2.h"
#else
    #error "PLATFORM not defined"
#endif


#endif // __HAL_CFG_H
