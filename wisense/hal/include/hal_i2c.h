/// @copyright
/// ========================================================================={{{
/// Copyright (c) 2012-2014 WizziLab                                           /
/// All rights reserved                                                        /
///                                                                            /
/// IMPORTANT: This Software may not be modified, copied or distributed unless /
/// embedded on a WizziLab product. Other than for the foregoing purpose, this /
/// Software and/or its documentation may not be used, reproduced, copied,     /
/// prepared derivative works of, modified, performed, distributed, displayed  /
/// or sold for any purpose. For the sole purpose of embedding this Software   /
/// on a WizziLab product, copy, modification and distribution of this         /
/// Software is granted provided that the following conditions are respected:  /
///                                                                            /
/// *  Redistributions of source code must retain the above copyright notice,  /
///    this list of conditions and the following disclaimer                    /
///                                                                            /
/// *  Redistributions in binary form must reproduce the above copyright       / 
///    notice, this list of conditions and the following disclaimer in the     /
///    documentation and/or other materials provided with the distribution.    /
///                                                                            /
/// *  The name of WizziLab can not be used to endorse or promote products     /
///    derived from this software without specific prior written permission.   /
///                                                                            /
/// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS        /
/// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED  /
/// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR /
/// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR          /
/// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,      /
/// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,        /
/// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,            /
/// OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY     /
/// OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING    /
/// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS         /
/// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.               /
/// WIZZILAB HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,       /
/// ENHANCEMENTS OR MODIFICATIONS.                                             /
///                                                                            /
/// Should you have any questions regarding your right to use this Software,   /
/// contact WizziLab at www.wizzilab.com.                                      /
///                                                                            /
/// =========================================================================}}}
/// @endcopyright

//  =======================================================================
/// @file           hal_i2c.h
/// @brief          API header of the I2C driver
/// @ingroup HAL
/// @defgroup HAL_I2C   HAL I2C
/// @{
//  =======================================================================

#ifndef __HAL_I2C_H__
#define __HAL_I2C_H__

#include "hal_types.h"
#include "hal_error.h"
#include "kal_process.h"

/// @ingroup HAL_I2C
/// @defgroup HAL_I2C_STR       HAL I2C Structures
/// @{

// =======================================================================
// hal_i2c_dev_t
// -----------------------------------------------------------------------
/// Template structure of the i2c device.
/// This structure is used as a template by the i2c driver:
/// - it can be used to access hal_i2c_dev_t objects through dereferencing
/// - it should not be instanciated directly.
// =======================================================================
typedef struct 
{
    process_t*  p;
    /// Slave address
    u16 saddr;
    /// evdata
    u8  evdata;
    /// Device status
    u8  status;
    /// clock divider, depending on the requested I2C clock frequency 
    u16 sclfreq;
    /// number of bytes to read
    u8  rx_bytes;
    /// number of bytes to write
    u8  tx_bytes;
    /// data buffer for wr/rd operations
    u8  buf[1];

} hal_i2c_dev_t;

// =======================================================================
// hal_i2c_dev_t(n)
// -----------------------------------------------------------------------
/// Structure of the i2c device.
/// This macro allows static instanciation of hal_i2c_dev_t with variable
/// buffer size. The parameter n indicates the maximum size in bytes of
/// reception/transmission transfers.
// =======================================================================
#define hal_i2c_dev_t(n)    struct { \
    process_t*  p; \
    u16 saddr; \
    u8  evdata; \
    u8  status; \
    u16  sclfreq; \
    u8  rx_bytes; \
    u8  tx_bytes; \
    u8  buf[1+n]; \
}

typedef enum 
{
    I2C_MASTER_MODE     = 0x2,
    I2C_MASTER_10B_MODE = 0x6,
    I2C_SLAVE_MODE      = 0x0,
    I2C_SLAVE_10B_MODE  = 0x4

} i2c_mode_t;


/// @} HAL_I2C_STR

/// @ingroup HAL_I2C
/// @defgroup HAL_I2C_API       HAL I2C API
/// @{

//======================================================================
// hal_i2c_open
//----------------------------------------------------------------------
/// @brief  Open I2C service - Setup IOs and start I2C thread
/// @param  idx         u8          context index
/// @retval             void
//======================================================================
public void hal_i2c_open(u8 idx);

//======================================================================
// hal_i2c_close
//----------------------------------------------------------------------
/// @brief  Close I2C service - Setup IOs and start I2C thread
/// @param  idx         u8          context index
/// @retval             void
//======================================================================
public void hal_i2c_close(u8 idx);

//======================================================================
// hal_i2c_new
//----------------------------------------------------------------------
/// @brief  Initialize an I2C device handler
/// @param  dev         void *      pointer to the I2C device structure
/// @param  saddr       u16         slave address of the target device
/// @param  mode        i2c_mode_t  I2C mode
/// @param  clk_in_khz  u16         requested I2C clock frequency
/// @retval             void        
//======================================================================
public void hal_i2c_new(void* dev, u16 saddr, i2c_mode_t mode, u16 clk_in_khz);

//======================================================================
// hal_i2c_free
//----------------------------------------------------------------------
/// @brief  Release an I2C device handler
/// @param  idx         u8          context index
/// @retval             void        
//======================================================================
public void hal_i2c_free(u8 idx);

//======================================================================
// hal_i2c_wr_reg
//----------------------------------------------------------------------
/// @brief  Writes a register in a device using indirect addressing.
/// @param  idx         u8          context index
/// @param  dev         void*       pointer to the I2C device handler
/// @param  addr        u8          register address
/// @param  data        u8          byte to be written
/// @retval             void        
//======================================================================
public void hal_i2c_wr_reg(u8 idx, void* dev, u8 addr, u8 data);

//======================================================================
// hal_i2c_wr_bytes
//----------------------------------------------------------------------
/// @brief  Sequentially writes bytes to a device.
///         Data to be transmitted must be stored in dev->buf
/// @param  idx         u8          context index
/// @param  dev         void*       pointer to the I2C device handler
/// @param  len         u8          number of bytes to be written
/// @retval             void        
//======================================================================
public void hal_i2c_wr_bytes(u8 idx, void* dev, u8 len);

//======================================================================
// hal_i2c_rd_regs
//----------------------------------------------------------------------
/// @brief  Reads one or several register(s) in a device using indirect
///         addressing. The device must support address auto-increment
///         to read out several consecutive registers.
///         Data retrieved will be stored in dev->buf
/// @param  idx         u8          context index
/// @param  dev         void*       pointer to the I2C device handler
/// @param  addr        u8          register address
/// @param  len         u8          number of bytes to be read out
/// @retval             void        
//======================================================================
public void hal_i2c_rd_regs(u8 idx, void* dev, u8 addr, u8 len);

//======================================================================
// hal_i2c_rd_bytes
//----------------------------------------------------------------------
/// @brief  Read one or several bytes from a device.
///         Data retrieved will be stored in dev->buf
/// @param  idx         u8          context index
/// @param  dev         void*       pointer to the I2C device handler
/// @param  len         u8          number of bytes to be read out
/// @retval             void        
//======================================================================
public void hal_i2c_rd_bytes(u8 idx, void* dev, u8 len);


//======================================================================
// hal_i2c_rd_2bregs
//----------------------------------------------------------------------
/// @brief  Reads one or several register(s) in a device using indirect
///         addressing. The device must support address auto-increment
///         to read out several consecutive registers.
///         Data retrieved will be stored in dev->buf
/// @param  idx         u8          context index
/// @param  dev         void*       pointer to the I2C device handler
/// @param  addr        u16         2 bytes register address
/// @param  len         u8          number of bytes to be read out
/// @retval             void        
//======================================================================
public void hal_i2c_rd_2bregs(u8 idx, void* dev, u16 addr, u8 len);


/// @} HAL_I2C_API
/// @} HAL_I2C
#endif // __HAL_I2C_H__
