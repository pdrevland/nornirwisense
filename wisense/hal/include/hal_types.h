/// @copyright
/// ========================================================================={{{
/// Copyright (c) 2012-2013 WizziLab                                           /
/// All rights reserved                                                        /
///                                                                            /
/// IMPORTANT: This Software may not be modified, copied or distributed unless /
/// embedded on a WizziLab product. Other than for the foregoing purpose, this /
/// Software and/or its documentation may not be used, reproduced, copied,     /
/// prepared derivative works of, modified, performed, distributed, displayed  /
/// or sold for any purpose. For the sole purpose of embedding this Software   /
/// on a WizziLab product, copy, modification and distribution of this         /
/// Software is granted provided that the following conditions are respected:  /
///                                                                            /
/// *  Redistributions of source code must retain the above copyright notice,  /
///    this list of conditions and the following disclaimer                    /
///                                                                            /
/// *  Redistributions in binary form must reproduce the above copyright       / 
///    notice, this list of conditions and the following disclaimer in the     /
///    documentation and/or other materials provided with the distribution.    /
///                                                                            /
/// *  The name of WizziLab can not be used to endorse or promote products     /
///    derived from this software without specific prior written permission.   /
///                                                                            /
/// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS        /
/// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED  /
/// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR /
/// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR          /
/// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,      /
/// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,        /
/// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,            /
/// OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY     /
/// OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING    /
/// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS         /
/// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.               /
/// WIZZILAB HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,       /
/// ENHANCEMENTS OR MODIFICATIONS.                                             /
///                                                                            /
/// Should you have any questions regarding your right to use this Software,   /
/// contact WizziLab at www.wizzilab.com.                                      /
///                                                                            /
/// =========================================================================}}}
/// @endcopyright

//	====================================================================
/// @file           hal_types.h
/// @brief          platform types definitions
/// @ingroup        HAL
/// @defgroup HAL_TYPES      HAL Types
/// @{
//	====================================================================

#ifndef __HAL_TYPES_H__
#define __HAL_TYPES_H__

//#include "stm32l1xx.h"

//======================================================================
// Common Types
//======================================================================
typedef signed long  s32;
typedef signed short s16;
typedef signed char  s8;

typedef unsigned long  u32;
typedef unsigned short u16;
typedef unsigned char  u8;

typedef volatile signed long  vs32;
typedef volatile signed short vs16;
typedef volatile signed char  vs8;

typedef volatile unsigned long  vu32;
typedef volatile unsigned short vu16;
typedef volatile unsigned char  vu8;

typedef signed long  const sc32;
typedef signed short const sc16;
typedef signed char  const sc8;

typedef unsigned long  const uc32;
typedef unsigned short const uc16;
typedef unsigned char  const uc8;

typedef volatile signed long  const vsc32;
typedef volatile signed short const vsc16;
typedef volatile signed char  const vsc8;

typedef volatile unsigned long  const vuc32;
typedef volatile unsigned short const vuc16;
typedef volatile unsigned char  const vuc8;

typedef unsigned int  uint;
typedef int  sint;

typedef volatile unsigned int  vuint;
typedef volatile int  vsint;

typedef unsigned int const  cuint;
typedef int const  csint;

typedef unsigned int size_t;
typedef unsigned long int uint32_t;

//======================================================================
// Common Enum / Defines
//======================================================================
typedef enum {
    FALSE = 0,
    TRUE  = 1
} bool;

#ifndef NULL
 #define NULL       (void*)0
#endif

#define U8_MAX     ((u8)255)
#define S8_MAX     ((s8)127)
#define S8_MIN     ((s8)-128)
#define U16_MAX    ((u16)65535u)
#define S16_MAX    ((s16)32767)
#define S16_MIN    ((s16)-32768)
#define U32_MAX    ((u32)4294967295uL)
#define S32_MAX    ((s32)2147483647)
#define S32_MIN    ((s32)-2147483648)

//======================================================================
// Common Macros
//======================================================================
#define HAL_U16_LO(a)           ((u8)((a) >> 0))
#define HAL_U16_HI(a)           ((u8)((a) >> 8))
#define HAL_TO_U16(a,b)         ((u16)((u16)(a) << 8)|(u16)(b))
#define HAL_U32_LO(a)           ((u16)((a) >> 0))
#define HAL_U32_HI(a)           ((u16)((a) >> 16))
#define U32TOU16ARGS(u)         (uint)(u)>>16,(uint)(u)&0xFFFF

//======================================================================
/// Endianness cast & conversion Macros
//======================================================================

#define U16_MSB(s)                      ((u8) (((s)>>8)&0xFF) )
#define U16_LSB(s)                      ((u8) ((s)&0xFF) )
#define U32_MSB(s)                      ((u16)(((s)>>16)&0xFFFF) )
#define U32_LSB(s)                      ((u16)((s)&0xFFFF) )
#define U16_TO_BE(n)                    (u16)((((u16)(n))>>8) | (((u16)(n))<<8))
#define U32_TO_BE(n)                    (u32)((((u32)(n) & 0xFF000000)>>24) | \
                                            (((u32)(n) & 0x00FF0000)>> 8) | \
                                            (((u32)(n) & 0x0000FF00)<< 8) | \
                                            (((u32)(n) & 0x000000FF)<<24) )

//======================================================================
// Attributes and macros
//======================================================================
#define TYPEDEF_STRUCT_PACKED  typedef struct __attribute__((packed))

#define IN_SECTION(sec)        __attribute__((section(#sec)))

#define WEAK_SYM               __attribute__((weak))

#define IN_RAM                 __attribute__((section(".data")))

//======================================================================
// hal_callback_t
//----------------------------------------------------------------------
/// @typedef hal_callback_t
/// signal : an event enumerator
/// param  : pointer to the parameter structure or buffer
/// looks like : void callback(u16 signal, u16 plen, void* param)
//======================================================================
typedef void (*hal_callback_t)(u16, void*);

#define private static
#define protected
#define public
#define global

/// @} HAL_TYPES

#endif // __HAL_TYPES_H__

