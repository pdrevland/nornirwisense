/// @copyright
/// ========================================================================={{{
/// Copyright (c) 2012-2013 WizziLab                                           /
/// All rights reserved                                                        /
///                                                                            /
/// IMPORTANT: This Software may not be modified, copied or distributed unless /
/// embedded on a WizziLab product. Other than for the foregoing purpose, this /
/// Software and/or its documentation may not be used, reproduced, copied,     /
/// prepared derivative works of, modified, performed, distributed, displayed  /
/// or sold for any purpose. For the sole purpose of embedding this Software   /
/// on a WizziLab product, copy, modification and distribution of this         /
/// Software is granted provided that the following conditions are respected:  /
///                                                                            /
/// *  Redistributions of source code must retain the above copyright notice,  /
///    this list of conditions and the following disclaimer                    /
///                                                                            /
/// *  Redistributions in binary form must reproduce the above copyright       / 
///    notice, this list of conditions and the following disclaimer in the     /
///    documentation and/or other materials provided with the distribution.    /
///                                                                            /
/// *  The name of WizziLab can not be used to endorse or promote products     /
///    derived from this software without specific prior written permission.   /
///                                                                            /
/// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS        /
/// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED  /
/// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR /
/// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR          /
/// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,      /
/// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,        /
/// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,            /
/// OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY     /
/// OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING    /
/// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS         /
/// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.               /
/// WIZZILAB HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,       /
/// ENHANCEMENTS OR MODIFICATIONS.                                             /
///                                                                            /
/// Should you have any questions regarding your right to use this Software,   /
/// contact WizziLab at www.wizzilab.com.                                      /
///                                                                            /
/// =========================================================================}}}
/// @endcopyright

#ifndef __HAL_RTC__
#define __HAL_RTC__

#include "hal_types.h"
typedef struct
{
    u8 hours;
    u8 minutes;
    u8 seconds;
} hal_rtc_time_t;

typedef struct
{
    u8 year;
    u8 month;
    u8 day;
    u8 weekday;
} hal_rtc_date_t;

typedef struct
{
    hal_rtc_date_t date;
    hal_rtc_time_t time;
} hal_rtc_datetime_t;

//======================================================================
// hal_rtc_open
//----------------------------------------------------------------------
/// @brief  Open a RTC timer
/// @param  none
/// @retval none
//======================================================================
public void hal_rtc_open();

//======================================================================
// hal_rtc_close
//----------------------------------------------------------------------
/// @brief  Close RTC
/// @param  none
/// @retval none
//======================================================================
public void hal_rtc_close();

//======================================================================
// hal_rtc_set_datetime
//----------------------------------------------------------------------
/// @brief  Initialize RTC with given datetime
/// @param  datetime
/// @retval none
//======================================================================
public void hal_rtc_set_datetime(hal_rtc_datetime_t* datetime);

//======================================================================
// hal_rtc_get_datetime
//----------------------------------------------------------------------
/// @brief  Initialize RTC with given datetime
/// @param  datetime
/// @retval none
//======================================================================
public void hal_rtc_get_datetime(hal_rtc_datetime_t* datetime);

//======================================================================
// hal_rtc_set_alarm
//----------------------------------------------------------------------
/// @brief  Set an alarm at the given datetime
/// @param  datetime : time and date of the alarm
/// @retval none
//======================================================================
public void hal_rtc_set_alarm(hal_rtc_datetime_t* datetime);


#endif // __HAL_RTC__
