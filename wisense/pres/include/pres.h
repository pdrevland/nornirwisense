/// @copyright
/// ========================================================================={{{
/// Copyright (c) 2012-2013 WizziLab                                           /
/// All rights reserved                                                        /
/// =========================================================================}}}
/// @endcopyright

//  =======================================================================
/// @file           pres.h
/// @brief          Presentation layers definition
/// @defgroup       PRES    Presentation Layer
//  =======================================================================

/// @ingroup PRES   Presentation Layer
/// @defgroup PRES_DEF       Presentation Layer Definitions
/// @{

#ifndef __PRES_H__
#define __PRES_H__

#include "hal_types.h"
#include "hal_flash.h"

//======================================================================
// Attributes and macros
//======================================================================
// Mem banks according to the file type

#define PRES_ISFB_ID(id)    (id)
#define PRES_ISFSB_ID(id)   (id)
#define PRES_GFB_ID(id)     (id)

#define PRES_ISFB_MEM_BANK      KAL_FILE_MEM_BANK_0
#define PRES_ISFSB_MEM_BANK     KAL_FILE_MEM_BANK_1
#define PRES_CUP_CFG_MEM_BANK   KAL_FILE_MEM_BANK_2
#define PRES_CUP_CODE_MEM_BANK  KAL_FILE_MEM_BANK_3

#define PRES_ISFB_IDX(file_id)        KAL_FILE_IDX(PRES_ISFB_MEM_BANK,\
                                                   PRES_ISFB_ID(file_id))
#define PRES_ISFSB_IDX(file_id)       KAL_FILE_IDX(PRES_ISFSB_MEM_BANK,\
                                                   PRES_ISFSB_ID(file_id))
#define PRES_CUP_CFG_IDX(file_id)         KAL_FILE_IDX(PRES_CUP_CFG_MEM_BANK,\
                                                   PRES_GFB_ID(file_id))
#define PRES_CUP_CODE_IDX(file_id)         KAL_FILE_IDX(PRES_CUP_CODE_MEM_BANK,\
                                                   PRES_GFB_ID(file_id))

#define PRES_FILE_IS_ISFB(file_idx)   (KAL_FILE_MEM_BANK_GET(file_idx)\
                                                    == PRES_ISFB_MEM_BANK)
#define PRES_FILE_IS_ISFSB(file_idx)  (KAL_FILE_MEM_BANK_GET(file_idx)\
                                                    == PRES_ISFSB_MEM_BANK)
#define PRES_FILE_IS_GFB(file_idx)   (KAL_FILE_MEM_BANK_GET(file_idx)\
                                                    == PRES_GFB_MEM_BANK)

// Short cuts
#define ISFSB_00(PAR)     ISFSB_##PAR##_TRANSIT_DATA
#define ISFSB_01(PAR)     ISFSB_##PAR##_CAPABILITY_DATA
#define ISFSB_02(PAR)     ISFSB_##PAR##_QUERY_RESULTS
#define ISFSB_03(PAR)     ISFSB_##PAR##_HARDWARE_FAULT
#define ISFSB_10(PAR)     ISFSB_##PAR##_DEVICE_DISCOVERY
#define ISFSB_11(PAR)     ISFSB_##PAR##_DEVICE_CAPABILITY
#define ISFSB_12(PAR)     ISFSB_##PAR##_DEVICE_CHANNEL_UTILIZATION
#define ISFSB_18(PAR)     ISFSB_##PAR##_LOCATION_DATA

#define ISFB_00(PAR)      ISFB_##PAR##_NETWORK_SETTINGS
#define ISFB_01(PAR)      ISFB_##PAR##_DEVICE_FEATURES
#define ISFB_02(PAR)      ISFB_##PAR##_CHANNEL_CONFIGURATION
#define ISFB_03(PAR)      ISFB_##PAR##_REAL_TIME_SCHEDULER
#define ISFB_04(PAR)      ISFB_##PAR##_SLEEP_SCAN_SEQUENCE
#define ISFB_05(PAR)      ISFB_##PAR##_HOLD_SCAN_SEQUENCE
#define ISFB_06(PAR)      ISFB_##PAR##_BEACON_TRANSMIT_SEQUENCE
#define ISFB_07(PAR)      ISFB_##PAR##_PROTOCOL_LIST
#define ISFB_08(PAR)      ISFB_##PAR##_ISFS_LIST
#define ISFB_09(PAR)      ISFB_##PAR##_GFB_FILE_LIST
#define ISFB_0A(PAR)      ISFB_##PAR##_LOCATION_DATA_LIST
#define ISFB_0B(PAR)      ISFB_##PAR##_IPV6_ADDRESSES
#define ISFB_0C(PAR)      ISFB_##PAR##_SENSOR_LIST
#define ISFB_0D(PAR)      ISFB_##PAR##_SENSOR_ALARMS
#define ISFB_0E(PAR)      ISFB_##PAR##_ROOT_AUTHENTICATION_KEY
#define ISFB_0F(PAR)      ISFB_##PAR##_USER_AUTHENTICATION_KEY
#define ISFB_10(PAR)      ISFB_##PAR##_ROUTING_CODE
#define ISFB_11(PAR)      ISFB_##PAR##_USER_ID
#define ISFB_12(PAR)      ISFB_##PAR##_OPTIONAL_COMMAND_LIST
#define ISFB_13(PAR)      ISFB_##PAR##_MEMORY_SIZE
#define ISFB_14(PAR)      ISFB_##PAR##_TABLE_QUERY_SIZE
#define ISFB_15(PAR)      ISFB_##PAR##_TABLE_QUERY_RESULTS
#define ISFB_16(PAR)      ISFB_##PAR##_HARDWARE_FAULT_STATUS
#define ISFB_17(PAR)      ISFB_##PAR##_EXTERNAL_EVENTS_LIST
#define ISFB_18(PAR)      ISFB_##PAR##_EXTERNAL_EVENTS_ALARM_LIST
#define ISFB_FF(PAR)      ISFB_##PAR##_APPLICATION_EXTENSION

#define FS(TYPE, PAR, ID)    TYPE##_##ID(PAR)

//=======================================================================
//  ISFSB
//=======================================================================

// IDS

#define ISFSB_ID_TRANSIT_DATA                 0x00
#define ISFSB_ID_CAPABILITY_DATA              0x01
#define ISFSB_ID_QUERY_RESULTS                0x02
#define ISFSB_ID_HARDWARE_FAULT               0x03
#define ISFSB_ID_DEVICE_DISCOVERY             0x10
#define ISFSB_ID_DEVICE_CAPABILITY            0x11
#define ISFSB_ID_DEVICE_CHANNEL_UTILIZATION   0x12
#define ISFSB_ID_LOCATION_DATA                0x18

// User ID
#define ISFSB_USER_ID(n) ((n) + ISFSB_ID_LOCATION_DATA + 1)

// Permisions
// TODO : Check these permissions
#define ISFSB_MOD_DEFAULT                     0
#define ISFSB_MOD_TRANSIT_DATA                0
#define ISFSB_MOD_CAPABILITY_DATA             0
#define ISFSB_MOD_QUERY_RESULTS               0
#define ISFSB_MOD_HARDWARE_FAULT              0
#define ISFSB_MOD_DEVICE_DISCOVERY            0
#define ISFSB_MOD_DEVICE_CAPABILITY           0
#define ISFSB_MOD_DEVICE_CHANNEL_UTILIZATION  0
#define ISFSB_MOD_LOCATION_DATA               0


//=======================================================================
//  ISFB
//=======================================================================

// IDs

#define ISFB_ID_NETWORK_SETTINGS                 0x00
#define ISFB_ID_DEVICE_FEATURES                  0x01
#define ISFB_ID_CHANNEL_CONFIGURATION            0x02
#define ISFB_ID_REAL_TIME_SCHEDULER              0x03
#define ISFB_ID_SLEEP_SCAN_SEQUENCE              0x04
#define ISFB_ID_HOLD_SCAN_SEQUENCE               0x05
#define ISFB_ID_BEACON_TRANSMIT_SEQUENCE         0x06
#define ISFB_ID_PROTOCOL_LIST                    0x07
#define ISFB_ID_ISFS_LIST                        0x08
#define ISFB_ID_GFB_FILE_LIST                    0x09
#define ISFB_ID_LOCATION_DATA_LIST               0x0A
#define ISFB_ID_IPV6_ADDRESSES                   0x0B
#define ISFB_ID_SENSOR_LIST                      0x0C
#define ISFB_ID_SENSOR_ALARMS                    0x0D
#define ISFB_ID_ROOT_AUTHENTICATION_KEY          0x0E
#define ISFB_ID_USER_AUTHENTICATION_KEY          0x0F
#define ISFB_ID_ROUTING_CODE                     0x10
#define ISFB_ID_USER_ID                          0x11
#define ISFB_ID_OPTIONAL_COMMAND_LIST            0x12
#define ISFB_ID_MEMORY_SIZE                      0x13
#define ISFB_ID_TABLE_QUERY_SIZE                 0x14
#define ISFB_ID_TABLE_QUERY_RESULTS              0x15
#define ISFB_ID_HARDWARE_FAULT_STATUS            0x16
#define ISFB_ID_EXTERNAL_EVENTS_LIST             0x17
#define ISFB_ID_EXTERNAL_EVENTS_ALARM_LIST       0x18
#define ISFB_ID_APPLICATION_EXTENSION            0xFF

// User ID
#define ISFB_USER_ID(n) ((n) + ISFB_ID_EXTERNAL_EVENTS_ALARM_LIST + 1)

// Permisions
#define ISFB_MOD_FILE_STANDARD                   0b00110100
#define ISFB_MOD_FILE_ALL                        0b00111111
#define ISFB_MOD_NETWORK_SETTINGS                ISFB_MOD_FILE_STANDARD
#define ISFB_MOD_DEVICE_FEATURES                 0b00100100
#define ISFB_MOD_CHANNEL_CONFIGURATION           ISFB_MOD_FILE_STANDARD
#define ISFB_MOD_REAL_TIME_SCHEDULER             ISFB_MOD_FILE_STANDARD
#define ISFB_MOD_SLEEP_SCAN_SEQUENCE             ISFB_MOD_FILE_STANDARD
#define ISFB_MOD_HOLD_SCAN_SEQUENCE              ISFB_MOD_FILE_STANDARD
#define ISFB_MOD_BEACON_TRANSMIT_SEQUENCE        ISFB_MOD_FILE_STANDARD
#define ISFB_MOD_PROTOCOL_LIST                   0b00100100
#define ISFB_MOD_ISFS_LIST                       0b00100100
#define ISFB_MOD_GFB_FILE_LIST                   ISFB_MOD_FILE_STANDARD
#define ISFB_MOD_LOCATION_DATA_LIST              0b00100100
#define ISFB_MOD_IPV6_ADDRESSES                  ISFB_MOD_FILE_STANDARD
#define ISFB_MOD_SENSOR_LIST                     0b00100100
#define ISFB_MOD_SENSOR_ALARMS                   0b00100100
#define ISFB_MOD_ROOT_AUTHENTICATION_KEY         0b00000000
#define ISFB_MOD_USER_AUTHENTICATION_KEY         0b00100000
#define ISFB_MOD_ROUTING_CODE                    ISFB_MOD_FILE_STANDARD
#define ISFB_MOD_USER_ID                         ISFB_MOD_FILE_STANDARD
#define ISFB_MOD_OPTIONAL_COMMAND_LIST           0b00100100
#define ISFB_MOD_MEMORY_SIZE                     0b00100100
#define ISFB_MOD_TABLE_QUERY_SIZE                0b00100100
#define ISFB_MOD_TABLE_QUERY_RESULTS             0b00100100
#define ISFB_MOD_HARDWARE_FAULT_STATUS           0b00100100
#define ISFB_MOD_EXTERNAL_EVENTS_LIST            0b00100100
#define ISFB_MOD_EXTERNAL_EVENTS_ALARM_LIST      0b00100100
#define ISFB_MOD_APPLICATION_EXTENSION           0b00100100
#define ISFB_MOD_NOTIFY                          0b10000000
#define ISFB_MOD_NACK                            0b01000000

#define PRES_FILE_ATTR_ANN_GET(attr)            (!!((attr) & 0x2))
#define PRES_FILE_ATTR_NO_RESP_GET(attr)        (!!((attr) & 0x1))
/// @} PRES_STR

// =========================================
// Headers
// =========================================

//======================================================================
// pres_pres_header_T
//----------------------------------------------------------------------
// This is the same as pres_file_header_t but in packet format
//======================================================================
// XXX
TYPEDEF_STRUCT_PACKED
{
    // address of the data of the file.
    u8* base_vaddr;
    // id of the file
    u8 id;
} pres_header_t;

extern volatile const pres_header_t header_sec[]  ;


//======================================================================
// ISFB
//======================================================================

extern u8 isfb_net_settings[];

extern u8 isfb_device_features[];

extern u8 isfb_channel_configuration[];

extern u8 isfb_sleep_scan_seq[];

extern u8 isfb_rac_calib[];

//======================================================================
// Local files
//======================================================================

extern u8 isfb_general_config[];

//======================================================================
// ISFB Header
//======================================================================
extern volatile const pres_header_t isfb_header[];

//======================================================================
// GFB Header
//======================================================================
extern volatile const pres_header_t gfb_header[];

// =========================================
// ISFSB FILES
// =========================================

extern u8 pres_isfsb_file_transit_data[];

extern u8 pres_isfsb_file_capability_data[];

extern u8 pres_isfsb_file_query_results[];

extern u8 pres_isfsb_file_hardware_fault[];

extern u8 pres_isfsb_file_device_discovery[];

extern u8 pres_isfsb_file_device_capability[];
#define ISFB_DEVICE_FEATURES_VERSION 32
#define ISFB_DEVICE_FEATURES_ALLOC_SIZE (256-SUB_HEADER_SIZE)

extern u8 pres_isfsb_file_device_channel_utilization[] ;

extern u8 pres_isfsb_file_location_data[];

// =========================================
// ISFSB Header
// =========================================

extern volatile const pres_header_t isfsb_header[];


#endif // __PRES_H__
