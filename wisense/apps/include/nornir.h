// @copyright
/// ========================================================================={{{
/// Copyright (c) 2013-2014 WizziLab                                           /
/// All rights reserved                                                        /
/// =========================================================================}}}
/// @endcopyright

//  =======================================================================
/// @file           nornir.h
/// @brief          NORNIR public header 
//  =======================================================================

#ifndef __NORNIR_H__
#define __NORNIR_H__


//======================================================================
// Software versions
//======================================================================
// Format idx, DD, MM, YY
#define WISENSE_VERSION                     1,29,7,14
#define WM_WISENSE_VERSION                  1,29,7,14
#define GW_VERSION                     	    1,29,7,14

//======================================================================
// Channel & band used for wireless communication
//======================================================================
// XXX: Be careful whe changing the config channel. It may change
// the BG duration in Scan Sleep file. If this value changes, an update
// is necessary in the Lua script to restore settings at the end of CUP
#define D7_CONFIG_CHANNEL              (0x90)
#define D7_REPORT_CHANNEL              (0x94)
#define D7_REPORT_CHANNEL2             (0x9C)
#define D7_CALIB_CHANNEL               (0x98)
#define D7_SNIFFER_CHANNEL             (0x94)
#define D7_ISM_BAND                    D7A_CHANNEL_BAND_433

//======================================================================
// Wireless Power Levels
//======================================================================
#define D7_GW_TX_EIRP                 (13)
#define D7_GW_TX_CCA                  (80)
#define D7_GW_RX_CCA                  (105)

#define D7_SENSOR_TX_EIRP              (13)
#define D7_SENSOR_TX_CCA              (80)
#define D7_SENSOR_RX_CCA              (100)


//======================================================================
// Subnets
//======================================================================
#define D7_SENSOR_SUBNET              (0xD4)
#define D7_GW_SUBNET                  (0x7B)

//======================================================================
// Scan & advertising period
//======================================================================
#define D7_SCAN_PERIOD                 (1000)
#define D7_ADV_PERIOD                  ((D7_SCAN_PERIOD * 11) / 10)

//======================================================================
// Retransmission policy
//======================================================================
// Number of times the mote will retransmit a packet
#define D7_NOTIF_RETRIES               (3)
// Time in s before the application retries a failed report
#define D7_RETRY_DELAY                 (20)

//======================================================================
// Response Timeout
//======================================================================
#define D7_SENSOR_RESPONSE_TIMEOUT     (30)
#define D7_GW_RESPONSE_TIMEOUT         (1000)
#define D7_SENSOR_TRANSMIT_TIMEOUT     (500)
#define D7_GW_TRANSMIT_TIMEOUT         (500)

//======================================================================
// Radio Calibration configuration
//======================================================================
#define D7_CALIB_SUBNET                (0xca)
#define D7_CALIB_SKIP_COUNT            (10)
#define D7_CALIB_AGC_AVERAGE           (10)
#define D7_CALIB_FOF_AVERAGE           (20)
#define D7_CALIB_PREAMBLE              (255)

//======================================================================
// Margin on file allocation (bytes)
//======================================================================
#define FILE_ALLOC_MARGIN                (8)

//======================================================================
// General purpose macros
//======================================================================
#define FS_DEPOSIT_16(v)               (((v)>>0)&0xff),(((v)>>8)&0xff)
#define FS_DEPOSIT_32(v)               (((v)>>0)&0xff),(((v)>>8)&0xff),(((v)>>16)&0xff),(((v)>>24)&0xff)

//======================================================================
/// Sensor configuration file (resides on the sensor)
//======================================================================

#define SENSOR_CFG_FILE_ID       PRES_ISFB_ID(0xC0)
#define SENSOR_CFG_FILE_SIZE     ((u16) sizeof(sensor_cfg_t))

/// Sensor detection configuration structure
TYPEDEF_STRUCT_PACKED
{
    u16 meas_period;
    u8 report_type;
    u16 low_threshold;
    u16 high_threshold;
} sens_cfg_t;

TYPEDEF_STRUCT_PACKED
{
    //------------------------
    // Retry strategy
    //------------------------
    // Number of times a cmd is sent to mote
    u8  max_cmd_retries;
    // Number of times a cmd is sent to mote after resetting it
    u8  max_retries_after_rst;
    // Number of times a report is sent if it is not acknowledged
    u8  max_report_retries;
    // Delay between to retries in case message not acknowledged
    u8  retry_delay;
    //------------------------
    // Sensors configuration
    //------------------------
    // Button configuration
    u16 button_debounce_time;
    // Photo sensor configuration
    sens_cfg_t photo_cfg;
    // Accel sensor configuration
    sens_cfg_t accel_cfg;
    // Temp sensor configuration
    sens_cfg_t temp_cfg;
    // Humidity sensor configuration
    sens_cfg_t hum_cfg;
    // Baro sensor configuration
    sens_cfg_t baro_cfg;

} sensor_cfg_t;

/// Maximum period between two reports (minutes, range 1 to 1439)
#define MAX_REPORT_PERIOD               (5)

/// Max serial retries in normal operation
#define MAX_CMD_RETRIES              (4)

// Max serial retries after reset
#define MAX_CMD_RETRIES_AFTER_RST    (2)

// Debounce time for buttons in ms
#define BUTTON_DEBOUNCE_TIME (500)

// Number of messages retries
#define D7_MSG_RETRIES    15

typedef enum
{
    RPT_ALWAYS =0,
    RPT_INSIDE_RANGE,
    RPT_OUTSIDE_RANGE,
    RPT_BOUNDARY_CROSSING,
    RPT_HYSTERESIS,
    RPT_CHANGE_ABOVE_LOW_THRESHOLD,
} report_trigger_type_t;

// Photo sensor configuration
#define PHOTO_MEAS_PERIOD            (5000)
#define PHOTO_REPORT_TYPE            (RPT_OUTSIDE_RANGE)
#define PHOTO_HIGH_THRESHOLD         (2500)
#define PHOTO_LOW_THRESHOLD          (500)

// Accelerometer sensor configuration
#define ACCEL_MEAS_PERIOD            (1000)
#define ACCEL_REPORT_TYPE            (RPT_OUTSIDE_RANGE)
#define ACCEL_HIGH_THRESHOLD         (40)
#define ACCEL_LOW_THRESHOLD          (0)

// Humidity sensor configuration
#define HUM_MEAS_PERIOD            (60000)
#define HUM_REPORT_TYPE            (RPT_HYSTERESIS)
#define HUM_HIGH_THRESHOLD         (40)
#define HUM_LOW_THRESHOLD          (20)

// Temperature sensor configuration
#define TEMP_MEAS_PERIOD            (60000)
#define TEMP_REPORT_TYPE            (RPT_CHANGE_ABOVE_LOW_THRESHOLD)
#define TEMP_HIGH_THRESHOLD         (20)
#define TEMP_LOW_THRESHOLD          (1)

// Barometer sensor configuration
#define BARO_MEAS_PERIOD            (30000)
#define BARO_REPORT_TYPE            (RPT_CHANGE_ABOVE_LOW_THRESHOLD)
#define BARO_HIGH_THRESHOLD         (40)
#define BARO_LOW_THRESHOLD          (2)

//======================================================================
/// Sensor led file (resides on the sensor)
//======================================================================

#define SENSOR_LED_FILE_ID          PRES_ISFB_ID(0xD1)
#define SENSOR_LED_FILE_SIZE        (2)

// Initial LED State
#define LED1_INIT_STATE 0
#define LED2_INIT_STATE 0

//======================================================================
/// Sensor dac file (resides on the sensor)
//======================================================================

#define SENSOR_DAC_FILE_ID          PRES_ISFB_ID(0xD2)
#define SENSOR_DAC_FILE_SIZE        (4)

#define DAC_OFF 0xFFFF
// Initial DAC State
#define DAC1_INIT_STATE DAC_OFF
#define DAC2_INIT_STATE DAC_OFF

//======================================================================
/// Sensor scratch file (resides on the sensor)
//======================================================================

#define SENSOR_SCRATCH_FILE_ID          PRES_ISFB_ID(0xC3)
#define SENSOR_SCRATCH_FILE_SIZE        (128)


//======================================================================
// Sensor data file
//======================================================================
// Resides on the wm1001
typedef enum {
    BTN1_DATA = 0,
    BTN2_DATA,
    TEMP_DATA,
    HUM_DATA,
    BARO_DATA,
    PHOTO_DATA,
    ACCEL_DATA,
    NB_REPORT_TYPES
} report_idx_t;


#define REPORT_FILE(n)                (0xE0+n)
#define BTN1_DATA_FILE_ID             REPORT_FILE(BTN1_DATA)
#define BTN2_DATA_FILE_ID             REPORT_FILE(BTN2_DATA)
#define TEMP_DATA_FILE_ID             REPORT_FILE(TEMP_DATA)
#define HUM_DATA_FILE_ID              REPORT_FILE(HUM_DATA)
#define BARO_DATA_FILE_ID             REPORT_FILE(BARO_DATA)
#define PHOTO_DATA_FILE_ID            REPORT_FILE(PHOTO_DATA)
#define ACCEL_DATA_FILE_ID            REPORT_FILE(ACCEL_DATA)

/// Button data (status) structure
#define BTN_OFF     0
#define BTN_ON      1
#define BTN_PUSH    2

TYPEDEF_STRUCT_PACKED
{
    u8  seqnum;
    u8  retry;
    u8  service_id[3];
    u8  status;
} btn_data_t;

#define BTN_DATA_FILE_SIZE           ((u16) sizeof(btn_data_t))

/// Photo data structure
TYPEDEF_STRUCT_PACKED
{
    u8  seqnum;
    u8  retry;
    u8  service_id[3];
    u16 val;
} photo_data_t;

#define PHOTO_DATA_FILE_SIZE           ((u16) sizeof(photo_data_t))

/// Temp data structure
TYPEDEF_STRUCT_PACKED
{
    u8  seqnum;
    u8  retry;
    u8  service_id[3];
    s8 val;
} temp_data_t;

#define TEMP_DATA_FILE_SIZE           ((u16) sizeof(temp_data_t))

// Humidity data structure
TYPEDEF_STRUCT_PACKED
{
    u8  seqnum;
    u8  retry;
    u8  service_id[3];
    u8 val;
} hum_data_t;

#define HUM_DATA_FILE_SIZE           ((u16) sizeof(hum_data_t))

// Accelerometer data structure
TYPEDEF_STRUCT_PACKED
{
    u8  seqnum;
    u8  retry;
    u8  service_id[3];
    s16 x;
    s16 y;
    s16 z;
} accel_data_t;

#define ACCEL_DATA_FILE_SIZE           ((u16) sizeof(accel_data_t))

// Baro data structure
TYPEDEF_STRUCT_PACKED
{
    u8  seqnum;
    u8  retry;
    u8  service_id[3];
    u16 val;
} baro_data_t;

#define BARO_DATA_FILE_SIZE           ((u16) sizeof(baro_data_t))

#define SENSOR_DATA_MAX_FILE_SIZE 16
//======================================================================
// Sensor remote control file
//======================================================================

// Resides on the wm1001
#define SENSOR_CTRL_FILE_ID         ISFB_USER_ID(4)
#define SENSOR_CTRL_FILE_SIZE       ((u16) sizeof(sensor_ctrl_t))

/// Sensor remote control structure
TYPEDEF_STRUCT_PACKED
{
    u8  reset;
} sensor_ctrl_t;


//======================================================================
// Revision files: contain the code version and the crc of the file system
//======================================================================
#define REVISION_WM_FILE_ID             ISFB_USER_ID(6)

TYPEDEF_STRUCT_PACKED
{
    u8 code_version[4];
    u32 fs_crc;
} revision_t;

#define RESTART_REPORT_FILE_ID            ISFB_USER_ID(7)

TYPEDEF_STRUCT_PACKED
{
    u8  status;
    u8  seqnum;
    revision_t rev;
} restart_report_t;

//======================================================================
// Remote files config id : contans list of files to be opened at boot
//======================================================================

// Resides on the wm1001
#define REMOTE_FILES_CFG_ID             ISFB_USER_ID(15)
#define REMOTE_FILES_CFG_SIZE           ((u16) sizeof(remote_files_cfg_t))

/// Remote files config structure
typedef struct
{
    u8 retry_nb;
    u8 idx[16];

} remote_files_cfg_t;


#endif //__NORNIR_H__

/// @}
// vim:fdm=marker:fdc=2
