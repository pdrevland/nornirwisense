/// @copyright
/// ========================================================================={{{
/// Copyright (c) 2012-2013 WizziLab                                           /
/// All rights reserved                                                        /
///                                                                            /
/// IMPORTANT: This Software may not be modified, copied or distributed unless /
/// embedded on a WizziLab product. Other than for the foregoing purpose, this /
/// Software and/or its documentation may not be used, reproduced, copied,     /
/// prepared derivative works of, modified, performed, distributed, displayed  /
/// or sold for any purpose. For the sole purpose of embedding this Software   /
/// on a WizziLab product, copy, modification and distribution of this         /
/// Software is granted provided that the following conditions are respected:  /
///                                                                            /
/// *  Redistributions of source code must retain the above copyright notice,  /
///    this list of conditions and the following disclaimer                    /
///                                                                            /
/// *  Redistributions in binary form must reproduce the above copyright       /
///    notice, this list of conditions and the following disclaimer in the     /
///    documentation and/or other materials provided with the distribution.    /
///                                                                            /
/// *  The name of WizziLab can not be used to endorse or promote products     /
///    derived from this software without specific prior written permission.   /
///                                                                            /
/// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS        /
/// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED  /
/// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR /
/// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR          /
/// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,      /
/// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,        /
/// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,            /
/// OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY     /
/// OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING    /
/// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS         /
/// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.               /
/// WIZZILAB HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,       /
/// ENHANCEMENTS OR MODIFICATIONS.                                             /
///                                                                            /
/// Should you have any questions regarding your right to use this Software,   /
/// contact WizziLab at www.wizzilab.com.                                      /
///                                                                            /
/// =========================================================================}}}
/// @endcopyright

#ifndef __APPS_H__
#define __APPS_H__

// Elementary commands
#define PING_CMD                            'P'
#define PONG_CMD                            'Q'
#define KEY_CMD                             'K'
#define LED_CMD                             'L'
#define TEMP_CMD                            'T'
#define LIGHT_CMD	                        'W'
#define ACCX_CMD	                        'X'
#define ACCY_CMD	                        'Y'
#define ACCZ_CMD	                        'Z'
#define PROX_CMD	                        'P'
#define ACC_CMD		                        'A'
#define MAG_CMD		                        'M'
#define BLER_CMD                            'B'
#define BUZZER_CMD                          'U'

#define PING_CHANNEL                        (0x14)
#define PING_ADV_TIME                       (500)
#define SENSOR_TEMP_REPORT_PERIOD           (5000)


/// Default Master TX RAC configuration
#define RAC_DEFAULT_MASTER_TX_CONFIG (rac_tx_cfg_t) \
{                                                   \
    .start      = RAC_TIME_NOW,                     \
    .timeout    = 100,                              \
    .adv_time   = PING_ADV_TIME,                    \
    .cca        = 80,                               \
    .csma       = D7A_CSMA_RIGD,                    \
    .channel    = PING_CHANNEL,                     \
    .subnet     = 0xff,                             \
    .eirp       = 0,                                \
    .pload      = NULL,                             \
}


/// Default Slave TX RAC configuration
#define RAC_DEFAULT_SLAVE_TX_CONFIG (rac_tx_cfg_t)  \
{                                                   \
    .start      = RAC_TIME_NOW,                     \
    .timeout    = 100,                              \
    .adv_time   = 0,                                \
    .cca        = 80,                               \
    .csma       = D7A_CSMA_RIGD,                    \
    .channel    = PING_CHANNEL,                     \
    .subnet     = 0xff,                             \
    .eirp       = 0,                                \
    .pload      = NULL,                             \
}


/// Default Master RX RAC configuration
#define RAC_DEFAULT_MASTER_RX_CONFIG (rac_rx_cfg_t) \
{                                                   \
    .start      = RAC_TIME_NOW,                     \
    .timeout    = RAC_RX_NO_TIMEOUT,                \
    .multiple   = RAC_RX_UNLMTD_PKT,                \
    .channel    = PING_CHANNEL,                     \
    .class_id   = D7A_MAC_FG_ID,                    \
    .rssi_min   = 100,                              \
    .subnet     = 0xff                              \
}


/// Default Slave RX RAC configuration
#define RAC_DEFAULT_SLAVE_RX_CONFIG (rac_rx_cfg_t)  \
{                                                   \
    .start      = RAC_TIME_NOW,                     \
    .timeout    = 6,                                \
    .multiple   = 1,                                \
    .channel    = PING_CHANNEL,                     \
    .class_id   = D7A_MAC_BG_ID,                    \
    .rssi_min   = 80,                               \
    .subnet     = 0xff                              \
}


/// Default Master TX MAC configuration
#define MAC_DEFAULT_MASTER_TX_CONFIG (mac_tx_cfg_t) \
{                                                   \
    .timeout        = 100,                          \
    .adv_time       = PING_ADV_TIME,                \
    .csma           = D7A_CSMA_RIGD,                \
    .channel        = PING_CHANNEL,                 \
    .addr           = NULL,                         \
    .subnet         = 0xff,                         \
    .ftype          = D7A_FTYPE_DIALOG,             \
    .nb             = 1,                            \
}


/// Default Slave TX MAC configuration
#define MAC_DEFAULT_SLAVE_TX_CONFIG (mac_tx_cfg_t)  \
{                                                   \
    .timeout        = 100,                          \
    .adv_time       = 0,                            \
    .csma           = D7A_CSMA_RIGD,                \
    .channel        = PING_CHANNEL,                 \
    .addr           = NULL,                         \
    .subnet         = 0xff,                         \
    .ftype          = D7A_FTYPE_DIALOG,             \
    .nb             = 1,                            \
}

#endif // __APPS_H__

