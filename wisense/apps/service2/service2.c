/// @copyright
/// ========================================================================={{{
/// Copyright (c) 2013-2014 WizziLab                                           /
/// All rights reserved                                                        /
/// =========================================================================}}}
/// @endcopyright

//  =======================================================================
/// @file           service2.c
/// @brief          Service2 
//  =======================================================================

#include "hal.h"
#include "kal.h"
#include "alp.h"
#include "alp_cmd.h"
#include "baro_mpl115.h"
#include "accel_mma8652.h"
#include "temp_rh_si7020_drv.h"
#include "service2.h"
#include "cup.h"

#define CODE_VERSION WISENSE_VERSION

// Inform the code about its own code version
CUP_SET_CODE_VERSION({CODE_VERSION});

#define REBOOT_ON_ERROR


//======================================================================
//  Error Handler
//======================================================================
private void error_handler(uint status)
{
    (void) status;
    DPRINT("Error Handler Called\n");
    #ifdef REBOOT_ON_ERROR

    hal_software_reset(0);
    #else
      hal_use_resource(HAL_RESOURCE_FULL);    
    #endif
}


//======================================================================
// Constant timeouts in ms
//======================================================================
// ACK timeout over the serial
#define ALP_ACK_TIMEOUT         (100)

// Reset toggle duration
#define MOTE_RST_TIMEOUT        (10)

// Minimal boot time of the mote
#define MOTE_BOOT_TIMEOUT       (1000)

// That one depends on the mote retry strategy
#define ALP_NTF_TIMEOUT         ((D7_NOTIF_RETRIES + 1) * (D7_SENSOR_TRANSMIT_TIMEOUT + D7_SENSOR_RESPONSE_TIMEOUT) + ALP_ACK_TIMEOUT)

#define ABS(a)                  (((a) > 0) ? (a) : (-(a)))

//======================================================================
// Global Variables
//======================================================================
sensor_state_t      g_sensor_state;
sensor_cfg_t        g_sensor_cfg; 

u8 g_temp;
u8 g_hum;

u8                  g_alp_tx_buf[SENSOR_DATA_MAX_FILE_SIZE+ALP_REC_HEADER_LEN+5];


//======================================================================
// Processes
//======================================================================
KAL_PROCESS(wisense_p, APP_PROC_SENSOR);
KAL_PROCESS(wisense_report_p, APP_PROC_SENSOR);

//======================================================================
/// Sensor configuration file (resides on the sensor)
//======================================================================
u8 wisense_cfg[SENSOR_CFG_FILE_SIZE+SUB_HEADER_SIZE] IN_SECTION(.pres_heap) = \
{
    // Sub-header File id
    FS_SUB_HEADER(SENSOR_CFG_FILE_SIZE, SENSOR_CFG_FILE_SIZE, ISFB_MOD_FILE_STANDARD),
    // cmd retries in normal operation
    MAX_CMD_RETRIES,
    // serial retries after reset
    MAX_CMD_RETRIES_AFTER_RST,
    // Messages retries 
    D7_MSG_RETRIES,
    // Nack period
    D7_RETRY_DELAY,
    // Sensor Configurations
    // Button debounce
    FS_DEPOSIT_16(BUTTON_DEBOUNCE_TIME),
    // Light configuration
    FS_DEPOSIT_16(PHOTO_MEAS_PERIOD),
    PHOTO_REPORT_TYPE,
    FS_DEPOSIT_16(PHOTO_LOW_THRESHOLD),
    FS_DEPOSIT_16(PHOTO_HIGH_THRESHOLD),
    // Accel configuration
    FS_DEPOSIT_16(ACCEL_MEAS_PERIOD),
    ACCEL_REPORT_TYPE,
    FS_DEPOSIT_16(ACCEL_LOW_THRESHOLD),
    FS_DEPOSIT_16(ACCEL_HIGH_THRESHOLD),
    // Temperature sensor configuration
    FS_DEPOSIT_16(TEMP_MEAS_PERIOD),
    TEMP_REPORT_TYPE,
    FS_DEPOSIT_16(TEMP_LOW_THRESHOLD),
    FS_DEPOSIT_16(TEMP_HIGH_THRESHOLD),
    // Humidity sensor configuration
    FS_DEPOSIT_16(HUM_MEAS_PERIOD),
    HUM_REPORT_TYPE,
    FS_DEPOSIT_16(HUM_LOW_THRESHOLD),
    FS_DEPOSIT_16(HUM_HIGH_THRESHOLD),
    // Barometer sensor configuration
    FS_DEPOSIT_16(BARO_MEAS_PERIOD),
    BARO_REPORT_TYPE,
    FS_DEPOSIT_16(BARO_LOW_THRESHOLD),
    FS_DEPOSIT_16(BARO_HIGH_THRESHOLD),
};

//======================================================================
/// WISENSE led file (resides on the sensor)
//======================================================================
u8 wisense_led[SENSOR_LED_FILE_SIZE+SUB_HEADER_SIZE] IN_SECTION(.pres_heap) = \
{
    // Sub-header File id
    FS_SUB_HEADER(SENSOR_LED_FILE_SIZE, SENSOR_LED_FILE_SIZE, ISFB_MOD_FILE_STANDARD),
    // LED1 initial State
    LED1_INIT_STATE,
    // LED2 initial State
    LED2_INIT_STATE
};

//======================================================================
/// WISENSE dac file (resides on the sensor)
//======================================================================
u8 wisense_dac[SENSOR_DAC_FILE_SIZE+SUB_HEADER_SIZE] IN_SECTION(.pres_heap) = \
{
    // Sub-header File id
    FS_SUB_HEADER(SENSOR_DAC_FILE_SIZE, SENSOR_DAC_FILE_SIZE, ISFB_MOD_FILE_STANDARD),
    // DAC1 initial State
    FS_DEPOSIT_16(DAC1_INIT_STATE),
    // DAC2 initial State
    FS_DEPOSIT_16(DAC2_INIT_STATE)
};

//======================================================================
/// WISENSE scratch file (resides on the sensor)
//======================================================================
u8 wisense_scratch[SENSOR_SCRATCH_FILE_SIZE+SUB_HEADER_SIZE] IN_SECTION(.pres_heap) = \
{
    // Sub-header File id
    FS_SUB_HEADER(SENSOR_SCRATCH_FILE_SIZE, 0, ISFB_MOD_FILE_STANDARD)
};

//======================================================================
/// Device features
//======================================================================
u8 isfb_device_features[36+SUB_HEADER_SIZE] IN_SECTION(.pres_heap) = \
{
    FS_SUB_HEADER(36, 36, ISFB_MOD_DEVICE_FEATURES),
    // File
    // UID XXX
    0xde, 0xad, 0xbe, 0xef, 0x8b, 0xad, 0xf0, 0x0d,
    // Supported settings
    0, 0, // TODO
    // Max Frame length,
    250,
    // Max frame per packet
    1,
    // DLLS Available Methods
    0,0,
    // NLS Available Methods
    0,0,
    // Total ISFB Memory
    0,0,
    // Available ISFB Memory
    0,0,
    // Total ISFSB File Memory
    0,0,
    // Available ISFSB File Memory
    0,0,
    // Total GFB Memory
    0,0,
    // Available GFB Memory
    0,0,
    // GFB File Size
    0,0,
    // RFU
    0,
    // Session Stack Depth
    0,
    // Firmware Version IDX D, M, Y
    WISENSE_VERSION
};


//======================================================================
/// WISENSE files map
//======================================================================
volatile const pres_header_t isfb_header[]   IN_SECTION(.fs_header_0) = \
{
    { isfb_device_features,         PRES_ISFB_ID(0x01) },
    { wisense_cfg,                  SENSOR_CFG_FILE_ID },
    { wisense_led,                  SENSOR_LED_FILE_ID },
    { wisense_dac,                  SENSOR_DAC_FILE_ID },
    { wisense_scratch,              SENSOR_SCRATCH_FILE_ID },
};

//======================================================================
//  Packet Seqnum Management
//======================================================================
private u8 next_seqnum(u8 current_seqnum)
{
    u8 next_seqnum;

    if (current_seqnum>=254)
    {
        // Skip 255 - reserved to indicate restart of sequence
        next_seqnum = 0;
    }
    else
    {
        next_seqnum = current_seqnum+1;
    }
    return next_seqnum;
}

private u8 init_seqnum()
{
    return 255;
}

//======================================================================
//  Service ID
//======================================================================
// ---------------------------------
// Service ID Init
// ---------------------------------

private void init_service_id(u8* ptr)
{
    ptr[0]=(RTW_SERVICE >>16)&0xFF;
    ptr[1]=(RTW_SERVICE >>8)&0xFF;
    ptr[2]=(RTW_SERVICE >>0)&0xFF;

}



//======================================================================
//  Button Management
//======================================================================
// ---------------------------------
// Button Debounce
// ---------------------------------
private void button_debounce(u16 sw)
{
    if (sw == HAL_GPIO_SW1)
    {
        if (hal_gpio_read(HAL_GPIO_SW1)==0)
        {
            kal_proc_post_evt(&wisense_p, HAL_EVT_BUTTON1, (evt_param_t)0);
        }
        hal_gpio_irq_en(HAL_GPIO_SW1);
    }

    if (sw == HAL_GPIO_SW2)
    {
        if (hal_gpio_read(HAL_GPIO_SW2)==0)
        {
            kal_proc_post_evt(&wisense_p, HAL_EVT_BUTTON2, (evt_param_t)0);
        }
        hal_gpio_irq_en(HAL_GPIO_SW2);
    }
}

// ---------------------------------
// Button IRQ
// ---------------------------------
private void button_isr(uint status)
{
    (void) status;

    if (hal_gpio_read(HAL_GPIO_SW1)==0)
    {
        hal_gpio_irq_dis(HAL_GPIO_SW1);
        hal_gpio_irq_clr(HAL_GPIO_SW1);
        kal_timer_start(&g_sensor_state.button1_tim, KAL_FTIMER, button_debounce, HAL_GPIO_SW1, g_sensor_cfg.button_debounce_time);
    }

    if (hal_gpio_read(HAL_GPIO_SW2)==0)
    {
        hal_gpio_irq_dis(HAL_GPIO_SW2);
        hal_gpio_irq_clr(HAL_GPIO_SW2);
        kal_timer_start(&g_sensor_state.button2_tim, KAL_FTIMER, button_debounce, HAL_GPIO_SW2, g_sensor_cfg.button_debounce_time);
    }
}

// ---------------------------------
// Button Open
// ---------------------------------
private void button_open()
{
    // Init Button timers
    kal_timer_init(&g_sensor_state.button1_tim);
    kal_timer_init(&g_sensor_state.button2_tim);

    // Button IO configuration - Detect falling adge (button pressed)
    hal_gpio_irq_set(HAL_GPIO_SW1,GPIO_IRQ_FALL,button_isr);
    hal_gpio_irq_set(HAL_GPIO_SW2,GPIO_IRQ_FALL,button_isr);
    hal_gpio_irq_en(HAL_GPIO_SW1);
    hal_gpio_irq_en(HAL_GPIO_SW2);

}

#if 0
// ---------------------------------
// Button Close
// ---------------------------------
private void button_close()
{
    hal_gpio_irq_dis(HAL_GPIO_SW1);
    hal_gpio_irq_dis(HAL_GPIO_SW2);
}
#endif 

//======================================================================
//  DAC Management
//======================================================================
// ---------------------------------------
// DAC Update State
// ---------------------------------------
private void dac_update()
{
    u8 i;
    u16 old_dac[2];
    u16 dac_status_before = DAC_OFF;
    u16 dac_status_after = DAC_OFF;
    
    // Save current State and check if DAC enabled
    for (i=0;i<SENSOR_DAC_FILE_SIZE/2;i++)
    {
        old_dac[i] = g_sensor_state.dac_state[i];
        dac_status_before &= old_dac[i];
    }

    // Read new state 
    kal_dbg_assert(kal_file_seek(g_sensor_state.dac_fd, 0, KAL_FILE_WHENCE_SET) == KAL_FILE_ERR_NO, "ERR SEEK DAC");

    kal_dbg_assert(kal_file_read(g_sensor_state.dac_fd, (u8*) g_sensor_state.dac_state, SENSOR_DAC_FILE_SIZE) == SENSOR_DAC_FILE_SIZE, "ERR RD DAC");

    for (i=0;i<SENSOR_DAC_FILE_SIZE/2;i++)
    {
         dac_status_after &= g_sensor_state.dac_state[i];
    }

    DPRINT("DAC Status Before:%x After:%x\n",dac_status_before,dac_status_after);
    if ((dac_status_after!=DAC_OFF) && (dac_status_before == DAC_OFF))
    {
        // Turn DAC ON
        DPRINT("TurnOn DAC\n");
        hal_dac_open();
    }

    if (dac_status_after==DAC_OFF && dac_status_before != DAC_OFF)
    {
        // Close DAC
        DPRINT("TurnOff DAC\n");
        hal_dac_close();
    }

    // Update DAC values when needed
    if (dac_status_after!=DAC_OFF)
    {
        for (i=0;i<SENSOR_DAC_FILE_SIZE/2;i++)
        {
            if (g_sensor_state.dac_state[i]!=DAC_OFF)
            {
                DPRINT("Set DAC %d to %d\n",HAL_DAC1+i,g_sensor_state.dac_state[i]);
                hal_dac_setup(HAL_DAC1+i, HAL_DAC_MODE_NORMAL | HAL_DAC_MODE_OUTBUF_EN, 0);
                hal_dac_set_value(HAL_DAC1+i, g_sensor_state.dac_state[i]);
                hal_dac_enable(HAL_DAC1+i, TRUE);
            }
            else
            {
                DPRINT("Disable DAC %d\n",HAL_DAC1+i);
                hal_dac_enable(HAL_DAC1+i, FALSE);
            }
        }
    }
}

//======================================================================
//  LED Management
//======================================================================
// ---------------------------------------
// LED Update State
// ---------------------------------------
private void led_update()
{
    u8 i;

    // Read state 
    kal_dbg_assert(kal_file_seek(g_sensor_state.led_fd, 0, KAL_FILE_WHENCE_SET) == KAL_FILE_ERR_NO, "ERR SEEK LED");

    kal_dbg_assert(kal_file_read(g_sensor_state.led_fd, g_sensor_state.led_state, SENSOR_LED_FILE_SIZE) == SENSOR_LED_FILE_SIZE, "ERR RD LED");

    DPRINT("LED Update %d %d\n",g_sensor_state.led_state[0],g_sensor_state.led_state[1]);
    for (i=0;i<SENSOR_LED_FILE_SIZE;i++)
    {
        if (g_sensor_state.led_state[i])
        {
            hal_led_on(i);
        }
        else
        {
            hal_led_off(i);
        }
    }

}

//======================================================================
//  Humidity Sensor Management
//======================================================================
//======================================================================
//  humidity_measure
private void humidity_measure(u16 status)
{
    (void) status;

    si7020_rd_rh_temp_nhmm( &(g_hum), &(g_temp) );
}

//======================================================================
//  Light Sensor Management
//======================================================================
//======================================================================
//  light_adc_measure
//----------------------------------------------------------------------
/// @brief  Light Sensor Measurement
//======================================================================
private void light_adc_measure(u16 action)
{
    if (action == PHOTO_TURNON)
    {
        // Start a delayed measurement
        hal_gpio_set(HAL_GPIO_PHOTO_PWR);
        kal_timer_start(&g_sensor_state.light_tim, KAL_FTIMER, &light_adc_measure, PHOTO_MEAS, PHOTO_SETUP_TIME);
    }
    else if (action == PHOTO_MEAS)
    {
        // Monitor light intensity
        hal_adc_single_run(ADC_CH_11, &wisense_p);
    }
}

//======================================================================
//  Report Management
//======================================================================
private void retry_report(u16 report_idx)
{
    g_sensor_state.report[report_idx].report_status=REPORT_DELAYED;
    g_sensor_state.report[report_idx].retry_left--;
    ((generic_report_t*) g_sensor_state.report[report_idx].file_data)->retry++;
    DPRINT("Retry RPT %d, retry %d retry_left %d\n",report_idx,((generic_report_t*) g_sensor_state.report[report_idx].file_data)->retry, g_sensor_state.report[report_idx].retry_left);
    kal_proc_post_evt(&wisense_report_p, APP_EVT_REPORT, (evt_param_t)((int)report_idx));
}

private void send_report(u16 report_idx)
{
    // If a report of the same type has been delayed, kill it and replace it by this new one
    if (g_sensor_state.report[report_idx].report_status==REPORT_DELAYED)
    {
        // Kill delayed report
        kal_timer_stop(&g_sensor_state.report[report_idx].retry_tim);
    }
    // Prepare new report
    // This is safe to use global variables here as the rport are sent from a task and used in a task.
    // There is no risk of inconsistent report
    g_sensor_state.report[report_idx].retry_left=g_sensor_cfg.max_report_retries;
    ((generic_report_t*) g_sensor_state.report[report_idx].file_data)->retry=0;
    ((generic_report_t*) g_sensor_state.report[report_idx].file_data)->seqnum=
        next_seqnum(((generic_report_t*) g_sensor_state.report[report_idx].file_data)->seqnum);

    if (g_sensor_state.report[report_idx].report_status==REPORT_PENDING)
    {
        // A report of the same type has been sent but not yet treated by the reporting task
        // Do not send an event - the content will just be updated
        DPRINT("Update Report %d SN:%d RT:%d\n",report_idx,((generic_report_t*) 
            g_sensor_state.report[report_idx].file_data)->seqnum, 
            ((generic_report_t*) g_sensor_state.report[report_idx].file_data)->retry);
    }
    else
    {
        g_sensor_state.report[report_idx].report_status=REPORT_PENDING;
        DPRINT("Send Report %d SN:%d RT:%d\n",report_idx,
        ((generic_report_t*) g_sensor_state.report[report_idx].file_data)->seqnum, 
        ((generic_report_t*) g_sensor_state.report[report_idx].file_data)->retry);
        kal_proc_post_evt(&wisense_report_p, APP_EVT_REPORT, (evt_param_t)((int)report_idx));
    }
}


//---------------------------------------------
// Generate timeout event for ALP cmd
//---------------------------------------------
void send_alp_rxtimeout(u16 param)
{
    kal_proc_post_evt(&wisense_report_p, ALP_IFACE_EVT_RX_TIMEOUT, (evt_param_t)(int)param);
}

//======================================================================
//  report process
//======================================================================
KAL_PROC_THREAD(wisense_report_p, ev, evp)
{
    u8 test;
    static u32 count;

    alp_iface_rx_msg_t *iface_msg = (alp_iface_rx_msg_t*)evp.msg;
    alp_rec_t *rec = iface_msg ? (alp_rec_t*)iface_msg->data : NULL;
    alp_return_error_tpl_t *err_tpl = iface_msg ? (alp_return_error_tpl_t*)rec->data : NULL;
    alp_null_op_t param = (alp_null_op_t)evp.data;


    KAL_PROC_BEGIN();

    DPRINT("Start WiSense report process\n");
    // override SDA/SCL setting to drive them low for a while
    hal_gpio_setup(HAL_GPIO_I2C1_SCL, HAL_IOMODE_OD|HAL_IOMODE_OUT);
    hal_gpio_setup(HAL_GPIO_I2C1_SDA, HAL_IOMODE_OD|HAL_IOMODE_OUT);
    
    count=0;
    test = hal_gpio_read(HAL_GPIO_I2C1_SCL) | (hal_gpio_read(HAL_GPIO_I2C1_SDA)<<1);
    DPRINT("Test I2C %x\n",test);

    while ((test != 0x3) && (count<500))
    {
        hal_gpio_clr(HAL_GPIO_I2C1_SCL);
        hal_gpio_clr(HAL_GPIO_I2C1_SDA);
        kal_timer_start(&g_sensor_state.alp_ack_tim, KAL_ETIMER, &wisense_report_p, 0, MOTE_RST_TIMEOUT);
        KAL_PROC_WAIT_UNTIL_EVENT(KAL_EVT_TIMER);
        hal_gpio_set(HAL_GPIO_I2C1_SCL);
        hal_gpio_set(HAL_GPIO_I2C1_SDA);
        kal_timer_start(&g_sensor_state.alp_ack_tim, KAL_ETIMER, &wisense_report_p, 0, MOTE_RST_TIMEOUT);
        KAL_PROC_WAIT_UNTIL_EVENT(KAL_EVT_TIMER);
        test = hal_gpio_read(HAL_GPIO_I2C1_SCL) | (hal_gpio_read(HAL_GPIO_I2C1_SDA)<<1);
        count++;
        DPRINT("Test I2C %x\n",test);
    }
    DPRINT("I2C Toggle count %x\n",count);
    if (count >=500)
    {
        DPRINT("Fatal I2C\n");
    }

    // SDA/SCL back to I2C mode
    hal_gpio_set(HAL_GPIO_I2C1_SCL);
    hal_gpio_set(HAL_GPIO_I2C1_SDA);
    hal_gpio_setup(HAL_GPIO_I2C1_SCL, HAL_IOMODE_OD|HAL_IOMODE_ALT);
    hal_gpio_setup(HAL_GPIO_I2C1_SDA, HAL_IOMODE_OD|HAL_IOMODE_ALT);

    // Turn off GREEN LED
    hal_led_off(HAL_LED_GREEN);

    // WiSense is ready - Reset Mote 
    hal_gpio_clr(HAL_GPIO_MOTE_RST);
    kal_timer_start(&g_sensor_state.alp_ack_tim, KAL_ETIMER, &wisense_report_p, 0, MOTE_RST_TIMEOUT);
    KAL_PROC_WAIT_UNTIL_EVENT(KAL_EVT_TIMER);
    hal_gpio_set(HAL_GPIO_MOTE_RST);

    // Wait until the mote is ready (ie booted and calib done)
    // Ignore all events during that time
    while(1)
    {
        kal_timer_start(&g_sensor_state.alp_ack_tim, KAL_ETIMER, &wisense_report_p, 0, 60000);
        KAL_PROC_WAIT_EVENT();
        if ((ev == ALP_IFACE_EVT_RX_NULL_CMD) && (param == ALP_OP_NULL_BOOT))
        {
            kal_timer_stop(&g_sensor_state.alp_ack_tim);
            DPRINT("MOTE READY\n");
            break;
        }
        else
        {
            // Unexpected answer
            // Reset everything
            DPRINT("Received evt %x\n",ev);
            hal_software_reset(0);
        }
    }
    // Turn off RED LED
    hal_led_off(HAL_LED_RED);

    // Open LED files (!) Do not close the files as we want to be notified of the changes
    g_sensor_state.led_fd = kal_file_open(SENSOR_LED_FILE_ID, KAL_FILE_ACCESS_MODE_READ | KAL_FILE_ACCESS_MODE_WRITE | KAL_FILE_ACCESS_MODE_MIRROR, KAL_FILE_GROUP_ROOT, 0, &wisense_p);

    // Open DAC files (!) Do not close the files as we want to be notified of the changes
    g_sensor_state.dac_fd = kal_file_open(SENSOR_DAC_FILE_ID, KAL_FILE_ACCESS_MODE_READ | KAL_FILE_ACCESS_MODE_WRITE | KAL_FILE_ACCESS_MODE_MIRROR, KAL_FILE_GROUP_ROOT, 0, &wisense_p);

    // Set LEDs initial state
    led_update();

    // Set DAC initial state 
    g_sensor_state.dac_state[0]=DAC_OFF;
    g_sensor_state.dac_state[1]=DAC_OFF;
    dac_update();

    // Launch Sensor process
    kal_proc_start(&wisense_p, (evt_param_t)NULL);

    // Main Reporting loop
    while(1)
    {
        KAL_PROC_WAIT_EVENT();

        if (APP_EVT_REPORT == ev)
        {
            g_sensor_state.current_report = (u16) evp.data;
            g_sensor_state.cmd_retry_is_after_rst = FALSE;
            g_sensor_state.cmd_retry=g_sensor_cfg.max_cmd_retries;
            g_sensor_state.report[g_sensor_state.current_report].report_status=REPORT_ACTIVE;

            while (g_sensor_state.cmd_retry-->0)
            {
                // Send reporting  to mote
                DPRINT("ALP RPT %d - SRL Attempt left %d\n",g_sensor_state.current_report, g_sensor_state.cmd_retry);
                alp_cmd_wf(0, ALP_FILE_BLOCK_ISFB , TRUE,
                        g_sensor_state.report[g_sensor_state.current_report].file_id,
                        0,
                        g_sensor_state.report[g_sensor_state.current_report].file_size,
                        g_sensor_state.report[g_sensor_state.current_report].file_data);

                g_sensor_state.mote_alive = FALSE; // We don't know yet
                g_sensor_state.report_state = RPT_START;

                // The normal sequence is as follow
                //  ALP_IFACE_EVT_RX_PKT with ALP_ERR_NO to acknowledge the write command
                //  ALP_IFACE_EVT_RX_NULL_CMD with ALP_OP_NULL_ACK acknowledge received from remote party
                //  ALP_IFACE_EVT_RX_NULL_CMD with ALP_OP_NULL_NACK Mote end of command

                // In case the mote does not answer, we don't receive the ALP_IFACE_EVT_RX_PKT and any following answers
                // In case there is no Acknowledge  from remote, we don't receive the ACK but we receive the NACK

                while (g_sensor_state.report_state != RPT_FINISH)
                {
                    // Launch a timer to generate timeout in case there is no answer from the mote
                    u16 timeout;

                    if (g_sensor_state.report_state ==  RPT_START)
                    {
                        timeout=ALP_ACK_TIMEOUT;
                    }
                    else
                    {
                        timeout=ALP_NTF_TIMEOUT;
                    }

                    kal_timer_start(&g_sensor_state.alp_ack_tim, KAL_FTIMER, send_alp_rxtimeout, 0, timeout);
                    ALP_WAIT_RX_EVENT();
                    kal_timer_stop(&g_sensor_state.alp_ack_tim);

                    if (ev == ALP_IFACE_EVT_RX_PKT)
                    {
                        if (g_sensor_state.report_state==RPT_START)
                        {
                            // Received an answer to write file command
                            // The mote is alive
                            g_sensor_state.mote_alive = TRUE;
    
                            u8 op = ALP_FILE_OP_GET(rec->cmd);
                            kal_dbg_assert(op == ALP_OP_RETURN_ERR, "ERR WISENSE: RX unexpected resp");
    
    
                            if (err_tpl->error_code == ALP_ERR_NO)
                            {
    
                                // We received the acnkowledge of write file without error
                                // Waiting for NULL cmd that indicate that we received the acknowledge 
                                // from remote Gateway
                                DPRINT("ALP SRL ACK\n");
                                g_sensor_state.report_state=RPT_WF_MOTE_ACK;
                            }
                            else
                            {
                                // We received the acknowledge of write file with an error
                                // Command failed, need to retry
                                DPRINT("ALP SRL ACK - Error code: %d\n", err_tpl->error_code);
                                g_sensor_state.report_state=RPT_FINISH;
                            }
                        }
                        else
                        {
                            // This is not expected to received this message here
                            // Just ignore it
                            DPRINT("Unexpected ALP_IFACE_EVT_RX_PKT - Ignore\n");

                        }
                        APP_FREE(iface_msg);
                    }
                    else if (ev == ALP_IFACE_EVT_RX_NULL_CMD)
                    {
                        if (param == ALP_OP_NULL_ACK)
                        {
                            if (g_sensor_state.report_state==RPT_WF_MOTE_ACK)
                            {
                                // Got Remote  GW ACK
                                DPRINT("ALP GW ACK\n");
                                g_sensor_state.report_state=RPT_WF_REMOTE_ACK;
                            }
                            else
                            {
                                // Unexpected ACK - Ignore it
                                DPRINT("Unexpected ALP_IFACE_EVT_RX_NULL_CMD ACK - Ignore\n");
                            }
                        }
                        else
                        {
                            if (g_sensor_state.report_state==RPT_WF_REMOTE_ACK)
                            {
                                // Transfer successfully finished
                                DPRINT("ALP RPT DONE\n");
                                g_sensor_state.report_state=RPT_FINISH;
                                g_sensor_state.report[g_sensor_state.current_report].report_status=REPORT_IDLE;
                            } 
                            else if (g_sensor_state.report_state==RPT_WF_MOTE_ACK)
                            {
                                // Received Nack transfer failed
                                g_sensor_state.report[g_sensor_state.current_report].report_status=REPORT_DELAYED;
                                DPRINT("ALP GW NACK - Retrying in %d seconds ... \n", g_sensor_cfg.retry_delay);
                                // Re-schedure transmission if some attempts left
                                if (g_sensor_state.report[g_sensor_state.current_report].retry_left>1)
                                {
                                    kal_timer_start(&g_sensor_state.report[g_sensor_state.current_report].retry_tim, 
                                            KAL_FTIMER, retry_report,
                                            g_sensor_state.current_report , 
                                            1000 * g_sensor_cfg.retry_delay);
                                }
                                g_sensor_state.report_state=RPT_FINISH;

                            }
                            else
                            {
                                // Unexpected NACK - Ignore it
                                DPRINT("Unexpected ALP_IFACE_EVT_RX_NULL_CMD NACK - Ignore\n");
                            }
                        }
                    }
                    else if (ev == ALP_IFACE_EVT_RX_TIMEOUT)
                    {
                        DPRINT("ALP_IFACE_EVT_RX_TIMEOUT\n");
                        if (g_sensor_state.report_state==RPT_START)
                        {
                            // Mote did not respond in time
                            if ((!g_sensor_state.cmd_retry) && (!g_sensor_state.cmd_retry_is_after_rst) && !g_sensor_state.mote_alive)
                            {
                                DPRINT("RESETING MOTE ...\n");
                                g_sensor_state.cmd_retry_is_after_rst = TRUE;
                                g_sensor_state.cmd_retry = g_sensor_cfg.max_retries_after_rst;

                                // Mote reset procedure
                                hal_gpio_clr(HAL_GPIO_MOTE_RST);
                                kal_timer_start(&g_sensor_state.alp_ack_tim, KAL_ETIMER, &wisense_report_p, 0, MOTE_RST_TIMEOUT);
                                KAL_PROC_WAIT_UNTIL_EVENT(KAL_EVT_TIMER);
                                hal_gpio_set(HAL_GPIO_MOTE_RST);

                                //kal_timer_start(&g_sensor_state.alp_ack_tim, KAL_ETIMER, &wisense_report_p, 0, MOTE_BOOT_TIMEOUT);
                                //KAL_PROC_WAIT_UNTIL_EVENT(KAL_EVT_TIMER);
                                KAL_PROC_WAIT_UNTIL_EVENT(ALP_IFACE_EVT_RX_NULL_CMD);
                                if ((ev == ALP_IFACE_EVT_RX_NULL_CMD) && (param == ALP_OP_NULL_BOOT))
                                {
                                    DPRINT("MOTE READY\n");
                                }
                                else
                                {
                                    DPRINT("Received param %x\n",param);
                                }
                            }
                        }
                        else
                        {
                            DPRINT("Timeout on ACK/NACK\n");
                        }
                        g_sensor_state.report_state=RPT_FINISH;
                    }
                    else
                    {
                        // We should never go there
                        kal_dbg_assert(FALSE, "APP: Unexpected evt 0x%02x\n", ev);
                    }
                }
                if (g_sensor_state.report[g_sensor_state.current_report].report_status!=REPORT_ACTIVE)
                {
                    break;
                }
            }
        }
        else if (ev == ALP_IFACE_EVT_RX_NULL_CMD)
        {
            if (param == ALP_OP_NULL_ACK)
            {
                DPRINT("Unexpected NTF ACK\n");
            }
            else
            {
                DPRINT("Unexpected NTF NACK\n");
            }
        }

        else if (ev == ALP_IFACE_EVT_RX_PKT)
        {
            DPRINT("Unexpected ALP WF ACK\n");
            APP_FREE(iface_msg);
        }

        else if (ev == ALP_IFACE_EVT_RX_TIMEOUT)
        {
            DPRINT("Unexpected ALP TIMEOUT\n");
            APP_FREE(iface_msg);
        }

        else
        {
            kal_dbg_assert(FALSE, "APP: Unexpected evt 0x%02x\n", ev);
        }
    }

    KAL_PROC_END();
}

//-----------------------
// Check reporting condition
//--------------------------
u8 report_needed(u8 report_type, int low_threshold, int high_threshold, int val, int last_val)
{
    u8 report=FALSE;

    if (report_type==RPT_ALWAYS)
    {
        report=TRUE;
    }
    else if (report_type==RPT_OUTSIDE_RANGE)
    {
        if ((val<low_threshold) || (val>high_threshold))
        {
            report=TRUE;
        }
    }
    else if (report_type==RPT_INSIDE_RANGE)
    {
        if ((val>low_threshold) && (val<high_threshold))
        {
            report=TRUE;
        }
    }
    else if (report_type==RPT_BOUNDARY_CROSSING)
    {
        if ( 
            ((val>high_threshold) && (last_val<high_threshold)) ||
            ((val<high_threshold) && (last_val>high_threshold)) ||
            ((val>low_threshold) && (last_val<low_threshold)) ||
            ((val<low_threshold) && (last_val>low_threshold)) )
        {
            report=TRUE;
        }
    }
    else if (report_type==RPT_HYSTERESIS)
    {
        if ( 
            ((val>high_threshold) && (last_val<high_threshold)) ||
            ((val<low_threshold) && (last_val>low_threshold)) )
        {
            report=TRUE;
        }
    }
    else if (report_type==RPT_CHANGE_ABOVE_LOW_THRESHOLD)
    {
        if ( 
            ABS(val-last_val)>low_threshold) 
        {
            report=TRUE;
        }
    }
    return report;
}
//======================================================================
//  Sensor Management
//======================================================================
//--------------------------------
//  sensor process
//--------------------------------
KAL_PROC_THREAD(wisense_p, ev, evp)
{

    KAL_PROC_BEGIN();
    DPRINT("Start WiSense process\n");

    // Open I2C for all sensors
    hal_i2c_open(HAL_I2C_SENSOR);

    // ===============
    // LED
    // ===============
    // Init LED timers
    kal_timer_init(&g_sensor_state.led_tim);

    // ===============
    // Enable buttons
    // ===============
    // Initialize reporting structure
    kal_timer_init(&g_sensor_state.report[BTN1_DATA].retry_tim);
    g_sensor_state.report[BTN1_DATA].report_status=REPORT_IDLE;
    g_sensor_state.button_rpt[0].seqnum=init_seqnum();
    init_service_id(g_sensor_state.button_rpt[0].service_id);
    kal_timer_init(&g_sensor_state.report[BTN2_DATA].retry_tim);
    g_sensor_state.report[BTN2_DATA].report_status=REPORT_IDLE;
    g_sensor_state.button_rpt[1].seqnum=init_seqnum();
    init_service_id(g_sensor_state.button_rpt[1].service_id);
    // Start button monitoring
    button_open();

#ifdef ACCEL_EN
    // ===============
    // Start Accelerometer
    // ===============
    // Initialize reporting structure
    kal_timer_init(&g_sensor_state.report[ACCEL_DATA].retry_tim);
    g_sensor_state.report[ACCEL_DATA].report_status=REPORT_IDLE;
    g_sensor_state.accel_rpt.seqnum=init_seqnum();
    init_service_id(g_sensor_state.accel_rpt.service_id);
    // Start accelerometer
    accel_mma8652_open(0, MMA_RATE_1_SPS, &wisense_p);
#endif

#ifdef HUMIDITY_EN
    // ===============
    // start temp and humidity sensor
    // ===============
    // Initialize reporting structure
    kal_timer_init(&g_sensor_state.report[HUM_DATA].retry_tim);
    g_sensor_state.report[HUM_DATA].report_status=REPORT_IDLE;
    g_sensor_state.humidity_rpt.seqnum=init_seqnum();
    init_service_id(g_sensor_state.humidity_rpt.service_id);
    kal_timer_init(&g_sensor_state.report[TEMP_DATA].retry_tim);
    g_sensor_state.report[TEMP_DATA].report_status=REPORT_IDLE;
    g_sensor_state.temperature_rpt.seqnum=init_seqnum();
    init_service_id(g_sensor_state.temperature_rpt.service_id);
    // Start Humidity sensor
    kal_timer_init(&g_sensor_state.humidity_tim);
    si7020_open( &wisense_p );
#endif

#ifdef BARO_EN
    // ===============
    // Start Pressure sensor
    // ===============
    // Initialize reporting structure
    kal_timer_init(&g_sensor_state.report[BARO_DATA].retry_tim);
    g_sensor_state.report[BARO_DATA].report_status=REPORT_IDLE;
    g_sensor_state.baro_rpt.seqnum=init_seqnum();
    init_service_id(g_sensor_state.baro_rpt.service_id);
    // Start Pressure sensor
    kal_timer_init(&g_sensor_state.baro_tim);
    baro_mpl115_open(0, &wisense_p);
#endif

#ifdef PHOTO_EN
    // Start periodical light measure routine
    // Open ADC in single snap mode
    // Initialize reporting structure
    kal_timer_init(&g_sensor_state.report[PHOTO_DATA].retry_tim);
    g_sensor_state.report[PHOTO_DATA].report_status=REPORT_IDLE;
    g_sensor_state.photo_rpt.seqnum=init_seqnum();
    init_service_id(g_sensor_state.photo_rpt.service_id);
    // Start photo monitoring
    hal_adc_single_open();
    kal_timer_init(&g_sensor_state.light_tim);
    kal_timer_start(&g_sensor_state.light_tim, KAL_FTIMER, light_adc_measure, PHOTO_TURNON, PHOTO_SETUP_TIME);
#endif

    while(1)
    {
        KAL_PROC_WAIT_EVENT();

        if (ev == HAL_EVT_BUTTON1)
        {
            if (0 == evp.data)
            {
                hal_led_on(HAL_LED_GREEN);
                DPRINT("RPT BTN1 %d\n",g_sensor_state.button_rpt[0].seqnum); 
                g_sensor_state.button_rpt[0].status=BTN_PUSH;
                g_sensor_state.report[BTN1_DATA].file_id=BTN1_DATA_FILE_ID;
                g_sensor_state.report[BTN1_DATA].file_size=BTN_DATA_FILE_SIZE;
                g_sensor_state.report[BTN1_DATA].file_data=(void*) &(g_sensor_state.button_rpt[0]);
                send_report(BTN1_DATA);

            }
        }

        else if (ev == HAL_EVT_BUTTON2)
        {
            if (0 == evp.data)
            {
                hal_led_off(HAL_LED_GREEN);
                DPRINT("RPT BTN2 %d\n",g_sensor_state.button_rpt[1].seqnum); 
                g_sensor_state.button_rpt[1].status=BTN_PUSH;
                g_sensor_state.report[BTN2_DATA].file_id=BTN2_DATA_FILE_ID;
                g_sensor_state.report[BTN2_DATA].file_size=BTN_DATA_FILE_SIZE;
                g_sensor_state.report[BTN2_DATA].file_data=(void*) &(g_sensor_state.button_rpt[1]);
                send_report(BTN2_DATA);
            }
        }

        else if (KAL_EVT_FILE_MODIFIED == ev)
        {
            DPRINT("Config modified\n");
            
            // sensor configuration changed
            if ((s16) evp.data == g_sensor_state.cfg_fd)
            {
                kal_dbg_assert(kal_file_seek(g_sensor_state.cfg_fd, 0, KAL_FILE_WHENCE_SET) 
                    == KAL_FILE_ERR_NO, "ERR SEEK CFG");
                kal_dbg_assert(kal_file_read(g_sensor_state.cfg_fd, &g_sensor_cfg, SENSOR_CFG_FILE_SIZE) 
                    == SENSOR_CFG_FILE_SIZE, "ERR RD CFG");

            }
            else if ((s16) evp.data == g_sensor_state.led_fd)
            {
                led_update();
            }
            else if ((s16) evp.data == g_sensor_state.dac_fd)
            {
                dac_update();
            }

        }

        else if (DRV_EVT_ACCEL_RES_AVAILABLE == ev)
        {
            u8 report;
            int motion;
            s16* data = (s16*)evp.msg;
            s16 last_x = g_sensor_state.accel_rpt.x;
            s16 last_y = g_sensor_state.accel_rpt.y;
            s16 last_z = g_sensor_state.accel_rpt.z;
            g_sensor_state.accel_rpt.x = data[0] >> 4;
            g_sensor_state.accel_rpt.y = data[1] >> 4;
            g_sensor_state.accel_rpt.z = data[2] >> 4;

            motion = ABS(g_sensor_state.accel_rpt.x-last_x)+ABS(g_sensor_state.accel_rpt.y-last_y)+ABS(g_sensor_state.accel_rpt.z-last_z);
            report = report_needed(g_sensor_cfg.accel_cfg.report_type, 
                        (int) g_sensor_cfg.accel_cfg.low_threshold, 
                        (int) g_sensor_cfg.accel_cfg.high_threshold, 
                        (int) motion, 
                        (int) 0);
            if (report==TRUE)
            {
                // Prepare report request message
                g_sensor_state.report[ACCEL_DATA].file_id=ACCEL_DATA_FILE_ID;
                g_sensor_state.report[ACCEL_DATA].file_size=ACCEL_DATA_FILE_SIZE;
                g_sensor_state.report[ACCEL_DATA].file_data=(void*) &(g_sensor_state.accel_rpt);
                // Send report request
                send_report(ACCEL_DATA);
                DPRINT("RPT ACC: x:%d y:%d z:%d mot:%d\n",g_sensor_state.accel_rpt.x,g_sensor_state.accel_rpt.y,g_sensor_state.accel_rpt.z,motion);
            }
            else
            {
                DPRINT("ACC: x:%d y:%d z:%d mot:%d\n",g_sensor_state.accel_rpt.x,g_sensor_state.accel_rpt.y,g_sensor_state.accel_rpt.z,motion);
            }

            APP_FREE(evp.msg);
        }

        else if (DRV_EVT_BARO_RES_AVAILABLE == ev)
        {
            u8 report;
            int last_baro=g_sensor_state.baro_rpt.val;

            g_sensor_state.baro_rpt.val=(u16)evp.data;

            // --------------------------
            // Baro reporting 
            // --------------------------
            report = report_needed(g_sensor_cfg.baro_cfg.report_type, 
                        (int) g_sensor_cfg.baro_cfg.low_threshold, 
                        (int) g_sensor_cfg.baro_cfg.high_threshold, 
                        (int) g_sensor_state.baro_rpt.val, 
                        (int) last_baro);
            if (report==TRUE)
            {
                // Prepare report request message
                g_sensor_state.report[BARO_DATA].file_id=BARO_DATA_FILE_ID;
                g_sensor_state.report[BARO_DATA].file_size=BARO_DATA_FILE_SIZE;
                g_sensor_state.report[BARO_DATA].file_data=(void*) &(g_sensor_state.baro_rpt);
                // Send report request
                send_report(BARO_DATA);
                DPRINT("RPT BARO: %d\n",g_sensor_state.baro_rpt.val);
            }
            else
            {
                DPRINT("BARO: %d\n",g_sensor_state.baro_rpt.val);
            }
            kal_timer_start(&g_sensor_state.baro_tim, KAL_FTIMER, baro_mpl115_get_press, 0, g_sensor_cfg.baro_cfg.meas_period);
        }
        
        else if (DRV_EVT_RH_TEMP_AVAILABLE == ev)
        {
            u8 report;
            int last_temp=g_sensor_state.temperature_rpt.val;
            int last_hum=g_sensor_state.humidity_rpt.val;

            g_sensor_state.temperature_rpt.val=g_temp;
            g_sensor_state.humidity_rpt.val=g_hum;

            // --------------------------
            // temperature reporting 
            // --------------------------
            report = report_needed(g_sensor_cfg.temp_cfg.report_type, 
                        (int) g_sensor_cfg.temp_cfg.low_threshold, 
                        (int) g_sensor_cfg.temp_cfg.high_threshold, 
                        (int) g_sensor_state.temperature_rpt.val, 
                        (int) last_temp);
            if (report==TRUE)
            {
                // Prepare report request message
                g_sensor_state.report[TEMP_DATA].file_id=TEMP_DATA_FILE_ID;
                g_sensor_state.report[TEMP_DATA].file_size=TEMP_DATA_FILE_SIZE;
                g_sensor_state.report[TEMP_DATA].file_data=(void*) &(g_sensor_state.temperature_rpt);
                // Send report request
                send_report(TEMP_DATA);
                DPRINT("RPT TEMP: %d\n",g_sensor_state.temperature_rpt.val);
            }
            else
            {
                DPRINT("TEMP: %d\n",g_sensor_state.temperature_rpt.val);
            }
            
            // --------------------------
            // humidity reporting 
            // --------------------------
            report = report_needed(g_sensor_cfg.hum_cfg.report_type, 
                        (int) g_sensor_cfg.hum_cfg.low_threshold, 
                        (int) g_sensor_cfg.hum_cfg.high_threshold, 
                        (int) g_sensor_state.humidity_rpt.val, 
                        (int) last_hum);
            if (report==TRUE)
            {
                // Prepare report request message
                g_sensor_state.report[HUM_DATA].file_id=HUM_DATA_FILE_ID;
                g_sensor_state.report[HUM_DATA].file_size=HUM_DATA_FILE_SIZE;
                g_sensor_state.report[HUM_DATA].file_data=(void*) &(g_sensor_state.humidity_rpt);
                // Send report request
                send_report(HUM_DATA);
                DPRINT("RPT HUM: %d\n",g_sensor_state.humidity_rpt.val);
            }
            else
            {
                DPRINT("HUM: %d\n",g_sensor_state.humidity_rpt.val);
            }
            kal_timer_start(&g_sensor_state.humidity_tim, KAL_FTIMER, humidity_measure, 0, g_sensor_cfg.hum_cfg.meas_period);
        }
        
        else if (DRV_EVT_ACCEL_INIT_DONE == ev)
        {
            DPRINT("ACCEL Init Done\n");
        }

        else if (DRV_EVT_BARO_INIT_DONE == ev)
        {
            DPRINT("BARO Init Done\n");
            baro_mpl115_get_press();
        }
        
        else if (DRV_EVT_RH_TEMP_INIT_DONE == ev)
        {
            DPRINT("RH/T Init Done\n");
            humidity_measure(0);
        }

        else if (HAL_EVT_ADC_CONV_DONE == ev)
        {
            u16 light_val= (u16)evp.data;
            u8 report = report_needed(g_sensor_cfg.photo_cfg.report_type, 
                        (int) g_sensor_cfg.photo_cfg.low_threshold, 
                        (int) g_sensor_cfg.photo_cfg.high_threshold, 
                        (int) light_val, 
                        (int) g_sensor_state.photo_rpt.val);

            if (report==TRUE)
            {
                // Prepare report content
                g_sensor_state.photo_rpt.val = light_val;
                // Prepare report request message
                g_sensor_state.report[PHOTO_DATA].file_id=PHOTO_DATA_FILE_ID;
                g_sensor_state.report[PHOTO_DATA].file_size=PHOTO_DATA_FILE_SIZE;
                g_sensor_state.report[PHOTO_DATA].file_data=(void*) &(g_sensor_state.photo_rpt);
                // Send report request
                send_report(PHOTO_DATA);
                DPRINT("RPT PHOTO %d\n",light_val); 
            }
            else
            {
                DPRINT("PHOTO %d\n",light_val); 
            }
    
            // Go back to Off state
            hal_gpio_clr(HAL_GPIO_PHOTO_PWR);
            // Schedule next measure
            kal_timer_start(&g_sensor_state.light_tim, KAL_FTIMER, &light_adc_measure,PHOTO_TURNON, g_sensor_cfg.photo_cfg.meas_period);
        }
        else
        {
            DPRINT("SENSOR unknown event %x %x\n",ev, evp.data);
        }
    }
    KAL_PROC_END();
}


//======================================================================
//  Main
//======================================================================
void main(void)
{
    
    hal_rtc_datetime_t datetime;

    // ===============
    // HAL General
    // ===============

    // Setup system: IOs, clock, ISRs
    u16 err=hal_open(error_handler,LPM2);

    // Open RTC
    hal_rtc_open();
    datetime.date.year      = 14;
    datetime.date.month     = 1;
    datetime.date.day       = 1;
    datetime.date.weekday   = 3;
    datetime.time.hours     = 0;
    datetime.time.minutes   = 0;
    datetime.time.seconds   = 0;
    hal_rtc_set_datetime(&datetime);

    hal_led_on(HAL_LED_RED);
    hal_led_on(HAL_LED_GREEN);

    // ===============
    // KAL
    // ===============

    // Open KAL layer
    kal_open();
    kal_com_map(DBG_COM, KAL_COM_FLOWID_CMD, NULL);

    DPRINT("WISENSE build " __DATE__ "\n");
    kal_dbg_assert(!err,"LSE Error %d\n",err);

    // ===============
    // CONFIG
    // ===============
    // Open configuration files (!) Do not close the files as we want to be notified of the changes
    g_sensor_state.cfg_fd = kal_file_open(SENSOR_CFG_FILE_ID, KAL_FILE_ACCESS_MODE_READ | KAL_FILE_ACCESS_MODE_WRITE, KAL_FILE_GROUP_ROOT, 0, &wisense_p);

    // Read configuration
    kal_dbg_assert(kal_file_seek(g_sensor_state.cfg_fd, 0, KAL_FILE_WHENCE_SET) == KAL_FILE_ERR_NO, "ERR SEEK CFG");

    kal_dbg_assert(kal_file_read(g_sensor_state.cfg_fd, &g_sensor_cfg, SENSOR_CFG_FILE_SIZE) == SENSOR_CFG_FILE_SIZE, "ERR RD CFG");

    // Open scratch file : for debug only
    kal_file_open(SENSOR_SCRATCH_FILE_ID, KAL_FILE_ACCESS_MODE_READ | KAL_FILE_ACCESS_MODE_WRITE | KAL_FILE_ACCESS_MODE_MIRROR, KAL_FILE_GROUP_ROOT, 0, NULL);

    // ===============
    // ALP
    // ===============
    // Open interfaces (interface with ID = 0 is receives the UNS)
    alp_open(ALP_IFACE_NONE,0,&wisense_report_p);
    alp_iface_open_com(0, MOTE_COM, KAL_FILE_GROUP_USER);
    alp_cmd_open(0);

    kal_timer_init(&g_sensor_state.alp_ack_tim);
    kal_timer_init(&g_sensor_state.alp_nack_tim);


    // Open cup background daemon
    cup_open();

    // ===============
    // PROCESS
    // ===============
    // Start Wisense process
    kal_proc_start(&wisense_report_p, (evt_param_t)NULL);

    // Run scheduler
    kal_proc_schedule();
}

/// @}
// vim:fdm=marker:fdc=2

