/// @copyright
/// ========================================================================={{{
/// Copyright (c) 2013-2014 WizziLab                                           /
/// All rights reserved                                                        /
/// =========================================================================}}}
/// @endcopyright

//  =======================================================================
/// @file           wisense.h
/// @brief          WISENSE public header 
//  =======================================================================

#ifndef __WISENSE_H__
#define __WISENSE_H__

#include "hal.h"
#include "kal.h"
#include "pres.h"
#include "nornir.h"

/// @ingroup APP
/// @defgroup Nornir       Wisense application
/// @{

//#define WISENSE_NO_PRINTF
#ifndef WISENSE_NO_PRINTF
#define DPRINT(...) kal_dbg_printf(__VA_ARGS__)
#else
#define DPRINT(...)
#endif

//======================================================================
// RTW Service definition
//======================================================================
#define RTW_SERVICE 0x000003

//======================================================================
// SENSOR ENABLE/DISABLE
//======================================================================
#define PHOTO_EN
#define HUMIDITY_EN
#define BARO_EN
#define ACCEL_EN

//======================================================================
// Types definition
//======================================================================
#define PHOTO_SETUP_TIME 10
#define PHOTO_HOLD_TIME 100

//======================================================================
// Application events
//======================================================================
typedef enum
{
    // Application events
    APP_EVT_SERIAL_RX_PACKET = _USR_EVENT_BASE,
    APP_EVT_REPORT,

} app_events_t;

typedef enum
{
    APP_PROC_SENSOR=USER_PID(0),
    APP_PROC_SERIAL,

} app_proc_t;

typedef enum 
{
    REPORT_IDLE,
    REPORT_PENDING,
    REPORT_ACTIVE,
    REPORT_DELAYED,
} report_status_t;

typedef struct
{
    kal_timer_t retry_tim;
    u8  report_status;
    u8  retry_left;
    u8  file_id;
    u8  file_size;
    u8* file_data;
} sensor_report_t;

typedef struct
{
    // Common part for all reports
    u8 seqnum;
    u8 retry;
    // Specific data starts here
} generic_report_t;

typedef enum {
    RPT_START=0,
    RPT_WF_MOTE_ACK,
    RPT_WF_REMOTE_ACK,
    RPT_FINISH,
    
} report_state_t;

typedef struct
{
    // Always open files file descriptors (volatile mirrored in RAM)
    s16 cfg_fd;
    s16 led_fd;
    s16 dac_fd;
    // Reporting States
    u16 current_report;
    u8  report_state;
    u8  seqnum;
    u8  send_restart_report;
    u8  report_retry;
    u8  report_restart_active;
    u8  cmd_retry;
    u8  cmd_retry_is_after_rst;
    bool mote_alive;
    sensor_report_t report[NB_REPORT_TYPES];
    // Report Structures
    btn_data_t      button_rpt[2];
    hum_data_t      humidity_rpt;
    temp_data_t     temperature_rpt;
    accel_data_t    accel_rpt;
    baro_data_t     baro_rpt;
    photo_data_t    photo_rpt;
    // Actuators values
    u8  led_state[SENSOR_LED_FILE_SIZE];
    u16 dac_state[SENSOR_DAC_FILE_SIZE/2];
    // Global timers
    kal_timer_t led_tim;
    kal_timer_t button1_tim;
    kal_timer_t button2_tim;
    kal_timer_t baro_tim;
    kal_timer_t light_tim;
    kal_timer_t humidity_tim;
    kal_timer_t alp_ack_tim;
    kal_timer_t alp_nack_tim;
} sensor_state_t;

typedef enum
{
    PHOTO_TURNON=0,
    PHOTO_MEAS
} light_action_t;

#endif // __WISENSE_H__

/// @}
// vim:fdm=marker:fdc=2
