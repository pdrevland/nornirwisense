#!/bin/sh

#####################################################################
# You should be where this README resides to start the installation
#####################################################################
export PROJ_DIR=`pwd`
cd $PROJ_DIR
################################################################################
# PPA with official GNU ARM Embedded Toolchain (Ubuntu 10.4/12.4 32bit/64bit)
################################################################################
sudo add-apt-repository ppa:terry.guo/gcc-arm-embedded
sudo apt-get update
sudo apt-get install gcc-arm-none-eabi

################################################################################
# Perl dependencies (for flash burn)
################################################################################
sudo apt-get install libnet-telnet-perl

################################################################################
# OpenOCD
################################################################################
# install dependencies
sudo apt-get install libftdi-dev libftdi1 libtool git-core asciidoc \
build-essential flex bison \
libgmp3-dev libmpfr-dev autoconf \
texinfo libncurses5-dev libexpat1 libexpat1-dev \
tk tk8.4 tk8.4-dev

# get openOCD sources
# do it in special directory ourside of your project MY_SOFTWARE_DIR
cd $MY_SOFTWARE_DIR
cd openocd
git clone http://openocd.zylin.com/openocd
./bootstrap
./configure --enable-maintainer-mode --enable-legacy-ft2232_libftdi --enable-stl
make -j4
sudo make install

################################################################################
# Setup rules for olimex arm usb tiny h
# copy env/sys/99-olimex_arm-usb-ocd-h.rules into /etc/udev/rules.d/99-olimex_arm-usb-ocd-h.rules
################################################################################
# If ou are using an olimex programmer
cd $PROJ_DIR
sudo cp env/sys/99-olimex_arm-usb-ocd-h.rules /etc/udev/rules.d/.
sudo cp env/sys/99-olimex_arm-usb-ocd.rules /etc/udev/rules.d/.

################################################################################
# Setup rules for ST-Linkv2
# copy 49-stlinkv2.rules into /etc/udev/rules.d/49-stlinkv2.rules
################################################################################
# If you are using a STlink programmer
cd $PROJ_DIR
sudo cp env/sys/49-stlinkv2.rules /etc/udev/rules.d/.

################################################################################
# Add your username to the plugdev group:
################################################################################
sudo useradd -G plugdev USERNAME
# then execute
sudo restart udev
sudo udevadm trigger

################################################################################
# Launch OpenOCD:
################################################################################
# A configuration script is in $PROJ_DIR/env/openocd/openocd.cfg
# Just launch openocd from $PROJ_DIR/env/openocd or run openocb -f env/openocd/openocd.cfg

# if build does not exist at the root of you project follow Case 1
# otherwise go to Case2

# ---------------------------
# Case1: No build directory
# ---------------------------
# create build dir
cd $PROJ_DIR
mkdir build

# copy openocd launch script in build
cp env/sys/openocd.sh build/.
# copy the burn scripts into build - it will be more practical for later
cp env/sys/debug.sh build/.
cp env/sys/burn.rb build/.

cd build

# Goto End Case

# ---------------------------
# Case2: build directory exists and contains setup.sh
# ---------------------------
cd $PROJ_DIR/build
./setup.sh

# Goto End Case


# ---------------------------
# End Case
# ---------------------------
# launch openocd
./openocd.sh 



# You should get  an output like this
# ---- Using ARM-USB-TINY-H debugger
# Open On-Chip Debugger 0.6.1 (2013-07-15-11:08)
# Licensed under GNU GPL v2
# For bug reports, read
#   http://openocd.sourceforge.net/doc/doxygen/bugs.html
# none separate
# Info : only one transport option; autoselect 'jtag'
# adapter speed: 300 kHz
# adapter_nsrst_delay: 100
# jtag_ntrst_delay: 100
# cortex_m3 reset_config sysresetreq
# Info : max TCK change to: 30000 kHz
# Info : clock speed 300 kHz
# Info : JTAG tap: stm32l.cpu tap/device found: 0x4ba00477 (mfg: 0x23b, part: 0xba00, ver: 0x4)
# Info : JTAG tap: stm32l.bs tap/device found: 0x06416041 (mfg: 0x020, part: 0x6416, ver: 0x0)
# Info : stm32l.cpu: hardware has 6 breakpoints, 4 watchpoints

# or
# ---- Using STLink V2 debugger
# Open On-Chip Debugger 0.8.0-dev-00309-g1520e37 (2014-01-15-14:06)
# Licensed under GNU GPL v2
# For bug reports, read
#   http://openocd.sourceforge.net/doc/doxygen/bugs.html
# trst_and_srst separate srst_nogate trst_push_pull srst_open_drain connect_assert_srst
# Info : This adapter doesn't support configurable speed
# Info : STLINK v2 JTAG v16 API v2 SWIM v4 VID 0x0483 PID 0x3748
# Info : using stlink api v2
# Info : Target voltage: 2.718319
# Info : stm32lx.cpu: hardware has 6 breakpoints, 4 watchpoints

# let this window running, as we will need to have openocd running for later
# open another terminal

################################################################################
# BUILD with CMAKE (NO IDE)
################################################################################
#------------------------------------------
# Build project
# For command-line out-of-source build of project (choose Debug or Release config):
#------------------------------------------
cd $PROJ_DIR
cd build
# This step has already been done if you executed ./setup.sh
cmake -G "Unix Makefiles" -DCMAKE_TOOLCHAIN_FILE=env/cmake/stm32l1.cmake -DCMAKE_BUILD_TYPE=Debug ..

# Launch compilation
make

#------------------------------------------
# BURN and DEBUG
#------------------------------------------
# openocd should be running in a separate terminal as it will remain open and print info all the time
# see above how to launch it

# DEBUG: run the program using gdb
./debug.sh <test_name>

# BURN: just flash and run the program without debugger
# type ./burn.rb to see all the options
./burn.rb -b a <test-name>.elf

###########################################################################
# ECLIPSE build with CMAKE and run
# Not Supported and barely tested
###########################################################################
# Install Eclipse Juno for C/C++ developper

# Add the following plugins in Help/Install New Software
# Zylin OpenOCD             http://opensource.zylin.com/zylincdt 
# EmbSysRegView plugin      http://embsysregview.sourceforge.net/update
# gnuarmeclipse             http://gnuarmeclipse.sourceforge.net/updates

#For eclipse the build tree should not be in the source one (TODO: resulting project not working well...):
cd $PROJ_DIR
cd ..
mkdir ${PROJ_DIR}_eclipse
cd ${PROJ_DIR}_eclipse
cmake -G "Eclipse CDT4 - Unix Makefiles" \
    -D_ECLIPSE_VERSION=4.2 \
    -DCMAKE_TOOLCHAIN_FILE=${PROJ_DIR}/env/cmake/stm32l1.cmake \ 
    -DCMAKE_BUILD_TYPE=Debug ${PROJ_DIR}

#launch eclipse
eclipse

# In Eclipse
# Go to File->Import
# Select "Existing Projects into Workspace"
# Select ${PROJ_DIR}_eclipse as the project root directory


# Example project
# git clone https://github.com/JorgeAparicio/bareCortexM.git
