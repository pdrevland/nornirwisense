/// @copyright
/// ========================================================================={{{
/// Copyright (c) 2012-2013 WizziLab                                           /
/// All rights reserved                                                        /
///                                                                            /
/// IMPORTANT: This Software may not be modified, copied or distributed unless /
/// embedded on a WizziLab product. Other than for the foregoing purpose, this /
/// Software and/or its documentation may not be used, reproduced, copied,     /
/// prepared derivative works of, modified, performed, distributed, displayed  /
/// or sold for any purpose. For the sole purpose of embedding this Software   /
/// on a WizziLab product, copy, modification and distribution of this         /
/// Software is granted provided that the following conditions are respected:  /
///                                                                            /
/// *  Redistributions of source code must retain the above copyright notice,  /
///    this list of conditions and the following disclaimer                    /
///                                                                            /
/// *  Redistributions in binary form must reproduce the above copyright       /
///    notice, this list of conditions and the following disclaimer in the     /
///    documentation and/or other materials provided with the distribution.    /
///                                                                            /
/// *  The name of WizziLab can not be used to endorse or promote products     /
///    derived from this software without specific prior written permission.   /
///                                                                            /
/// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS        /
/// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED  /
/// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR /
/// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR          /
/// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,      /
/// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,        /
/// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,            /
/// OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY     /
/// OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING    /
/// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS         /
/// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.               /
/// WIZZILAB HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,       /
/// ENHANCEMENTS OR MODIFICATIONS.                                             /
///                                                                            /
/// Should you have any questions regarding your right to use this Software,   /
/// contact WizziLab at www.wizzilab.com.                                      /
///                                                                            /
/// =========================================================================}}}
/// @endcopyright

//  =======================================================================
/// @file           kal_file.c
/// @brief          Implement the system file.
//  =======================================================================

#ifndef __KAL_FILE_H__
#define __KAL_FILE_H__

#include "hal_types.h"
#include "kal_process.h"


//======================================================================
// Sub-headers sizes and definition to create a new predefined file
//======================================================================
#define SUB_HEADER_SIZE 5

#define FS_SUB_HEADER(ALC, LEN, MOD) U16_LSB(ALC), U16_MSB(ALC), U16_LSB(LEN), U16_MSB(LEN), MOD

//======================================================================
// Permission masks
//======================================================================

//----------------------------------------------------------------------
// Permission Code Structure
// ------------------------------------------------------------------
// |     Ctrl flags       |      User          |       Guest        |
// ------------------------------------------------------------------
// | Encrypted  | Runable | Read | Write | Run | Read | Write | Run |
// ------------------------------------------------------------------
// |    b7      |    b6   |  b5  |   b4  |  b3 |  b2  |   b1  |  b0 |
// ------------------------------------------------------------------
#define KAL_FILE_MOD_CTRL_ENCRYPTED (1 << 7)
#define KAL_FILE_MOD_CTRL_RUNABLE   (1 << 6)
#define KAL_FILE_MOD_USER_MASK      0b00111000
#define KAL_FILE_MOD_USER_POS       3
#define KAL_FILE_MOD_GUEST_MASK     0b00000111
#define KAL_FILE_MOD_GUEST_POS      0
#define KAL_FILE_MOD_R              0b100
#define KAL_FILE_MOD_W              0b010
#define KAL_FILE_MOD_X              0b001

/// The index that ideitifies the file to be used in kal_file_open
#define KAL_FILE_IDX(mem_bank, file_id) (((mem_bank) << 8) | (file_id))

//======================================================================
// Kal file get and set
//======================================================================

#define KAL_FILE_ATTR_GET(mod) (((mod) >> 6) & 0x3)
#define KAL_FILE_ATTR(attr) (((attr) & 0x3) << 6)

#define KAL_FILE_MEM_BANK_GET(idx) (((idx) >> 8) & 0xf)
#define KAL_FILE_MEM_BANK(mem_bank) (((mem_bank) >> 8) & 0xf)

#define KAL_FILE_ID_GET(idx) ((idx)  & 0xff)
#define KAL_FILE_ID(id) id

//======================================================================
// kal_file_mem_bank_t
//----------------------------------------------------------------------
/// The memory bank where the file is.
/// @see kal_file_open()
//======================================================================
typedef enum
{
    KAL_FILE_MEM_BANK_0 = 0,
    KAL_FILE_MEM_BANK_1,
    KAL_FILE_MEM_BANK_2,
    KAL_FILE_MEM_BANK_3,
} kal_file_mem_bank_t;

//======================================================================
// kal_file_error_t
//----------------------------------------------------------------------
/// Possible erros
//======================================================================
// Note: Errors are always a negative number
typedef enum
{
    KAL_FILE_ERR_NO,
    KAL_FILE_ERR_PERMISSION_DENY    = -1,
    KAL_FILE_ERR_FILE_EXIST         = -2,
    KAL_FILE_ERR_FD_NOT_AVAILABLE   = -4,
    KAL_FILE_ERR_FILE_LIMIT_REACHED = -5,
    KAL_FILE_ERR_NOT_ENOUGH_SPACE   = -6,
    KAL_FILE_ERR_INVALID_CURSOR     = -7,
    KAL_FILE_ERR_INVALID_OPERATION  = -8,
    KAL_FILE_ERR_FILE_NOT_EXIST     = -9,
    KAL_FILE_ERR_FILE_BUSY          = -10,
    KAL_FILE_ERR_FLUSH              = -11,
    KAL_FILE_ERR_INVALID_ALLOC_SIZE = -12,
} kal_file_error_t;

//======================================================================
// kal_file_access_t
//----------------------------------------------------------------------
/// Definition of access types
//======================================================================
typedef enum
{
    KAL_FILE_GROUP_ROOT,
    KAL_FILE_GROUP_USER,
    KAL_FILE_GROUP_GUEST,
} kal_file_group_t;

//======================================================================
// kal_file_access_mode_t
//----------------------------------------------------------------------
// Flags used by kal_file_open that defines the access mode of a file.
//======================================================================
typedef enum
{
    // Open flag in read mode. Combine with
    // KAL_FILE_ACCESS_MODE_WRITE for write access.
    KAL_FILE_ACCESS_MODE_READ       = 0x01,

    // Open flag in write mode. Combine with
    // KAL_FILE_ACCESS_MODE_READ for read access.
    KAL_FILE_ACCESS_MODE_WRITE      = 0x02,

    // Create a new file if it does not exist.
    KAL_FILE_ACCESS_MODE_CREATE     = 0x04,

    // Truncate and overwrites the file
    KAL_FILE_ACCESS_MODE_REPLACE    = 0x08,

    // Load and keep the file in ram until
    // a flush or a close is made.
    KAL_FILE_ACCESS_MODE_MIRROR     = 0x10,
} kal_file_access_mode_t;

//======================================================================
// kal_file_whence_t
//----------------------------------------------------------------------
// Flags used by kal_file_seek. Defines how the parameter offset is
// interpreted.
//======================================================================
typedef enum
{
    // The cursor is set to offset bytes.
    KAL_FILE_WHENCE_SET,
    // The cursor is set to the current cursor plus the offset bytes
    KAL_FILE_WHENCE_CUR,
    // The cursor is set to the end of the file plus the offset bytes
    KAL_FILE_WHENCE_END,
} kal_file_whence_t;

//======================================================================
// kal_file_attr_t
//----------------------------------------------------------------------
/// Structure sent to the watcher process in notification
/// @see kal_file_watcher_set()
//======================================================================
typedef struct
{
    u16 idx;
    u8 attr;
} kal_file_attr_t;

//======================================================================
// FILE SYSTEM FUNCTIONS
//======================================================================

//======================================================================
// kal_file_system_open
//----------------------------------------------------------------------
/// @brief  Initialize the file system.
//======================================================================
public void kal_file_system_open(void);

//======================================================================
// kal_file_watcher_set
//----------------------------------------------------------------------
/// @brief  Set a process to watch for modifications in files with
///         special attributes
//======================================================================
public void kal_file_watcher_set(process_t *w_process);

//======================================================================
// kal_file_open
//----------------------------------------------------------------------
/// @brief  Open the file with the id file_id and return a file descriptor.
/// @param  u16                 idx         The id of the file to be created in the last 8 bits
///                                         and the bank memory in (file_id << 8) & 0xf
/// @param  u16                 access_mode The access mode to open the file (READ, WRITE, CREATE, REPLACE).
///                                         See kal_file_access_mode_t for more information.
/// @param  kal_file_group_t    user_group  The access mode to the user (USER, ROOT, GUEST).
///                                         If the access is not compatible with
///                                         the permission of the file, KAL_FILE_ERR_PERMISSION_DENY is returned.
/// @param  u16                 alloc_size  The max size of the file. This parameter is just used when
///                                         KAL_FILE_ACCESS_MODE_CREATE is used in access_mode.
/// @param  process_t*          tgt_process The process to be notified if the file is modified.
/// @return s16                             The file descriptor or error if it is a negative value.
///                                         See kal_file_error_t for the meaning of each error.
//======================================================================
public s16 kal_file_open(u16 idx, u16 access_mode, kal_file_group_t user_group, u16 alloc_size, process_t* tgt_process);

//======================================================================
// kal_file_close
//----------------------------------------------------------------------
/// @brief  Close a opened file.
/// @param  u16     fd          The file descriptor.
/// @return kal_file_error_t    Error code
//======================================================================
public kal_file_error_t kal_file_close(u16 fd);

//======================================================================
// kal_file_remove
//----------------------------------------------------------------------
/// @brief  Delete a file. Fails if the file is already open.
/// @param  u16                 idx         The id of the file to be removed in the last 8 bits
///                                         and the bank memory in (file_id << 8) & 0xf
/// @param  kal_file_group_t    user_group  The access mode to the used (USER, ROOT, GUEST).
///                                         If the access is not compatible with
///                                         the permission of the file, NULL is returned.
/// @return kal_file_error_t    Error code
//======================================================================
public kal_file_error_t kal_file_remove(u16 idx, kal_file_group_t user_group);

//======================================================================
// kal_file_write
//----------------------------------------------------------------------
/// @brief  Write data to a opened file
///         If the length is bigger than the cursor + allocated size of the file
///         this functions will write just the first bytes of data until
///         the end of the file's reserved space.
/// @param  u16 fd              The file descriptor.
/// @param  const void* data    The buffer where the data to be written is.
/// @param  u16 length          How many data to be written
/// @return s16                 How many bytes was written.
///                             If it is negative then it is an error
///                             of type kal_file_error_t.
//======================================================================
public s16 kal_file_write(u16 fd, const void *data, u16 length);

//======================================================================
// kal_file_write_flush
//----------------------------------------------------------------------
/// @brief  Write data to a opened file and force this specific data
///         to be written in flash.
///         If the length is bigger than the cursor + allocated size of the file
///         this functions will write just the first bytes of data until
///         the end of the file's reserved space.
/// @param  u16 fd              The file descriptor.
/// @param  const void* data    The buffer where the data to be written is.
/// @param  u16 length          How many data to be written
/// @return s16                 How many bytes was written.
///                             If it is negative then it is an error
///                             of type kal_file_error_t.
//======================================================================
public s16 kal_file_write_flush(u16 fd, const void *data, u16 length);

//======================================================================
// kal_file_read
//----------------------------------------------------------------------
/// @brief  Read data from a file
/// @param  u16 fd              The file descriptor.
/// @param  void* data          The buffer to be filled when reading the data.
/// @param  u16 length          Maximum data to be read
/// @return s16                 How many bytes was loaded into the buffer.
///                             If it is negative then it is an error
///                             of type kal_file_error_t.
//======================================================================
public s16 kal_file_read(u16 fd, void *data, u16 length);

//======================================================================
// kal_file_size
//----------------------------------------------------------------------
/// @brief  Return the file's current size
/// @param  u16 fd              The file descriptor.
/// @return u16                 The file size
//======================================================================
public u16 kal_file_size(u16 fd);

//======================================================================
// kal_file_seek
//----------------------------------------------------------------------
/// @brief  Place the cursor at the offset relative to the position described
///         in parameter whence.
/// @param  u16 fd              The file descriptor.
/// @param  s16 offset          The offset to place the cursor.
/// @param  kal_file_whence_t   whence           Determines how to interpret the offset parameter.
///                                              KAL_FILE_WHENCE_SET - Offset from the beginning of the file.
///                                              KAL_FILE_WHENCE_END - Offset from the end of the file.
///                                              KAL_FILE_WHENCE_CUR - Offset from the current location of the cursor.
/// @return kal_file_error_t    Error code
//======================================================================
public kal_file_error_t kal_file_seek(u16 fd, s32 offset, kal_file_whence_t whence);

//======================================================================
// kal_file_flush
//----------------------------------------------------------------------
/// @brief  Force data to be written into the flash
/// @param  u16 fd              The file descriptor.
/// @return kal_file_error_t    Error code
//======================================================================
public kal_file_error_t kal_file_flush(u16 fd);

//======================================================================
// kal_file_chmod
//----------------------------------------------------------------------
/// @brief  Change the file's permission, you will not be able to read
/// or write into the file if the new permission is incompatible with your
/// access mode.
/// @param  u16 fd              The file descriptor.
/// @param  u8  new_perm        The new permission byte.
/// @return kal_file_error_t    Error code
//======================================================================
public kal_file_error_t kal_file_chmod(u16 fd, u8 new_perm);

//======================================================================
// kal_file_lsmod
//----------------------------------------------------------------------
/// @brief  Read the file's permission
/// @param  u16 fd              The file descriptor.
/// @return u8                  permission
//======================================================================
public u8 kal_file_lsmod(const u16 fd);

//======================================================================
// kal_file_notify_modification
//----------------------------------------------------------------------
/// @brief  Perform a kal_proc_post_evt to other processes listening to the
///         same file opened by fd except to the process related to this fd.
///         Use this function if you want to force a notification without
///         flushing the file to flash, otherwise closing the file or
///         flushing it will automatically call this function.
/// @param  u16 fd  The file descriptor of an opened file.
//======================================================================
public void kal_file_notify_modification(u16 fd);

#endif // __KAL_FILE_H__
