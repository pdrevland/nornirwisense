/// @copyright
/// ========================================================================={{{
/// Copyright (c) 2013 WizziLab                                                /
/// All rights reserved                                                        /
///                                                                            /
/// IMPORTANT: This Software may not be modified, copied or distributed unless /
/// embedded on a WizziLab product. Other than for the foregoing purpose, this /
/// Software and/or its documentation may not be used, reproduced, copied,     /
/// prepared derivative works of, modified, performed, distributed, displayed  /
/// or sold for any purpose. For the sole purpose of embedding this Software   /
/// on a WizziLab product, copy, modification and distribution of this         /
/// Software is granted provided that the following conditions are respected:  /
///                                                                            /
/// *  Redistributions of source code must retain the above copyright notice,  /
///    this list of conditions and the following disclaimer                    /
///                                                                            /
/// *  Redistributions in binary form must reproduce the above copyright       /
///    notice, this list of conditions and the following disclaimer in the     /
///    documentation and/or other materials provided with the distribution.    /
///                                                                            /
/// *  The name of WizziLab can not be used to endorse or promote products     /
///    derived from this software without specific prior written permission.   /
///                                                                            /
/// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS        /
/// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED  /
/// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR /
/// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR          /
/// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,      /
/// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,        /
/// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,            /
/// OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY     /
/// OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING    /
/// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS         /
/// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.               /
/// WIZZILAB HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,       /
/// ENHANCEMENTS OR MODIFICATIONS.                                             /
///                                                                            /
/// Should you have any questions regarding your right to use this Software,   /
/// contact WizziLab at www.wizzilab.com.                                      /
///                                                                            /
/// =========================================================================}}}
/// @endcopyright

#ifndef __KAL_MATH_H__
#define __KAL_MATH_H__

#include "hal_types.h"

// =============================================================================
// General purpose constants
// =============================================================================

#define MIN_S8                      ((s8 )(0x80))
#define MAX_S8                      ((s8 )(0x7F))
#define MIN_U8                      ((u8 )(0x0))
#define MAX_U8                      ((u8 )(0xFF))

#define MIN_S12                     ((s16)(0xF800))
#define MAX_S12                     ((s16)(0x7FF))
#define MIN_U12                     ((u16)(0x0))
#define MAX_U12                     ((u16)(0xFFF))

#define MIN_S16                     ((s16)(0x8000))
#define MAX_S16                     ((s16)(0x7FFF))
#define MIN_U16                     ((u16)(0x0))
#define MAX_U16                     ((u16)(0xFFFF))

#define MIN_U32                     ((u32)(0x0))
#define MAX_U32                     ((u32)(0xFFFFFFFF))
#define MIN_S32                     ((s32)(0x80000000))
#define MAX_S32                     ((s32)(0x7FFFFFFF))

// =============================================================================
// Compare operators
// =============================================================================

#define KAL_MAX(a,b)                (((a) > (b)) ? (a) : (b))
#define KAL_MIN(a,b)                (((a) < (b)) ? (a) : (b))
#define KAL_ABS(a)                  (((a) > 0) ? (a) : (-(a)))
#define KAL_SAT_S16(a)              (((a) < MIN_S16) ? MIN_S16 : ((a) > MAX_S16) ? MAX_S16 : (s16)(a))
#define KAL_SAT_U16(a)              (((a) > MAX_U16) ? MAX_U16 : (u16)(a))
#define KAL_SAT_S8(a)               (((a) < MIN_S8)  ? MIN_S8  : ((a) > MAX_S8)  ? MAX_S8 :  (s8)(a))
#define KAL_SAT_U8(a)               (((a) > MAX_U8)  ? MAX_U8 :  (u8)(a))
#define KAL_COMP16_FAST(a,b)        (((b) - (a)) >> 15)
#define KAL_ABS16_FAST(a)           ((a) ^ ((a) >> 15))
#define KAL_ABS32_FAST(a)           ((a) ^ ((a) >> 31))

// =============================================================================
// Arithmetic operators
// =============================================================================

#define KAL_MUL(x,y)                ((x)*(y))
#define KAL_MLF(x,y,frac)           (((x)*(y)) >> (frac))


// =======================================================================
// kal_sqrt
// -----------------------------------------------------------------------
/// @brief  Fixed point square root
/// @param  x           u16         input fixed point value
/// @param  format      u8          fixed point format (shift)
/// @retval             u16         square root of the input in the given format
// =======================================================================
public u16 kal_sqrt(u16 x, u8 format);


// =======================================================================
// kal_div
// -----------------------------------------------------------------------
/// @brief  Fixed point division of two s16 values
/// @param  nom         s16         Numerator
/// @param  denom       s16         Denominator
/// @param  format      u8          fixed point format (shift)
/// @retval             s16         division result in the given format
// =======================================================================
public s16 kal_div(s16 num, s16 denom, u8 format);


// =======================================================================
// kal_log2
// -----------------------------------------------------------------------
/// @brief  Fast log2 computation
/// @param  in          u32         operand
/// @retval             u8          log2 of the operand
// =======================================================================
public u8 kal_log2(u32 in);


#endif // __KAL_MATH_H__
