/// @copyright
/// ========================================================================={{{
/// Copyright (c) 2012-2013 WizziLab                                           /
/// All rights reserved                                                        /
///                                                                            /
/// IMPORTANT: This Software may not be modified, copied or distributed unless /
/// embedded on a WizziLab product. Other than for the foregoing purpose, this /
/// Software and/or its documentation may not be used, reproduced, copied,     /
/// prepared derivative works of, modified, performed, distributed, displayed  /
/// or sold for any purpose. For the sole purpose of embedding this Software   /
/// on a WizziLab product, copy, modification and distribution of this         /
/// Software is granted provided that the following conditions are respected:  /
///                                                                            /
/// *  Redistributions of source code must retain the above copyright notice,  /
///    this list of conditions and the following disclaimer                    /
///                                                                            /
/// *  Redistributions in binary form must reproduce the above copyright       / 
///    notice, this list of conditions and the following disclaimer in the     /
///    documentation and/or other materials provided with the distribution.    /
///                                                                            /
/// *  The name of WizziLab can not be used to endorse or promote products     /
///    derived from this software without specific prior written permission.   /
///                                                                            /
/// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS        /
/// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED  /
/// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR /
/// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR          /
/// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,      /
/// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,        /
/// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,            /
/// OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY     /
/// OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING    /
/// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS         /
/// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.               /
/// WIZZILAB HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,       /
/// ENHANCEMENTS OR MODIFICATIONS.                                             /
///                                                                            /
/// Should you have any questions regarding your right to use this Software,   /
/// contact WizziLab at www.wizzilab.com.                                      /
///                                                                            /
/// =========================================================================}}}
/// @endcopyright

//  =======================================================================
/// @file           kal_timer.h
/// @brief          KAL Timers
//  =======================================================================
#ifndef _KAL_TIMER_H_
#define _KAL_TIMER_H_

#include "hal_types.h"

//========================================================================
// Enumerators and definitions
//========================================================================
#define KAL_FTIMER              0x0
#define KAL_ETIMER              0x1
#define KAL_TIM_EXPIRED         0x3
#define KAL_TIM_NOSTART         0xFF

// =======================================================================
// Types
// =======================================================================
// =======================================================================
// kal_timer_callback_t
// -----------------------------------------------------------------------
/// timer callback function type
/// looks like: void callback(u16 param)
// =======================================================================
typedef void (*kal_timer_callback_t)(u16);

//======================================================================
// Global Functions
//======================================================================

//======================================================================
// Timer Functions
//======================================================================
// kal_timer_t
// -----------------------------------------------------------------------
/// Type for timer storage
// =======================================================================
typedef struct kal_timer
{
    /// timer interval
    u16 time;
    //  timer_type
    u8  type;
    /// timer event destination process or timer callback function
    void* action;
    /// timer parameter (evt_param_t or parameter to pass to the function)
    u16 param;
    /// Pointer to next timer in list
    struct kal_timer* next;
} kal_timer_t;

//======================================================================
// kal_timer_open
//----------------------------------------------------------------------
/// @brief  Open kal timer module
//======================================================================
public void kal_timer_open(void);


//======================================================================
// kal_timer_close
//----------------------------------------------------------------------
/// @brief  Close kal timer, shut down timer
//======================================================================
public void kal_timer_close(void);


//======================================================================
// kal_timer_init
//----------------------------------------------------------------------
/// @brief initialize timer structure 
/// @param kal_timer_t* tim
/// @retval TRUE when timer is expired, FALSE otherwise
//======================================================================
public void kal_timer_init(kal_timer_t* tim);


//======================================================================
// kal_timer_start
//----------------------------------------------------------------------
/// @brief  creates a timer
/// @param  process_t* destination_proc  target process of the event generated at timer expiration
/// @param  evt_param_t evt_param  passed as event parameter when the timer expires
/// @param  time       expiration time of the timer
/// @retval timer_id    
//======================================================================
public void kal_timer_start(kal_timer_t* tim, u8 type, void* action ,u16 param, u16 time);


//======================================================================
// kal_timer_stop
//----------------------------------------------------------------------
/// @brief  stops a timer
/// @param timer_id    
//======================================================================
public void kal_timer_stop(kal_timer_t* tim);

//======================================================================
// kal_timer_gettime
//----------------------------------------------------------------------
/// @brief  returns remaining time
/// @param timer_id    
/// @retval remaining_time
//======================================================================
public u16 kal_timer_gettime(kal_timer_t* tim);

//======================================================================
// kal_timer_expired
//----------------------------------------------------------------------
/// @brief  
/// @param kal_timer_t* tim
/// @retval TRUE when timer is expired, FALSE otherwise
//======================================================================
public u8 kal_timer_expired(kal_timer_t* tim);


#endif // _KAL_TIMER_H_

