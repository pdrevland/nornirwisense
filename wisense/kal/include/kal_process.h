#ifndef _KAL_PROCESS_H_
#define _KAL_PROCESS_H_

#include "hal_types.h"
#include "hal_sys.h"
#include "pt.h"
#include "process.h"
#include "kal_events.h"
#include "kal_process_id.h"

typedef union {
    void*   msg;
    int     data;
} evt_param_t;

typedef u16 process_evt_t;

typedef struct process {
    /// Linked list pointer
    struct process*   next;
    /// Protothread function
    PT_THREAD(     (* thread)(struct pt *, process_evt_t, evt_param_t));
    /// Wake up event
    process_evt_t     wue;
    /// Wake up mask
    process_evt_t     wum;
    /// Protothread structure
    struct pt         pt;
    /// Process ID, also serves as priority (hence should be unique)
    u8                id;
    /// Process state 
    u8                state;
    /// Top of the process waiting Queue
    u8                wqt;
    /// End of the process waiting Queue
    u8                wqe;
    /// Size of the waiting Queue
    u8                wqs;
} process_t;


//======================================================================
// KAL_EVT_QUEUE_SIZE/RESTORE
//----------------------------------------------------------------------
/// @brief Size of the event queue, i.e. maximum number of simultaneously
///        pending events. Can be resized depending on needs.
//======================================================================
#define KAL_EVT_QUEUE_SIZE      (32)

extern process_t*   g_kal_proc_current;
#define KAL_PROC_CURRENT    (g_kal_proc_current)
extern process_t*   g_kal_proc_broadcast;
#define KAL_PROC_BROADCAST  (g_kal_proc_broadcast)

//======================================================================
// KAL_PROC_THREAD
//----------------------------------------------------------------------
/// @brief Macro defining a Protothread function.
//======================================================================
#define KAL_PROC_THREAD(name, ev, evp) \
static PT_THREAD(process_thread_##name(struct pt *process_pt, \
                       process_evt_t ev, \
                       evt_param_t evp))

//======================================================================
// KAL_PROCESS_DECLARE
//----------------------------------------------------------------------
/// @brief Macro declaring an existing process for external reference.
//======================================================================
#define KAL_PROCESS_DECLARE(name) extern process_t name

//======================================================================
// KAL_PROCESS
//----------------------------------------------------------------------
/// @brief Macro creating a process.
/// @param name name of the process
/// @param pid  Process ID and priority. The lower the ID, the higher 
///             the priority.
//======================================================================
#define KAL_PROCESS(name, pid) \
KAL_PROC_THREAD(name, ev, evp); \
process_t name = {  .next   = NULL, \
                    .thread = process_thread_##name, \
                    .wue    = 0, \
                    .wum    = 0, \
                    .pt.lc  = 0, \
                    .pt.idx = 0, \
                    .id     = pid, \
                    .state  = PROCESS_STATE_NONE, \
                    .wqt    = 0, \
                    .wqe    = 0, \
                    .wqs    = 0 }


//======================================================================
// KAL_PROCESS_SHARED_THREAD
//----------------------------------------------------------------------
/// @brief Macro creating a process that will execute the same thread
///        as another (root) process.
/// @param name   name of the process
/// @param pid    Process ID and priority. The lower the ID, the higher 
///               the priority.
/// @param index  number used to identify the current process during
///               thread execution. The root process will have index 0.
/// @param root_name name of the 'normaly created' process that will
///               provide the thread.
//======================================================================
#define KAL_PROCESS_SHARED_THREAD(name, pid, index, root_p) \
process_t name = {  .next   = NULL, \
                    .thread = process_thread_##root_p, \
                    .wue    = 0, \
                    .wum    = 0, \
                    .pt.lc  = 0, \
                    .pt.idx = index, \
                    .id     = pid, \
                    .state  = PROCESS_STATE_NONE, \
                    .wqt    = 0, \
                    .wqe    = 0, \
                    .wqs    = 0 }

//======================================================================
// KAL_PROC_IDX
//----------------------------------------------------------------------
/// @brief Macro returning the process index. To be used in shared threads
//======================================================================
#define KAL_PROC_IDX()  ((process_pt)->idx)

//======================================================================
// KAL_PROC_BEGIN
//----------------------------------------------------------------------
/// @brief Macro opening a process thread. To be called at the beginning
///        of any process thread function
//======================================================================
#define KAL_PROC_BEGIN()                PROCESS_BEGIN()

//======================================================================
// KAL_PROC_END
//----------------------------------------------------------------------
/// @brief Macro closing a process thread. To be called at the end
///        of any process thread function
//======================================================================
#define KAL_PROC_END()                  PROCESS_END()

//======================================================================
// KAL_PROC_WAIT_EVENT
//----------------------------------------------------------------------
/// @brief When calling this macro the process will yield and wait for
///        the scheduler to call it next time an (any) event target this
///        process.
//======================================================================
#define KAL_PROC_WAIT_EVENT()           PROCESS_WAIT_EVENT()

//======================================================================
// KAL_PROC_WAIT_EVENT_UNTIL(c)
//----------------------------------------------------------------------
/// @brief Same as KAL_PROC_WAIT_EVENT but additionally checks for the
///        condition c to be TRUE.
/// @param c condition to be realized for the process to carry on.
//======================================================================
#define KAL_PROC_WAIT_EVENT_UNTIL(c)    PROCESS_WAIT_EVENT_UNTIL(c)
#define KAL_PROC_WAIT_EVENT_WHILE(c)    PROCESS_WAIT_EVENT_UNTIL(!(c))

//======================================================================
// KAL_PROC_WAIT_UNTIL_EVENT(e)
//----------------------------------------------------------------------
/// @brief Same as KAL_PROC_WAIT_EVENT but the process will be
///        rescheduled only when requested event will arise. All others
///        events targeting this process will be stored in the queue w/o
///        being scheduled.
/// @param e wakeup event awaited before being rescheduled
//======================================================================
#define KAL_PROC_WAIT_UNTIL_EVENT(e)    do { \
    KAL_PROC_CURRENT->wue = e; \
    KAL_PROC_CURRENT->wum = 0xFFFF; \
    PT_YIELD_FLAG = 0; \
    LC_SET((process_pt)->lc); \
    if(PT_YIELD_FLAG == 0) { \
        return PT_WAIT_WUE; \
    } \
} while(0)

//======================================================================
// KAL_PROC_WAIT_UNTIL_EVENT_RANGE(e,m)
//----------------------------------------------------------------------
/// @brief Same as KAL_PROC_WAIT_UNTIL_EVENT except that events are
///        going through a binary mask before being checked, allowing
///        a range of events to actually wakeup the process.
///        Events values/masks have to be carefully designed to use this
///        feature.
/// @param e wakeup event awaited before being rescheduled
/// @param m binary mask applied to incoming events and wake-up event
///        before they get compared.
//======================================================================
#define KAL_PROC_WAIT_UNTIL_EVENT_RANGE(e,m)    do { \
    KAL_PROC_CURRENT->wue = (process_evt_t)(e) & (process_evt_t)(m); \
    KAL_PROC_CURRENT->wum = (process_evt_t)m; \
    PT_YIELD_FLAG = 0; \
    LC_SET((process_pt)->lc); \
    if(PT_YIELD_FLAG == 0) { \
        return PT_WAIT_WUE; \
    } \
} while(0)

//======================================================================
// KAL_PROC_YIELD
//----------------------------------------------------------------------
/// @brief When calling this macro the process will yield, allowing all
///        higher priority processes to be executed before being
///        rescheduled.
//======================================================================
#define KAL_PROC_YIELD(evp)                do  { \
    kal_proc_post_evt(KAL_PROC_CURRENT, KAL_EVT_YIELD, evp); \
    PROCESS_YIELD(); \
} while(0)

//======================================================================
// KAL_PROC_YIELD_UNTIL
//----------------------------------------------------------------------
/// @brief Same as KAL_PROC_YIELD but additionally checks for the
///        condition c to be TRUE.
///        A process that is yielding will not get any event until it
///        stops yielding. If events targeting this process arise during
///        the 'yield-period' they will be queued.
///        Also note that a process 'yielding' will continuously be
///        rescheduled preventing the system to go into low-power mode.
/// @param c condition to be realized for the process to carry on.
//======================================================================
#define KAL_PROC_YIELD_UNTIL(c,evp)         do  { \
    PT_YIELD_FLAG = 0; \
    LC_SET((process_pt)->lc); \
    if((PT_YIELD_FLAG == 0) || !(c)) { \
        kal_proc_post_evt(KAL_PROC_CURRENT, KAL_EVT_YIELD, evp); \
        return PT_YIELDED; \
    } \
} while(0)
#define KAL_PROC_YIELD_WHILE(c,evp)         KAL_PROC_YIELD_UNTIL(!(c),evp)


//======================================================================
// KAL_PROC_EXIT
//----------------------------------------------------------------------
/// @brief Stop the current process and remove it from process list
//======================================================================
#define KAL_PROC_EXIT()                 PROCESS_EXIT()

//======================================================================
// KAL_PROC_RESTART
//----------------------------------------------------------------------
/// @brief Re-initialize current process.
//======================================================================
#define KAL_PROC_RESTART()              do  { \
    kal_proc_post_evt(KAL_PROC_CURRENT, KAL_EVT_EXIT, (evt_param_t)NULL); \
    kal_proc_post_evt(KAL_PROC_CURRENT, KAL_EVT_INIT, (evt_param_t)NULL); \
    LC_SET((process_pt)->lc);				\
    return PT_YIELDED;			\
} while(0)

//======================================================================
// KAL_PROC_EXITHANDLER
//----------------------------------------------------------------------
/// @brief Macro to be placed before KAL_PROC_BEGIN, that will intercept
///        EXIT event and allow to execute specific exit code.
/// @param handler specific code to be executed
//======================================================================
#define KAL_PROC_EXITHANDLER(handler)   if(ev == KAL_EVT_EXIT) { handler; }

//======================================================================
// KAL_PROC_RESTART
//----------------------------------------------------------------------
/// @brief Macro to be placed before KAL_PROC_BEGIN, that will intercept
///        INIT event and allow to execute specific initialization code.
/// @param handler specific code to be executed
//======================================================================
#define KAL_PROC_INITHANDLER(handler)   if(ev == KAL_EVT_INIT) { handler; }

//======================================================================
// KAL_PROC_PT_SPAWN
//----------------------------------------------------------------------
/// @brief Allows to run a 'child' process inside another. The child
///        will be scheduled in place of the parent (on parent's events)
///        until it exits, the parent will then resume after the
///        spawning point.
/// @param child process
//======================================================================
#define KAL_PROC_PT_SPAWN(child)   PT_SPAWN(process_pt, \
                                            &(&child)->pt, \
                                            (&child)->thread(&(&child)->pt,ev,evp))

#define KAL_PROC_RUNNING(p)             ((p)->state != PROCESS_STATE_NONE)

//======================================================================
// PROCESS_CONTEXT_SWITCH/RESTORE
//----------------------------------------------------------------------
/// @brief Macros to temporary switch context to the specified process.
///        PROCESS_CONTEXT_SWITCH() must be followed by PROCESS_CONTEXT_RESTORE()
///        The code in-between is executed as if runned by the target process.
/// @param p    The process to use as context
//======================================================================
#define KAL_PROC_CONTEXT_SWITCH(p) {\
    process_t* tmp = KAL_PROC_CURRENT;\
    g_kal_proc_current = p

#define KAL_PROC_CONTEXT_RESTORE(p) g_kal_proc_current = tmp; }

//======================================================================
// kal_proc_open
//----------------------------------------------------------------------
/// @brief  Opens KAL's process scheduling service
//======================================================================
void kal_proc_open(void);

//======================================================================
// kal_proc_start
//----------------------------------------------------------------------
/// @brief  Insert a new process to the scheduler list and initialize it
/// @param  p       target process
/// @param  evp     optional parameter passed to the process during init
//======================================================================
void kal_proc_start(process_t *p, evt_param_t evp);

//======================================================================
// kal_proc_exit
//----------------------------------------------------------------------
/// @brief  Closes a currently running process.
/// @param  p   target process
//======================================================================
void kal_proc_exit(process_t* p);

//======================================================================
// kal_proc_post_evt
//----------------------------------------------------------------------
/// @brief  Posts an event to a target process. Handling of the event
///         will be effective when the target process will be scheduled
///         by kal_proc_run.
///         Target process can be BROADCAST, in which case all active
///         processes will receive the event.
/// @todo   define BROADCAST process priority
/// @param  p   target process
/// @param  ev  event to be sent to the process
/// @param  evp event parameter (value or pointer depending on event)
//======================================================================
int kal_proc_post_evt(process_t* p, process_evt_t ev, evt_param_t evp);

//======================================================================
// kal_proc_schedule
//----------------------------------------------------------------------
/// @brief  Run the process sheduler. This function never returns and
///         schedules relevant processes execution when events arise.
///         It automatically keeps the system into sleep when no events
///         are pending.
//======================================================================
void kal_proc_schedule(void);

//======================================================================
// kal_proc_get_usr_evt
//----------------------------------------------------------------------
/// @brief  Return a free event ID that can be used for application
///         purposes.
/// @return user event ID
//======================================================================
u16 kal_proc_get_usr_evt(void);

#endif // _KAL_PROCESS_H_
