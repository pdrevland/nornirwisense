/// @copyright
/// ========================================================================={{{
/// Copyright (c) 2012-2013 WizziLab                                           /
/// All rights reserved                                                        /
///                                                                            /
/// IMPORTANT: This Software may not be modified, copied or distributed unless /
/// embedded on a WizziLab product. Other than for the foregoing purpose, this /
/// Software and/or its documentation may not be used, reproduced, copied,     /
/// prepared derivative works of, modified, performed, distributed, displayed  /
/// or sold for any purpose. For the sole purpose of embedding this Software   /
/// on a WizziLab product, copy, modification and distribution of this         /
/// Software is granted provided that the following conditions are respected:  /
///                                                                            /
/// *  Redistributions of source code must retain the above copyright notice,  /
///    this list of conditions and the following disclaimer                    /
///                                                                            /
/// *  Redistributions in binary form must reproduce the above copyright       / 
///    notice, this list of conditions and the following disclaimer in the     /
///    documentation and/or other materials provided with the distribution.    /
///                                                                            /
/// *  The name of WizziLab can not be used to endorse or promote products     /
///    derived from this software without specific prior written permission.   /
///                                                                            /
/// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS        /
/// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED  /
/// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR /
/// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR          /
/// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,      /
/// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,        /
/// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,            /
/// OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY     /
/// OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING    /
/// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS         /
/// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.               /
/// WIZZILAB HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,       /
/// ENHANCEMENTS OR MODIFICATIONS.                                             /
///                                                                            /
/// Should you have any questions regarding your right to use this Software,   /
/// contact WizziLab at www.wizzilab.com.                                      /
///                                                                            /
/// =========================================================================}}}
/// @endcopyright

//  =======================================================================
/// @file           kal_dbg.h
/// @brief          KAL debug functions
//  =======================================================================

#ifndef __KAL_DBG_H__
#define __KAL_DBG_H__
#include "stdarg.h"
#include "hal_isr.h"

// Enable HEAP profiling. Not intrusive, can be enabled by default
#define _KAL_PROFILE_HEAP_

// Enable ISR profiling. Rather intrusive.
//#define _KAL_PROFILE_ISR_

// Enable Malloc profiling. Not intrusive, can be enabled by default
#define _KAL_PROFILE_MALLOC_

// Enable Events profiling. Not intrusive, can be enabled by default
#define _KAL_PROFILE_EVT_

// Enable Malloc tracing.
// XXX this is very verbose and intrusive, use it only for specific debug
//define _KAL_TRACE_KAL_MALLOC_
//define _KAL_TRACE_RAC_MALLOC_
//define _KAL_TRACE_MAC_MALLOC_
//define _KAL_TRACE_ALP_MALLOC_
//define _KAL_TRACE_TRA_MALLOC_
//define _KAL_TRACE_DRV_MALLOC_
//define _KAL_TRACE_APP_MALLOC_

/// @ingroup KAL
/// @defgroup KAL_DBG_API       KAL Debug and trace functions
/// @{

//======================================================================
// kal_dbg_set_assert_handler
//----------------------------------------------------------------------
/// @brief  User defined handler in case of reset
/// @param  handler         hal_isr_t           user handler (may be NULL)
/// @retval                 void
//======================================================================
public void kal_dbg_set_assert_handler(hal_isr_t handler);

//======================================================================
// kal_dbg_set_mode
//----------------------------------------------------------------------
/// @brief  Select mode for serial output
/// @param  mode : serial mode
/// @todo work this out
//======================================================================
public void kal_dbg_set_mode(u8 mode);

//======================================================================
// kal_dbg_printf
//----------------------------------------------------------------------
/// @brief  Print debug information over serial link
/// @param  format      string to print, taking printf-style arguments
//======================================================================
public void kal_dbg_printf(const char* format,...);

//======================================================================
// kal_dbg_dump
//----------------------------------------------------------------------
/// @brief  Dump buffer information over Serial
/// @param  buf     u8*         pointer on the buffer to print
/// @param  len     u8          length of the buffer to print
/// @param  id      u8          ascii or hex format
/// @retval         void
//======================================================================
public void kal_dbg_dump(u8* buf, u8 len, u8 id);

//======================================================================
// kal_dbg_assert
//----------------------------------------------------------------------
/// @brief  Print debug information over serial link
/// @param  cond        assert condition
/// @param  format      string to print, taking printf-style arguments
//======================================================================
public void kal_dbg_assert(s16 cond, const char* format,...);

public void kal_dbg_print_sysinfo(void);
public void* kal_dbg_malloc(size_t size, const char* f, uint l);

/// @} kal_dbg_API

//======================================================================
// kal_dbg_t
//----------------------------------------------------------------------
/// @brief  Debug structure that can be dumped on request
//======================================================================
typedef struct {
    /// start of heap
    u32 heap_start;
    /// max heap top
    u32 heap_max;
    /// max time in isr
    u16 isr_tmax;
    /// heaviest isr
    u8  isr_id;
    /// max time in cs
    u16 cs_tmax;
    /// Malloc/Free delta
    s16 malloc_delta;
    /// Max number of events stacked
    u8  max_evt;
} kal_dbg_t;

extern kal_dbg_t g_kal_dbg;

//======================================================================
// HardWare Trace
// Defining the __HAL_HWT_ENABLED__ compile flag takes over GPIOs to
// trace a set of (software) events. This is used for debug/benchmarks,
// and will obviously interfer with any applicative use of IOs.
// Use and modify it with care !
//======================================================================

//#define __HAL_HWT_ENABLED__
#ifdef __HAL_HWT_ENABLED__
#warning "Using __HAL_HWT_ENABLED__ compile flag will interfer with any application using IOs !!!"
#else
#define HWT_SETUP       do{}while(0)
#define HWT_TX_ON       do{}while(0)
#define HWT_TX_OFF      do{}while(0)
#define HWT_RX_ON       do{}while(0)
#define HWT_RX_OFF      do{}while(0)
#define HWT_RAC_ON      do{}while(0)
#define HWT_RAC_OFF     do{}while(0)
#define HWT_SLEEP_ON    do{}while(0)
#define HWT_SLEEP_OFF   do{}while(0)
#define HWT_DBG0_ON     do{}while(0)
#define HWT_DBG0_OFF    do{}while(0)
#define HWT_DBG1_ON     do{}while(0)
#define HWT_DBG1_OFF    do{}while(0)
#endif


#endif // __KAL_DBG_H__
