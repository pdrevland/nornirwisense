/// @copyright
/// ========================================================================={{{
/// Copyright (c) 2012-2013 WizziLab                                           /
/// All rights reserved                                                        /
///                                                                            /
/// IMPORTANT: This Software may not be modified, copied or distributed unless /
/// embedded on a WizziLab product. Other than for the foregoing purpose, this /
/// Software and/or its documentation may not be used, reproduced, copied,     /
/// prepared derivative works of, modified, performed, distributed, displayed  /
/// or sold for any purpose. For the sole purpose of embedding this Software   /
/// on a WizziLab product, copy, modification and distribution of this         /
/// Software is granted provided that the following conditions are respected:  /
///                                                                            /
/// *  Redistributions of source code must retain the above copyright notice,  /
///    this list of conditions and the following disclaimer                    /
///                                                                            /
/// *  Redistributions in binary form must reproduce the above copyright       / 
///    notice, this list of conditions and the following disclaimer in the     /
///    documentation and/or other materials provided with the distribution.    /
///                                                                            /
/// *  The name of WizziLab can not be used to endorse or promote products     /
///    derived from this software without specific prior written permission.   /
///                                                                            /
/// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS        /
/// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED  /
/// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR /
/// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR          /
/// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,      /
/// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,        /
/// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,            /
/// OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY     /
/// OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING    /
/// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS         /
/// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.               /
/// WIZZILAB HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,       /
/// ENHANCEMENTS OR MODIFICATIONS.                                             /
///                                                                            /
/// Should you have any questions regarding your right to use this Software,   /
/// contact WizziLab at www.wizzilab.com.                                      /
///                                                                            /
/// =========================================================================}}}
/// @endcopyright

//  =======================================================================
/// @file           kal_com.h
/// @brief          Public header of kal_com.c
//  =======================================================================


#ifndef __KAL_COM_H__
#define __KAL_COM_H__

#include "hal_types.h"
#include "hal_error.h"
#include "kal.h"
#include "hal_defs.h"

/// XXX TODO PRIVATE/PUBLIC ?
// ALL THIS WAS PUBLIC

//======================================================================
// kal_com_fid_t
//----------------------------------------------------------------------
/// Enumerator of serial Flow-ids
//======================================================================
typedef enum
{
    /// Trace channel
    KAL_COM_FLOWID_TRC = 0,
    /// General purpose Command channel
    KAL_COM_FLOWID_CMD,
    /// ALP channel
    KAL_COM_FLOWID_ALP,
    /// System notifications
    KAL_COM_FLOWID_SYS,

    KAL_COM_FLOWID_QTY
} kal_com_fid_t;
#define KAL_COM_FLOW(fid,type)      ((((fid)&0x7)<<4) | ((type)&0xF))
#define KAL_COM_FLOWID(id)          (((id)>>4)&0x7)

#define KAL_COM_FLOWID_REDIRECT     0x80

//======================================================================
// kal_com_flow_t
//----------------------------------------------------------------------
/// Enumerator of serial flows
//======================================================================
typedef enum
{
    /// Default printf type
    KAL_COM_FLOW_PRINTF                     = KAL_COM_FLOW(KAL_COM_FLOWID_TRC,0),
    /// Substitute the string by a codeword
    /// interpreted by the PC com tool
    KAL_COM_FLOW_PRINTF_COMPRESSED          = KAL_COM_FLOW(KAL_COM_FLOWID_TRC,1),
    /// Display the payload as hex data
    KAL_COM_FLOW_PRINT_HEX                  = KAL_COM_FLOW(KAL_COM_FLOWID_TRC,2),

    /// AT command
    KAL_COM_FLOW_AT_CMD                     = KAL_COM_FLOW(KAL_COM_FLOWID_ALP,0),
    /// AT command response
    KAL_COM_FLOW_AT_RESP                    = KAL_COM_FLOW(KAL_COM_FLOWID_ALP,1),
    /// AT unsolicited message
    KAL_COM_FLOW_AT_UNS                     = KAL_COM_FLOW(KAL_COM_FLOWID_ALP,2),
    /// AT unsolicited error
    KAL_COM_FLOW_AT_ERR                     = KAL_COM_FLOW(KAL_COM_FLOWID_ALP,3),

    /// Remote Commands
    KAL_COM_FLOW_CMD                        = KAL_COM_FLOW(KAL_COM_FLOWID_CMD,0),

    /// Remote System reset
    KAL_COM_FLOW_SYS_RST                    = KAL_COM_FLOW(KAL_COM_FLOWID_SYS,0),
    /// Button Emulator
    KAL_COM_FLOW_SYS_BTN                    = KAL_COM_FLOW(KAL_COM_FLOWID_SYS,1),
    /// Dump Debug parameters
    KAL_COM_FLOW_SYS_DBG                    = KAL_COM_FLOW(KAL_COM_FLOWID_SYS,2),
    /// CUP signalisation
    KAL_COM_FLOW_SYS_CUP                    = KAL_COM_FLOW(KAL_COM_FLOWID_SYS,3),

} kal_com_flow_t;


/// Maximum number of printf args in a transmit message
#define KAL_COM_TX_MSG_MAX_ARGS     8

//======================================================================
// kal_com_tx_msg_t
//----------------------------------------------------------------------
/// Transmit message structure
//======================================================================
typedef struct
{
    /// identifier kal_com_flow_t
    u8  id;
    /// length of the string buffer defined by a pointer
    u8  plen;
    /// length of the allocated buffer
    u8  alen;
    /// pointer to a string buffer that does not need to be freed
    u8* pbuf;
    /// pointer to argument that does not need to be freed
    u8* abuf;

} kal_com_tx_msg_t;





//======================================================================
// kal_com_rx_msg_t
//----------------------------------------------------------------------
/// Receive message structure
//======================================================================
typedef struct
{
    /// error message
    hal_error_t err;
    /// length of the log (string) buffer
    u8  blen;
    /// identifier kal_com_flow_t
    u8  id;
    /// Com port where the message came from
    u8 com_id;
    /// pointer to the log buffer
    u8  buffer[1];

} kal_com_rx_msg_t;

/// Use this macro to allocate a RX string buffer of the desired size
#define KAL_COM_RX_MSG_MALLOC_LEN(len)   sizeof(kal_com_rx_msg_t) - 1 + len


/// @}KAL_COM_STR

/// @ingroup KAL_COM
/// @defgroup KAL_COM_API    HAL Serial COM Link API
/// @{

//======================================================================
// kal_com_open
//----------------------------------------------------------------------
/// @brief  Open COM module
//======================================================================
public hal_error_t kal_com_open(void);

//======================================================================
// kal_com_map
//----------------------------------------------------------------------
/// @brief  Maps a WIZ_PKT COM port Flow ID to a target process.
///         Opens the corresponding COM device if needed.
/// @param  com_id COM port index. COM ports are target specific.
/// @param  flow_id flow ID used for demultiplexing.
/// @param  proc_func Pointer to the target process or rx callback
///         function
/// @retval 0 on success, negative on error
//======================================================================
public hal_error_t kal_com_map(u8 com_id, u8 flow_id, void* proc_func);

//======================================================================
// kal_com_set_redirection
//----------------------------------------------------------------------
/// @brief  Redirects a WL_PKT COM port to another WL_PKT COM port
///         Must be done before ports are open
/// @param  com_id_src COM port index of source COM port
/// @param  com_id_dst COM port index of destination COM port
/// @retval 0 on success, negative on error
//======================================================================
public hal_error_t kal_com_set_redirection(u8 com_id_src, u8 com_id_dst);


//======================================================================
// kal_com_close
//----------------------------------------------------------------------
/// @brief  Close COM Port
/// @param  com_id COM port index. COM ports are target specific.
/// @retval 0 on success, negative on error
//======================================================================
public hal_error_t kal_com_close(u8 com_id);


//======================================================================
// kal_com_busy
//----------------------------------------------------------------------
/// @brief  Returns if COM Port is busy or IDLE
/// @param  com_id COM port index. COM ports are target specific.
/// @retval TRUE when busy, FALSE when idle
//======================================================================
public bool kal_com_busy(u8 com_id);


//======================================================================
// kal_com_send_msg
//----------------------------------------------------------------------
/// @brief  Send a message over the serial messenger
/// @param  com_id COM port index. COM ports are target specific.
/// @param  msg    pointer to the message to send
/// @retval 0 on success, negative on error
//======================================================================
public hal_error_t kal_com_send_msg(u8 com_id, kal_com_tx_msg_t* msg);


//======================================================================
// kal_com_vprintf
//----------------------------------------------------------------------
/// @brief  Print debug information over COM port
/// @param  com_id COM port index. COM ports are target specific.
/// @param  flow	type of flow amongst kal_com_flow_t
/// @param  format      string to print, with printf-style format
/// @param  ap          variadic-argument list type
//======================================================================
public void kal_com_vprintf(u8 com_id, u8 flow, const char* format, va_list ap);

//======================================================================
// kal_com_dump
//----------------------------------------------------------------------
/// @brief  Dump buffer information over COM
/// @param  com_id  u8          COM port index. COM ports are target specific.
/// @param  buf     u8*         pointer on the buffer to print
/// @param  len     u8          length of the buffer to print
/// @param  flow	kal_com_flow_t  Type of flow amongst kal_com_flow_t
/// @retval         void
//======================================================================
public void kal_com_dump(u8 com_id, u8* buf, u8 len, kal_com_flow_t flow);

//======================================================================
// kal_com_flush
//----------------------------------------------------------------------
/// @brief  Flush the TX channel (blocking if DMA is busy)
/// @param  com_id COM port index. COM ports are target specific.
//======================================================================
public void kal_com_flush(u8 com_id);

//======================================================================
// kal_com_sleep
//----------------------------------------------------------------------
/// @brief  Sets serial module in sleep mode
/// @param  com_id COM port index. COM ports are target specific.
//======================================================================
public void kal_com_sleep(u8 com_id);

//======================================================================
// kal_com_wakeup
//----------------------------------------------------------------------
/// @brief  Sets serial module in sleep mode
/// @param  com_id COM port index. COM ports are target specific.
//======================================================================
public void kal_com_wakeup(u8 com_id);

/// XXX TODO PRIVATE/PUBLIC ?
// ALL THIS WAS PRIVATE

// =======================================================================
//
//  MACROS AND DEFINES
//
// =======================================================================
// ----------------------------------
// Serial buffers
// ----------------------------------

/// Size of the TX message buffer
#define KAL_COM_TX_BUFFER_BYTE_LEN   512

/// Size of the RX message buffer
#define KAL_COM_RX_BUFFER_BYTE_LEN   256


// ----------------------------------
// WL Serial protocol
// ----------------------------------

/// +--------------+--------+--------+--------+---- - - - - - - - - - - --------+
/// |     SYNC     |   LEN  |   SEQ  |   ID   |             PAYLOAD             |
/// +--------------+--------+--------+--------+---- - - - - - - - - - - --------+
///
///      2 bytes     1 byte   1 byte   1 byte              LEN bytes
/// |<------------>|<------>|<------>|<------>|<--- - - - - - - - - - - ------->|


// first byte of the sync word 
// (ASCII start of heading)
#define KAL_COM_SYNC_BYTE_0          0x01

// second byte of the sync word 
// (ASCII group separator)
#define KAL_COM_SYNC_BYTE_1          0x1F

// message header length in byte
#define KAL_COM_HEADER_LEN           5

// ----------------------------------
// BG Serial protocol
// ----------------------------------

/// --------+----------+---------+----------+--------+- - - - - - -+
/// CMD LEN | MSG TYPE | LEN LOW | Class ID | CMD ID | PAYLOAD     |
/// --------+----------+---------+----------+--------+- - - - - - -+
///         |           CMD LEN bytes
/// 1 byte  |  1 bytes    1 byte     1 byte    1 byte    LEN bytes
/// <------>|<-------->|<------->|<-------->|<------>|<--- - - - ->|
///         bit7 0=CMD/RESP
///              1=EVT
///         bit6:3 0000=BT4.0
///         bit2:0 LEN HIGH
/// In Rx Mode, the first byte len is omitted

#define KAL_COM_BG_HEADER_LEN        4
#define KAL_COM_BG_MAX_LEN           (64+KAL_COM_BG_HEADER_LEN)


// ----------------------------------
// FIFO management
// ----------------------------------

// FIFO read pointer
#define KAL_COM_FIFO_RD_PTR(fifo)    ((fifo).start + (fifo).read)

// FIFO write pointer
#define KAL_COM_FIFO_WR_PTR(fifo)    ((fifo).start + (fifo).write)
//
// =======================================================================
//
//  ENUMERATORS AND STRUCTURES
//
// =======================================================================
#define KAL_COM_RX_BUSY     (u16)(0x01)
#define KAL_COM_TX_BUSY     (u16)(0x02)

// =======================================================================
// kal_com_state_t
// -----------------------------------------------------------------------
/// Enumerator of serial states
// =======================================================================
typedef enum
{
    /// Off
    KAL_COM_STATE_OFF,
    /// Idle, nothing to do
    KAL_COM_STATE_IDLE,
    /// Receive message header
    KAL_COM_STATE_RX_HEADER,
    /// Receive message body
    KAL_COM_STATE_RX_BODY,
    /// Send message
    KAL_COM_STATE_TX,

} kal_com_state_t;


// =======================================================================
// kal_com_fifo_t
// -----------------------------------------------------------------------
/// FIFO structure for the serial used to queue messages
// =======================================================================
typedef struct
{
    /// Start address of the FIFO
    u8* start;
    /// Size of the FIFO
    u16 size;
    /// Read pointer (offset vs start)
    u16 read;
    /// Write pointer (offset vs start)
    u16 write;
    /// Allocated memory, but not filled (for multiple entries)
    u16 ongoing;
    /// Filled (used) memory
    u16 filled;
    /// Number functions accessing simultaneously the same FIFO
    u16 users;

} kal_com_fifo_t;

// forward com structure
struct _kal_com_port_t;

typedef void (*kal_com_cb_t)(struct _kal_com_port_t* com);

typedef u16 (*kal_com_devio_t)(struct _kal_com_port_t* com, u8* addr, u16 len);

typedef u16 (*kal_com_devfx_t)(struct _kal_com_port_t* com);

typedef u16 (*kal_com_usr_rx_cb_t)(char c);

typedef struct
{
    u8  type;
    u8  dev_id;
    u8  baudrate;
    u8  use_tx_dma;
    u8  use_rx_dma;
    u8  mode;
    u16 txbuf_size;
    u8  wu_setup;
    u8  wu_out_pin;
    u8  wu_in_pin;
} kal_com_def_t;

// =======================================================================
// kal_com_dev_t
// -----------------------------------------------------------------------
/// Structure of a serial Device
// =======================================================================
typedef struct
{
    /// Serial Device type
    u8                      type; 
    /// ID or Index of the device
    u8                      id;
    /// Device Functions
    kal_com_devio_t         get;
    kal_com_devio_t         put;
    kal_com_devfx_t         close;
    kal_com_devfx_t         flush;
} kal_com_dev_t;

#define KAL_COM_REDIRECT_SRC   0x0
#define KAL_COM_REDIRECT_DST   0x1

// =======================================================================
// kal_com_port_t
// -----------------------------------------------------------------------
/// Structure of a serial COM port
// =======================================================================
typedef struct _kal_com_port_t
{
    /// COM Port definition (points to a static structure definied at compile time)
    kal_com_def_t*          def;
    /// Serial device used by the COM port
    kal_com_dev_t           dev;
    /// Function called by device after Rx IRQ
    kal_com_cb_t            rx_callback;
    /// Function called by device after Tx IRQ
    kal_com_cb_t            tx_callback;

    /// COM port index
    u8                      id;
    /// Serial Mode
    kal_com_mode_t          mode;
    /// TX sequence number ID
    u8                      tx_seq;
    /// RX sequence number ID
    u8                      rx_seq;
    /// Target process for sent events
    process_t*              process[KAL_COM_FLOWID_QTY];
    /// Redirection to/from com id
    u8                      redirect_com_id;
    /// Redirection direction
    /// KAL_COM_REDIRECT_SRC when this port is the source
    /// KAL_COM_REDIRECT_DST when this port is the destination
    u8                      redirect_direction;

    /// TX state
    volatile kal_com_state_t         tx_state;
    /// RX state
    kal_com_state_t         rx_state;
    /// TX fifo structure
    kal_com_fifo_t          tx_fifo;
    /// RX header (not used as fifo, as there is an single source, for header part only)
    u8                      rx_header[KAL_COM_HEADER_LEN];
    /// RX msg pointer
    kal_com_rx_msg_t*       rx;
    /// Keep the size of last Tx transfer
    u16                     tx_sz;
    /// Timer for "delayed put" feature
    kal_timer_t             tim;
    /// Flag the "gpio wake-up" usage
    u8                      wu;
} kal_com_port_t;

/// Flags for the "gpio wake-up" usage
typedef enum
{
    KAL_COM_WU_OUT = 1,
    KAL_COM_WU_IN = 2,
} kal_com_wu_t;

// =======================================================================
// kal_com_t
// -----------------------------------------------------------------------
/// Structure of the COM global state
// =======================================================================
typedef struct
{
    /// COM ports
    kal_com_port_t          com[KAL_COM_NB];
    /// TX buffer (used as fifo, to allow multiple sources)
    volatile u8             tx_buffer[KAL_COM_TOTAL_TXBUF_LEN];
} kal_com_t;

#endif // __KAL_COM_H__

// vim:fdm=marker:fdc=2
