/// @copyright
/// ========================================================================={{{
/// Copyright (c) 2012-2013 WizziLab                                           /
/// All rights reserved                                                        /
///                                                                            /
/// IMPORTANT: This Software may not be modified, copied or distributed unless /
/// embedded on a WizziLab product. Other than for the foregoing purpose, this /
/// Software and/or its documentation may not be used, reproduced, copied,     /
/// prepared derivative works of, modified, performed, distributed, displayed  /
/// or sold for any purpose. For the sole purpose of embedding this Software   /
/// on a WizziLab product, copy, modification and distribution of this         /
/// Software is granted provided that the following conditions are respected:  /
///                                                                            /
/// *  Redistributions of source code must retain the above copyright notice,  /
///    this list of conditions and the following disclaimer                    /
///                                                                            /
/// *  Redistributions in binary form must reproduce the above copyright       / 
///    notice, this list of conditions and the following disclaimer in the     /
///    documentation and/or other materials provided with the distribution.    /
///                                                                            /
/// *  The name of WizziLab can not be used to endorse or promote products     /
///    derived from this software without specific prior written permission.   /
///                                                                            /
/// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS        /
/// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED  /
/// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR /
/// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR          /
/// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,      /
/// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,        /
/// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,            /
/// OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY     /
/// OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING    /
/// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS         /
/// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.               /
/// WIZZILAB HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,       /
/// ENHANCEMENTS OR MODIFICATIONS.                                             /
///                                                                            /
/// Should you have any questions regarding your right to use this Software,   /
/// contact WizziLab at www.wizzilab.com.                                      /
///                                                                            /
/// =========================================================================}}}
/// @endcopyright

//  =======================================================================
/// @file           kal_string.h
/// @brief          usual string-related functions
//  =======================================================================
#ifndef _KAL_STRING_H_
#define _KAL_STRING_H_

#define __need_size_t
#include <stddef.h>
#include "stdarg.h"
#include "stdio.h"
#include "string.h"

//======================================================================
// strcmp
//----------------------------------------------------------------------
/// @brief  Compare 2 strings.
/// @param  s1              char*                   1st string to compare
/// @param  s2              char*                   2nd string to compare
/// @retval                 u32                     0 if match
//======================================================================
public int strcmp(const char *s1, const char *s2);

//======================================================================
// strncmp
//----------------------------------------------------------------------
/// @brief  Compare 2 strings on N bytes
/// @param  s1              char*                   1st string to compare
/// @param  s2              char*                   2nd string to compare
/// @param  len             uint                  maximum length to compare
/// @retval                 u32                     0 if match
//======================================================================
public int strncmp(const char *s1, const char *s2, uint n);

//======================================================================
// strcpy
//----------------------------------------------------------------------
/// @brief  Copy string
/// @param  to              char*                   Destination string
/// @param  from            char*                   Source string
/// @retval                 char*                   start of the destination string
//======================================================================
public char * strcpy(char *to, const char *from);

//======================================================================
// strncpy
//----------------------------------------------------------------------
/// @brief  Copy string, truncating or null-padding to always copy n bytes
/// @param  dst             char*                   Destination string
/// @param  src             char*                   Source string
/// @param  n               uint                  maximum length to compare
/// @retval                 char*                   start of the destination string
//======================================================================
public char * strncpy(char *dst, const char *src, uint n);

//======================================================================
// strlen
//----------------------------------------------------------------------
/// @brief  Length of a string
/// @param  str             char*                   String to evaluate
/// @retval                 uint                  string length result
//======================================================================
public uint strlen(const char *str);

//======================================================================
// strnlen
//----------------------------------------------------------------------
/// @brief  Length of a string, limited to N chars
/// @param  str             char*                 String to evaluate
/// @param  len             uint                  maximum length of the string
/// @retval                 uint                  string length result
//======================================================================
public uint strnlen(const char *str, uint len);

//======================================================================
// strsep
//----------------------------------------------------------------------
/// @brief  Get next token from string *stringp, where tokens are possibly-empty
/// strings separated by characters from delim.  
///
/// Writes NULs into the string at *stringp to end tokens.
/// delim need not remain constant from call to call.
/// On return, *stringp points past the last NUL written (if there might
/// be further tokens), or is NULL (if there are definitely no more tokens).

/// If *stringp is NULL, strsep returns NULL.
/// 
/// @param  stringp         char**                  Pointer to the string to parse
/// @param  len             const char *            delimiter
/// @retval                 char*                   result string until delimiter
//======================================================================
public char * strsep(char **stringp, const char *delim);

//======================================================================
// strtok
//----------------------------------------------------------------------
/// @brief Get string until token. Advance pointer
/// @param  s               char*                   string to parse
/// @param  len             const char *            delimiter
/// @retval                 char*                   result string until delimiter
//======================================================================
public char * strtok(char *s, const char *delim);

//======================================================================
// vsnprintf
//----------------------------------------------------------------------
/// @brief  Write formatted data from variable argument list to sized buffer
/// @param  buf             char*                   Pointer to a buffer where the resulting C-string is store
///                                                 The buffer should have a size of at least n characters.
/// @param  size            uint                    Maximum number of bytes to be used in the buffer.
/// @param  fmt             const char*             format
/// @param  ap              va_list                 variable arguments list
/// @retval                 int                     size of the resulting string
//======================================================================
public int vsnprintf(char *buf, size_t size, const char *fmt, va_list ap);

//======================================================================
// vsprintf
//----------------------------------------------------------------------
/// @brief  Write formatted data from variable argument list to unsized buffer
/// @param  buf             char*                   Pointer to a buffer where the resulting C-string is store
///                                                 The buffer should have a size of at least n characters.
/// @param  fmt             const char*             format
/// @param  ap              va_list                 variable arguments list
/// @retval                 int                     size of the resulting string
//======================================================================
public int vsprintf(char *buf, const char *fmt, va_list ap);

//======================================================================
// sprintf
//----------------------------------------------------------------------
/// @brief  Write formatted data to string
/// @param  buf             char*                   Pointer to a buffer where the resulting C-string is store
///                                                 The buffer should have a size of at least n characters.
/// @param  fmt             const char*             format
/// @param  ...             ...                     additional arguments
/// @retval                 int                     size of the resulting string
//======================================================================
public int sprintf(char *buf, const char *fmt, ...);

//======================================================================
// sprintf
//----------------------------------------------------------------------
/// @brief  Write formatted data to sized string
/// @param  buf             char*                   Pointer to a buffer where the resulting C-string is store
///                                                 The buffer should have a size of at least n characters.
/// @param  fmt             const char*             format
/// @param  size            uint                    Maximum number of bytes to be used in the buffer.
/// @param  ...             ...                     additional arguments
/// @retval                 int                     size of the resulting string
//======================================================================
public int snprintf(char *buf, size_t size, const char *fmt, ...);


#endif // _KAL_STRING_H_

