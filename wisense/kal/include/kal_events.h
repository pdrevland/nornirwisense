#ifndef _KAL_EVENTS_H_
#define _KAL_EVENTS_H_

// Define per-module base event value
// Try to keep it orthogonal: 6-10 bits scheme:
//  - 6 for the module 64 module should be far enough,
//  - 10 for actual event id, enough to play with wakeup event mask
#define EVT_MOD_MASK    (0xFC00)
#define EVT_ID_MASK     (0x03FF)
enum {
    _KAL_EVENT_BASE = ( 0 << 10),
    _HAL_EVENT_BASE = ( 1 << 10),
    _RAC_EVENT_BASE = ( 2 << 10),
    _MAC_EVENT_BASE = ( 3 << 10),
    _TRAN_EVENT_BASE= ( 4 << 10),
    _ALP_EVENT_BASE = ( 5 << 10),
    _DRV_EVENT_BASE = (16 << 10),
    _USR_EVENT_BASE = (48 << 10),
};

// KAL module events
typedef enum {
    // INTERNAL PROCESS MANAGEMENT---------------------
    KAL_EVT_NONE = _KAL_EVENT_BASE,
    // The process must initialize
    KAL_EVT_INIT,
    // The process must exit
    KAL_EVT_EXIT,
    // The process yields : it sends this event to himself...
    KAL_EVT_YIELD,
    // ... and should receive this event to continue.
    KAL_EVT_CONTINUE,
    // Sent to active events when another process exits
    // evp : process that has exited
    KAL_EVT_EXITED,

    // TIMER-------------------------------------------
    // an ETIMER has expired
    KAL_EVT_TIMER,

    // FILE SYSTEM ------------------------------------
    // A file is modified,
    KAL_EVT_FILE_MODIFIED,
    // write to file
    KAL_EVT_FILE_WRITE,

    _KAL_EVENT_LAST,
    _KAL_EVENT_QTY = (_KAL_EVENT_LAST-_KAL_EVENT_BASE-1),
} kal_events_t;

#endif // _KAL_EVENTS_H_

