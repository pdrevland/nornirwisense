#ifndef _KAL_PROCESS_ID_H_
#define _KAL_PROCESS_ID_H_

typedef enum 
{
    // Put here all fixed priority processes, ID 0 is forbidden
    RAC_PROCESS_ID              = 1,
    MAC_PROCESS_ID              = 2,
    ALP_PROCESS_ID              = 3,
    I2C_PROCESS_ID              = 4,
    TRAN_PROCESS_ID             = 5,
    PRES_PROCESS_ID             = 6,
    CUP_PROCESS_ID              = 7,
    ADC_PROCESS_ID              = 8,

    // Driver IDs starting here
    DRV_MMA_PROCESS_ID          = 0x10,
    DRV_VCNL_PROCESS_ID         = 0x11,
    DRV_BTLE_PROCESS_ID         = 0x12,
    DRV_GSM_CMD_PROCESS_ID      = 0x13,
    DRV_GSM_URC_PROCESS_ID      = 0x14,
    DRV_BTLE_SRL_PROCESS_ID     = 0x15,
    DRV_MPL_PROCESS_ID          = 0x16,
    DRV_HMC_PROCESS_ID          = 0x17,
    DRV_XCV_CC2500_PROCESS_ID   = 0x18,
    DRV_DAC_PROCESS_ID          = 0x19,
    DRV_RFID_RW_PROCESS_ID      = 0x1A,
    DRV_RFID_BG_PROCESS_ID      = 0x1B,
    DRV_RH_TEMP_PROCESS_ID      = 0x1C,

    // User IDs starting here 
    USER_PROCESS_ID             = 0x80,

} kal_process_id_t;

// Get a user ID
#define USER_PID(n) (USER_PROCESS_ID + (n))

#endif // _KAL_PROCESS_ID_H_


