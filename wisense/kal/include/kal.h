/// @copyright
/// ========================================================================={{{
/// Copyright (c) 2012-2013 WizziLab                                           /
/// All rights reserved                                                        /
///                                                                            /
/// IMPORTANT: This Software may not be modified, copied or distributed unless /
/// embedded on a WizziLab product. Other than for the foregoing purpose, this /
/// Software and/or its documentation may not be used, reproduced, copied,     /
/// prepared derivative works of, modified, performed, distributed, displayed  /
/// or sold for any purpose. For the sole purpose of embedding this Software   /
/// on a WizziLab product, copy, modification and distribution of this         /
/// Software is granted provided that the following conditions are respected:  /
///                                                                            /
/// *  Redistributions of source code must retain the above copyright notice,  /
///    this list of conditions and the following disclaimer                    /
///                                                                            /
/// *  Redistributions in binary form must reproduce the above copyright       / 
///    notice, this list of conditions and the following disclaimer in the     /
///    documentation and/or other materials provided with the distribution.    /
///                                                                            /
/// *  The name of WizziLab can not be used to endorse or promote products     /
///    derived from this software without specific prior written permission.   /
///                                                                            /
/// THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS        /
/// "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED  /
/// TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR /
/// PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR          /
/// CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,      /
/// EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,        /
/// PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA,            /
/// OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY     /
/// OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING    /
/// NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS         /
/// SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.               /
/// WIZZILAB HAS NO OBLIGATION TO PROVIDE MAINTENANCE, SUPPORT, UPDATES,       /
/// ENHANCEMENTS OR MODIFICATIONS.                                             /
///                                                                            /
/// Should you have any questions regarding your right to use this Software,   /
/// contact WizziLab at www.wizzilab.com.                                      /
///                                                                            /
/// =========================================================================}}}
/// @endcopyright

//  =======================================================================
/// @file           kal.h
/// @brief          Kernel Abstraction Layer public API header
/// @defgroup KAL
//  =======================================================================

#ifndef __KAL_H__
#define __KAL_H__

#include "hal_types.h"
#include "kal_timer.h"
#include "kal_process.h"
#include "kal_process_id.h"
#include "kal_string.h"
#include "kal_stdio.h"
#include "kal_math.h"
#include "kal_utils.h"
#include "kal_file.h"
#include "kal_com.h"
#include "kal_dbg.h"

#include <string.h> // for size_t

/// @ingroup KAL
/// @defgroup KAL_API       Kernel Abstraction Layer API
/// @{

// =======================================================================
// kal_error_t
// -----------------------------------------------------------------------
/// Error types enumerator for KAL
// =======================================================================
typedef enum
{
    /// No error
    KAL_ERROR_NO = 0,
    /// All resources used
    KAL_ERROR_NO_RESSOURCES,

} kal_error_t;

//======================================================================
// kal_open
//----------------------------------------------------------------------
/// @brief  Initialize process, timers and the file system
/// @param  none
/// @retval none
//======================================================================
public void kal_open(void);

//======================================================================
// Memory management functions
//======================================================================
#ifdef _KAL_TRACE_KAL_MALLOC_
    #define KAL_MALLOC(s)   kal_dbg_malloc(s,__FUNCTION__,__LINE__)
    #define KAL_FREE(p)     kal_dbg_free(p,__FUNCTION__,__LINE__)
#else
    #define KAL_MALLOC(s)   kal_malloc(s)
    #define KAL_FREE(p)     kal_free(p)
#endif
#ifdef _KAL_TRACE_RAC_MALLOC_
    #define RAC_MALLOC(s)   kal_dbg_malloc(s,__FUNCTION__,__LINE__)
    #define RAC_FREE(p)     kal_dbg_free(p,__FUNCTION__,__LINE__)
#else
    #define RAC_MALLOC(s)   kal_malloc(s)
    #define RAC_FREE(p)     kal_free(p)
#endif
#ifdef _KAL_TRACE_MAC_MALLOC_
    #define MAC_MALLOC(s)   kal_dbg_malloc(s,__FUNCTION__,__LINE__)
    #define MAC_FREE(p)     kal_dbg_free(p,__FUNCTION__,__LINE__)
#else
    #define MAC_MALLOC(s)   kal_malloc(s)
    #define MAC_FREE(p)     kal_free(p)
#endif
#ifdef _KAL_TRACE_ALP_MALLOC_
    #define ALP_MALLOC(s)   kal_dbg_malloc(s,__FUNCTION__,__LINE__)
    #define ALP_FREE(p)     kal_dbg_free(p,__FUNCTION__,__LINE__)
#else
    #define ALP_MALLOC(s)   kal_malloc(s)
    #define ALP_FREE(p)     kal_free(p)
#endif
#ifdef _KAL_TRACE_TRA_MALLOC_
    #define TRA_MALLOC(s)   kal_dbg_malloc(s,__FUNCTION__,__LINE__)
    #define TRA_FREE(p)     kal_dbg_free(p,__FUNCTION__,__LINE__)
#else
    #define TRA_MALLOC(s)   kal_malloc(s)
    #define TRA_FREE(p)     kal_free(p)
#endif
#ifdef _KAL_TRACE_DRV_MALLOC_
    #define DRV_MALLOC(s)   kal_dbg_malloc(s,__FUNCTION__,__LINE__)
    #define DRV_FREE(p)     kal_dbg_free(p,__FUNCTION__,__LINE__)
#else
    #define DRV_MALLOC(s)   kal_malloc(s)
    #define DRV_FREE(p)     kal_free(p)
#endif
#ifdef _KAL_TRACE_APP_MALLOC_
    #define APP_MALLOC(s)   kal_dbg_malloc(s,__FUNCTION__,__LINE__)
    #define APP_FREE(p)     kal_dbg_free(p,__FUNCTION__,__LINE__)
#else
    #define APP_MALLOC(s)   kal_malloc(s)
    #define APP_FREE(p)     kal_free(p)
#endif

//======================================================================
// kal_malloc
//----------------------------------------------------------------------
/// @brief  memory allocation 
/// @param  size            size_t              size to allocate in bytes
/// @retval                 void*               pointer to the allocated buffer
//======================================================================
public void* kal_malloc(size_t size);

//======================================================================
// kal_dbg_malloc
//----------------------------------------------------------------------
/// @brief  verbose Malloc for debug
//======================================================================
void* kal_dbg_malloc(size_t size, const char* f, uint l);


//======================================================================
// kal_free
//----------------------------------------------------------------------
/// @brief  free memory
/// @param  p               void*               pointer to the allocated buffer
//======================================================================
public void kal_free(void *p);

//======================================================================
// kal_dbg_free
//----------------------------------------------------------------------
/// @brief  verbose Free for debug
//======================================================================
void kal_dbg_free(void* p, const char* f, uint l);

/// @} KAL_API


#endif // __KAL_H__
