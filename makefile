BUILD_DIR:=build

all:gatway.ipk

# Create the package - temporary intermediate tar files as prerequisits
%.ipk:  $(BUILD_DIR)/%-data.tar.gz $(BUILD_DIR)/%-control.tar.gz $(BUILD_DIR)/debian-binary 
	cp $< $(BUILD_DIR)/data.tar.gz
	cp $(word 2,$^) $(BUILD_DIR)/control.tar.gz
	cd $(BUILD_DIR) && tar -czvf ../$@ data.tar.gz control.tar.gz debian-binary && cd -
# Remove the build directory when finished
	rm -r $(BUILD_DIR)

# Create the data tarball intermediate file - data directory as prerequiste
$(BUILD_DIR)/%-data.tar.gz: %/data
	cp -r $< $(BUILD_DIR)
	cd $(BUILD_DIR)/data && tar czvf ../../$@ * && cd - 

# Create the control tarball intermediate file - control directory as prerequiste
# NOTE the tar must not contain any path info 
$(BUILD_DIR)/%-control.tar.gz: %/control
	cp -r $< $(BUILD_DIR)
	cd $(BUILD_DIR)/control && tar czvf ../../$@ * && cd -

# create the debian-binary file TODO should this be copied instead?
$(BUILD_DIR)/debian-binary: $(BUILD_DIR)
	echo 2.0 > $</debian-binary

# Create the build directory
$(BUILD_DIR):
	mkdir $@


# Clean Up - PHONY file just in case someone creates a file called "clean"
.PHONY: $(BUILD_DIR) clean
clean:
	-rm -r $(BUILD_DIR)
	-rm *.ipk


